<?php

	function dFilter($text)
	{
		$text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
        return htmlentities($text, ENT_QUOTES, 'UTF-8');
	}

	function getBrief($text, $maxchar=150)
    {
        if (!$text)
            return '';

        $text = str_replace("\n", ' ', $text);
        $text = str_replace("\r", ' ', $text);
        $text = str_replace("\t", ' ', $text);
        $text = str_replace('&nbsp;', ' ', $text);
        $text = preg_replace('!\s+!', ' ', $text);

        $text=strip_tags($text);
        if(mb_strlen($text, 'UTF-8')>$maxchar) {
            $text = mb_substr($text, 0, $maxchar, 'UTF-8');
            $words = mb_split(' ', $text);
            $tmp = sizeof($words);
            $text = join(' ', array_slice($words, 0, $tmp-2));
            return $text;
        } else
            return $text;
    }

	header('Content-Type: text/html; charset=UTF-8');
	$link = mysqli_connect("localhost", "bmroot", "gfhjkm", "budmisto");
	mysqli_set_charset($link, "utf8");
	$exist = false;

	if ($_POST) {
		//var_dump(unserialize($_POST['specs']));
		//var_dump($_POST);
		//die;
		$p = $_POST;
		$sql = "SELECT id FROM bm_users WHERE (user_email = '{$p['email']}' AND user_email <> '' AND user_email IS NOT NULL) OR (user_phone = '{$p['phone1']}' AND user_phone <> '' AND user_phone IS NOT NULL) ";
		$tmp = mysqli_query($link, $sql);
		$exist = mysqli_fetch_assoc($tmp);

		if (!$exist) {
			if (!$p['email'])
				$sql = "INSERT INTO bm_users (user_name, user_phone, user_password, user_role_id, is_custom)
						VALUES ('{$p['name']}', '{$p['phone1']}', '', '{$p['role']}', '{$p['user_id']}')";
			else
				$sql = "INSERT INTO bm_users (user_name, user_email, user_phone, user_password, user_role_id, is_custom)
						VALUES ('{$p['name']}', '{$p['email']}', '{$p['phone1']}', '', '{$p['role']}', '{$p['user_id']}')";
			mysqli_query($link, $sql);

			$uid = mysqli_insert_id($link);

			if ($uid) {
				if ($p['descr']) {
					$descr = dFilter($p['descr']);
					$brief = getBrief($descr);
				} else {
					$brief = '';
					$descr = '';
				}
				if (isset($p['phone2']))
					$phone1 = $p['phone2'];
				else
					$phone1 = '';
				if (isset($p['phone3']))
					$phone2 = $p['phone3'];
				else
					$phone2 = '';

				$sql = "INSERT INTO bm_perfs_info (phone1, phone2, description, brief, user_id)
						VALUES ('{$phone1}', '{$phone2}', '{$descr}', '{$brief}', {$uid})";
				mysqli_query($link, $sql);

				$spec = unserialize($p['specs']);
				foreach($spec as $key => $s) {
					$speckey = $uid . '-' . $s['work_id'] . '-' . $s['spec_id'];
					$sql = "INSERT IGNORE INTO builder_specializations (user_id, wrk_type_id, wrk_spec_id, speckey)
							VALUES ({$uid}, {$s['work_id']}, {$s['spec_id']}, '{$speckey}')";
					mysqli_query($link, $sql);
				}

				if (!$p['placextra']) {
					$placedistrict = ($p['placedistrict2'])?$p['placedistrict2']:$p['placedistrict3'];
					$placedistrict = str_replace('горсовет', 'регион', $placedistrict);
					$placedistrict = str_replace('міськрада', 'громада', $placedistrict);
				} else {
					$placedistrict = $p['placextra'];
				}

				$sql = "SELECT id FROM bm_regions WHERE ru_region_name = '{$p['placeregion']}' OR uk_region_name = '{$p['placeregion']}'";
				$tmp = mysqli_query($link, $sql);
				$region_id = mysqli_fetch_assoc($tmp)['id'];
				if ($region_id == 26) $region_id = 10;

				$sql = "INSERT INTO bm_users_places (route, town, district, region, country, lat, lng, place_id, user_id, region_id)
						VALUES ('{$p['placeroute']}', '{$p['placetown']}', '{$placedistrict}', '{$p['placeregion']}', '{$p['placecountry']}', '{$p['placelat']}', '{$p['placelng']}', '{$p['placeid']}', {$uid}, {$region_id})";
				mysqli_query($link, $sql);

				$sql = "UPDATE stroiteli_users SET parseres = {$uid} WHERE id = {$p['user_id']}";
				mysqli_query($link, $sql);
			}
		}
	}

	if (!$exist)
		$result = mysqli_query($link, "SELECT * FROM stroiteli_users WHERE parseres = 1 ORDER BY RAND() LIMIT 0,1");
	else
		$result = mysqli_query($link, "SELECT * FROM stroiteli_users WHERE parseres = 1 AND id ={$p['user_id']}");

	if (!$result) echo '<h1>Возможно, что пользователи уже закончились</h1>';
	$data = mysqli_fetch_assoc($result);
	$specs = explode('|', $data['specs']);
	if ($specs) {
		$tmp = array();
		foreach($specs as $key => $val) {
			$sql = "SELECT m_specs.*, work_types.type_name, specializations.spec_name AS spec_name2
					FROM m_specs
					LEFT JOIN work_types ON work_types.id = m_specs.work_id
					LEFT JOIN specializations ON specializations.id = m_specs.spec_id
					WHERE m_specs.spec_name = '{$val}'";
			$result = mysqli_query($link, $sql);
			if ($result)
				$tmp[] = mysqli_fetch_assoc($result);
		}
		$specs = $tmp;
	}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
    <meta charset="utf-8">
    <title>Создать пользователя из строители.нет.юа</title>
    <style>
		.row { margin-bottom: 10px; }
	</style>
  </head>
  <body>
	<div class="container">
	<h3>Добавление пользователя строители.нет.юа</h3>
	<p>&nbsp;</p>
	Ссылка: <a href="http://stroiteli.net.ua/company/<?php echo $data['id']?>/" target="_blank">http://stroiteli.net.ua/company/<?php echo $data['id']?></a>
	<p>&nbsp;</p>

<?php if ($exist) {
	if (isset($exist[1])) {
		echo '<h3 style="color:red">Пользователи с такими контактами уже существуют:</h3>';
		foreach($exist as $e)
			echo '<a href="http://budmisto.com.ua/user/', $e['id'],'" target="_blank">http://budmisto.com.ua/user/', $e['id'], '</a><br />';
	} else {
		echo '<h3 style="color:red">Пользователь с такими контактами уже существует:</h3>';
		echo '<a href="http://budmisto.com.ua/user/', $exist['id'],'" target="_blank">http://budmisto.com.ua/user/', $exist['id'], '</a>';
	}
	echo '<p>&nbsp;</p>';
} ?>

	<form method="post" id="user-form">
		<u>Тип:</u>
		<select name="role" id="role" class="form-control">
			<option value="0">-- Тип --</option>
			<option value="4">Мастер</option>
			<option value="5">Бригада</option>
			<option value="6">Строительная компания</option>
		</select>
		<p>&nbsp;</p>
		<input type="hidden" name="user_id" value="<?php echo $data['id']?>" />
		<u>Название:</u>
		<input type="text" class="form-control" id="name" name="name" value='<?php echo $data['name']?>' />
		<p>&nbsp;</p>
		<u>Телефон:</u> 
<?php $phone = str_replace("+38 ", '', $data['phone']);?>
		<input type="text" id="phone1" class="form-control phone" name="phone1" placeholder="введите телефон" value="<?php echo $phone?>" />
		<p>&nbsp;</p>
		<u>E-mail:</u>
		<input type="email" class="form-control" id="email" name="email" placeholder="введите e-mail" value="<?php echo $data['email']?>" />
		<p>&nbsp;</p>
		<u>Специализации по соответствию:</u><br />
<?php if ($specs) {
		$tmp = array();
		foreach($specs as $s) {
			if (!$s['work_id']) continue;
			$tmp[] = array('work_id' => $s['work_id'], 'spec_id' => $s['spec_id']);
			echo '<strong>', $s['spec_name'], '</strong> - ', $s['type_name'], ' | ', $s['spec_name2'], '<br />';
		}
		if (!$tmp) echo '<strong style="color:red">не указаны</strong>';
?>
		<input type="hidden" name="specs" value='<?php echo serialize($tmp);?>' />
<?php } else echo '<strong style="color:red">не указаны</strong>';?>
		<p>&nbsp;</p>
		<u>Описание:</u>
		<textarea name="descr" class="form-control"><?php echo $data['descr']?></textarea>
		<p>&nbsp;</p>
		<u>Адреса:</u><br />
		<?php echo $data['addr']?'-'.$data['addr']:'не указан'?>
		<input type="text" id="autocomplete" class="form-control clearable" name="addr" placeholder="введите адрес" />
        <input type="hidden" name="placelat" id="placelat" />
        <input type="hidden" name="placelng" id="placelng" />
        <input type="hidden" name="placeid" id="placeid" />
        <input type="hidden" name="placeaddr" id="placeaddr" />
        <input type="hidden" name="placetown" id="locality" />
        <input type="hidden" name="placedistrict3" id="administrative_area_level_3" />
        <input type="hidden" name="placedistrict2" id="administrative_area_level_2" />
        <input type="hidden" name="placeregion" id="administrative_area_level_1" />
        <input type="hidden" name="placecountry" id="country" />
        <input type="hidden" name="placeroute" id="route" />
        <input type="hidden" name="placextra" id="sublocality_level_1" />
		<p>&nbsp;</p>
		<button class="btn btn-success btn-lg" onclick="return doFormSubmit('user-form');">Сохранить</button>
		<button class="btn btn-warning btn-lg" onclick="return doRefresh();">Следующий</button>
	</form>
	<p>&nbsp;</p>
	</div>
	<script type="text/javascript" src="/js/jquery.min.js"></script>
	<script type="text/javascript" src="/js/jquery.maskedinput.min.js"></script>
	<script>
		try {
            $('.phone').mask('(999) 999-99-99');
        } catch(err) { }

		function doFormSubmit(form_id, listFields) {
			console.log('do submit');
			var form = $('#'+form_id);

			var name = $('#name').val();
			var role = $('#role').val();
			if (role == 0) {
				alert('Не заполнены обязательные поля');
				return false;
			}
			var placeid = $('#placeid').val();
			var phone = $('#phone1').val();
			if (!name || !placeid || !phone) {
				alert('Не заполнены обязательные поля');
				return false;
			}

			form.submit();
		};

		function doRefresh() {
			window.location = 'migstr.php';
			return false;
		}

		function fillInAddress() {
			$('#administrative_area_level_1').val('');
			$('#administrative_area_level_2').val('');
			$('#administrative_area_level_3').val('');
			$('#placeaddr').val('');
			$('#location').val('');
			$('#route').val('');
			$('#sublocality_level_1').val('');
			var place = autocomplete.getPlace();
			$('#placelat').val(place.geometry.location.lat());
			$('#placelng').val(place.geometry.location.lng());
			$('#placeid').val(place.place_id);
			$('#placeaddr').val(place.formatted_address);
			var componentForm = {
				street_number: 'short_name',
				route: 'long_name',
				locality: 'long_name',
				administrative_area_level_1: 'long_name',
				administrative_area_level_2: 'long_name',
				administrative_area_level_3: 'long_name',
				country: 'long_name',
				sublocality_level_1: 'long_name'
			};

			for (var i = 0; i < place.address_components.length; i++) {
				var addressType = place.address_components[i].types[0];
				if (componentForm[addressType]) {
					var val = place.address_components[i][componentForm[addressType]];
					var place_type = place.address_components[i].types[0];
					$('#'+place_type).val(val);
				}
			}
		}
	</script>
	<script type="text/javascript">
        var placeSearch, autocomplete;
        function initAutocomplete() {
            autocomplete = new google.maps.places.Autocomplete((document.getElementById('autocomplete')), {types: ['geocode'], componentRestrictions: {country: 'ua'}});
            autocomplete.addListener('place_changed', fillInAddress);
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?language=ru&key=AIzaSyBD4PoD1N963qaJe3WFtECj26a_zwAL1l4&signed_in=false&libraries=places&callback=initAutocomplete" async defer></script>
  </body>
</html>