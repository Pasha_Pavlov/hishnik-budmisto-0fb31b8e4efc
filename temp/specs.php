<?php

	header('Content-Type: text/html; charset=UTF-8');
	$link = mysqli_connect("localhost", "bmroot", "gfhjkm", "budmisto");
	mysqli_set_charset($link, "utf8");

	if ($_POST) {
		$post = $_POST;
		foreach($post as $key => $value) {
			if (strpos($key, 'work') !== false) {
				$id = str_replace('work', '', $key);
				$sql = "UPDATE m_specs SET work_id = {$value} WHERE id = {$id}";
				mysqli_query($link, $sql);
			}
			if (strpos($key, 'spec') !== false) {
				$id = str_replace('spec', '', $key);
				$sql = "UPDATE m_specs SET spec_id = {$value} WHERE id = {$id}";
				mysqli_query($link, $sql);
			}
		}
		header('Location: specs.php');
	}

	$m_specs = mysqli_query($link, "SELECT * FROM m_specs ORDER BY spec_name");
	$work_types = mysqli_query($link, "SELECT * FROM work_types ORDER BY type_name");
	$result = mysqli_query($link, "SELECT * FROM specializations WHERE is_active = 1 ORDER BY type_id, spec_name");
	$sub_specs = array();
	while($data = mysqli_fetch_assoc($result)) {
		$sub_specs[$data['type_id']][$data['id']]['spec_name'] = $data['spec_name'];
	}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
    <meta charset="utf-8">
    <title>Настройка соответствия специализаций</title>
    <style>
		.row { margin-bottom: 10px; }
	</style>
  </head>
  <body>
	<div class="container">
	<h3>Таблица соответствий специализаций</h3>
	<p>&nbsp;</p>
	<form method="post">
<?php foreach($m_specs as $specs):?>
		<div class="row">
			<div class="col-xs-6"><strong><?php echo $specs['spec_name']?></strong></div>
			<div class="col-xs-3">
				<select class="form-control" name="work<?php echo $specs['id']?>">
					<option value="0">-----</option>
<?php foreach($work_types as $works):?>
					<option value="<?php echo $works['id']?>"<?php if($specs['work_id'] == $works['id']) echo ' selected';?>><?php echo $works['type_name']?></option>
<?php endforeach;?>
				</select>
			</div>
			<div class="col-xs-3">
<?php if ($specs['work_id']):?>
				<select class="form-control" name="spec<?php echo $specs['id']?>">
					<option value="0">-----</option>
<?php foreach($sub_specs[$specs['work_id']] as $key => $value):?>
					<option value="<?php echo $key?>"<?php if($specs['spec_id'] == $key) echo ' selected';?>><?php echo $value['spec_name']?></option>
<?php endforeach;?>
				</select>
<?php endif;?>
			</div>
		</div>
		<hr />
<?php endforeach;?>
		<p>&nbsp;</p>
		<button class="btn btn-success btn-lg">Сохранить</button>
	</form>
	<p>&nbsp;</p>
	</div>
  </body>
</html>