<?php

    namespace Budmisto\Lib\Mailer;

    use Phalcon\DI;

    class Mail
    {

        private $charset;
        private $from;
        private $mailBody;
        private $type; // possible options: mass, user
        private $template; // possible options: recovery, confirm
        private $lang;
        private $site_url;

        public function __construct($type, $template)
        {
            $this->charset = 'UTF-8';
            $this->from = '<budmisto@budmisto.kiev.ua>';
            $this->type = $type;
            $this->template = $template;
            $this->lang = 'ru';
            $this->site_url = 'http://budmisto.kiev.ua';
        }

        public function setOptions($site_url, $lang = false, $from = false)
        {
            if($lang)
                $this->lang = $lang;
            if($from)
                $this->from = $from;
            $this->site_url = $site_url;
        }

        private function _filterEmail($email)
        {
            $email = trim($email);
            $rule = array("\r" => '', "\n" => '', "\t" => '', '"' => '', ',' => '', '<' => '', '>' => '');
            $email = strtr($email, $rule);
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            return $email;
        }

        private function _prepareReceivers($receivers)
        {
            $receivers = explode('#', $receivers);
            $result = array();
            for ($i = 0; $i < count($receivers); $i++) {
                $result[] = '<' . $this->_filterEmail($receivers[$i]) . '>';
            }
            return implode(', ', $result);
        }

        private function _renderTemplate($data)
        {
            $site_url = $this->site_url;
            $mailBody = include(__DIR__ . '/templates/' . $this->type . '/' . $this->lang . '_' . $this->template . '.php');
            return $mailBody;
        }

        private function _getHeaders()
        {
            $headers = "From: " . $this->from . "\r\n";
            $headers .= "Reply-To: " . $this->from . "\r\n";
            $headers .= "Return-Path: " . $this->from . "\r\n";
            //$headers.= 'X-Mailer: PHP';
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=" . $this->charset . "\r\n";

            return $headers;
        }

        public function send($subject, $receivers, $data)
        {
            if(stripos($receivers, '@a.ua') !== false)
                return false;
            if(stripos($receivers, '@t.ua') !== false)
                return false;
            if(stripos($receivers, '@bm.ua') !== false)
                return false;
            /* if (stripos($receivers, '@budmisto.kiev.ua') !== false)
              return false;
             * 
             */
            if(stripos($receivers, '@budmisto.com.ua') !== false)
                return false;

            $subject = sprintf('=?UTF-8?B?%s?=', base64_encode($subject));

            $receivers = $this->_prepareReceivers($receivers);
            $message = $this->_renderTemplate($data);

            $headers = $this->_getHeaders();
            $result = mail($receivers, $subject, $message, $headers, '-f ' . $this->from);
            return $result;
        }

        /* public function sendToQueue($subject, $receiver, $data, $user_id)
          {
          $db = DI::getDefault()->getShared('db');
          $sql = 'INSERT INTO bm_mail_queue (mail_to, mail_title, mail_data, mail_type, mail_template, mail_lang, user_id)
          VALUES (?, ?, ?, ?, ?, ?, ?)';
          $data = serialize($data);
          $db->query($sql, array($receiver, $subject, $data, $this->type, $this->template, $this->lang, $user_id));
          } */
    }
    