<?php

    $html = <<<EOT
<!doctype html>
<html>
<head>
<title>Будмісто | Восстановление доступа</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<style type="text/css">
	.ReadMsgBody {width: 100%;}
	.ExternalClass {width: 100%;}
	.appleBody a {color:#68440a; text-decoration: none;}
	.appleFooter a {color:#999999; text-decoration: none;}

	@media screen and (max-width: 525px) {

		table[class="wrapper"]{
		  width:100% !important;
		}

		td[class="mobile-hide"]{
		  display:none;}

		img[class="mobile-hide"]{
		  display: none !important;
		}

		img[class="img-max"]{
		  max-width: 100%;
		  height:auto;
		}

		table[class="responsive-table"]{
		  width:100%!important;
		}

		td[class="padding"]{
		  padding: 10px 5% 15px 5% !important;
		}

		td[class="padding-copy"]{
		  padding: 10px 5% 10px 5% !important;
		}

		td[class="padding-meta"]{
		  padding: 30px 5% 0px 5% !important;
		}

		td[class="no-pad"]{
		  padding: 0 0 20px 0 !important;
		}

		td[class="no-padding"]{
		  padding: 0 !important;
		}

		td[class="section-padding"]{
		  padding: 50px 15px 50px 15px !important;
		}

		td[class="section-padding-bottom-image"]{
		  padding: 50px 15px 0 15px !important;
		}
		
	}
</style>
</head>
<body style="margin: 0; padding: 0;">

<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td bgcolor="#eeeeee" align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td style="padding: 20px 0px 20px 0px;" align="center" colspan="2">
						<table width="500" border="0" cellspacing="0" cellpadding="0" align="center" class="responsive-table">
							<tr>
								<td align="center" width="342" valign="middle" style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#999999; -webkit-text-size-adjust: none">
									<h2 class="appleFooter" style="color:#999999; -webkit-text-size-adjust: none">Восстановление доступа к сайту</h2>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td bgcolor="#ffffff" align="center" style="padding: 40px 15px 70px 15px;" class="section-padding">
			<table border="0" cellpadding="0" cellspacing="0" width="500" style="padding:0 0 20px 0;" class="responsive-table">
				<tr>
					<td valign="top" style="padding: 40px 0 0 0;" class="mobile-hide"><a href="http://budmisto.ua"><img src="{$site_url}/images/icon.gif" /></a></td>
					<td style="padding: 40px 0 0 0;" class="no-padding">
						<table border="0" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td align="left" style="padding: 10px 0 15px 25px; font-size: 14px; line-height: 24px; font-family: Helvetica, Arial, sans-serif; color: #888888;" class="padding-copy">
									Восстановление доступа к сайту &laquo;Будмісто&raquo;
<br /><br />
									Ваши регистрационные данные:<br />
									<span style="color: #3E3F3E; font-weight: bold;">E-mail:</span> {$data['email']}<br />
									<span style="color: #3E3F3E; font-weight: bold;">Пароль: {$data['passwd']}</span>
                                                                        Быстрый вход: <a href="http://{$_SERVER[HTTP_HOST]}/index/{$data['fast_login']}"><button>Личный кабинет</button></a></span>
<br /><br />


                                                                        
<br /><br />
									<a href="http://budmisto.ua">BudMisto.Ua</a> &copy; 2016
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
EOT;

    return $html;
    