<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/admin/models/Admin.php
     *
     * Admin model
     *
     */

    namespace Budmisto\Modules\Admin\Models;

    class Admin extends \Phalcon\Mvc\Model
    {

        private $db;
        private $security;
        private $session;

        public function initialize()
        {

            $this->db = $this->getDi()->getShared('db');
            $this->security = $this->getDi()->getShared('security');
            $this->session = $this->getDi()->getShared('session');
        }

        public function signinAdmin($name, $passwd)
        {
            $this->signoutAdmin();

            $sql = 'SELECT id, user_password, user_name, user_role_id
                FROM bm_users WHERE user_role_id <3 AND is_active = 1 AND user_name = LOWER(?)';
            $result = $this->db->query($sql, array($name))->fetch();
            if(!$result)
                return false;

            if($this->security->checkHash($passwd, $result['user_password'])) {
                $this->setLastActivity($result['id']);
                $this->setUsrSession($result['id'], $result['user_name'], $result['user_role_id']);
                return true;
            } else
                return false;
        }

        public function setLastActivity($user_id)
        {
            $timestamp = date('Y-m-d G:i:s');
            $sql = 'UPDATE bm_users SET last_activity = ? WHERE id = ?';
            $this->db->query($sql, array($timestamp, $user_id));
        }

        private function setUsrSession($user_id, $name, $role)
        {
            $this->session->set('usr_id', $user_id);
            $this->session->set('usr_name', $name);
            $this->session->set('usr_level', $role);
        }

        public function signoutAdmin()
        {
            $user_id = $this->session->get('usr_id');
            $this->setLastActivity($user_id);

            $this->session->remove('usr_id');
            $this->session->remove('usr_level');
            $this->session->remove('usr_name');
            $this->session->remove('usr_phone');
        }

        public function getLastUsersList($start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $sql = 'SELECT bm_users.user_phone, bm_users.stars AS stars, bm_users.is_confirmed, bm_users.user_email, user_reg_device, bm_users.user_name, bm_users.id AS uid,
                       bm_users.user_role_id, bm_users.stars AS stars,
                       DATE_FORMAT(user_reg_date, "%d/%m/%Y (%H:%i)") AS user_reg_date
                FROM bm_users
                WHERE bm_users.user_role_id > 3 AND bm_users.user_role_id < 7 AND bm_users.is_active = 1';
            $sql .= " GROUP BY uid ORDER BY uid DESC LIMIT {$start}, {$limit}";
            $res = $this->db->query($sql)->fetchAll();
            $uids = array();
            $res_count = count($res);
            foreach ($res as $r) {
                $uids[] = $r['uid'];
            }
            $uids = implode(',', $uids);
            $tmp = 0;
            $sql = "SELECT user_id, speckey FROM builder_specializations WHERE user_id IN ({$uids}) GROUP BY user_id";
            $specs = $this->db->query($sql)->fetchAll();
            for ($i = 0; $i < $res_count; $i++) {
                foreach ($specs as $s) {
                    if($s['user_id'] == $res[$i]['uid'])
                        $tmp = 1;
                }
                $res[$i]['speckey'] = $tmp;
                $tmp = 0;
            }
            $tmp = 0;
            $sql = "SELECT user_id, place_id FROM bm_users_places WHERE user_id IN ({$uids}) GROUP BY user_id";
            $places = $this->db->query($sql)->fetchAll();
            for ($i = 0; $i < $res_count; $i++) {
                foreach ($places as $p) {
                    if($p['user_id'] == $res[$i]['uid'])
                        $tmp = 1;
                }
                $res[$i]['place_id'] = $tmp;
                $tmp = 0;
            }
            return $res;
            // NEED REFACT !!!
        }

        public function getBlockedUsersList($start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $sql = 'SELECT bm_users.avatar, bm_users.user_phone, bm_users.is_confirmed, bm_users.user_email, bm_users.user_name, bm_users.id AS uid,
                       builder_specializations.speckey, bm_users.user_role_id, bm_users.stars AS stars,
                       bm_users_places.id AS place_id
                FROM bm_users
                LEFT JOIN builder_specializations ON builder_specializations.user_id = bm_users.id
                LEFT JOIN bm_users_places ON bm_users_places.user_id = bm_users.id
                WHERE bm_users.user_role_id > 2 AND bm_users.user_role_id < 7 AND bm_users.is_active = 0';
            $sql .= " GROUP BY uid ORDER BY uid DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql)->fetchAll();
            // NEED REFACT !!!
        }

        public function getUsersMarkedList($start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $sql = 'SELECT bm_users.avatar, bm_users.user_phone, bm_users.is_confirmed, bm_users.user_email, bm_users.user_name, bm_users.id AS uid,
                       builder_specializations.speckey, bm_users.user_role_id, bm_users.stars AS stars,
                       bm_users_places.id AS place_id
                FROM bm_users
                LEFT JOIN builder_specializations ON builder_specializations.user_id = bm_users.id
                LEFT JOIN bm_users_places ON bm_users_places.user_id = bm_users.id
                WHERE bm_users.user_role_id > 2 AND bm_users.user_role_id < 7 AND bm_users.stars AS stars = 1';
            $sql .= " GROUP BY uid ORDER BY uid DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql)->fetchAll();
            // NEED REFACT !!!
        }

        public function getUsersCommentedList($start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $sql = 'SELECT bm_users.avatar, bm_users.user_phone, bm_users.is_confirmed, bm_users.user_email, bm_users.user_name, bm_users.id AS uid,
                       builder_specializations.speckey, bm_users.user_role_id, bm_users.stars AS stars,
                       bm_users_places.id AS place_id
                FROM bm_admin_info
                LEFT JOIN bm_users ON bm_users.id = bm_admin_info.user_id
                LEFT JOIN builder_specializations ON builder_specializations.user_id = bm_admin_info.user_id
                LEFT JOIN bm_users_places ON bm_users_places.user_id = bm_admin_info.user_id
                WHERE bm_users.user_role_id > 2 AND bm_users.user_role_id < 7';
            $sql .= " GROUP BY uid ORDER BY uid DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql)->fetchAll();
        }

        public function searchUsersList($email, $phone, $placelat, $placelng, $stars, $speciality, $subspeciality, $note, $descr, $start, $limit, $distance = 10, $role = false)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $limit++;

            if($role)
                $role = " AND bm_users.user_role_id = {$role}";
            else
                $role = " AND bm_users.user_role_id BETWEEN 2 AND 7 ";

            if($note and ! $descr) {
                $sql = "SELECT bm_admin_info.user_id FROM bm_admin_info
                    LEFT JOIN bm_users ON bm_users.id = bm_admin_info.user_id
                    WHERE 1=1 $role AND bm_admin_info.description LIKE '%$note%'";
                $res = $this->db->query($sql)->fetchAll();
                if(!$res)
                    return false;
                $tmp = array();
                foreach ($res as $r) {
                    $tmp[] = $r['user_id'];
                }
                $ids = implode(',', $tmp);
            }

            if(!$note and $descr) {

                $sql = "SELECT user_id FROM bm_perfs_info
                    LEFT JOIN bm_users ON bm_users.id = bm_perfs_info.user_id
                    WHERE description LIKE '%$descr%' $role";
                $res = $this->db->query($sql)->fetchAll();
                if(!$res)
                    return false;
                $tmp = array();
                foreach ($res as $r) {
                    $tmp[] = $r['user_id'];
                }
                $ids = implode(',', $tmp);
            }
            if($note and $descr) {
                $sql = "SELECT bm_admin_info.user_id, bm_perfs_info.user_id  FROM bm_admin_info
                    LEFT JOIN bm_users ON bm_users.id = bm_admin_info.user_id
                    LEFT JOIN bm_perfs_info ON bm_users.id = bm_perfs_info.user_id 
                    WHERE 1=1 $role AND bm_admin_info.description LIKE '%$note%' AND bm_perfs_info.description LIKE '%$descr%'";
                $res = $this->db->query($sql)->fetchAll();
                if(!$res)
                    return false;
                $tmp = array();
                foreach ($res as $r) {
                    $tmp[] = $r['user_id'];
                }
                $ids = implode(',', $tmp);
            }
//        
            if($speciality) {
                $add = " LEFT JOIN builder_specializations ON builder_specializations.user_id = bm_users.id";
            }
            $sql = 'SELECT bm_users.user_phone, bm_users.is_active, bm_users.user_reg_date, bm_users.is_confirmed, bm_users.is_active, bm_users.user_email, bm_users.user_name, bm_users.id AS uid, bm_users.user_role_id, bm_users.stars AS stars,
                       bm_users_places.id AS place_id
                FROM bm_users
                LEFT JOIN bm_users_places ON bm_users_places.user_id = bm_users.id
                ' . $add . "
                WHERE 1=1 $role";
            if($note or $descr)
                $sql .= " AND bm_users.id IN ({$ids})";
            if($email)
                $sql .= " AND LOWER(bm_users.user_email) = LOWER('{$email}')";
            if($phone)
                $sql .= " AND bm_users.user_phone = '{$phone}'";
            if($stars)
                $sql .= " AND bm_users.stars = {$stars}";
            if($speciality && $subspeciality)
                $sql .= " AND builder_specializations.wrk_type_id = {$speciality} AND builder_specializations.wrk_spec_id = {$subspeciality}";
            if($speciality && !$subspeciality)
                $sql .= " AND builder_specializations.wrk_type_id = {$speciality}";

            $sql .= " GROUP BY bm_users.id ORDER BY bm_users.id DESC LIMIT {$start}, {$limit}";

            if($placelat && $placelng) {

                $sql = "SELECT bm_users.user_phone, bm_users.is_confirmed, bm_users.is_active, bm_users.user_email, bm_users.user_name, bm_users.id AS uid, bm_users.user_reg_date , bm_users.user_role_id,  bm_users.stars AS stars,
(6371 * acos (
      cos ( radians({$placelat}) )
      * cos( radians( bm_users_places.lat ) )
      * cos( radians( bm_users_places.lng ) - radians({$placelng}) )
      + sin ( radians({$placelat}) )
      * sin( radians( bm_users_places.lat ) )
    )
) AS distance,
bm_users_places.route, bm_users_places.town, bm_users_places.district, bm_users_places.region
                FROM bm_users_places
                LEFT JOIN bm_users ON bm_users.id = bm_users_places.user_id
                $add
                WHERE 1=1 $role";

                if($speciality && $subspeciality)
                    $sql .= " AND builder_specializations.wrk_type_id = {$speciality} AND builder_specializations.wrk_spec_id = {$subspeciality}";
                if($speciality && !$subspeciality)
                    $sql .= " AND builder_specializations.wrk_type_id = {$speciality}";
                if($note or $descr)
                    $sql .= " AND bm_users.id IN ({$ids})";
                if($stars)
                    $sql .= " AND bm_users.stars = {$stars}";
                $sql .= " GROUP BY bm_users_places.user_id HAVING distance < $distance ORDER BY distance ASC, bm_users_places.user_id DESC LIMIT {$start}, {$limit}";
            }

            return $this->db->query($sql)->fetchAll();
            // NEED REFACT !!!
        }

        public function searchUsersByNoteList($note, $start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $sql = "SELECT bm_admin_info.user_id FROM bm_admin_info
                LEFT JOIN bm_users ON bm_users.id = bm_admin_info.user_id
                WHERE bm_users.user_role_id > 2 AND bm_users.user_role_id < 7 AND bm_admin_info.description LIKE '%$note%'";
            $res = $this->db->query($sql)->fetchAll();
            if(!$res)
                return false;
            $tmp = array();
            foreach ($res as $r) {
                $tmp[] = $r['user_id'];
            }
            $ids = implode(',', $tmp);
            $sql = "SELECT bm_users.user_phone, bm_users.is_confirmed, bm_users.user_email, bm_users.user_name, bm_users.id AS uid,
                       builder_specializations.speckey, bm_users.user_role_id, bm_users.stars AS stars,
                       bm_users_places.id AS place_id
                FROM bm_users
                LEFT JOIN builder_specializations ON builder_specializations.user_id = bm_users.id
                LEFT JOIN bm_users_places ON bm_users_places.user_id = bm_users.id
                WHERE bm_users.id IN ({$ids})
                GROUP BY bm_users.id ORDER BY bm_users.id DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql)->fetchAll();
        }

        public function getOffersByUser($user_id)
        {
            $sql = 'SELECT * FROM bm_orders_connect WHERE offer_id = ?';
            return $this->db->query($sql, array($user_id))->fetchAll();
        }

        public function getOrdersByUser($user_id)
        {
            $sql = 'SELECT * FROM bm_orders WHERE user_id = ?';
            return $this->db->query($sql, array($user_id))->fetchAll();
        }

        public function getChoicesByUser($user_id)
        {
            $sql = 'SELECT * FROM bm_orders_connect WHERE choice_id = ?';
            return $this->db->query($sql, array($user_id))->fetchAll();
        }

        public function getChoicesByOrder($order_id)
        {
            $sql = 'SELECT * FROM bm_orders_connect WHERE order_id = ? AND choice_id IS NOT NULL';
            return $this->db->query($sql, array($order_id))->fetchAll();
        }

        public function countOrders()
        {
            $sql = 'SELECT COUNT(id) AS oc FROM bm_orders ORDER BY id DESC';
            return $this->db->query($sql)->fetch()['oc'];
        }

        public function countUsers()
        {
            $sql = 'SELECT COUNT(id) AS uc FROM bm_users WHERE is_active = 1 AND user_role_id > 2 AND user_role_id < 7';
            return $this->db->query($sql)->fetch()['uc'];
        }

        public function countBlockedUsers()
        {
            $sql = 'SELECT COUNT(id) AS uc FROM bm_users WHERE is_active = 0 AND user_role_id > 2 AND user_role_id < 7';
            return $this->db->query($sql)->fetch()['uc'];
        }

        public function countUsersMarked()
        {
            $sql = 'SELECT COUNT(id) AS uc FROM bm_users WHERE is_active = 1 AND user_role_id > 2 AND user_role_id < 7 AND is_marked = 1';
            return $this->db->query($sql)->fetch()['uc'];
        }

        public function countUsersCommented()
        {
            $sql = 'SELECT COUNT(id) AS uc FROM bm_admin_info';
            return $this->db->query($sql)->fetch()['uc'];
        }

        public function countBlank()
        {
            $sql = 'SELECT COUNT(bm_orders.id) AS uc FROM bm_orders
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                WHERE bm_orders.order_status = 0 OR bm_orders_places.id IS NULL';
            return $this->db->query($sql)->fetch()['uc'];
        }

        public function getOrdersList($start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $sql = 'SELECT bm_orders.id, show_is_old, bm_orders.order_brief AS descr_short, work_types.type_name, specializations.spec_name,
                DATE_FORMAT(bm_orders.order_date,"%d/%m/%Y (%H:%i)") AS order_date,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region
                FROM bm_orders
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec';
            $sql .= " ORDER BY bm_orders.order_date DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql)->fetchAll();
        }

        public function checkOrder($num)
        {
            $sql = 'SELECT id FROM bm_orders WHERE id = ?';
            return $this->db->query($sql, array($num))->fetch()['id'];
        }

        public function checkUser($num)
        {
            $sql = 'SELECT id FROM bm_users WHERE id = ?';
            return $this->db->query($sql, array($num))->fetch()['id'];
        }

        public function searchOrdersList($email, $descr, $tid, $sid, $placelat, $placelng, $start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $sql = 'SELECT bm_orders.id, bm_orders.order_brief AS descr_short, bm_orders.order_addr,
                work_types.type_name, specializations.spec_name, DATE_FORMAT(bm_orders.order_date,"%d/%m/%Y (%H:%i)") AS order_date,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region
                FROM bm_orders
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                LEFT JOIN bm_users ON bm_users.id = bm_orders.user_id
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
                WHERE 1=1';
            if($email)
                $sql .= " AND bm_users.user_email = '{$email}'";
            if($descr)
                $sql .= " AND bm_orders.order_brief LIKE '%{$descr}%' ";
            if($tid)
                $sql .= " AND work_types.id = '{$tid}'";
            if($sid)
                $sql .= " AND specializations.id = '{$sid}'";
            $sql .= " ORDER BY bm_orders.order_date DESC LIMIT {$start}, {$limit}";


            if($placelat && $placelng) {
                $sql = "SELECT bm_orders.id, bm_orders.order_brief AS descr_short, bm_orders.order_addr,
work_types.type_name, specializations.spec_name, DATE_FORMAT(bm_orders.order_date,'%d/%m/%Y (%H:%i)') AS order_date,
bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region,
(6371 * acos (
      cos ( radians({$placelat}) )
      * cos( radians( bm_orders_places.lat ) )
      * cos( radians( bm_orders_places.lng ) - radians({$placelng}) )
      + sin ( radians({$placelat}) )
      * sin( radians( bm_orders_places.lat ) )
    )
) AS distance
FROM bm_orders
LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
LEFT JOIN bm_users ON bm_users.id = bm_orders.user_id
LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
WHERE 1=1";
                if($descr)
                    $sql .= " AND bm_orders.order_brief LIKE '%{$descr}%' ";
                if($tid)
                    $sql .= " AND work_types.id = '{$tid}'";
                if($sid)
                    $sql .= " AND specializations.id = '{$sid}'";


                $sql .= " HAVING distance <= 5 ORDER BY bm_orders.order_date DESC, distance ASC LIMIT {$start}, {$limit}";
            }

            return $this->db->query($sql)->fetchAll();
            // NEED REFACT
        }

        public function getBlankOrdersList($start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $sql = 'SELECT bm_orders.id, bm_orders.order_brief AS descr_short, DATE_FORMAT(order_date,"%d/%m/%Y (%H:%i)") AS order_date,
                bm_orders_places.id AS place_id, bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region
                FROM bm_orders
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                WHERE order_status = 0 OR bm_orders_places.id IS NULL';
            $sql .= " ORDER BY bm_orders.order_date DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql)->fetchAll();
        }

        public function deleteOrderOffer($offer_id)
        {
            $sql = 'DELETE FROM bm_orders_connect WHERE id = ?';
            $this->db->query($sql, array($offer_id));
        }

        public function deleteOrder($order_id)
        {
            $sql = 'DELETE FROM bm_orders WHERE id = ?';
            //$this->db->query($sql, array($order_id));
            $this->db->execute($sql, array($order_id));
            echo $this->db->affectedRows();
            $sql = 'DELETE FROM bm_orders_connect WHERE order_id = ?';
            $this->db->query($sql, array($order_id));
        }

        public function getMark($user_id)
        {
            $sql = 'SELECT bm_admin_info.description FROM bm_admin_info WHERE bm_admin_info.user_id = ?';
            $desc = $this->db->query($sql, array($user_id))->fetch()['description'];
            $sql = 'SELECT bm_users.stars,  bm_users.premium_hours , bm_users.premium_hours_used FROM bm_users WHERE bm_users.id = ?';
            $mark = $this->db->query($sql, array($user_id))->fetch();

            return array('description' => $desc, 'stars' => $mark['stars'], 'premium_hours' => $mark['premium_hours'], 'premium_hours_used' => $mark['premium_hours_used']);
        }

        public function getOrderConnect($order_id)
        {
            $sql = 'SELECT bm_orders_connect.*, bm1.is_confirmed, bm1.user_phone, bm1.user_email, bm1.user_name AS c_user_name, bm1.id AS c_uid,
bm2.user_name AS o_user_name, bm2.id AS o_uid, bm2.is_confirmed AS bm2isc, bm2.user_phone AS bm2uph, bm2.user_email AS bm2uml,
bm1.stars AS bm1stars, bm2.stars AS bm2stars
FROM bm_orders_connect
LEFT JOIN bm_users bm1 ON bm1.id = bm_orders_connect.choice_id
LEFT JOIN bm_users bm2 ON bm2.id = bm_orders_connect.offer_id
WHERE bm_orders_connect.order_id = ?';
            $res = $this->db->query($sql, array($order_id))->fetchAll(); //var_dump($res); die;
            $count = count($res);
            $choice = array();
            $offer = array();
            for ($i = 0; $i < $count; $i++) {
                if($res[$i]['choice_id'])
                    $choice[] = $res[$i];
                else
                    $offer[] = $res[$i];
            }
            return array('choice' => $choice, 'offer' => $offer);
        }

        public function getNextOrder($order_id)
        {
            $sql = 'SELECT id FROM bm_orders WHERE id < ? ORDER BY id DESC LIMIT 1';
            return $this->db->query($sql, array($order_id))->fetch()['id'];
        }

        public function admCreateUser($email, $phone, $role, $passwd, $name)
        {
            $ip = $_SERVER['REMOTE_ADDR'];
            $passwd = $this->security->hash($passwd);
            $sql = 'INSERT IGNORE INTO bm_users (user_name, user_email, user_phone, user_password, user_role_id, user_reg_ip, stars) VALUES (?, ?, ?, ?, ?, ?, 1)';
            $this->db->query($sql, array($name, $email, $phone, $passwd, $role, $ip));
            $user_id = $this->db->lastInsertId();
            return $user_id;
        }

        public function editUser($user_id, $d)
        {
            $sql = 'INSERT INTO bm_perfs_info (description, brief, user_id) VALUES (?, ?, ?)
                    ON DUPLICATE KEY UPDATE description = ?, brief =?';
            $data = array($d['description'], $d['brief'], $user_id,
                $d['description'], $d['brief']);
            $this->db->query($sql, $data);

            if($d['user_name']) {
                $sql = 'UPDATE bm_users SET user_name = ? WHERE id = ?';
                $this->db->query($sql, array($d['user_name'], $user_id));
            }

            if($d['user_passwd']) {
                $user_passwd = $this->security->hash($d['user_passwd']);
                $sql = 'UPDATE bm_users SET user_password = ? WHERE id = ?';
                $this->db->query($sql, array($user_passwd, $user_id));
            }
        }

        public function updateEmail($user_id, $email)
        {
            $sql = 'SELECT user_email FROM bm_users WHERE user_email = ?';
            $res = $this->db->query($sql, array($email))->fetch()['user_email'];

            if($res != $email) {
                $sql = 'UPDATE bm_users SET user_email = ? WHERE id = ?';
                $this->db->query($sql, array($email, $user_id));
                return true;
            }
            return false;
        }

        public function updatePhone($user_id, $phone)
        {
            $sql = 'SELECT user_phone FROM bm_users WHERE user_phone = ?';
            $res = $this->db->query($sql, array($phone))->fetch()['user_phone'];

            if($res != $email) {
                $sql = 'UPDATE bm_users SET user_phone = ? WHERE id = ?';
                $this->db->query($sql, array($phone, $user_id));
                return true;
            }
            return false;
        }

        public function changeRole($role, $user_id)
        {
            $sql = 'UPDATE bm_users SET user_role_id = ? WHERE id = ?';
            $this->db->execute($sql, array($role, $user_id));
            return $this->db->affectedRows();
        }

        public function setMark($user_id, $admininfo, $stars, $premium_hours, $limit_choices)
        {
            if(!$stars) {
                $stars = 2;
            }
            if($admininfo != "") {
                $sql = 'DELETE FROM bm_admin_info WHERE user_id = ?';
                $this->db->query($sql, array($user_id));
                if($admininfo) {
                    $sql = 'INSERT INTO bm_admin_info (description, user_id) VALUES (?, ?)';
                    $this->db->query($sql, array($admininfo, $user_id));
                }
            }

            $sql = 'UPDATE bm_users SET stars = ?, premium_hours=?, limit_choices=?  WHERE id = ?';
            $this->db->query($sql, array($stars, $premium_hours, $limit_choices, $user_id));
            // NEED REFACT
        }

        public function setScale($scale_trust, $scale_cost, $scale_prof, $user_id)
        {
            $sql = 'UPDATE bm_users SET scale_trust=(scale_trust+?), scale_cost=(scale_cost+?), scale_prof=(scale_prof+?), scale_count=(scale_count+1)  WHERE id = ?';
            $this->db->query($sql, array($scale_trust, $scale_cost, $scale_prof, $user_id));
        }

        public function blockUser($user_id)
        {
            $sql = 'UPDATE bm_users SET is_active = IF(is_active=1, 0, 1) WHERE id = ?';
            $this->db->query($sql, array($user_id));
        }

        public function deleteUser($user_id)
        {
            $sql = 'DELETE FROM bm_users WHERE id = ?';
            $this->db->query($sql, array($user_id));

            $sql = 'UPDATE bm_orders SET user_id = 2, actual_period = NOW(), order_end = NOW() WHERE user_id = ?';
            $this->db->query($sql, array($user_id));

            $sql = 'DELETE FROM bm_orders_connect WHERE choice_id = ? OR offer_id = ?';
            $this->db->query($sql, array($user_id, $user_id));

            $sql = 'DELETE FROM bm_perfs_info WHERE user_id = ?';
            $this->db->query($sql, array($user_id));

            $sql = 'DELETE FROM bm_perfs_prices WHERE user_id = ?';
            $this->db->query($sql, array($user_id));

            $sql = 'DELETE FROM bm_files WHERE user_id = ?';
            $this->db->query($sql, array($user_id));

            $sql = 'DELETE FROM bm_admin_info WHERE user_id = ?';
            $this->db->query($sql, array($user_id));

            $sql = 'DELETE FROM builder_specializations WHERE user_id = ?';
            $this->db->query($sql, array($user_id));

            $sql = 'DELETE FROM bm_users_places WHERE user_id = ?';
            $this->db->query($sql, array($user_id));
        }

        public function getUnchainedCity()
        {
            /* $sql = "SELECT bm_places.*, bm_districts.district_name, bm_districts.id AS did
              FROM bm_places
              LEFT JOIN bm_districts ON bm_districts.region_id = bm_places.region_id
              WHERE district_id = 0 AND oc_code <> ''
              ORDER BY bm_places.id DESC";
              return $this->db->query($sql)->fetchAll(); */
            $sql = "SELECT * FROM bm_cat_places WHERE region_id = 0";
            return $this->db->query($sql)->fetchAll();

            if(!$res)
                return false;

            /* $regions = array();
              $districts = array();
              foreach($res as $r) {
              if (in_array($r['region_id'], $regions))
              continue;
              $regions[] = $r['region_id'];
              $sql = 'SELECT * FROM bm_regions WHERE id > 1';
              $d = $this->db->query($sql, array($r['region_id']))->fetchAll();
              $districts['r'.$r['region_id']] = $d;
              }
              $return = array();
              $return['cities'] = $res;
              $return['districts'] = $districts; */

            return $return;
        }

        public function setChainCity($city_id, $district_id)
        {
            $sql = 'UPDATE bm_cat_places SET region_id = ? WHERE id = ?';
            $this->db->query($sql, array($district_id, $city_id));
        }

        /* public function correctCity($city_id, $name, $region, $district)
          {
          $sql = 'SELECT bm_regions.ru_region_name, bm_districts.district_name FROM bm_regions, bm_districts WHERE bm_regions.id = ? AND bm_districts.id = ?';
          $r_names = $this->db->query($sql, array($region, $district))->fetch();
          $sql = 'UPDATE bm_places SET place_town = ?, place_district = ?, place_region = ?, region_id = ?, district_id = ? WHERE id = ?';
          $this->db->execute($sql, array($name, $r_names['district_name'], $r_names['ru_region_name'], $region, $district, $city_id));
          return $this->db->affectedRows();
          } */

        public function getRegions()
        {
            $sql = 'SELECT id, ru_region_name FROM bm_regions WHERE id > 1 ORDER BY ru_region_name';
            return $this->db->query($sql)->fetchAll();
        }

        public function getDistricts($region_id)
        {
            $sql = 'SELECT id, district_name FROM bm_districts WHERE region_id = ? AND in_city = 0 ORDER BY district_name';
            return $this->db->query($sql, array($region_id))->fetchAll();
        }

        public function getCities($region, $district)
        {
            $sql = 'SELECT * FROM bm_places WHERE region_id = ? AND district_id = ?';
            return $this->db->query($sql, array($region, $district))->fetchAll();
        }

        public function checkEmail($email, $type)
        {
            $sql = 'SELECT id FROM bm_users WHERE user_email = ?';
            if($type == 'bud')
                $sql .= ' AND user_role_id > 3 AND user_role_id < 7';
            else
                $sql .= ' AND user_role_id > 2';
            return $this->db->query($sql, array($email))->fetch()['id'];
        }

        public function checkPhone($phone, $type)
        {
            $sql = 'SELECT id FROM bm_users WHERE user_phone = ?';

            $sql .= ' AND user_role_id > 2';
            return $this->db->query($sql, array($phone))->fetch()['id'];
        }

        public function changePasswd($user_id, $passwdold, $passwdnew)
        {
            $sql = 'SELECT id, user_password, user_role_id FROM bm_users WHERE is_active = 1 AND id = ? AND user_role_id < 3';
            $result = $this->db->query($sql, array($user_id))->fetch();
            if(!$result)
                return false;
            if($this->security->checkHash($passwdold, $result['user_password'])) {
                $passwd = $this->security->hash($passwdnew);
                $sql = 'UPDATE bm_users SET user_password = ? WHERE id = ?';
                $this->db->query($sql, array($passwd, $user_id));
                return true;
            } else
                return false;
        }

        public function setRegion($order_id, $region_id, $district_id)
        {
            $sql = 'UPDATE bm_orders SET place_region_id = ? WHERE id = ?';
            $this->db->query($sql, array($region_id, $order_id));
            if($district_id) {
                $sql = 'UPDATE bm_orders SET place_district_id = ? WHERE id = ?';
                $this->db->query($sql, array($district_id, $order_id));
            }
        }

        public function getSubSpecList()
        {
            $sql = 'SELECT id, spec_name, type_id, slug FROM specializations WHERE is_active = 1 ORDER BY id';
            $result = $this->db->query($sql);
            $count = $result->numRows();
            $result = $result->fetchAll();
            $ret = false;
            if($count) {
                $ret = array();
                foreach ($result as $r) {
                    $indx = 'sub' . $r['type_id'];
                    if(!isset($ret[$indx]))
                        $ret[$indx] = array();
                    array_push($ret[$indx], array('spec_name' => $r['spec_name'], 'spec_id' => $r['id']));
                }
            }
            return $ret;
        }

        public function getSpec2($id)
        {
            $sql = 'SELECT * FROM specializations WHERE id = ?';
            return $this->db->query($sql, array($id))->fetch();
        }

        public function updateSpec2($id, $name, $speciality, $slug, $desc)
        {
            $sql = 'UPDATE specializations SET spec_name = ?, type_id = ?, slug =?, description = ? WHERE id = ?';
            $this->db->execute($sql, array($name, $speciality, $slug, $desc, $id));
            return $this->db->affectedRows();
        }

        public function getSpec($id)
        {
            $sql = 'SELECT * FROM work_types WHERE id = ?';
            return $this->db->query($sql, array($id))->fetch();
        }

        public function updateSpec($id, $name, $slug, $desc)
        {
            $sql = 'UPDATE work_types SET type_name = ?, slug =?, description = ? WHERE id = ?';
            $this->db->execute($sql, array($name, $slug, $desc, $id));
            return $this->db->affectedRows();
        }

        public function addSpec($name, $speciality, $slug)
        {
            if($speciality) {
                $sql = 'INSERT INTO specializations (type_id, spec_name, slug) VALUES (?, ?, ?)';
                $data = array($speciality, $name, $slug);
            } else {
                $sql = 'INSERT INTO work_types (type_name, slug) VALUES (?, ?)';
                $data = array($name, $slug);
            }
            $this->db->query($sql, $data);
            return $this->db->lastInsertId();
        }

        public function specAddRef($spec_id, $speciality, $subspeciality)
        {
            $sql = 'INSERT INTO bm_reference_spec (spec_id, ref_id, parent_ref_id) VALUES (?, ?, ?)';
            $this->db->query($sql, array($spec_id, $subspeciality, $speciality));
        }

        public function specDelRef($id)
        {
            $sql = 'DELETE FROM bm_reference_spec WHERE id = ?';
            $this->db->query($sql, array($id));
        }

        public function getSpecRef($spec_id)
        {
            $sql = 'SELECT specializations.spec_name, work_types.type_name, bm_reference_spec.id
                FROM bm_reference_spec
                LEFT JOIN specializations ON specializations.id = bm_reference_spec.ref_id
                LEFT JOIN work_types ON work_types.id = bm_reference_spec.parent_ref_id
                WHERE bm_reference_spec.spec_id = ?';
            return $this->db->query($sql, array($spec_id))->fetchAll();
        }

        public function addShopNet($name, $slug)
        {
            $sql = 'INSERT INTO bm_shop_networks (net_name, slug) VALUES (?, ?)';
            $this->db->query($sql, array($name, $slug));
        }

        public function getShopNetwork($net_id)
        {
            $sql = 'SELECT * FROM bm_shop_networks WHERE id = ?';
            return $this->db->query($sql, array($net_id))->fetch();
        }

        public function editShopNet($net_id, $name, $slug)
        {
            $sql = 'UPDATE bm_shop_networks SET net_name = ?, slug = ? WHERE id = ?';
            $this->db->query($sql, array($name, $slug, $net_id));
        }

        public function deleteShop($shop_id)
        {
            $this->deleteUser($shop_id);

            $sql = 'DELETE FROM bm_shops_info WHERE shop_id = ?';
            $this->db->query($sql, array($shop_id));

            $sql = 'DELETE FROM bm_shop_places WHERE shop_id = ?';
            $this->db->query($sql, array($shop_id));

            $sql = 'DELETE FROM shop_specializations WHERE shop_id = ?';
            $this->db->query($sql, array($shop_id));
        }

        public function searchShopList($name, $desc, $speciality, $start, $limit)
        {
            $sql = 'SELECT bm_users.user_name, bm_users.id AS uid, DATE_FORMAT(bm_users.user_reg_date, "%d/%m/%Y (%H:%i)") AS user_reg_date, bm_shops_info.brief
                FROM bm_users
                LEFT JOIN bm_shops_info ON bm_shops_info.shop_id = bm_users.id
                LEFT JOIN shop_specializations ON shop_specializations.shop_id = bm_users.id
                WHERE bm_users.user_role_id = 7';
            if($name)
                $sql .= " AND bm_users.user_name LIKE '%{$name}%'";
            if($desc)
                $sql .= " AND bm_shops_info.description LIKE '%{$desc}%'";
            if($speciality)
                $sql .= " AND shop_specializations.wrk_type_id = '{$speciality}'";
            $sql .= "GROUP BY uid ORDER BY bm_users.user_reg_date DESC LIMIT {$start}, {$limit}";

            return $this->db->query($sql)->fetchAll();
        }

        public function searchShopListByCoords($data, $start, $limit, $distance = 10)
        {

            $sql = "SELECT bm_users.user_name, bm_users.id AS uid, DATE_FORMAT(bm_users.user_reg_date, '%d/%m/%Y (%H:%i)') AS user_reg_date, bm_shops_info.brief,
(6371 * acos (
      cos ( radians({$data['placelat']}) )
      * cos( radians( bm_shop_places.lat ) )
      * cos( radians( bm_shop_places.lng ) - radians({$data['placelng']}) )
      + sin ( radians({$data['placelat']}) )
      * sin( radians( bm_shop_places.lat ) )
    )
) AS distance
            FROM bm_shop_places
            LEFT JOIN bm_users ON bm_users.id = bm_shop_places.shop_id
            LEFT JOIN bm_shops_info ON bm_shops_info.shop_id = bm_shop_places.shop_id
            WHERE bm_users.user_role_id = 7";
            $sql .= " HAVING distance < {$distance} ORDER BY distance ASC LIMIT {$start}, {$limit}";
            return $this->db->query($sql)->fetchAll();
        }

        public function searchShopListByNote($admininfo, $start, $limit)
        {
            $sql = "SELECT bm_users.user_name, bm_users.id AS uid, DATE_FORMAT(bm_users.user_reg_date, '%d/%m/%Y (%H:%i)') AS user_reg_date, bm_shops_info.brief
                FROM bm_users
                LEFT JOIN bm_shops_info ON bm_shops_info.shop_id = bm_users.id
                LEFT JOIN bm_admin_info ON bm_admin_info.user_id = bm_users.id
                WHERE bm_users.user_role_id = 7 AND bm_admin_info.description LIKE '%{$admininfo}%'";
            $sql .= " ORDER BY bm_users.user_reg_date DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql)->fetchAll();
        }

        public function leftoverDel($post_id)
        {
            $sql = 'DELETE FROM bm_leftovers WHERE id = ?';
            $this->db->query($sql, array($post_id));
        }

        public function leftoverClose($post_id)
        {
            $sql = 'UPDATE bm_leftovers SET lo_close_date = NOW() WHERE id = ?';
            $this->db->query($sql, array($post_id));
        }

        public function getStatusSMSByOrder($order_id)
        {
            $sql = "SELECT * FROM cron_blocked WHERE order_id=?";
            return $this->db->query($sql, array($order_id))->fetch();
        }

        public function getCommercialInterestedUsers($commercial_id)
        {
            $sql = "SELECT  bm_users.user_name, bm_users.user_phone
            FROM
                bm_users_shop            
            LEFT JOIN
                bm_users ON bm_users.id = bm_users_shop.user_id            
            WHERE 
                bm_users_shop.last_commercial_id =?";
            $res = $this->db->query($sql, [$commercial_id])->fetchAll();
            return $res;
        }

        public function setSMSError($us_id)
        {
            $sql = "SELECT sms_errors
            FROM
                bm_users   
            WHERE 
                bm_users.id =?";
            $res = $this->db->query($sql, [$us_id])->fetch()['sms_errors'];

            if($res >= 2) {
                $sql = 'UPDATE bm_users SET sms_errors=(sms_errors+1), is_active=2 WHERE id = ?';
            } else {
                $sql = 'UPDATE bm_users SET sms_errors=(sms_errors+1) WHERE id = ?';
            }

            $res = $this->db->query($sql, [$us_id]);
            return $res;
        }

    }
    