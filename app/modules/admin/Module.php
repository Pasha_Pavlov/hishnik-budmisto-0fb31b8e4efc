<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/admin/Module.php
     *
     * Admin module autoloader
     *
     */

    namespace Budmisto\Modules\Admin;

    class Module
    {

        public function registerAutoloaders()
        {
            $loader = new \Phalcon\Loader();
            $loader->registerNamespaces(array(
                'Budmisto\Modules\Admin\Controllers' => __DIR__ . '/controllers/',
                'Budmisto\Modules\Admin\Models' => __DIR__ . '/models/',
                'Budmisto\Modules\Front\Models' => APP_DIR . 'app/modules/front/models/',
                'Budmisto\Modules\Common\Models' => APP_DIR . 'app/modules/common/models/',
                'Budmisto\Lib\Mailer' => APP_DIR . 'app/library/mailer/'
            ));
            $loader->register();
        }

        public function registerServices($di)
        {
            $di->set('view', function()
            {
                $view = new \Phalcon\Mvc\View();
                $view->setViewsDir(__DIR__ . '/views/');
                return $view;
            });
        }

    }
    