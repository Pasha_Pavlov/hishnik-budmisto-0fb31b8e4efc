<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/admin/controllers/IndexController.php
     *
     * Admin controller
     *
     */

    namespace Budmisto\Modules\Admin\Controllers;

    use Budmisto\Modules\Admin\Models\Admin as BmAdmin;
    use Budmisto\Modules\Front\Models\Catalog as BmCatalog;
    use Budmisto\Modules\Front\Models\Users as BmUsers;
    use Budmisto\Modules\Common\Models\Common as Common;
    use Budmisto\Lib\Mailer\Mail as BMail;
    use Budmisto\Modules\Front\Models\Leftovers as BmLO;
    use Budmisto\Modules\Front\Controllers\ConnectController as ConnectController;

    class IndexController extends ControllerBase
    {

        public $admin;

        public function initialize()
        {
            parent::initialize();
            $this->admin = new BmAdmin();
        }

        function dsCrypt($input, $decrypt = false)
        {
            $input = preg_replace("|^bud|", '', $input);
            $o = $s1 = $s2 = array(); // Arrays for: Output, Square1, Square2
            // формируем базовый массив с набором символов
            $basea = array('(', '@', ';', '$', "]", '*', '!!', ')', '_', '|', '['); // base symbol set
            $basea = array_merge($basea, range('a', 'z'), range('A', 'Z'), range(0, 9));

            $dimension = 8; // of squares
            for ($i = 0; $i < $dimension; $i++) { // create Squares
                for ($j = 0; $j < $dimension; $j++) {
                    $s1[$i][$j] = $basea[$i * $dimension + $j];
                    $s2[$i][$j] = str_rot13($basea[($dimension * $dimension - 1) - ($i * $dimension + $j)]);
                }
            }
            unset($basea);
            $m = floor(strlen($input) / 2) * 2; // !strlen%2
            $symbl = $m == strlen($input) ? '' : $input[strlen($input) - 1]; // last symbol (unpaired)
            $al = array();
            // crypt/uncrypt pairs of symbols
            for ($ii = 0; $ii < $m; $ii += 2) {
                $symb1 = $symbn1 = strval($input[$ii]);
                $symb2 = $symbn2 = strval($input[$ii + 1]);
                $a1 = $a2 = array();
                for ($i = 0; $i < $dimension; $i++) { // search symbols in Squares
                    for ($j = 0; $j < $dimension; $j++) {
                        if($decrypt) {
                            if($symb1 === strval($s2[$i][$j]))
                                $a1 = array($i, $j);
                            if($symb2 === strval($s1[$i][$j]))
                                $a2 = array($i, $j);
                            if(!empty($symbl) && $symbl === strval($s2[$i][$j]))
                                $al = array($i, $j);
                        }
                        else {
                            if($symb1 === strval($s1[$i][$j]))
                                $a1 = array($i, $j);
                            if($symb2 === strval($s2[$i][$j]))
                                $a2 = array($i, $j);
                            if(!empty($symbl) && $symbl === strval($s1[$i][$j]))
                                $al = array($i, $j);
                        }
                    }
                }
                if(sizeof($a1) && sizeof($a2)) {
                    $symbn1 = $decrypt ? $s1[$a1[0]][$a2[1]] : $s2[$a1[0]][$a2[1]];
                    $symbn2 = $decrypt ? $s2[$a2[0]][$a1[1]] : $s1[$a2[0]][$a1[1]];
                }
                $o[] = $symbn1 . $symbn2;
            }
            if(!empty($symbl) && sizeof($al)) // last symbol
                $o[] = $decrypt ? $s1[$al[1]][$al[0]] : $s2[$al[1]][$al[0]];

            if($decrypt)
                return implode('', $o);
            return "bud" . implode('', $o);
        }

        public function getHashAction($user_id)
        {
            $users = new BmUsers();
            $profile = $users->getUserProfile($user_id);
            print_r("http://{$_SERVER[HTTP_HOST]}/index/" . $this->dsCrypt($profile['user_email']));
            exit();
        }

        public function indexAction()
        {
            $tmp = $this->user_level;
            if($tmp) {
                $this->response->redirect($this->siteUrl . '/workz/board');
                $this->response->send();
                die;
            } else {
                if($this->request->isPost() && $this->security->checkToken()) {
                    $name = $this->request->getPost('name', array('trim', 'string'), false);
                    $surname = $this->request->getPost('surname', array('trim', 'string'), false);
                    if($name && $surname) {
                        $login = $this->admin->signinAdmin($name, $surname);
                        if($login) {
                            $this->response->redirect($this->siteUrl . '/workz/board');
                            $this->response->send();
                            die;
                        }
                    }
                }
            }
        }

        public function quitAction()
        {
            $this->view->disable();
            $this->admin->signoutAdmin();
            $this->session->destroy();
            $this->response->redirect($this->siteUrl . '/workz');
        }

        public function microtime_float()
        {
            list($usec, $sec) = explode(" ", microtime());
            return ((float) $usec + (float) $sec);
        }

        public function boardAction()
        {

            $catalog = new BmCatalog();
            //$time_start = $this->microtime_float();
            $orders = $catalog->getLastOrdersList(0, 10);
            $this->view->setVar('orders', $orders);
            /* $time_end = $this->microtime_float();
              $time = $time_end - $time_start;
              $this->view->setVar('time1', $time); */

            //$time_start = $this->microtime_float();
            $workers = $this->admin->getLastUsersList(0, 10);
            $this->view->setVar('workers', $workers);
            /* $time_end = $this->microtime_float();
              $time = $time_end - $time_start;
              $this->view->setVar('time2', $time); */
        }

        public function ordersAction()
        {
            $tmp = $this->request->get('page', 'int');
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            $orders = $this->admin->getOrdersList($page, 30);
            $this->view->setVar('orders', $orders);

            $count = $this->admin->countOrders();
            $links_html = $this->createPaginator($count, $page, 30, $this->siteUrl . '/workz/orders');
            $this->view->setVar('pager_html', $links_html);
            $this->view->setVar('o_count', $count);
        }

        public function orderEditAction()
        {
            $this->view->disable();
            if($this->request->isPost() && $this->security->checkToken()) {
                $order_id = $this->request->getPost('order_id', 'int', false);

                $district = $this->request->getPost('district', array('trim', 'string'), false);
                if($district)
                    $data['placeid'] = $district;
                $data['placelat'] = false;
                $data['placelng'] = false;
                // NEED REFACT

                $chplace = $this->request->getPost('chplace', 'int', 0);
                if($chplace) {
                    $data['placelat'] = $this->request->getPost('placelat', 'float', false);
                    $data['placelng'] = $this->request->getPost('placelng', 'float', false);
                    $data['placeid'] = $this->request->getPost('placeid', array('trim', 'string'), false);
                    $data['placeaddr'] = $this->request->getPost('placeaddr', array('trim', 'string'), false);
                    $data['placetown'] = $this->request->getPost('placetown', array('trim', 'string'), false);
                    $data['placedistrict2'] = $this->request->getPost('placedistrict2', array('trim', 'string'), false);
                    $data['placedistrict3'] = $this->request->getPost('placedistrict3', array('trim', 'string'), false);
                    $data['placeregion'] = $this->request->getPost('placeregion', array('trim', 'string'), false);
                    $data['placecountry'] = $this->request->getPost('placecountry', array('trim', 'string'), false);
                    $data['placeroute'] = $this->request->getPost('placeroute', array('trim', 'string'), false);
                    $data['placextra'] = $this->request->getPost('placextra', array('trim', 'string'), false);

                    /* Глючит гугл, не выдает киевскую область! */
                    if($data['placetown'] == "Киев")
                        $data['placeregion'] = 'Киевская область';


                    if($data['placetown'] && $data['placelat'] && $data['placelng'] && $data['placeid'] && $data['placeregion'] && $data['placecountry']) {
//                    echo '<pre>';
//                    print_r($data);
//                    echo '</pre>';
//                    exit();



                        $common = new Common();
                        $common->editOrderPlace($order_id, $data);
                        //$common->addNewPlace($data);
                    }
                }

                $data['speciality'] = $this->request->getPost('speciality', 'int', false);
                $data['subspeciality'] = $this->request->getPost('subspeciality', 'int', false);
                $data['bugdet'] = $this->request->getPost('budget', 'int', false);
                $data['limcall'] = $this->request->getPost('limcall', 'int', false);
                $data['start'] = $this->request->getPost('start', 'int', false);
                $actual = $this->request->getPost('period', 'int', false);
                $data['description'] = $this->request->getPost('desc', array('trim', 'string'), false);
                $data['admin_description'] = $this->request->getPost('admin_description', array('trim', 'string'), false);
                $data['user_id'] = $this->user_id;
                $data['order_id'] = $order_id;

                $data['user_level'] = $this->user_level;
                $ok = false;
                if($data['speciality'] && $data['subspeciality'] && $data['bugdet'] && $data['limcall'] && $data['start'] && $data['description']) {
                    $data['actual_period'] = $actual ? date('Y-m-d H:i:s', strtotime('+' . $actual . ' days')) : false;
                    $filter = $this->getInputFilter();
                    $data['description'] = $filter->sanitize($data['description'], 'user_input');

                    $catalog = new BmCatalog();
                    $ok = $catalog->updateOrder($data);
                }

                $this->response->redirect($this->siteUrl . '/workz/orders/view/' . $order_id);
                $this->response->send();
                die;
            }
        }

//Автообновление профиля в фильтре через ajax
        public function checkUpdateAction()
        {
            if($this->request->isAjax()) {
                if(!is_null($this->session->get('filter_update'))) {
                    $user_id = $this->session->get('filter_update');
                    $users = new BmUsers();
                    $profile = $users->getUserProfile($user_id);
                    print_r(json_encode($profile));
                    $this->session->set('filter_update', '');
                }
            }
            exit();
        }

        public function cronStatusAction()
        {
            if($this->request->isAjax()) {

                $order_id = $this->request->getPost('order_id', 'int', false);
                $status = $this->request->getPost('status', 'int', false);
                $model = new BmCatalog();

                if($status == 1) {
                    $res = $model->deleteBlockSMS($order_id);
                } else {

                    $res = $model->setBlockSMS($order_id);

                    $mail = new BMail('common', 'info');
                    $mail->setOptions($this->siteUrl);
                    $data['title'] = 'Отключение автоматического СМС оповещения';
                    $data['message'] = 'Автоматическое оповещение по СМС отключено на  <a href="http://' . $_SERVER['HTTP_HOST'] . '/workz/orders/view/' . $order_id . '">заказе</a> ';
                    $mail->send('Отключение автоматического СМС', "olegwww@gmail.com", $data);
                }
                if($res) {
                    print_r("success");
                } else {
                    print_r("error");
                }
            }
        }

        public function oldStatusAction()
        {
            if($this->request->isAjax()) {

                $order_id = $this->request->getPost('order_id', 'int', false);
                $status = $this->request->getPost('status', 'int', false);
                $model = new BmCatalog();

                $res = $model->setIsOldStatus($order_id, $status);

                if($res) {
                    print_r("success");
                } else {
                    print_r("error");
                }
            }
        }

        public function premiumBlockStatusAction()
        {
            if($this->request->isAjax()) {

                $order_id = $this->request->getPost('order_id', 'int', false);
                $status = $this->request->getPost('status', 'int', false);
                $model = new BmCatalog();

                $res = $model->setPremiumStatusByOrder($order_id, $status);

                if($res) {
                    print_r("success");
                } else {
                    print_r("error");
                }
            }
            exit();
        }

        public function deletePremiumBlockAction()
        {
            $order_id = $this->request->getPost('order_id', 'int', false);

            $catalog = new BmCatalog();
            $block = $catalog->getCurrentPremiumBlockByOrder($order_id);
            if(!$block)
                exit("no_data");

            $last_time = strtotime($block['block_end']) - time();
            if(!$last_time)
                exit("error last time");

            $res = $catalog->returnPremiumTime($block['user_id'], $last_time);
            if(!$res)
                exit("error update user");

            $catalog->deleteCurrentPremiumBlock($block['order_id']);
            exit("success");
        }

        public function activateEternalFreezeAction()
        {
            $order_id = $this->request->getPost('order_id', 'int', false);
            $catalog = new BmCatalog();

            $current_block = $catalog->getCurrentPremiumBlockByOrder($order_id);
            if($current_block) {
                $duration = ceil(strtotime($block['block_end']) - strtotime($block['block_start']));
                $catalog->returnPremiumHours($current_block['user_id'], $duration);
            }

            $catalog->deleteAllPremiumBlocks($order_id);

            $catalog->closeOrder($order_id);

            print_r("success");
            exit();
        }

        public function orderViewAction()
        {
            $order_id = $this->dispatcher->getParam('id');
            if(!$order_id)
                die;

            $catalog = new BmCatalog();
            $common = new Common();
            $users = new BmUsers();

            $order = $catalog->getOrder($order_id);
            if(!$order)
                die;

            $informed_users = json_decode($order['informed_users'], 1);

            foreach ($informed_users as $key => $value) {
                $informed[] = $users->getUserProfile($value);
            }

            if($this->request->isAjax()) {

                /* форма добавления юзера ajax */

                if((int) $_POST['add_filter_user'] > 0) {
                    $user_id = $this->request->getPost('add_filter_user', 'int', false);
                    $catalog->setConnect($order_id, $user_id, 'performer', $text);
                    $user = $users->getUserProfile($user_id);
                    if($user['stars'] < 5) {
                        $this->admin->setMark($user_id, 0, 5);
                    }
                    print_r(json_encode($user));
                    exit();
                }

                $stars = $this->request->getPost('filter_stars', 'int', false);

                $tid = (int) $_POST['tid'];
                $sid = (int) $_POST['sid'];
                $lat = (float) $_POST['lat'];
                $lng = (float) $_POST['lng'];
                $distance = (int) $_POST['distance'];

                $this->config->lists->limit = 250;

                $blocked_workers = $catalog->getBlockedWorkers($order['user_id']);
              
                if($blocked_workers) {
                    $blocked_workers = json_decode($blocked_workers['blocked_workers'], true);
                }

                $order_connect = BmAdmin::getChoicesByOrder($order_id);
                if($order_connect) {
                    foreach ($order_connect as $key => $value) {
                        $connects[] = $value['choice_id'];
                    }
                }

                $customerOffers = BmUsers::getOffersFromCustomer($order['user_id']);
             
                if(isset($customerOffers)) {
                    foreach ($customerOffers as $key => $value) {
                        $offers_connect[] = $value['id'];
                    }
                }


                $connect = $this->admin->getOrderConnect($order_id);
                if(isset($connect['offer'])) {
                    foreach ($connect['offer'] as $key => $value) {
                        $offers_connect[] = $value['o_uid'];
                    }
                }

                $stars = $stars > 0 ? $stars : false;

                if($lat and $lng) {
                    $workers = $catalog->getSubWorkersListByCoords($tid, $sid, $lat, $lng, 0, $this->config->lists->limit, $stars, $distance);
                } else {
                    $workers = $catalog->getSubWorkersList($tid, $sid, 0, $this->config->lists->limit, $stars);
                }
         

                if($stars > 0) {
                    if(is_array($workers)) {
                        foreach ($workers as $key => $value) {
                            if($value['stars'] != $stars)
                                unset($workers[$key]);
                        }
                    }
                }

                if(count($workers) > 0) {

                    $html .= '<div class="table-responsive">
				<table class="table table-hover table-striped" >
				<tbody>
				<thead>
					<tr>
						<th style="text-align: center;">Имя</th>
						<th>Город</th>
                                                <th>Заметки</th>
						<th style="text-align: center;" >Действие</th>
					</tr>
				</thead>';


                    foreach ($workers as $worker) {

                        /**
                         * Отсеиваем оповещенных
                         */
                        $skip = false;
                        if(is_array($informed)) {
                            foreach ($informed as $key => $value) {
                                if($value['id'] == $worker['uid'])
                                    $skip = true;
                            }
                        }

                        if(in_array($worker['uid'], $offers_connect) or $skip) {
                            continue;
                        }

                        $worker['admin_info'] = $catalog->getadmin_info($worker['uid']);

                        $worker['blocked'] = (in_array($worker['uid'], $blocked_workers)) ? "&nbsp;<font color='red'>(-) </font>" : "";
                        $worker['choised'] = (in_array($worker['uid'], $connects)) ? "&nbsp;<font color='orange'>(+) </font>" : "";

                        $worker['scale'] = $worker['scale_trust'] + $worker['scale_cost'] + $worker['scale_prof'];
                        $html .= "<tr>
                                            <th style='max-width: 200px;'><a target='_blank' href='http://" . $_SERVER['HTTP_HOST'] . "/workz/users/view/" . $worker['uid'] . "'>" . $worker['user_name'] . " (" . $worker['stars'] . ")&nbsp;<font color='green'>(" . $worker['scale'] . ") </font> {$worker['blocked']} {$worker['choised']} </a></th>
                                            <th style='max-width: 150px;'>" . $worker['town'] . "<br>" . $worker['user_phone'] . "</th>
                                            <th style='max-width: 400px;'>" . $worker['admin_info']['description'] . "</th>
                                            <th style=\"text-align: center;\"><button class='btn btn-warning  btn-xs js_filter_user_hide '>Скрыть</button>&nbsp;&nbsp;<button id='" . $worker['uid'] . "' stars='" . $worker['stars'] . "' data_email='" . $worker['user_email'] . "' class='btn btn-success btn-xs js_filter_user_add ' style=\"text-align: center;\">Добавить</button></th></tr>";
                    }

                    $html .= "</tbody>
				</table>
				</div>";
                }

                print_r($html);

                exit();
            }

            $this->view->setVar('order_id', $order_id);

            if($this->request->isPost() && $this->security->checkToken()) {
                $text = $this->request->getPost('message', array('trim', 'string'), false);
                $user_id = $this->request->getPost('users_id', 'int', false);
                $catalog->setConnect($order_id, $user_id, 'performer', $text);
                $this->response->redirect($this->siteUrl . '/workz/orders/view/' . $order_id);
                $this->response->send();
                die;
            }


            $this->view->setVar('order', $order);


            $res = $catalog->getStatusSMSByOrder($order_id);
            if($res) {
                $this->view->setVar('sms_status', false);
            } else {
                $this->view->setVar('sms_status', true);
            }


            $res = $catalog->getStatusPremiumBlockByOrder($order_id);
            if($res == 1) {
                $this->view->setVar('premium_block', true);
            } else {
                $this->view->setVar('premium_block', false);
            }

            $show_is_old = $catalog->show_is_old($order_id);
            $this->view->setVar('show_is_old', $show_is_old);


            $this->view->setVar('informed_users', $informed);

            $res = $catalog->getCurrentPremiumBlockByOrder($order_id);
            if($res) {
                $res['user'] = $users->getUserProfile($res['user_id']);
            }


            /* очередь блокировки */
            $get_series = $catalog->getSeriesPremiumBlockByOrderId($order_id);

            if($get_series) {
                foreach ($get_series as $key => $value) {
                    $series[] = [
                        'user' => $users->getUserProfile($value['user_id']),
                        'duration' => $value['duration']
                    ];
                }

                $this->view->setVar('series', $series);
            }

            /* лог блокировок */

            $get_series_ended = $catalog->getEndedPremiumBlockByOrderId($order_id);

            if($get_series_ended) {
                foreach ($get_series_ended as $key => $value) {
                    $series_log[] = [
                        'user' => $users->getUserProfile($value['user_id']),
                        'block_start' => $value['block_start'],
                        'block_end' => $value['block_end']
                    ];
                }

                $this->view->setVar('series_log', $series_log);
            }




            $this->view->setVar('current_premium_block', ($res) ? $res : false);

            $this->view->setVar('eternal_freeze_request', $catalog->getEternalFreeze($order_id));

            $admininfo = $this->admin->getMark($order['user_id']);
            $this->view->setVar('admininfo', $admininfo);


            $this->view->setVar('map_key', $this->config->application->map_key);

            /* if ($order['extra_place_id']) {
              $xplace = $common->getExtraPlace($order['extra_place_id']);
              $this->view->setVar('xplace', $xplace);
              } */
            $top = $catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);
            if($order['order_wrk_type']) {
                $sub = $catalog->getSubSpecListById($order['order_wrk_type']);
                $this->view->setVar('sub_catalog', $sub);
            }

            $budget_list = $catalog->getBudgetList();
            $this->view->setVar('budget_list', $budget_list);
            $call_count = $catalog->getCallCountList();
            $this->view->setVar('call_count', $call_count);
            $when_list = $catalog->getWhenList();
            $this->view->setVar('when_list', $when_list);

            /* $districts = $common->getDistricts($order['place_id']);
              $this->view->setVar('districts', $districts); */

            $user = $users->getUserProfile($order['user_id']);
            $this->view->setVar('profile', $user);

            $back_url = false; //$this->request->getHTTPReferer();
            if(!$back_url)
                $back_url = $this->siteUrl . '/workz/board';
            $this->view->setVar('back_url', $back_url);

            $connect = $this->admin->getOrderConnect($order_id);
            $this->view->setVar('connect', $connect);

            $customerOffers = BmUsers::getOffersFromCustomer($order['user_id']);
            $this->view->setVar('customerOffers', $customerOffers);


            $next_id = $this->admin->getNextOrder($order_id);
            $next_id = $next_id ? $next_id : $order_id;
            $this->view->setVar('next_id', $next_id);

            $this->view->setVar('sms_informed', $this->get_informed_users($order_id));


            /* $regions = $this->admin->getRegions();
              $this->view->setVar('regions', $regions); */

            /* if ($order['order_wrk_type'] && $order['order_wrk_spec'] && !$order['close_date'] && $order['finish_date'] > time()) {
              $workers = $catalog->getSubWorkersList($order['order_wrk_type'], $order['order_wrk_spec'], $order['place_id'], 0, 10);
              if (!$workers) {
              $place = array('region_id' => $order['place_region_id'], 'district_id' => $order['place_district_id']);
              $workers = $catalog->getSubWorkersListOnPlace($order['order_wrk_type'], $order['order_wrk_spec'], $place, 0, 10);
              $this->view->setVar('workers_not_found_place', true);
              }
              $this->view->setVar('workers', $workers);
              } */
            //var_dump($order['lat'], $order['lng']); die;

            /* Не используются
              $workers = $catalog->getSubWorkersListByCoords($order['order_wrk_type'], $order['order_wrk_spec'], $order['lat'], $order['lng'], 0, 20);

              $this->view->setVar('workers', $workers);

             */
        }

        public function get_informed_users($order_id)
        {
            $users = BmUsers::get_informed_users($order_id);
            foreach ($users as $value) {
                $us[] = $value['phone'];
            }
            $us = array_unique($us);
            return $us;
        }

        public function orderDelOfferAction()
        {
            $this->view->disable();
            $offer_id = $this->dispatcher->getParam('id');
            if(!$offer_id)
                die;

            $this->admin->deleteOrderOffer($offer_id);
            $back = $this->request->getHTTPReferer();
            $this->response->redirect($back);
            $this->response->send();
            die;
        }

        public function orderCreateAction()
        {
            $catalog = new BmCatalog();
            if($this->request->isPost() && $this->security->checkToken()) {
                $data['placelat'] = $this->request->getPost('placelat', 'float', false);
                $data['placelng'] = $this->request->getPost('placelng', 'float', false);
                $data['placeid'] = $this->request->getPost('placeid', array('trim', 'string'), false);
                $data['placeaddr'] = $this->request->getPost('placeaddr', array('trim', 'string'), false);
                $data['placetown'] = $this->request->getPost('placetown', array('trim', 'string'), false);
                $data['placedistrict2'] = $this->request->getPost('placedistrict2', array('trim', 'string'), false);
                $data['placedistrict3'] = $this->request->getPost('placedistrict3', array('trim', 'string'), false);
                $data['placeregion'] = $this->request->getPost('placeregion', array('trim', 'string'), false);
                $data['placecountry'] = $this->request->getPost('placecountry', array('trim', 'string'), false);
                $data['placeroute'] = $this->request->getPost('placeroute', array('trim', 'string'), false);
                $data['placextra'] = $this->request->getPost('placextra', array('trim', 'string'), false);

                $data['speciality'] = $this->request->getPost('speciality', 'int', false);
                $data['subspeciality'] = $this->request->getPost('subspeciality', 'int', false);
                $data['bugdet'] = $this->request->getPost('budget', 'int', false);
                $data['limcall'] = $this->request->getPost('limcall', 'int', false);
                $data['start'] = $this->request->getPost('start', 'int', false);
                $actual = $this->request->getPost('period', 'int', false);
                $data['description'] = $this->request->getPost('desc', array('trim', 'string'), false);
                $data['user_id'] = $this->request->getPost('users_id', 'int', false);

                if(!$data['placetown'] || !$data['placeid']) {
                    $this->dumbError('(Admin create order) Wrong location', false, true);
                }

                $order_id = $catalog->createNewOrder($data);
                if(!$order_id) {
                    $this->dumbError('(Admin create order form) Failed to create order');
                }

                $common = new Common();
                $common->addOrderPlace($order_id, $data);

                $data['order_id'] = $order_id;
                if($data['speciality'] && $data['subspeciality'] && $data['bugdet'] && $data['limcall'] && $data['start'] && $actual && $data['description']) {
                    $data['actual_period'] = date('Y-m-d H:i:s', strtotime('+' . $actual . ' days'));
                    $filter = $this->getInputFilter();
                    $data['description'] = $filter->sanitize($data['description'], 'user_input');
                    $catalog->updateOrder($data);
                }
                $this->response->redirect($this->siteUrl . '/workz/board');
                $this->response->send();
                die;
            }

            $budget_list = $catalog->getBudgetList();
            $this->view->setVar('budget_list', $budget_list);
            $call_count = $catalog->getCallCountList();
            $this->view->setVar('call_count', $call_count);
            $when_list = $catalog->getWhenList();
            $this->view->setVar('when_list', $when_list);
            $top = $catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);
            $this->view->setVar('map_key', $this->config->application->map_key);
            $back_url = $this->siteUrl . '/workz/board';
            $this->view->setVar('back_url', $back_url);
        }

        public function orderSearchAction()
        {

            $catalog = new BmCatalog();
            if($this->request->isPost() && $this->security->checkToken()) {
                $placelat = $this->request->getPost('placelat', 'float', false);
                $placelng = $this->request->getPost('placelng', 'float', false);

                $descr = $this->request->getPost('descr', array('trim'), false);
                $email = $this->request->getPost('email', array('trim', 'email'), false);
                $tid = $this->request->getPost('speciality', 'int', false);
                $sid = $this->request->getPost('subspeciality', 'int', false);
                $num = $this->request->getPost('num', 'int', false);

                if($email || $descr || $tid || $sid || ($placelat && $placelng)) {
                    $result = $this->admin->searchOrdersList($email, $descr, $tid, $sid, $placelat, $placelng, 0, 20);
                    $this->view->setVar('orders', $result);
                    if(!$result)
                        $this->view->setVar('error', true);
                } elseif($num) {
                    $ok = $this->admin->checkOrder($num);
                    if($ok) {
                        $this->response->redirect($this->siteUrl . '/workz/orders/view/' . $num);
                        $this->response->send();
                        die;
                    } else {
                        $this->view->setVar('error_num', $num);
                    }
                }
            }

            $top = $catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);
            $this->view->setVar('map_key', $this->config->application->map_key);
        }

        public function orderDeleteAction()
        {
            $this->view->disable();
            $r = $this->request->getHTTPReferer();
            if(strpos($r, $this->siteUrl) === false)
                die;

            $order_id = $this->dispatcher->getParam('id');
            if(!$order_id)
                die;

            $this->admin->deleteOrder($order_id);

            if(!$this->request->isAjax()) {
                $this->response->redirect($this->siteUrl . '/workz/board');
                $this->response->send();
            }
            die;
        }

        public function ordersBlankAction()
        {
            $tmp = $this->request->get('page', 'int');
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            $result = $this->admin->getBlankOrdersList($page, 30);
            $this->view->setVar('orders', $result);

            $count = $this->admin->countBlank();
            $links_html = $this->createPaginator($count, $page, 30, $this->siteUrl . '/workz/orders/blank');
            $this->view->setVar('pager_html', $links_html);
        }

        public function orderCloseAction()
        {
            $this->view->disable();
            $order_id = $this->dispatcher->getParam('id');
            if(!$order_id)
                die;

            $catalog = new BmCatalog();
            $catalog->closeOrder($order_id);
            //$this->admin->closeOrder($order_id);
            $this->response->redirect($this->siteUrl . '/workz/orders/view/' . $order_id);
            $this->response->send();
            die;
        }

        public function userEditAction()
        {

            if($this->request->isPost() && $this->security->checkToken()) {
                $user_id = $this->request->getPost('user_id', 'int', false);
                $email = $this->request->getPost('email', array('email', 'trim'), false);
                $phone = $this->request->getPost('us_phone', "string", false);
                $desc = $this->request->getPost('desc', array('trim', 'string'), false);
                $data['user_name'] = $this->request->getPost('user_name', array('trim', 'string'), false);
                $data['user_passwd'] = $this->request->getPost('user_passwd', array('trim', 'string'), false);
                $sendPasswordToMail = $this->request->getPost('sendtomail', 'int', false);
                $data['placeid'] = $this->request->getPost('placeid', array('trim', 'string'), false);

                if($data['placeid']) {
                    $placeid = $data['placeid'];
                    $data['placelat'] = $this->request->getPost('placelat', 'float', false);
                    $data['placelng'] = $this->request->getPost('placelng', 'float', false);
                    $data['placeaddr'] = $this->request->getPost('placeaddr', array('trim', 'string'), false);
                    $data['placetown'] = $this->request->getPost('placetown', array('trim', 'string'), false);
                    $data['placedistrict2'] = $this->request->getPost('placedistrict2', array('trim', 'string'), false);
                    $data['placedistrict3'] = $this->request->getPost('placedistrict3', array('trim', 'string'), false);
                    $data['placeregion'] = $this->request->getPost('placeregion', array('trim', 'string'), false);
                    $data['placecountry'] = $this->request->getPost('placecountry', array('trim', 'string'), false);
                    $data['placeroute'] = $this->request->getPost('placeroute', array('trim', 'string'), false);
                    $data['placextra'] = $this->request->getPost('placextra', array('trim', 'string'), false);

                    if($data['placetown'] && $data['placelat'] && $data['placelng'] && $data['placeid'] && $data['placeregion'] && $data['placecountry']) {
                        $common = new Common();

                        if($data['placeroute']) {
                            $key = $this->config->application->server_key;
                            $ch = curl_init();
                            $url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query=' . urlencode($data['placetown'] . ',' . $data['placeregion']);
                            $url .= '&key=' . $key . '&language=ru';
                            $cert = APP_DIR . 'var/cacerts.pem';

                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HEADER, false);
                            curl_setopt($ch, CURLOPT_CAINFO, $cert);

                            session_write_close();
                            $responce = curl_exec($ch);
                            session_start();
                            curl_close($ch);

                            $res = json_decode($responce);
                            $d['placetown'] = $res->results[0]->name;
                            $d['placeid'] = $res->results[0]->place_id;
                            $d['placelat'] = $res->results[0]->geometry->location->lat;
                            $d['placelng'] = $res->results[0]->geometry->location->lng;
                            $d['placeregion'] = $data['placeregion'];
                            $common->addNewPlace($d);
                        } else
                            $common->addNewPlace($data);

                        $common->addUserPlace($user_id, $data);
                    }
                }

                if($desc || $data['user_name'] || $data['user_passwd']) {
                    $data['description'] = $desc;
                    if($desc) {
                        $filter = $this->getInputFilter();
                        $data['description'] = $this->textengine->convertBB2Html($filter->sanitize($desc, 'user_input'));
                        $data['brief'] = $this->textengine->getBrief($data['description'], $this->config->lists->big_brief);
                    } else {
                        $data['brief'] = '';
                    }
                    $this->admin->editUser($user_id, $data);
                    $this->session->set('filter_update', $user_id);
                }
                if($email) {
                    $query = $this->admin->updateEmail($user_id, $email);
                    $query = $this->admin->updatePhone($user_id, $phone);
                }


                if($sendPasswordToMail && $email && $data['user_passwd']) {
                    $mail = new BMail('users', 'recovery');
                    $mail->setOptions($this->siteUrl);
                    $data['email'] = $email;
                    $data['passwd'] = $data['user_passwd'];
                    $data['fast_login'] = $this->dsCrypt($email);
                    $mail->send('Ваш новый пароль', $email, $data);
                    $mail->send('Новый пароль пользователя ' . $data['user_name'], $this->config->other->admin_email, $data);
                }

                if($query) {
                    $mail = new BMail('users', 'new_email');
                    $mail->setOptions($this->siteUrl);
                    $data['email'] = $email;
                    $data['fast_login'] = $this->dsCrypt($email);
                    $mail->send('Ваш новый e-mail', $email, $data);
                }

                $this->response->redirect($this->siteUrl . '/workz/users/view/' . $user_id);
                $this->response->send();
                die;
            }
        }

        public function userAddSpecAction()
        {
            if($this->request->isPost() && $this->security->checkToken()) {
                $user_id = $this->request->getPost('user_id', 'int', false);
                $tid = $this->request->getPost('speciality', 'int', false);
                $sid = $this->request->getPost('subspeciality', 'int', false);
                if(!$user_id || !$tid || !$sid)
                    die;

                if($sid == 1000) {
                    $sub_list = BmCatalog::getSubSpecListById($tid);
                    if(!$sub_list)
                        return false;

                    $users = new BmUsers();
                    foreach ($sub_list as $key => $value) {
                        $res = $users->addSpec($tid, $value['id'], $user_id);
                    }
                } else {
                    $users = new BmUsers();
                    $users->addSpec($tid, $sid, $user_id);
                }
            }
            $this->response->redirect($this->siteUrl . '/workz/users/view/' . $user_id);
            $this->response->send();
            die;
        }

        public function userDelSpecAction()
        {
            $user_id = $this->request->get('user', 'int', false);
            $spec = $this->request->get('id', 'int', false);
            if(!$user_id || !$spec)
                die;

            $users = new BmUsers();
            $users->delSpec($spec, $user_id);
            $this->response->redirect($this->siteUrl . '/workz/users/view/' . $user_id);
            $this->response->send();
            die;
        }

        public function userDelPlaceAction()
        {
            $user_id = $this->request->get('user', 'int', false);
            $place = $this->request->get('id', 'int', false);
            if(!$user_id || !$place)
                die;

            $users = new BmUsers();
            $users->delPlace($place, $user_id);
            $this->response->redirect($this->siteUrl . '/workz/users/view/' . $user_id);
            $this->response->send();
            die;
        }

        public function userViewAction()
        {
            $user_id = $this->dispatcher->getParam('id');
            if(!$user_id)
                die;
            $users = new BmUsers();
            $common = new Common();


            if($this->request->isPost() && $this->security->checkToken()) {
                if(isset($_POST['mark-form'])) {
                    $admininfo = $this->request->getPost('admininfo', array('trim', 'string'), false);
                    $limit_choices = $this->request->getPost('limit_choices', 'int', false);
                    $limit_choices = $limit_choices ? $limit_choices : 0;
                    $stars = $this->request->getPost('stars', 'int', false);
                    $premium_hours = $this->request->getPost('premium_hours', 'float', false);
                    $premium_hours = $premium_hours ? $premium_hours * 3600 : 0;
                    $this->admin->setMark($user_id, $admininfo, $stars, $premium_hours, $limit_choices);
                    $this->response->redirect($this->siteUrl . '/workz/users/view/' . $user_id);
                    $this->response->send();
                    die;
                }
                if(isset($_POST['scale-form'])) {
                    $scale_trust = $this->request->getPost('scale_trust', 'int', false);
                    $scale_cost = $this->request->getPost('scale_cost', 'int', false);
                    $scale_prof = $this->request->getPost('scale_prof', 'int', false);
                    $this->admin->setScale($scale_trust, $scale_cost, $scale_prof, $user_id);
                }
            }



            $profile = $users->getUserProfile($user_id);
            if(!$profile)
                die;
            $this->view->setVar('profile', $profile);
            $this->view->setVar('map_key', $this->config->application->map_key);
            /* $place = $common->getPlace($profile['user_place_id']);
              $this->view->setVar('place', $place); */

            $addrs = $users->getUserPlaces($user_id);
            $this->view->setVar('places', $addrs);

            $back_url = false; //$this->request->getHTTPReferer();
            if(!$back_url)
                $back_url = $this->siteUrl . '/workz/board';
            $this->view->setVar('back_url', $back_url);
            $admininfo = $this->admin->getMark($user_id);

            $this->view->setVar('admininfo', $admininfo);

            if($profile['user_role_id'] > 3 && $profile['user_role_id'] < 7) {
                $user_info = $users->getUserInfo($user_id);
                if($user_info['description'])
                    $user_info['description'] = $this->textengine->convertHtml2BB($user_info['description']);
                $this->view->setVar('info', $user_info);

                $user_files = $users->getUserFiles('file', $user_id);
                $user_images = $users->getUserFiles('image', $user_id);
                $this->view->setVar('img_files', $user_images);
                $this->view->setVar('doc_files', $user_files);
                $this->view->setVar('fpath', '/data/users/');

                $user_specs = $users->getUserSpecs($user_id);
                $this->view->setVar('user_specs', $user_specs);
                if($user_specs) {
                    $user_prices = $users->getUserPrices($user_id);
                    $this->view->setVar('user_prices', $user_prices);
                }
                $offers = $this->admin->getOffersByUser($user_id);
                $this->view->setVar('offers', $offers);
                $choices = $this->admin->getChoicesByUser($user_id);
                $this->view->setVar('choices', $choices);

                $catalog = new BmCatalog();
                $top_catalog = $catalog->getTopSpecList();
                $this->view->setVar('top_catalog', $top_catalog);
            }
            $orders = $this->admin->getOrdersByUser($user_id);
            $this->view->setVar('orders', $orders);
        }

        public function usersAction()
        {
            $tmp = $this->request->get('page', 'int');
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            $users = $this->admin->getLastUsersList($page, 30);
            $this->view->setVar('users', $users);

            $count = $this->admin->countUsers();
            $links_html = $this->createPaginator($count, $page, 30, $this->siteUrl . '/workz/users');
            $this->view->setVar('pager_html', $links_html);
            $this->view->setVar('u_count', $count);
        }

        public function usersBlockedAction()
        {
            $tmp = $this->request->get('page', 'int');
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            $users = $this->admin->getBlockedUsersList($page, 30);
            $this->view->setVar('users', $users);

            $count = $this->admin->countBlockedUsers();
            $links_html = $this->createPaginator($count, $page, 30, $this->siteUrl . '/workz/users');
            $this->view->setVar('pager_html', $links_html);
            $this->view->setVar('u_count', $count);
        }

        public function userCreateAction()
        {
            if($this->request->isPost() && $this->security->checkToken()) {
                $name = $this->request->getPost('name', array('trim', 'string'), false);
                $email = $this->request->getPost('email', array('trim', 'email'), false);
                $phone = $this->request->getPost('phone', array('trim', 'string'), false);
                $role = $this->request->getPost('type', 'int', false);
                $passwd = $this->request->getPost('passwd', array('trim', 'string'), false);

                if($name && $email && $phone && $role && $passwd) {
                    $user_id = $this->admin->admCreateUser($email, $phone, $role, $passwd, $name);
                    $this->response->redirect($this->siteUrl . '/workz/users/view/' . $user_id);
                    $this->response->send();
                    die;
                }
            }
        }

        public function userDeleteAction()
        {
            $user_id = $this->dispatcher->getParam('id');
            if(!$user_id)
                die;

            $this->admin->deleteUser($user_id);
            $configPath = $this->config->files->users;
            $path = $configPath . $user_id . '/';
            if(file_exists($path)) {
                array_map('unlink', glob($path . '*.*'));
                rmdir($path);
            }
            $r = $this->request->getHTTPReferer();
            if(strpos($r, $this->siteUrl . '/workz/users/search') !== false)
                $this->response->redirect($this->siteUrl . '/workz/users/search');
            else
                $this->response->redirect($this->siteUrl . '/workz/board');
            $this->response->send();
        }

        public function userSearchAction()
        {

            if($this->request->isPost()) { //&& $this->security->checkToken()) {
                $tmp = $this->request->getPost('page', 'int', false);
                $page = $tmp ? $tmp : 0;
                $this->view->setVar('page', $page);

                $email = $this->request->getPost('email', array('trim', 'email'), false);
                $phone = $this->request->getPost('phone', array('trim', 'string'), false);
                $stars = $this->request->getPost('stars', array('trim', 'int'), false);
                $placelat = $this->request->getPost('placelat', 'float', false);
                $placelng = $this->request->getPost('placelng', 'float', false);
                $num = $this->request->getPost('num', 'int', false);
                $descr = $this->request->getPost('descr', array('trim', 'string'), false);
                $note = $this->request->getPost('note', array('trim', 'string'), false);
                $speciality = $this->request->getPost('speciality', 'int', false);
                $subspeciality = $this->request->getPost('subspeciality', 'int', false);
                $distance = $this->request->getPost('distance', 'int', false);
                $distance = $distance ? $distance : 10;

                if($email || $phone || ($placelat && $placelng) || $speciality || $note || $descr || $stars) {
                    $result = $this->admin->searchUsersList($email, $phone, $placelat, $placelng, $stars, $speciality, $subspeciality, $note, $descr, $page, 20, $distance);
                    // print_r($result);
                    //exit();
                    // print_r($result);exit();
                    if(!$result)
                        $this->view->setVar('error', true);
                    elseif(count($result) > 20) {
                        $next = ($page == 0) ? 2 : ($page + 1);
                        array_pop($result);
                    } else
                        $next = 0;
                    $this->view->setVar('nextpage', $next);
                    $this->view->setVar('users', $result);
                } elseif($num) {
                    $ok = $this->admin->checkUser($num);
                    if($ok) {
                        $this->response->redirect($this->siteUrl . '/workz/users/view/' . $num);
                        $this->response->send();
                        die;
                    } else {
                        $this->view->setVar('error_num', $num);
                    }
                } /* elseif ($note) {
                  $result = $this->admin->searchUsersByNoteList($note, 0, 20);
                  $this->view->setVar('users', $result);
                  if (!$result)
                  $this->view->setVar('error', true);
                  } */
                $this->view->setVar('post_data', $_POST);
            }
            $catalog = new BmCatalog();
            $top_catalog = $catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top_catalog);
            $this->view->setVar('map_key', $this->config->application->map_key);
        }

        public function smsSenderAction()
        {

            $catalog = new BmCatalog();
            $sms_orders = $catalog->getSMSOrders();

            if($this->request->isPost()) { //&& $this->security->checkToken()) {
                if(isset($_POST['search_form'])) {
                    $email = $this->request->getPost('email', array('trim', 'email'), false);
                    $phone = $this->request->getPost('phone', array('trim', 'string'), false);
                    $stars = $this->request->getPost('stars', array('trim', 'int'), false);
                    $placelat = $this->request->getPost('placelat', 'float', false);
                    $placelng = $this->request->getPost('placelng', 'float', false);
                    $descr = $this->request->getPost('descr', array('trim', 'string'), false);
                    $note = $this->request->getPost('note', array('trim', 'string'), false);
                    $speciality = $this->request->getPost('speciality', 'int', false);
                    $subspeciality = $this->request->getPost('subspeciality', 'int', false);
                    $distance = $this->request->getPost('distance', 'int', false);
                    $distance = $distance ? $distance : 10;
                    $role = $this->request->getPost('role', 'int', false);
                    $is_active = $this->request->getPost('is_active', 'int', false);
                    $usrs = $this->admin->searchUsersList($email, $phone, $placelat, $placelng, $stars, $speciality, $subspeciality, $note, $descr, $page, 10000, $distance, $role);
                    $sms_order = $this->request->getPost('sms_order', 'int', false);

                    $sms_order = $catalog->getSMSOrders($sms_order != 0 ? $sms_order : 'null');

                    $this->view->setVar('sms_order', $sms_order);

                    if(is_array($usrs)) {

                        $phones = json_decode($sms_order[0]['sended_to'], true);

                        foreach ($usrs as $key => $value) {
                            if($is_active == $value['is_active']) {
                                if(!in_array($value['user_phone'], $phones))
                                    $users[] = $value['user_phone'];
                            }
                        }
                    }

                    if(!count($users)) {
                        $this->view->setVar('no_new_users', "Новых пользователей не отобрано");
                    }
                }

                if(isset($_POST['getCSV'])) {
                    $phones = json_decode($_POST['users_array'], true);

                    $order_id = $this->request->getPost('sms_order', 'int', false);

                    if($order_id > 0) {

                        $sms_order = $catalog->getSMSOrders($order_id);

                        $old_phones = json_decode($sms_order[0]['sended_to'], true);

                        if(is_array($old_phones)) {

                            array_unshift($phones, '*');

                            $phones = array_merge($old_phones, $phones);
                        }

                        $new_phones = $phones;
                    }

                    $cols = array_keys($new_phones, '*');


                    for ($i = 0; $i <= count($cols); $i++) {
                        $titles[] = 'Телефон';
                    }

                    // Раскладываем данные по массивам
                    $d = implode('*', $new_phones);
                    $d = explode('***', $d);
                    $count = 0;

                    foreach ($d as $key => $value) {
                        $data[$key] = explode('*', $value);
                        if(count($data[$key]) > $count)
                            $count = count($data[$key]);
                    }

                    // раскладываем по ключу в разные столбцы
                    for ($i = 0; $i < $count; $i++) {
                        for ($title = 0; $title < count($titles); $title ++) {
                            $new_data[$i][] = $data[$title][$i];
                        }
                    }

                    $csv_data = array_merge([$titles], $new_data);

                    $filename = time() . '.csv';

                    $this->create_csv_file($csv_data, $filename);
                }
                if(isset($_POST['sendSMS'])) {

                    $phones = json_decode($_POST['users_array'], true);

                    $SMS_text = $this->request->getPost('sms_text', array('trim', 'string'), false);
                    $order_id = $this->request->getPost('sms_order', 'int', false);

                    if($SMS_text != '') {
                        if(count($phones) > 0) {

                            $this->sendMSG($phones, $SMS_text);

                            $order_id = $this->request->getPost('sms_order', 'int', false);

                            if($order_id > 0) {

                                $sms_order = $catalog->getSMSOrders($order_id);

                                $old_phones = json_decode($sms_order[0]['sended_to'], true);

                                if(is_array($old_phones)) {

                                    array_unshift($phones, '*');

                                    foreach ($old_phones as $k => $v) {
                                        array_unshift($phones, $v);
                                    }
                                }
                                $new_phones = $phones;

                                $catalog->updateSMSOrderUsers($order_id, json_encode($new_phones));
                            }

                            $this->view->setVar('success', "СМС переданы оператору");
                        } else {
                            $this->view->setVar('error', "Пользователи не найдены");
                        }
                    } else {
                        $this->view->setVar('error', "Введите текст смс");
                    }
                }
                if(isset($_POST['sendToJob'])) {

                    $phones = json_decode($_POST['users_array'], true);

                    $order_id = $this->request->getPost('sms_order', 'int', false);

                    if(count($phones) > 0) {

                        $order_id = $this->request->getPost('sms_order', 'int', false);

                        if($order_id > 0) {

                            $sms_order = $catalog->getSMSOrders($order_id);

                            $old_phones = json_decode($sms_order[0]['sended_to'], true);

                            if(is_array($old_phones)) {

                                array_unshift($phones, '*');

                                foreach ($old_phones as $k => $v) {
                                    array_unshift($phones, $v);
                                }
                            }
                            $new_phones = $phones;

                            $catalog->updateSMSOrderUsers($order_id, json_encode($new_phones));
                            $this->view->setVar('success', "Пользователи успешно добавлены к заданию");
                        } else {
                            $this->view->setVar('error', "Задание не выбрано");
                        }
                    } else {
                        $this->view->setVar('error', "Пользователи не найдены");
                    }
                }
            }

            $this->view->setVar('sms_orders', $sms_orders);
            $top_catalog = $catalog->getTopSpecList();
            $this->view->setVar('users', $users);
            $this->view->setVar('top_catalog', $top_catalog);
            $this->view->setVar('map_key', $this->config->application->map_key);
            return $this->view->pick('index/smssender');
        }

        public function smsOrdersAction()
        {
            $catalog = new BmCatalog();
            $sms_orders = $catalog->getSMSOrders();
            $this->view->setVar('sms_orders', $sms_orders);

            $selected_order = $this->dispatcher->getParam('id');
            $this->view->setVar('selected_order', $selected_order);

            if(isset($_POST['getCSV'])) {

                $order_id = $this->request->getPost('sms_order', 'int', false);

                if($order_id > 0) {

                    $sms_order = $catalog->getSMSOrders($order_id);

                    $new_phones = json_decode($sms_order[0]['sended_to'], true);

                    $cols = array_keys($new_phones, '*');

                    for ($i = 0; $i <= count($cols); $i++) {
                        $titles[] = 'Телефон';
                    }

                    // Раскладываем данные по массивам
                    $d = implode('*', $new_phones);
                    $d = explode('***', $d);
                    $count = 0;

                    foreach ($d as $key => $value) {
                        $data[$key] = explode('*', $value);
                        if(count($data[$key]) > $count)
                            $count = count($data[$key]);
                    }

                    // раскладываем по ключу в разные столбцы
                    for ($i = 0; $i < $count; $i++) {
                        for ($title = 0; $title < count($titles); $title ++) {
                            $new_data[$i][] = $data[$title][$i];
                        }
                    }

                    $csv_data = array_merge([$titles], $new_data);

                    $filename = time() . '.csv';

                    $this->create_csv_file($csv_data, $filename);
                }
            }

            return $this->view->pick('index/smsorders');
        }

        public function smsGetOrderAction()
        {
            $catalog = new BmCatalog();
            $order_id = (int) $this->request->getPost('order_id', 'int', false);
            $sms_order = $catalog->getSMSOrders($order_id);
            echo json_encode($sms_order[0]);
            exit();
        }

        public function smsSetOrderAction()
        {
            $catalog = new BmCatalog();
            $order_id = (int) $this->request->getPost('sms_order', 'int', false);
            if($order_id > 0)
                $sms_order = $catalog->getSMSOrders($order_id)[0];

            $name = $this->request->getPost('name', array('trim', 'string'), false);
            $text = $this->request->getPost('text', array('trim', 'string'), false);

            if($sms_order)
                $catalog->updateSMSOrderData($order_id, $name, $text);
            else
                $catalog->setSMSOrderData($name, $text);

            $this->response->redirect($this->siteUrl . '/workz/users/sms-orders');
            $this->response->send();
            return;
        }

        public function smsDeleteOrderAction()
        {
            $catalog = new BmCatalog();
            $order_id = (int) $this->request->getPost('sms_order', 'int', false);
            $catalog->deleteSMSOrder($order_id);
            $this->response->redirect($this->siteUrl . '/workz/users/sms-orders');
            $this->response->send();
            return;
        }

        public function sendMSG($phone, $msg)
        {

            // If need set test mode uncommented

            if($_SERVER['SERVER_ADDR'] == "127.0.0.1") {
                return true;
            }

            $client = new \SoapClient('http://turbosms.in.ua/api/wsdl.html');

            $auth = [
                'login' => 'olegwww',
                'password' => 'budsms211'
            ];

            // Авторизируемся на сервере   
            $result = $client->Auth($auth);

            $result = $client->GetCreditBalance();

            if((float) $result->GetCreditBalanceResult > 0.5) {

                if(is_array($phone)) {
                    foreach ($phone as $key => $value) {
                        $phones .= '+38' . $this->clearPhone($value) . ',';
                    }
                    $phones = substr($phones, 0, -1);
                } else {
                    $phones = '+38' . $phone;
                    $phone = explode(';', $phone);
                }

                $sms = [
                    'sender' => 'Budmisto',
                    'destination' => $phones,
                    'text' => $msg
                ];

                $result = $client->SendSMS($sms);

                if($result->SendSMSResult->ResultArray[0] == "Сообщения успешно отправлены") {

                    if(count($result->SendSMSResult->ResultArray) - 1 == count($phone)) {
                        foreach ($result->SendSMSResult->ResultArray as $key => $value) {

                            if($key == 0)
                                continue;

                            if(preg_match("/([а-яА-Я]+)/", $value)) {
                                $this->setMsgError($phone[$key - 1]);
                            }
                        }
                    }

                    return true;
                } else {
                    return false;
                }
            }
        }

        public function setMsgError($phone)
        {
            $us_id = $this->admin->checkPhone($phone);
            $this->admin->setSMSError($us_id);
        }

        public function clearPhone($phone)
        {
            return str_replace(["(", ")", " ", "_", "-"], "", $phone);
        }

        function create_csv_file($create_data, $file = null, $col_delimiter = ';', $row_delimiter = "\r\n")
        {

            if(!is_array($create_data))
                return false;

            if($file && !is_dir(dirname($file)))
                return false;

            // строка, которая будет записана в csv файл
            $collected_rows = array();

            // перебираем все данные
            foreach ($create_data as $row) {
                $cols = array();

                foreach ($row as $col_val) {
                    // строки должны быть в кавычках ""
                    // кавычки " внутри строк нужно предварить такой же кавычкой "
                    if($col_val && preg_match('/[",;\r\n]/', $col_val)) {
                        // поправим перенос строки
                        if($row_delimiter === "\r\n") {
                            $col_val = str_replace("\r\n", '\n', $col_val);
                            $col_val = str_replace("\r", '', $col_val);
                        } elseif($row_delimiter === "\n") {
                            $col_val = str_replace("\n", '\r', $col_val);
                            $col_val = str_replace("\r\r", '\r', $col_val);
                        }

                        $col_val = str_replace('"', '""', $col_val); // предваряем "
                        $col_val = '"' . $col_val . '"'; // обрамляем в "
                    }

                    $cols[] = $col_val; // добавляем колонку в данные
                }

                $collected_rows[] = implode($col_delimiter, $cols); // добавляем строку в данные
            }

            $CSV_str = implode($row_delimiter, $collected_rows); // объединяем строки
            // задаем кодировку windows-1251 для строки
            $CSV_str = iconv("UTF-8", "cp1251", $CSV_str);

            if($file) {

                $file = 'export.csv';

                file_put_contents(APP_DIR . 'html/' . $file, $CSV_str);

                if(ob_get_level()) {
                    ob_end_clean();
                }

                // заставляем браузер показать окно сохранения файла                
                header('Content-Type: application/csv');
                header('Content-Disposition: attachment; filename= export.csv');
                header('Content-Length: ' . filesize(APP_DIR . 'html/' . $file));
                // читаем файл и отправляем его пользователю
                readfile(APP_DIR . 'html/' . $file);
                exit();
            }

            return false;
        }

        public function usersMarkedAction()
        {
            $tmp = $this->request->get('page', 'int');
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            $users = $this->admin->getUsersMarkedList($page, 30);
            $this->view->setVar('users', $users);

            $count = $this->admin->countUsersMarked();
            $links_html = $this->createPaginator($count, $page, 30, $this->siteUrl . '/workz/users');
            $this->view->setVar('pager_html', $links_html);
        }

        public function usersCommentedAction()
        {
            $tmp = $this->request->get('page', 'int');
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            $users = $this->admin->getUsersCommentedList($page, 30);
            $this->view->setVar('users', $users);

            $count = $this->admin->countUsersCommented();
            $links_html = $this->createPaginator($count, $page, 30, $this->siteUrl . '/workz/users');
            $this->view->setVar('pager_html', $links_html);
        }

        public function userBlockAction()
        {
            $this->view->disable();
            $user_id = $this->dispatcher->getParam('id');
            if(!$user_id)
                die;

            $this->admin->blockUser($user_id);
            $this->response->redirect($this->siteUrl . '/workz/users/view/' . $user_id);
            $this->response->send();
            die;
        }

        public function dberrorAction()
        {
            $this->response->setStatusCode(404, 'Not Found');
        }

        public function route404Action()
        {
            $this->response->setStatusCode(404, 'Not Found');
        }

        public function route403Action()
        {
            $this->view->enable();
            $this->response->setStatusCode(403, 'Forbidden');
            $return = $this->request->getHTTPReferer();
            $return = $return ? $return : '/';
            $this->view->setVar('return', $return);
        }

        public function changeRoleAction()
        {
            $this->view->disable();
            if($this->request->isPost() && $this->request->isAjax()) {
                $role = $this->request->getPost('role', 'int', false);
                $user_id = $this->request->getPost('user_id', 'int', false);
                if($role && $user_id) {
                    $result = $this->admin->changeRole($role, $user_id);
                    if($result) {
                        $notify = $this->request->getPost('notify', 'int', false);
                        if($notify) {
                            $user = new BmUsers();
                            $email = $user->getUserInfo($user_id)['email'];
                            $mail = new BMail('common', 'info');
                            $mail->setOptions($this->siteUrl);
                            $data['title'] = 'Изменение роли';
                            $data['message'] = 'Ваша роль изменена.';
                            $mail->send('Изменение роли', $email, $data);
                        }
                        // NEED REFACT
                        echo 'AllOk';
                    }
                }
            }
        }

        public function emailExistAction()
        {
            $this->view->disable();
            if($this->request->isPost() && $this->request->isAjax()) {
                $email = $this->request->getPost('mail', 'email', false);
                $type = $this->request->getPost('type', array('trim', 'string'), false);
                if($email) {
                    $result = $this->admin->checkEmail($email, $type);
                    if($result)
                        echo $result;
                    else
                        echo 'AllBad';
                } else
                    echo 'AllBad';
            } else
                echo 'AllBad';
        }

        public function phoneExistAction()
        {
            $this->view->disable();
            if($this->request->isPost() && $this->request->isAjax()) {
                $phone = $this->request->getPost('phone', 'string', false);
                $type = $this->request->getPost('type', array('trim', 'string'), false);
                if($phone) {
                    $result = $this->admin->checkPhone($phone, $type);
                    if($result)
                        echo $result;
                    else
                        echo 'AllBad';
                } else
                    echo 'AllBad';
            } else
                echo 'AllBad';
        }

        public function cityCatalogAction()
        {
            $common = new Common();
            $top_cities = $common->getCatCities();
            $this->view->setVar('top_cities', $top_cities);
            $sub_cities = $common->getCatDistricts(); //var_dump($sub_cities); die;
            $this->view->setVar('sub_cities', $sub_cities);
        }

        public function unchainedCityAction()
        {
            $res = $this->admin->getUnchainedCity();
            $this->view->setVar('list', $res);
            $res = $this->admin->getRegions();
            $this->view->setVar('regions', $res);
        }

        /*    public function correctionAction()
          {
          $city_id = $this->dispatcher->getParam('id');
          if (!$city_id) die;

          $common = new Common();
          if ($this->request->isPost() && $this->security->checkToken()) {
          $name = $this->request->getPost('name', array('trim','string'), false);
          $region = $this->request->getPost('region', 'int', false);
          $district = $this->request->getPost('district', 'int', false);
          if ($name && $region && $district) {
          $res = $this->admin->correctCity($city_id, $name, $region, $district);
          if ($res) {
          $this->response->redirect($this->siteUrl.'/workz/city/unchained');
          $this->response->send();
          die;
          }
          }
          }

          $city = $common->getPlaceById($city_id);
          $this->view->setVar('city', $city);
          $this->view->setVar('map_key', $this->config->application->map_key);
          $regions = $this->admin->getRegions();
          $this->view->setVar('regions', $regions);
          } */

        public function setChainCityAction()
        {
            $this->view->disable();
            $id = $this->request->get('id', 'int', false);
            $did = $this->request->get('did', 'int', false);
            if($id && $did)
                $this->admin->setChainCity($id, $did);
            $this->response->redirect($this->siteUrl . '/workz/city/unchained');
            $this->response->send();
            die;
        }

        /*    public function citySearchAction()
          {
          $region = $this->request->get('region', 'int', false);
          $district = $this->request->get('district', 'int', false);
          if ($region && $district) {
          $list = $this->admin->getCities($region, $district);
          $this->view->setVar('list', $list);
          if (!$list)
          $this->view->setVar('error', true);
          $this->view->setVar('region_id', $region);
          $this->view->setVar('district_id', $district);
          $districts = $this->admin->getDistricts($region);
          $this->view->setVar('districts', $districts);
          }
          $regions = $this->admin->getRegions();
          $this->view->setVar('regions', $regions);
          }

          public function districtsAction()
          {
          $this->view->disable();
          if ($this->request->isAjax()) {
          $region_id = $this->request->get('region', 'int', false);
          $tmp = array();
          $text = 'Выберите район';
          echo '{';
          if ($region_id) {
          $sub_list = $this->admin->getDistricts($region_id);
          if ($sub_list) {
          $tmp[] = '"0" : "-- ' . $text . ' --"';
          foreach($sub_list as $sl)
          $tmp[] = '"'.$sl['id'].'" : "'.$sl['district_name'].'"';
          echo implode(', ', $tmp);
          }
          }
          if (empty($tmp)) {
          $tmp[] = '"" : "-- ' . $text . ' --"';
          echo implode(', ', $tmp);
          }
          echo '}';
          }
          } */

        public function settingsAction()
        {
            if($this->request->isPost()) {// && $this->request->isAjax()) {
                $passwdold = $this->request->getPost('passwdold', array('trim', 'string'), false);
                $passwdnew = $this->request->getPost('passwd1', array('trim', 'string'), false);
                $result = $this->admin->changePasswd($this->user_id, $passwdold, $passwdnew);
                if(!$result) {
                    $this->view->setVar('res', false);
                } else {
                    $this->view->setVar('res', true);
                }
            }
        }

        public function setRegionAction()
        {
            $order_id = $this->request->getPost('order_id', 'int', false);
            $region_id = $this->request->getPost('region', 'int', false);
            $district_id = $this->request->getPost('district', 'int', false);
            if($order_id && $region_id) {
                $this->admin->setRegion($order_id, $region_id, $district_id);
            }
            $this->response->redirect($this->siteUrl . '/workz/orders/view/' . $order_id);
            $this->response->send();
            die;
        }

        public function specListAction()
        {
            $catalog = new BmCatalog();
            $top_catalog = $catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top_catalog);
            $sub_catalog = $this->admin->getSubSpecList();
            $this->view->setVar('sub_catalog', $sub_catalog);
        }

        public function specSubEditAction()
        {
            $id = $this->dispatcher->getParam('id');
            if(!$id)
                die;

            if($this->request->isPost() && $this->security->checkToken()) {
                $speciality = $this->request->getPost('speciality', 'int', false);
                $name = $this->request->getPost('name', array('trim', 'string'), false);
                $slug = $this->request->getPost('slug', array('trim', 'string'), false);
                $desc = $this->request->getPost('desc', array('trim', 'string'), false);
                if($name && $speciality && $slug) {
                    $res = $this->admin->updateSpec2($id, $name, $speciality, $slug, $desc);
                    if($res) {
                        $this->response->redirect($this->siteUrl . '/workz/spec/list');
                        $this->response->send();
                        die;
                    }
                }
            }

            $spec = $this->admin->getSpec2($id);
            if(!$spec)
                die;
            $this->view->setVar('spec', $spec);

            $catalog = new BmCatalog();
            $top = $catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);
            $spec_ref = $this->admin->getSpecRef($id);
            $this->view->setVar('spec_ref', $spec_ref);
        }

        public function specRefAddAction()
        {
            if($this->request->isPost() && $this->security->checkToken()) {
                $spec_id = $this->request->getPost('id', 'int', false);
                $speciality = $this->request->getPost('speciality', 'int', false);
                $subspeciality = $this->request->getPost('subspeciality', 'int', false);
                if($spec_id && $speciality && $subspeciality) {
                    $this->admin->specAddRef($spec_id, $speciality, $subspeciality);
                }
            }
            $this->response->redirect($this->siteUrl . '/workz/spec/subedit/' . $spec_id);
            $this->response->send();
            die;
        }

        public function specRefDelAction()
        {
            $id = $this->dispatcher->getParam('id');
            if(!$id)
                die;
            $this->admin->specDelRef($id);
            $back = $this->request->getHTTPReferer();
            $this->response->redirect($back);
            $this->response->send();
            die;
        }

        public function specTopEditAction()
        {
            $id = $this->dispatcher->getParam('id');
            if(!$id)
                die;

            if($this->request->isPost() && $this->security->checkToken()) {
                $name = $this->request->getPost('name', array('trim', 'string'), false);
                $slug = $this->request->getPost('slug', array('trim', 'string'), false);
                $desc = $this->request->getPost('desc', array('trim', 'string'), false);
                if($name && $slug) {
                    $res = $this->admin->updateSpec($id, $name, $slug, $desc);
                    if($res) {
                        $this->response->redirect($this->siteUrl . '/workz/spec/list');
                        $this->response->send();
                        die;
                    }
                }
            }

            $spec = $this->admin->getSpec($id);
            if(!$spec)
                die;
            $this->view->setVar('spec', $spec);
        }

        public function specAddAction()
        {
            if($this->request->isPost() && $this->security->checkToken()) {
                $speciality = $this->request->getPost('speciality', 'int', false);
                $name = $this->request->getPost('name', array('trim', 'string'), false);
                $slug = $this->request->getPost('slug', array('trim', 'string'), false);
                if($name && $slug) {
                    $res = $this->admin->addSpec($name, $speciality, $slug);
                    if($res) {
                        $this->response->redirect($this->siteUrl . '/workz/spec/list');
                        $this->response->send();
                        die;
                    }
                }
            }
            $catalog = new BmCatalog();
            $top = $catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);
        }

        public function shopListAction()
        {
            $catalog = new BmCatalog();
            if($this->request->isPost() && $this->security->checkToken()) {
                $name = $this->request->getPost('name', array('trim', 'string'), false);
                $desc = $this->request->getPost('desc', array('trim', 'string'), false);
                $admininfo = $this->request->getPost('admininfo', array('trim', 'string'), false);
                $speciality = $this->request->getPost('speciality', 'int', false);
                $data['placelat'] = $this->request->getPost('placelat', 'float', false);
                $data['placelng'] = $this->request->getPost('placelng', 'float', false);
                $distance = $this->request->getPost('distance', 'int', false);


                if($name || $desc || $speciality and ! $data['placelat']) {
                    $result = $this->admin->searchShopList($name, $desc, $speciality, 0, 20);
                    $this->view->setVar('shops', $result);
                    if(!$result)
                        $this->view->setVar('error', true);
                } elseif($data['placelat'] && $data['placelng']) {

                    $result = $this->admin->searchShopListByCoords($data, 0, 20, $distance);
                    $this->view->setVar('shops', $result);
                    if(!$result)
                        $this->view->setVar('error', true);
                } elseif($admininfo) {
                    $result = $this->admin->searchShopListByNote($admininfo, 0, 20);
                    $this->view->setVar('shops', $result);
                    if(!$result)
                        $this->view->setVar('error', true);
                }
            } else {
                $tmp = $this->request->get('page', 'int');
                $page = $tmp ? $tmp : 0;
                $this->view->setVar('page', $page);

                $shops = $catalog->getShops($page, 20);
                $this->view->setVar('shops', $shops);
                $this->view->setVar('newshops', true);

                $count = $catalog->countShops();
                $links_html = $this->createPaginator($count, $page, 20, $this->siteUrl . '/workz/shops/list');
                $this->view->setVar('pager_html', $links_html);
                $this->view->setVar('s_count', $count);
            }
            $this->view->setVar('map_key', $this->config->application->map_key);
            $top = $catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);
        }

        public function shopAddAction()
        {
            if($this->request->isPost() && $this->security->checkToken()) {
                $name = $this->request->getPost('name', array('trim', 'string'), false);
                $passwd = $this->request->getPost('passwd', array('trim', 'string'), false);
                $chief = $this->request->getPost('chief', array('trim', 'string'), false);
                $admininfo = $this->request->getPost('admininfo', array('trim', 'string'), false);
                $phone1 = $this->request->getPost('phone1', array('trim', 'string'), false);
                $phone2 = $this->request->getPost('phone2', array('trim', 'string'), false);
                $email1 = $this->request->getPost('email1', array('trim', 'email'), false);
                $email2 = $this->request->getPost('email2', array('trim', 'email'), false);
                $web = $this->request->getPost('web', array('trim', 'string'), false);
                $placeid = $this->request->getPost('placeid', array('trim', 'string'), false);
                $speciality = $this->request->getPost('speciality', 'int', false);
                $desc = $this->request->getPost('desc', array('trim', 'string'), false);
                $network = $this->request->getPost('network', 'int', 0);
                $brands = $this->request->getPost('brands', array('trim', 'string'), '');

                if(!$name || !$passwd || !$phone1 || !$email1 || !$placeid || !$speciality) {
                    $this->response->redirect($this->siteUrl . '/workz/shops/add');
                    $this->response->send();
                    die;
                }


                $data['placelat'] = $this->request->getPost('placelat', 'float', false);
                $data['placelng'] = $this->request->getPost('placelng', 'float', false);
                $data['placeaddr'] = $this->request->getPost('placeaddr', array('trim', 'string'), false);
                $data['placetown'] = $this->request->getPost('placetown', array('trim', 'string'), false);
                $data['placedistrict2'] = $this->request->getPost('placedistrict2', array('trim', 'string'), false);
                $data['placedistrict3'] = $this->request->getPost('placedistrict3', array('trim', 'string'), false);
                $data['placeregion'] = $this->request->getPost('placeregion', array('trim', 'string'), false);
                $data['placecountry'] = $this->request->getPost('placecountry', array('trim', 'string'), false);
                $data['placeroute'] = $this->request->getPost('placeroute', array('trim', 'string'), false);
                $data['placextra'] = $this->request->getPost('placextra', array('trim', 'string'), false);
                $data['budnum'] = $this->request->getPost('budnum', array('trim', 'string'), false);

                $users = new BmUsers();
                $sd = array();
                $sd['name'] = $name;
                $sd['passwd'] = $passwd;
                $sd['chief'] = $chief;
                $sd['phone'] = $phone1;
                $sd['phone1'] = $phone1;
                $sd['phone2'] = $phone2;
                $sd['email'] = $email1;
                $sd['email2'] = $email2;
                $sd['website'] = $web;
                $trimmedArray = array_map('trim', array($data['placeroute'] . ' ' . $data['budnum'], $data['placetown'], $data['placedistrict2'], $data['placeregion']));
                $sd['address'] = implode(', ', array_filter($trimmedArray));
                $sd['speciality'] = $speciality;
                $sd['desc'] = $desc;
                $sd['net_id'] = $network;
                $sd['brands'] = $brands;
                if($sd['desc']) {
                    $filter = $this->getInputFilter();
                    $sd['text'] = $this->textengine->convertBB2Html($filter->sanitize($sd['desc'], 'user_input'));
                    $sd['brief'] = $this->textengine->getBrief($sd['text'], $this->config->lists->big_brief);
                } else {
                    $sd['text'] = '';
                    $sd['brief'] = '';
                }
                $shop_id = $users->addShop($sd);

                if(!$shop_id) {
                    $this->response->redirect($this->siteUrl . '/workz/shops/add');
                    $this->response->send();
                    die;
                }

                if($admininfo)
                    $this->admin->setMark($shop_id, $admininfo, false);

                if($placeid) {
                    $data['placeid'] = $placeid;

                    if($data['placetown'] && $data['placelat'] && $data['placelng'] && $data['placeid'] && $data['placeregion'] && $data['placecountry']) {
                        $common = new Common();

                        if($data['placeroute']) {
                            $key = $this->config->application->server_key;
                            $ch = curl_init();
                            $url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query=' . urlencode($data['placetown'] . ',' . $data['placeregion']);
                            $url .= '&key=' . $key . '&language=ru';
                            $cert = APP_DIR . 'var/cacerts.pem';

                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HEADER, false);
                            curl_setopt($ch, CURLOPT_CAINFO, $cert);

                            session_write_close();
                            $responce = curl_exec($ch);
                            session_start();
                            curl_close($ch);

                            $res = json_decode($responce);
                            $d['placetown'] = $res->results[0]->name;
                            $d['placeid'] = $res->results[0]->place_id;
                            $d['placelat'] = $res->results[0]->geometry->location->lat;
                            $d['placelng'] = $res->results[0]->geometry->location->lng;
                            $d['placeregion'] = $data['placeregion'];
                            $common->addNewPlace($d);
                        } else
                            $common->addNewPlace($data);

                        $common->addShopPlace($shop_id, $data);
                    }
                }
                $this->response->redirect($this->siteUrl . '/workz/shops/list');
                $this->response->send();
                die;
            }

            $catalog = new BmCatalog();
            $top = $catalog->getTopSpecList();
            $networks = $catalog->getShopNetworks();
            $this->view->setVar('top_catalog', $top);
            $this->view->setVar('networks', $networks);
            $this->view->setVar('map_key', $this->config->application->map_key);
        }

        public function commercialInterestedAction()
        {
            $commercial_id = $this->dispatcher->getParam('id');

            $users = BmAdmin::getCommercialInterestedUsers($commercial_id);

            $this->view->setVar('users', $users);
            return $this->view->pick('index/_commercial_interested_list');
            exit();
        }

        public function shopEditAction()
        {

            $shop_id = $this->dispatcher->getParam('id');
            if(!$shop_id)
                die;

            $users = new BmUsers();
            $shop = $users->getShop($shop_id);
            if(!$shop)
                die;

            if($this->request->isAjax()) {

                $comercial_id = $this->request->getPost('comercial_id', 'int', false);

                $comercial = $users->getSingleComercial($comercial_id);

                print_r(json_encode($comercial));
                exit();
            }

            if($this->request->isPost() && $this->security->checkToken()) {
                $form_type = $this->request->getPost('form_type', 'int', false);

                if($form_type == 1) {
                    $name = $this->request->getPost('name', array('trim', 'string'), false);
                    $passwd = $this->request->getPost('passwd', array('trim', 'string'), false);
                    $chief = $this->request->getPost('chief', array('trim', 'string'), false);
                    $admininfo = $this->request->getPost('admininfo', array('trim', 'string'), false);
                    $phone1 = $this->request->getPost('phone1', array('trim', 'string'), false);
                    $phone2 = $this->request->getPost('phone2', array('trim', 'string'), false);
                    $email1 = $this->request->getPost('email1', array('trim', 'email'), false);
                    $email2 = $this->request->getPost('email2', array('trim', 'email'), false);
                    $web = $this->request->getPost('web', array('trim', 'string'), false);
                    $placeid = $this->request->getPost('placeid', array('trim', 'string'), false);
                    $tid = $this->request->getPost('speciality', 'int', false);
                    $sid = $this->request->getPost('subspeciality', 'int', false);
                    $desc = $this->request->getPost('desc', array('trim', 'string'), false);
                    $network = $this->request->getPost('network', 'int', 0);
                    $brands = $this->request->getPost('brands', array('trim', 'string'), '');

                    if(!$name || !$email1) {
                        $this->response->redirect($this->siteUrl . '/workz/shops/edit/' . $shop_id);
                        $this->response->send();
                        die;
                    }

                    $data['placelat'] = $this->request->getPost('placelat', 'float', false);
                    $data['placelng'] = $this->request->getPost('placelng', 'float', false);
                    $data['placeaddr'] = $this->request->getPost('placeaddr', array('trim', 'string'), false);
                    $data['placetown'] = $this->request->getPost('placetown', array('trim', 'string'), false);
                    $data['placedistrict2'] = $this->request->getPost('placedistrict2', array('trim', 'string'), false);
                    $data['placedistrict3'] = $this->request->getPost('placedistrict3', array('trim', 'string'), false);
                    $data['placeregion'] = $this->request->getPost('placeregion', array('trim', 'string'), false);
                    $data['placecountry'] = $this->request->getPost('placecountry', array('trim', 'string'), false);
                    $data['placeroute'] = $this->request->getPost('placeroute', array('trim', 'string'), false);
                    $data['placextra'] = $this->request->getPost('placextra', array('trim', 'string'), false);
                    $data['budnum'] = $this->request->getPost('budnum', array('trim', 'string'), false);

                    $sd = array();
                    $sd['name'] = $name;
                    $sd['passwd'] = $passwd;
                    $sd['chief'] = $chief;
                    $sd['phone'] = $phone1;
                    $sd['phone1'] = $phone1;
                    $sd['phone2'] = $phone2;
                    $sd['email'] = $email1;
                    $sd['email2'] = $email2;
                    $sd['website'] = $web;
                    $trimmedArray = array_map('trim', array($data['placeroute'] . ' ' . $data['budnum'], $data['placetown'], $data['placedistrict2'], $data['placeregion']));
                    $sd['address'] = implode(', ', array_filter($trimmedArray));
                    $sd['tid'] = $tid;
                    $sd['sid'] = $sid;
                    $sd['desc'] = $desc;
                    $sd['net_id'] = $network;
                    $sd['brands'] = $brands;
                    if($sd['desc']) {
                        $filter = $this->getInputFilter();
                        $sd['text'] = $this->textengine->convertBB2Html($filter->sanitize($sd['desc'], 'user_input'));
                        $sd['brief'] = $this->textengine->getBrief($sd['text'], $this->config->lists->big_brief);
                    } else {
                        $sd['text'] = '';
                        $sd['brief'] = '';
                    }

                    $shop_id = $users->editShop($sd, $shop_id);

                    if(!$shop_id) {
                        $this->response->redirect($this->siteUrl . '/workz/shops/edit/' . $shop_id);
                        $this->response->send();
                        die;
                    }

                    if($admininfo)
                        $this->admin->setMark($shop_id, $admininfo, false);

                    if($placeid) {
                        $data['placeid'] = $placeid;

                        if($data['placetown'] && $data['placelat'] && $data['placelng'] && $data['placeid'] && $data['placeregion'] && $data['placecountry']) {
                            $common = new Common();

                            if($data['placeroute']) {
                                $key = $this->config->application->server_key;
                                $ch = curl_init();
                                $url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query=' . urlencode($data['placetown'] . ',' . $data['placeregion']);
                                $url .= '&key=' . $key . '&language=ru';
                                $cert = APP_DIR . 'var/cacerts.pem';

                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_HEADER, false);
                                curl_setopt($ch, CURLOPT_CAINFO, $cert);

                                session_write_close();
                                $responce = curl_exec($ch);
                                session_start();
                                curl_close($ch);

                                $res = json_decode($responce);
                                $d['placetown'] = $res->results[0]->name;
                                $d['placeid'] = $res->results[0]->place_id;
                                $d['placelat'] = $res->results[0]->geometry->location->lat;
                                $d['placelng'] = $res->results[0]->geometry->location->lng;
                                $d['placeregion'] = $data['placeregion'];
                                $common->addNewPlace($d);
                            } else
                                $common->addNewPlace($data);

                            $common->addShopPlace($shop_id, $data);
                        }
                    }
                    $this->response->redirect($this->siteUrl . '/workz/shops/edit/' . $shop_id);
                    $this->response->send();
                    die;
                }elseif($form_type == 2) {

                    $data['comercial_id'] = $this->request->getPost('comercial_id', 'int', false);
                    if((int) $data['comercial_id'] < 1)
                        $data['comercial_id'] = 0;

                    $data['comercial_name'] = $this->request->getPost('comercial_name', array('trim', 'string'), false);
                    $data['comercial_active'] = ($this->request->getPost('comercial_active', array('trim', 'string'), false) == "on") ? '1' : '0';
                    $data['comercial_descr'] = $this->textengine->convertBB2Html($_POST['comercial_descr']);
                    if(count($_POST['user_role']) > 0) {
                        foreach ($_POST['user_role'] as $k => $v) {
                            $data['user_role'] .= $v;
                        }
                    } else {
                        $data['user_role'] = '';
                    }

                    if($_FILES['comercial_image']['name']) {
                        $time = time();
                        // Каталог, в который мы будем принимать файл:
                        $uploaddir = './../html/images/comercial_image/' . $time;
                        $uploadfile = $uploaddir . basename($_FILES['comercial_image']['name']);

                        // Копируем файл из каталога для временного хранения файлов:
                        if(copy($_FILES['comercial_image']['tmp_name'], $uploadfile)) {
                            $data['comercial_image'] = $time . $_FILES['comercial_image']['name'];
                        }
                    }

                    foreach ($_POST['comercial_main_spec'] as $key => $value) {
                        $data['wrk_type_id'] .= "{$value}";
                    }
                    foreach ($_POST['comercial_sub_spec'] as $key => $value) {
                        $data['wrk_spec_id'] .= "{$value}";
                    }
                    $data['shop_id'] = $shop_id;

                    $users->update_comercial($data);
                }
            }


            $shop_places = $users->getShopPlaces($shop_id);
            $shop_specs = $users->getShopSpecs($shop_id);

            if($shop['description'])
                $shop['description'] = $this->textengine->convertHtml2BB($shop['description']);
            $this->view->setVar('shop', $shop);
            $this->view->setVar('shop_places', $shop_places);
            $this->view->setVar('shop_specs', $shop_specs);

            $catalog = new BmCatalog();
            $top = $catalog->getTopSpecList();
            $networks = $catalog->getShopNetworks();
            $this->view->setVar('top_catalog', $top);
            $this->view->setVar('networks', $networks);
            $this->view->setVar('map_key', $this->config->application->map_key);

            $admininfo = $this->admin->getMark($shop_id);
            $this->view->setVar('admininfo', $admininfo);

            $main_spec = \Budmisto\Modules\Front\Models\Rotator::get_main_spec();
            $sub_spec = \Budmisto\Modules\Front\Models\Rotator::get_sub_spec();

            $this->view->setVar('main_spec', $main_spec);
            $this->view->setVar('sub_spec', $sub_spec);

            $shop_commercial = $users->getComercials($shop_id);

            $this->view->setVar('shop_commercial', $shop_commercial);
        }

        public function comercial_deleteAction()
        {
            $id = $this->dispatcher->getParam('id');
            $shop_id = $this->dispatcher->getParam('shop_id');
            $users = new BmUsers();
            $users->deleteComercials($id);

            $this->response->redirect($this->siteUrl . '/workz/shops/edit/' . $shop_id);
            $this->response->send();
        }

        public function shopDelSpecAction()
        {
            $this->view->disable();
            $sid = $this->request->get('sid', 'int', false);

            $shop_id = $this->request->get('shop', 'int', false);
            if($sid && $shop_id) {
                $users = new BmUsers();
                $users->shopDelSpec($sid, $shop_id);
            }
            $this->response->redirect($this->siteUrl . '/workz/shops/edit/' . $shop_id);
            $this->response->send();
            die;
        }

        public function shopDelPlaceAction()
        {
            $this->view->disable();
            $id = $this->request->get('id', 'int', false);
            $shop_id = $this->request->get('shop', 'int', false);
            if($id && $shop_id) {
                $users = new BmUsers();
                $users->shopDelPlace($id, $shop_id);
            }
            $this->response->redirect($this->siteUrl . '/workz/shops/edit/' . $shop_id);
            $this->response->send();
            die;
        }

        public function shopNetAction()
        {
            $tmp = $this->request->get('page', 'int');
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            $catalog = new BmCatalog();
            $networks = $catalog->getShopNetworks();
            $this->view->setVar('networks', $networks);

            $count = $catalog->countShopsNets();
            $links_html = $this->createPaginator($count, $page, 30, $this->siteUrl . '/workz/shops/network');
            $this->view->setVar('pager_html', $links_html);
            $this->view->setVar('s_count', $count);
        }

        public function shopNetAddAction()
        {
            if($this->request->isPost() && $this->security->checkToken()) {
                $name = $this->request->getPost('name', array('trim', 'string'), false);
                if(!$name)
                    die;
                $slug = $this->textengine->translit($name);
                $this->admin->addShopNet($name, $slug);
                $this->response->redirect($this->siteUrl . '/workz/shops/network');
                $this->response->send();
                die;
            }
        }

        public function shopNetEditAction()
        {
            $net_id = $this->dispatcher->getParam('id');
            if(!$net_id)
                die;
            if($this->request->isPost() && $this->security->checkToken()) {
                $name = $this->request->getPost('name', array('trim', 'string'), false);
                if(!$name)
                    die;
                $slug = $this->textengine->translit($name);
                $this->admin->editShopNet($net_id, $name, $slug);
                $this->response->redirect($this->siteUrl . '/workz/shops/network');
                $this->response->send();
                die;
            }
            $net = $this->admin->getShopNetwork($net_id);
            $this->view->setVar('net', $net);
        }

        public function shopDeleteAction()
        {
            $shop_id = $this->dispatcher->getParam('id');
            if(!$shop_id)
                die;
            $this->admin->deleteShop($shop_id);
            $this->response->redirect($this->siteUrl . '/workz/shops/list');
            $this->response->send();
            die;
        }

        public function leftoversAction()
        {
            $limit = 30;
            $lo = new BmLO();

            $tmp = $this->request->get('page', 'int');
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            $count = $lo->countLO();
            $this->view->setVar('count', $count);
            $this->view->setVar('limit', $limit);

            $links_html = $this->createPaginator($count, $page, $limit, $this->siteUrl . '/workz/leftovers');
            $this->view->setVar('pager_html', $links_html);

            $leftovers = $lo->getLoList($page, $limit);
            $this->view->setVar('leftovers', $leftovers);
        }

        public function leftoversDeleteAction()
        {
            $post_id = $this->dispatcher->getParam('id');
            if(!$post_id)
                die;
            $this->admin->leftoverDel($post_id);
            $back = $this->request->getHTTPReferer();
            $this->response->redirect($back);
            $this->response->send();
            die;
        }

        public function leftoversCloseAction()
        {
            $post_id = $this->dispatcher->getParam('id');
            if(!$post_id)
                die;
            $this->admin->leftoverClose($post_id);
            $back = $this->request->getHTTPReferer();
            $this->response->redirect($back);
            $this->response->send();
            die;
        }

    }
    