<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/admin/controllers/BaseController.php
     *
     * Base controller
     *
     */

    namespace Budmisto\Modules\Admin\Controllers;

    use Phalcon\Mvc\Dispatcher;
    use Budmisto\Modules\Admin\Models\Admin as BmAdmin;

    class ControllerBase extends \Phalcon\Mvc\Controller
    {

        public $siteUrl;
        public $actionName;
        public $myCache;
        public $user_level;
        public $user_id;

        public function onConstruct()
        {
            $this->siteUrl = $this->config->application->site_url;
            $this->user_level = $this->session->get('usr_level');
        }

        public function beforeExecuteRoute(Dispatcher $dispatcher)
        {
            $path = $dispatcher->getActionName();

            if($this->user_level && $this->user_level > 2) {
                $admin = new BmAdmin();
                $admin->signoutAdmin();
                $this->session->destroy();
                $this->response->redirect($this->siteUrl . '/workz');
                $this->response->send();
                die;
            }
            if(!$this->user_level && $path != 'index') {
                $this->response->redirect($this->siteUrl . '/workz');
                $this->response->send();
                die;
            }
        }

        public function initialize()
        {
            $this->view->setVar('url', $this->siteUrl);
            $this->view->setVar('is_logged', false);

            if($this->user_level) {
                $this->user_id = $this->session->get('usr_id');
                $this->view->setVar('usr_id', $this->user_id);
                $name = $this->session->get('usr_name');
                $this->view->setVar('usr_name', $name);
                $this->view->setVar('usr_level', $this->user_level);
                $this->view->setVar('is_logged', true);
            }
        }

        public function getInputFilter()
        {
            $filter = new \Phalcon\Filter();
            $filter->add('user_input', function($value)
            {
                $value = html_entity_decode($value, ENT_QUOTES, 'UTF-8');
                return htmlentities($value, ENT_QUOTES, 'UTF-8');
            });
            return $filter;
        }

        protected function checkModuleAccess($module)
        {
            $access = unserialize($this->session->get('access'));
            if(!$access) {
                $this->response->redirect($this->siteUrl . '/403'); // control redirect bug in beforeExecuteRoute
                $this->response->send();
                die;
            }
            return in_array($module, $access) ? true : false;
        }

        public function createPaginator($quantity, $current, $limit, $url)
        {
            if($quantity == 0)
                return;
            $current = (!$current) ? $current + 1 : $current;
            $list_class = "pagination pagination-lg";
            $links = 3;

            //var_dump($quantity, $current, $limit, $url); die;
            $last = ceil($quantity / $limit);
            $start = ( ( $current - $links ) > 0 ) ? $current - $links : 1;
            $end = ( ( $current + $links ) < $last ) ? $current + $links : $last;
            $html = '<ul class="' . $list_class . '">';
            $class = ( $current == 1 ) ? "disabled" : "";
            $prev = ( ($current - 1) <= 1) ? $url : $url . '?page=' . ( $current - 1 );
            $html .= '<li class="' . $class . '"><a href="' . $prev . '">&laquo;</a></li>';

            if($start > 1) {
                $html .= '<li><a href="' . $url . '">1</a></li>';
                $html .= '<li class="disabled"><span>...</span></li>';
            }

            for ($i = $start; $i <= $end; $i++) {
                $class = ( $current - 1 == $i - 1 ) ? "active" : "";
                $curr = ( $i == 1 ) ? $url : $url . '?page=' . $i;
                $html .= '<li class="' . $class . '"><a href="' . $curr . '">' . $i . '</a></li>';
            }

            if($end < $last) {
                $html .= '<li class="disabled"><span>...</span></li>';
                $html .= '<li><a href="' . $url . '?page=' . $last . '">' . $last . '</a></li>';
            }

            $class = ( $current == $last ) ? "disabled" : "";
            //$next = ( $current == $last )?$url . '?page=' . ( $current ):$url . '?page=' . ( $current + 1 );
            $next = ( $current == $last ) ? '#' : $url . '?page=' . ( $current + 1 );
            $html .= '<li class="' . $class . '"><a href="' . $next . '">&raquo;</a></li>';
            $html .= '</ul>';

            return $html;
        }

        public function dumbError($message, $redirect = false, $show404 = false)
        {
            $ip = $_SERVER['REMOTE_ADDR'];
            $this->logger->log($message . ': ' . $ip, \Phalcon\Logger::ERROR);
            if(!$redirect) {
                echo '<h1>Unexpected error! Try again latter, please.</h1>';
            } else {
                if($show404)
                    $this->response->redirect($this->siteUrl . '/404');
                else
                    $this->response->redirect($this->siteUrl . '/helpcenter');
                $this->response->send();
            }
            die;
        }

    }
    