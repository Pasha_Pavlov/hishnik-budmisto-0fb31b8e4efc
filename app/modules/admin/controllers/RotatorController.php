<?php

    /*

     *     Author:  Hishnik  
     *     E-mail:  arenovec@gmail.com    
     *     skype:   kidw810i 
     * 
     *     Copyright (C) 2016 Hishnik
     */

    namespace Budmisto\Modules\Admin\Controllers;

    use Budmisto\Modules\Admin\Models\Admin as BmAdmin;
    use Budmisto\Modules\Front\Models\Rotator;

    class RotatorController extends ControllerBase
    {

        public $admin;

        public function initialize()
        {
            parent::initialize();
            $this->admin = new BmAdmin();
            $this->view->setVar('map_key', $this->config->application->map_key);
        }

        public function indexAction()
        {

            $all_banners = Rotator::get_all_banners();

            $this->view->setVar('all_banners', $all_banners);
        }

        public function textbannersAction()
        {
            $all_text_banners = Rotator::get_all_banners('text');

            $this->view->setVar('all_banners', $all_text_banners);
        }

        public function createAction()
        {

            if($this->request->isPost()) {

                $data['name'] = $_POST['name'];
                
                if(!$data['name'])
                    return;
                
                $data['url'] = $_POST['url'];
                ($_POST['active'] == 'on') ? $data['active'] = 1 : $data['active'] = 0;

                if(count($_POST['controller']) > 0) {
                    foreach ($_POST['controller'] as $k => $v) {
                        if($k > 0)
                            $data['controller'] .= ',';
                        $data['controller'] .= $v;
                    }
                }else {
                    $data['controller'] = '';
                }

                $data['position'] = $_POST['position'];

                foreach ($_POST['main_spec'] as $key => $value) {
                    $data['main_spec'] .= "[{$value}]";
                }
                foreach ($_POST['sub_spec'] as $key => $value) {
                    $data['sub_spec'] .= "[{$value}]";
                }

                if($_FILES['image']['name']) {
                    $time = time();
                    // Каталог, в который мы будем принимать файл:
                    $uploaddir = './../html/images/banners/' . $time;
                    $uploadfile = $uploaddir . basename($_FILES['image']['name']);

                    // Копируем файл из каталога для временного хранения файлов:
                    if(copy($_FILES['image']['tmp_name'], $uploadfile)) {
                        $data['text'] = '<img width="500" height="100" src="http://' . $_SERVER['HTTP_HOST'] . '/images/banners/' . $time . $_FILES['image']['name'] . '">';
                    }
                } else {
                    $data['text'] = $this->textengine->convertBB2Html($_POST['desc']);
                }

                $data['placeid'] = $this->request->getPost('placeid', array('trim', 'string'), false);

                $city_filtration = $this->request->getPost('city_filtration', 'string', false);
                
                if($data['placeid'] != '' and $city_filtration) {
                    $placeid = $data['placeid'];
                    $data['placelat'] = $this->request->getPost('placelat', 'float', false);
                    $data['placelng'] = $this->request->getPost('placelng', 'float', false);
                    $data['placeaddr'] = $this->request->getPost('placeaddr', array('trim', 'string'), false);
                    $data['placetown'] = $this->request->getPost('placetown', array('trim', 'string'), false);
                    $data['placedistrict2'] = $this->request->getPost('placedistrict2', array('trim', 'string'), false);
                    $data['placedistrict3'] = $this->request->getPost('placedistrict3', array('trim', 'string'), false);
                    $data['placeregion'] = $this->request->getPost('placeregion', array('trim', 'string'), false);
                    $data['placecountry'] = $this->request->getPost('placecountry', array('trim', 'string'), false);
                    $data['placeroute'] = $this->request->getPost('placeroute', array('trim', 'string'), false);
                    $data['placextra'] = $this->request->getPost('placextra', array('trim', 'string'), false);
                }

                $distance = $this->request->getPost('distance', 'int', false);
                $data['city_distance'] = $distance ? $distance : 10;
               
                if (!$city_filtration)
                    $data['placeaddr'] = '';
                
                $distance = $this->request->getPost('distance', 'int', false);
                $data['city_distance'] = $distance ? $distance : 10;

                Rotator::update_banner('banner', $data);
                $this->response->redirect($this->siteUrl . '/workz/rotator');
                $this->response->send();
            }


            $main_spec = Rotator::get_main_spec();
            $sub_spec = Rotator::get_sub_spec();

            $this->view->setVar('main_spec', $main_spec);
            $this->view->setVar('sub_spec', $sub_spec);
        }

        public function textbannerscreateAction()
        {

            if($this->request->isPost()) {

                $data['name'] = $_POST['name'];
                ($_POST['active'] == 'on') ? $data['active'] = 1 : $data['active'] = 0;

                if(count($_POST['controller']) > 0) {
                    foreach ($_POST['controller'] as $k => $v) {
                        if($k > 0)
                            $data['controller'] .= ',';
                        $data['controller'] .= $v;
                    }
                }else {
                    $data['controller'] = '';
                }

                if($_POST['position']) {
                    $data['position'] = $_POST['position'];
                } else {
                    $data['position'] = 'left';
                }

                foreach ($_POST['main_spec'] as $key => $value) {
                    $data['main_spec'] .= "[{$value}]";
                }
                foreach ($_POST['sub_spec'] as $key => $value) {
                    $data['sub_spec'] .= "[{$value}]";
                }

                $data['text'] = $this->textengine->convertBB2Html($_POST['desc']);


                Rotator::update_banner('text', $data);
                $this->response->redirect($this->siteUrl . '/workz/textbanners');
                $this->response->send();
            }


            $main_spec = Rotator::get_main_spec();
            $sub_spec = Rotator::get_sub_spec();

            $this->view->setVar('main_spec', $main_spec);
            $this->view->setVar('sub_spec', $sub_spec);
        }

        public function editAction()
        {
            $banner_id = $this->dispatcher->getParam('id');

            if($this->request->isPost()) {

                $data['name'] = $_POST['name'];
                $data['url'] = $_POST['url'];
                ($_POST['active'] == 'on') ? $data['active'] = 1 : $data['active'] = 0;

                if(count($_POST['controller']) > 0) {
                    foreach ($_POST['controller'] as $k => $v) {
                        if($k > 0)
                            $data['controller'] .= ',';
                        $data['controller'] .= $v;
                    }
                }else {
                    $data['controller'] = '';
                }

                $data['position'] = $_POST['position'];

                foreach ($_POST['main_spec'] as $key => $value) {
                    $data['main_spec'] .= "[{$value}]";
                }

                foreach ($_POST['sub_spec'] as $key => $value) {
                    $data['sub_spec'] .= "[{$value}]";
                }

                if($_FILES['image']['name']) {
                    $time = time();
                    // Каталог, в который мы будем принимать файл:
                    $uploaddir = './../html/images/banners/' . $time;
                    $uploadfile = $uploaddir . basename($_FILES['image']['name']);

                    // Копируем файл из каталога для временного хранения файлов:
                    if(copy($_FILES['image']['tmp_name'], $uploadfile)) {
                        $data['text'] = '<img width="100%" height="100" src="http://' . $_SERVER['HTTP_HOST'] . '/images/banners/' . $time . $_FILES['image']['name'] . '">';
                    }
                } else {
                    $data['text'] = $this->textengine->convertBB2Html($_POST['desc']);
                }

                $data['placeid'] = $this->request->getPost('placeid', array('trim', 'string'), false);

                $city_filtration = $this->request->getPost('city_filtration', 'string', false);
                
                if($data['placeid'] != '' and $city_filtration) {
                    $placeid = $data['placeid'];
                    $data['placelat'] = $this->request->getPost('placelat', 'float', false);
                    $data['placelng'] = $this->request->getPost('placelng', 'float', false);
                    $data['placeaddr'] = $this->request->getPost('placeaddr', array('trim', 'string'), false);
                    $data['placetown'] = $this->request->getPost('placetown', array('trim', 'string'), false);
                    $data['placedistrict2'] = $this->request->getPost('placedistrict2', array('trim', 'string'), false);
                    $data['placedistrict3'] = $this->request->getPost('placedistrict3', array('trim', 'string'), false);
                    $data['placeregion'] = $this->request->getPost('placeregion', array('trim', 'string'), false);
                    $data['placecountry'] = $this->request->getPost('placecountry', array('trim', 'string'), false);
                    $data['placeroute'] = $this->request->getPost('placeroute', array('trim', 'string'), false);
                    $data['placextra'] = $this->request->getPost('placextra', array('trim', 'string'), false);
                }

                $distance = $this->request->getPost('distance', 'int', false);
                $data['city_distance'] = $distance ? $distance : 10;
               
                if (!$city_filtration)
                    $data['placeaddr'] = '';

                Rotator::update_banner('banner', $data, $banner_id);
            }

            $banner = Rotator::get_banner_by_id($banner_id);

            $banner['text_bb'] = $this->textengine->convertHtml2BB($banner['text']);
            $banner['spec_main'] = explode("]", str_replace("[", '', $banner['spec_main']));
            $banner['spec_sub'] = explode("]", str_replace("[", '', $banner['spec_sub']));

            /*
              if(is_array($banner['spec_sub'])) {

              $catalog  = new \Budmisto\Modules\Front\Models\Catalog();

              foreach ($banner['spec_sub'] as $key => $value) {

              if($value<1)
              continue;

              $data = $catalog->getSubSpecInfo($value);

              if(!in_array($data['type_id'],$banner['spec_main']))
              $banner['spec_main'][] = $data['type_id'];
              }
              }
             */

            $this->view->setVar('banner', $banner);

            $main_spec = Rotator::get_main_spec();

            $sub_spec = Rotator::get_sub_spec();

            $this->view->setVar('main_spec', $main_spec);
            $this->view->setVar('sub_spec', $sub_spec);
        }

        public function textbannerseditAction()
        {
            $banner_id = $this->dispatcher->getParam('id');

            if($this->request->isPost()) {

                $data['name'] = $_POST['name'];
                $data['url'] = $_POST['url'];
                ($_POST['active'] == 'on') ? $data['active'] = 1 : $data['active'] = 0;

                if(count($_POST['controller']) > 0) {
                    foreach ($_POST['controller'] as $k => $v) {
                        if($k > 0)
                            $data['controller'] .= ',';
                        $data['controller'] .= $v;
                    }
                }else {
                    $data['controller'] = '';
                }

                if($_POST['position']) {
                    $data['position'] = $_POST['position'];
                } else {
                    $data['position'] = 'left';
                }

                foreach ($_POST['main_spec'] as $key => $value) {
                    $data['main_spec'] .= "[{$value}]";
                }

                foreach ($_POST['sub_spec'] as $key => $value) {
                    $data['sub_spec'] .= "[{$value}]";
                }

                $data['text'] = $this->textengine->convertBB2Html($_POST['desc']);


                Rotator::update_banner('text', $data, $banner_id);
            }

            $banner = Rotator::get_banner_by_id($banner_id, 'text');

            $banner['text_bb'] = $this->textengine->convertHtml2BB($banner['text']);
            $banner['spec_main'] = explode("]", str_replace("[", '', $banner['spec_main']));
            $banner['spec_sub'] = explode("]", str_replace("[", '', $banner['spec_sub']));

            $this->view->setVar('banner', $banner);

            $main_spec = Rotator::get_main_spec();
            $sub_spec = Rotator::get_sub_spec();

            $this->view->setVar('main_spec', $main_spec);
            $this->view->setVar('sub_spec', $sub_spec);
        }

        public function deleteAction()
        {
            $banner_id = $this->dispatcher->getParam('id');
            Rotator::delete_banner('banner', $banner_id);
            $this->response->redirect($this->siteUrl . '/workz/rotator');
            $this->response->send();
        }

        public function textbannersdeleteAction()
        {
            $banner_id = $this->dispatcher->getParam('id');
            Rotator::delete_banner('text', $banner_id);
            $this->response->redirect($this->siteUrl . '/workz/textbanners');
            $this->response->send();
        }

        public function text_orderAction()
        {

            if($this->request->isAjax()) {
                $spec_id = (int) $_GET['spec_id'];
                $req = Rotator::get_order_text($spec_id);
                $req['textBB'] = $this->textengine->convertHtml2BB($req['text']);
                print_r(json_encode($req));
                exit();
            }

            if($this->request->isPost()) {

                $data['spec_id'] = (int) $_POST['spec_id'];
                $data['text'] = $this->textengine->convertBB2Html($_POST['desc']);
                ($_POST['active'] == 'on') ? $data['active'] = 1 : $data['active'] = 0;
                $data['text_block_id'] = (int) $_POST['text_block_id'];
                Rotator::update_order_text($data);
            }

            $main_spec = Rotator::get_main_spec();
            $this->view->setVar('main_spec', $main_spec);
        }

        public function ctr_filterAction()
        {

            if($this->request->isAjax()) {
                if($_GET['filter_data'] == 1) {
                    $spec_id = (int) $_GET['spec_id'];
                    $filter = Rotator::get_CTR_filter_data($spec_id);
                    print_r(json_encode($filter));
                    exit();
                }

                if($_POST['save_data'] == 1) {

                    $data['spec_id'] = (int) $_POST['spec_id'];
                    $data['stars_min'] = (int) $_POST['stars_min'];
                    $data['stars_max'] = (int) $_POST['stars_max'];
                    $data['views_min'] = (int) $_POST['views_min'];
                    $data['views_max'] = (int) $_POST['views_max'];
                    $data['logins_min'] = (int) $_POST['logins_min'];
                    $data['logins_max'] = (int) $_POST['logins_max'];
                    $data['limit_max'] = (int) $_POST['limit_max'];
                    $data['photo_priority'] = (int) $_POST['photo_priority'];

                    $res = Rotator::set_CTR_filter_data($data);
                    if($res) {
                        print_r('success');
                    }
                    exit();
                }
            }

            $main_spec = Rotator::get_main_spec();
            $filter = Rotator::get_CTR_filter_data(0);
            $this->view->setVar('filter', $filter);
            $this->view->setVar('main_spec', $main_spec);
        }

        public function callbackAction()
        {
            $active_callback = Rotator::get_active_callback();
            $this->view->setVar('active_callback', $active_callback);
        }

        public function callback_editAction()
        {
            $id = $this->dispatcher->getParam('id');
            Rotator::update_callback($id);
            $this->response->redirect($this->siteUrl . '/workz/callback');
            $this->response->send();
        }

    }
    