<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/front/models/Catalog.php
     *
     * Catalog model
     *
     */

    namespace Budmisto\Modules\Front\Models;

    use Budmisto\Modules\Common\Models\Common as Common;

    class Catalog extends \Phalcon\Mvc\Model
    {

        private $db;
        private $lang;
        private $config;

        public function initialize()
        {
            $this->db = $this->getDi()->getShared('db');
            $this->config = $this->getDi()->getShared('config');
        }

        public function setLang($lang)
        {
            $this->lang = $lang;
        }

        public function getTopSpecList()
        {
            $sql = 'SELECT id, type_name, slug FROM work_types WHERE is_active = 1 ORDER BY id';
            return $this->db->query($sql)->fetchAll();
        }

        public function countShops()
        {
            $sql = 'SELECT count(id) AS cnt FROM bm_users WHERE user_role_id = 7';
            return $this->db->query($sql)->fetch()['cnt'];
        }

        public function countShopsNets()
        {
            $sql = 'SELECT count(id) AS cnt FROM bm_shop_networks';
            return $this->db->query($sql)->fetch()['cnt'];
        }

        public function countUserOrders($user_id)
        {
            $sql = 'SELECT count(id) AS cnt FROM bm_orders WHERE user_id = ?';
            return $this->db->query($sql, array($user_id))->fetch()['cnt'];
        }

        public function countOrders()
        {
            $sql = 'SELECT count(id) AS cnt FROM bm_orders WHERE order_status = 1';
            return $this->db->query($sql)->fetch()['cnt'];
        }

        public function countWorkers()
        {
            $sql = 'SELECT COUNT(id) AS cnt FROM bm_users
                LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = bm_users.id
                WHERE
                bm_users.user_role_id > 3 AND bm_users.user_role_id < 7 AND bm_perfs_info.brief IS NOT NULL AND bm_perfs_info.brief <> "" AND bm_users.is_active = 1';
            return $this->db->query($sql)->fetch()['cnt'];
        }

        public function countTopOW($top_id, $place = false)
        {
            /* $sql = 'SELECT builder_specializations.id FROM builder_specializations
              LEFT JOIN bm_users ON bm_users.id = builder_specializations.user_id
              WHERE bm_users.is_active = 1 AND builder_specializations.wrk_type_id = ? GROUP BY builder_specializations.user_id';
              $result['workers'] = count($this->db->query($sql, array($top_id))->fetchAll()); */
            $sql = 'SELECT COUNT(id) AS oc FROM bm_orders WHERE order_wrk_type = ?';
            $result['orders'] = $this->db->query($sql, array($top_id))->fetch()['oc'];
            return $result;
            // NEED REFACT
        }

        public function countSubOW($top_id, $sub_id, $place = false)
        {
            //$sql = 'SELECT COUNT(id) AS bc FROM builder_specializations WHERE wrk_type_id = ? AND wrk_spec_id = ?';
            //$result['workers'] = $this->db->query($sql, array($top_id, $sub_id))->fetch()['bc'];
            $sql = 'SELECT id FROM builder_specializations WHERE wrk_type_id = ? AND wrk_spec_id = ? GROUP BY user_id';
            $result['workers'] = count($this->db->query($sql, array($top_id, $sub_id))->fetchAll());
            $sql = 'SELECT COUNT(id) AS oc FROM bm_orders WHERE order_wrk_type = ? AND order_wrk_spec = ?';
            $result['orders'] = $this->db->query($sql, array($top_id, $sub_id))->fetch()['oc'];
            return $result;
        }

        public function getSubSpecList()
        {
            //$sql = 'SELECT id, spec_name, type_id, slug FROM specializations WHERE is_active = 1 ORDER BY id';
            $sql = 'SELECT specializations.id, specializations.spec_name, specializations.type_id, specializations.slug
                FROM builder_specializations
                LEFT JOIN specializations on specializations.id = builder_specializations.wrk_spec_id
                WHERE specializations.is_active = 1 GROUP BY specializations.id ORDER BY id';
            $result = $this->db->query($sql);
            $count = $result->numRows();
            $result = $result->fetchAll();
            $ret = false;
            if($count) {
                $ret = array();
                foreach ($result as $r) {
                    $indx = 'sub' . $r['type_id'];
                    if(!isset($ret[$indx]))
                        $ret[$indx] = array();
                    array_push($ret[$indx], array('spec_name' => $r['spec_name'], 'spec_id' => $r['id'], 'slug' => $r['slug']));
                }
            }
            return $ret;
        }

        public function getSubSpecListById($id)
        {
            //$sql = 'SELECT id, spec_name, slug FROM specializations WHERE is_active = 1 AND type_id = ? ORDER BY id ';
            $sql = 'SELECT id, spec_name, slug, (SELECT count(*) FROM  builder_specializations WHERE wrk_spec_id = specializations.id) AS num FROM specializations WHERE is_active = 1 AND type_id = ? ORDER BY num DESC';
            return $this->db->query($sql, array($id))->fetchAll();
        }

        public function getSLbyS($top_rubric)
        {
            $sql = 'SELECT work_types.id AS tid, work_types.type_name FROM work_types WHERE work_types.slug = ?';
            $data = array($top_rubric);
            return $this->db->query($sql, $data)->fetch();
        }

        public function selectById($top_id)
        {
            $sql = 'SELECT work_types.slug AS top_slug FROM work_types
                WHERE work_types.id = ?';
            $data = array($top_id);
            return $this->db->query($sql, $data)->fetch();
        }

        public function getSLbyS2($top_rubric, $sub_rubric)
        {
            $sql = 'SELECT specializations.id AS sid, work_types.id AS tid, specializations.spec_name, work_types.type_name
                FROM specializations
                LEFT JOIN work_types ON work_types.id = specializations.type_id
                WHERE work_types.slug = ? AND specializations.slug = ?';
            $data = array($top_rubric, $sub_rubric);
            return $this->db->query($sql, $data)->fetch();
        }

        public function selectById2($top_id, $sub_id)
        {
            $sql = 'SELECT specializations.slug AS sub_slug, work_types.slug AS top_slug FROM specializations
                LEFT JOIN work_types ON work_types.id = specializations.type_id
                WHERE work_types.id = ? AND specializations.id = ?';
            $data = array($top_id, $sub_id);
            return $this->db->query($sql, $data)->fetch();
        }

        public function getUserOrdersList($user_id, $start, $limit, $brief_limit = false)
        {
            if($brief_limit)
                $brief_letters_limit = 80;
            else
                $brief_letters_limit = $this->config->lists->big_brief;
            $sql = "SELECT bm_orders.id, bm_orders.order_addr, SUBSTR(bm_orders.order_description, 1, {$brief_letters_limit}) AS brief,
                work_types.type_name, specializations.spec_name, bm_orders.order_status, DATE_FORMAT(bm_orders.order_date,'%d/%m/%Y (%H:%i)') AS order_date,
                UNIX_TIMESTAMP(bm_orders.actual_period) AS finish_date, UNIX_TIMESTAMP(bm_orders.order_end) AS close_date
                FROM bm_orders
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
                WHERE bm_orders.user_id = ? ORDER BY id DESC, actual_period DESC";
            return $this->db->query($sql, array($user_id))->fetchAll();
        }

        public function getLastUserOrder($user_id)
        {
            $sql = 'SELECT * FROM bm_orders WHERE user_id = ? ORDER BY id DESC LIMIT 1';
            return $this->db->query($sql, array($user_id))->fetch();
        }

        public function getLastConnectList($user_id, $start = 0, $limit = 50)
        {
            $brief_letters_limit = $this->config->lists->big_brief;
            $sql = "SELECT bm_users.user_phone, bm_orders.premium_block AS premium_active, bm_orders.id, SUBSTR(bm_orders.order_description, 1, {$brief_letters_limit}) AS brief, UNIX_TIMESTAMP(bm_orders.order_end) AS close_date,  UNIX_TIMESTAMP(bm_orders.actual_period) AS actual_period,
                work_types.type_name, specializations.spec_name, bm_orders.order_status, bm_orders_connect.choice_id, bm_orders_connect.offer_id,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region
                FROM bm_orders
                LEFT JOIN bm_users ON bm_users.id = bm_orders.user_id
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
                LEFT JOIN bm_orders_connect ON bm_orders_connect.order_id = bm_orders.id
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                WHERE bm_orders_connect.choice_id = ?
                    OR bm_orders_connect.offer_id = ?
                    ORDER BY cnct_date DESC";
            return $this->db->query($sql, array($user_id, $user_id))->fetchAll();
        }

        public function getLastOrdersList($start, $limit)
        {
            $time = time();
            $brief_letters_limit = $this->config->lists->small_brief;
            $sql = "SELECT bm_orders.id, show_is_old, bm_orders.order_brief AS descr_short, work_types.type_name, specializations.spec_name,
                DATE_FORMAT(bm_orders.order_date,'%d/%m/%Y (%H:%i)') AS order_date,
                UNIX_TIMESTAMP(bm_orders.actual_period) AS finish_date, UNIX_TIMESTAMP(bm_orders.order_end) AS close_date,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region
                FROM bm_orders
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
                WHERE bm_orders.order_status = 1 AND bm_orders.order_status = 1 AND UNIX_TIMESTAMP(bm_orders.actual_period) > '{$time}' AND UNIX_TIMESTAMP(bm_orders.order_end) IS NULL
                ORDER BY bm_orders.order_date DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql)->fetchAll();
        }

        public function getOrdersListByCoords($lat, $lng, $start, $limit)
        {
            if(!$lat || !$lng)
                return false;
            $brief_letters_limit = $this->config->lists->small_brief;
            $sql = "SELECT bm_orders.id, bm_orders.order_brief AS descr_short, work_types.type_name, specializations.spec_name,
                DATE_FORMAT(bm_orders.order_date,'%d/%m/%Y (%H:%i)') AS order_date,
                UNIX_TIMESTAMP(bm_orders.actual_period) AS finish_date, UNIX_TIMESTAMP(bm_orders.order_end) AS close_date,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region,
(6371 * acos (
      cos ( radians(?) )
      * cos( radians( bm_orders_places.lat ) )
      * cos( radians( bm_orders_places.lng ) - radians(?) )
      + sin ( radians(?) )
      * sin( radians( bm_orders_places.lat ) )
    )
) AS distance
                FROM bm_orders
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
                WHERE bm_orders.order_status = 1 AND bm_orders.id > (SELECT id FROM bm_orders ORDER by id DESC LIMIT 1) - 500 AND CURRENT_TIMESTAMP < bm_orders.actual_period
                ORDER BY distance ASC, bm_orders.order_date DESC LIMIT {$start}, {$limit}";

            return $this->db->query($sql, array($lat, $lng, $lat))->fetchAll();
        }

        public function getWorkersListByCoords($lat, $lng, $start, $limit)
        {
            if(!$lat || !$lng)
                return false;
            $sql = 'SELECT bm_users.user_name, bm_users.id AS uid, bm_perfs_info.brief, avatar,
                       bm_users_places.route, bm_users_places.town, bm_users_places.district, bm_users_places.region,
(6371 * acos (
      cos ( radians(?) )
      * cos( radians( bm_users_places.lat ) )
      * cos( radians( bm_users_places.lng ) - radians(?) )
      + sin ( radians(?) )
      * sin( radians( bm_users_places.lat ) )
    )
) AS distance
                FROM bm_users_places
                LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = bm_users_places.user_id
                LEFT JOIN bm_users ON bm_users.id = bm_users_places.user_id
                WHERE bm_users.user_role_id > 3 AND bm_users.user_role_id < 7 AND bm_perfs_info.brief IS NOT NULL AND bm_perfs_info.brief <> "" AND bm_users.is_active = 1';
            $sql .= " ORDER BY distance ASC, uid DESC LIMIT {$start}, {$limit}";
            $res = $this->db->query($sql, array($lat, $lng, $lat))->fetchAll();

            $CTR_filter = $this->getCTRUsersByCoords($res, $lat, $lng);
            if($CTR_filter) {
                $res = array_merge($res, $CTR_filter);
                shuffle($res);
            }

            /* Обновляем счетчик показов */
            if(is_array($res) and count($res) > 0) {
                $all_id = $this->getUsIdsFromQuery($res, 'uid');
                $this->updateUserViews($all_id);
            }
            /* Обновляем счетчик показов */

            return $res;
        }

        public function getRandomWorkersList($start, $limit)
        {
            $sql = 'SELECT bm_users.user_name, avatar, bm_users.id AS uid, bm_perfs_info.brief, bm_perfs_info.brief
                FROM bm_users
                LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = bm_users.id
                WHERE bm_users.user_role_id > 3 AND bm_users.user_role_id < 7 AND bm_perfs_info.brief IS NOT NULL AND bm_perfs_info.brief <> "" AND bm_users.is_active = 1';
            $sql .= " GROUP BY uid ORDER BY RAND() LIMIT {$start}, {$limit}";
            $res = $this->db->query($sql)->fetchAll();
            if(!$res)
                return false;

            return $this->getWPlaces($res);
        }

        public function getBonusWorkersList($start, $limit)
        {
            $time = time() - 86400;
            $sql = 'SELECT bm_users.user_name, avatar, bm_users.id AS uid, bm_perfs_info.brief
                FROM bm_users
                LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = bm_users.id
                WHERE bm_users.user_role_id > 3 
                AND bm_users.user_role_id < 7 
                AND bm_perfs_info.brief IS NOT NULL 
                AND bm_perfs_info.brief <> "" 
                AND UNIX_TIMESTAMP(bm_users.user_reg_date) > ' . $time;
            $sql .= " GROUP BY uid ORDER BY bm_users.user_reg_date DESC LIMIT {$start}, {$limit}";
            $res = $this->db->query($sql)->fetchAll();
            if(!$res)
                return false;

            return $this->getWPlaces($res);
        }

        public function getLastUserOrdersList($user_id, $start = 0, $limit = 10, $distance = 150)
        {
            $inner_sql = '';
            $sql = 'SELECT builder_specializations.wrk_type_id, builder_specializations.wrk_spec_id
                FROM builder_specializations
                LEFT JOIN bm_users ON bm_users.id = builder_specializations.user_id
                WHERE builder_specializations.user_id = ?';
            $res = $this->db->query($sql, array($user_id))->fetchAll();
            if(!$res)
                return false;


            $tmp = array();
            foreach ($res as $r) {
                $tmp[] = "(bm_orders.order_wrk_type = '{$r['wrk_type_id']}' AND bm_orders.order_wrk_spec = '{$r['wrk_spec_id']}')";
            }

            if(count($tmp) > 1) {
                $inner_sql = implode(' OR ', $tmp);
                $inner_sql = '(' . $inner_sql . ')';
            } else {
                $inner_sql = $tmp[0];
            }
            $inner_sql = ' AND ' . $inner_sql;

            $sql = 'SELECT avg(lat) AS lat, avg(lng) AS lng FROM bm_users_places WHERE user_id = ?';
            $place = $this->db->query($sql, array($user_id))->fetch();
            if(!$place || !$place['lat'] || !$place['lng'])
                return false;

            $time = time();

            $sql = "SELECT bm_orders.id, bm_orders.order_brief AS descr_short, work_types.type_name, specializations.spec_name,
                DATE_FORMAT(bm_orders.order_date,'%d/%m/%Y (%H:%i)') AS order_date,
                UNIX_TIMESTAMP(bm_orders.actual_period) AS finish_date, UNIX_TIMESTAMP(bm_orders.order_end) AS close_date,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region,
(6371 * acos (
      cos ( radians({$place['lat']}) )
      * cos( radians( bm_orders_places.lat ) )
      * cos( radians( bm_orders_places.lng ) - radians({$place['lng']}) )
      + sin ( radians({$place['lat']}) )
      * sin( radians( bm_orders_places.lat ) )
    )
) AS distance
                FROM bm_orders
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
                WHERE bm_orders.order_status = 1 AND UNIX_TIMESTAMP(bm_orders.actual_period) > '{$time}' AND UNIX_TIMESTAMP(bm_orders.order_end) IS NULL  {$inner_sql} 
                HAVING distance < {$distance}
                ORDER BY bm_orders.order_date DESC, distance ASC   LIMIT {$start}, {$limit}"; //var_dump($sql); die;

            return $this->db->query($sql)->fetchAll();
        }

        public function getOrdersList($start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $brief_letters_limit = $this->config->lists->big_brief;
            $sql = 'SELECT bm_orders.id, bm_orders.order_brief AS descr_short, work_types.type_name,
                specializations.spec_name, DATE_FORMAT(bm_orders.order_date,"%d/%m/%Y (%H:%i)") AS order_date,
                UNIX_TIMESTAMP(bm_orders.actual_period) AS finish_date, UNIX_TIMESTAMP(bm_orders.order_end) AS close_date,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region
                FROM bm_orders
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
                WHERE bm_orders.order_status = 1';

            $sql .= " ORDER BY bm_orders.order_date DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql)->fetchAll();
        }

        public function getTopOrdersList($top_id, $start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $brief_letters_limit = $this->config->lists->big_brief;
            $sql = 'SELECT bm_orders.id, bm_orders.order_brief AS descr_short, work_types.type_name,
                specializations.spec_name, DATE_FORMAT(bm_orders.order_date,"%d/%m/%Y (%H:%i)") AS order_date,
                UNIX_TIMESTAMP(bm_orders.actual_period) AS finish_date, UNIX_TIMESTAMP(bm_orders.order_end) AS close_date,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region
                FROM bm_orders
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
                WHERE bm_orders.order_wrk_type = ? AND bm_orders.order_status = 1';

            $limit++;
            $sql .= " ORDER BY bm_orders.order_date DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql, array($top_id))->fetchAll();
        }

        public function getTopOrdersListByCoords($rub, $lat, $lng, $start, $limit, $distance = 25)
        {
            if(!$lat || !$lng)
                return false;
            $brief_letters_limit = $this->config->lists->small_brief;
            $start = $start ? ($limit * ($start - 1)) : 0;
            $limit++;
            $sql = "SELECT bm_orders.id, bm_orders.order_brief AS descr_short, work_types.type_name, specializations.spec_name,
                DATE_FORMAT(bm_orders.order_date,'%d/%m/%Y (%H:%i)') AS order_date,
                UNIX_TIMESTAMP(bm_orders.actual_period) AS finish_date, UNIX_TIMESTAMP(bm_orders.order_end) AS close_date,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region,
(6371 * acos (
      cos ( radians(?) )
      * cos( radians( bm_orders_places.lat ) )
      * cos( radians( bm_orders_places.lng ) - radians(?) )
      + sin ( radians(?) )
      * sin( radians( bm_orders_places.lat ) )
    )
) AS distance
                FROM bm_orders
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
                WHERE bm_orders.order_wrk_type = ? AND bm_orders.order_status = 1 AND bm_orders.id > (SELECT id FROM bm_orders ORDER by id DESC LIMIT 1) - 500 AND CURRENT_TIMESTAMP < bm_orders.actual_period
                HAVING distance < {$distance} ORDER BY distance ASC, bm_orders.order_date DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql, array($lat, $lng, $lat, $rub))->fetchAll();
        }

        public function getSubOrdersListByCoords($rub, $sub, $lat, $lng, $start, $limit, $distance = 25)
        {
            $sql = "SELECT bm_orders.id, bm_orders.order_brief AS descr_short, work_types.type_name, specializations.spec_name,
                DATE_FORMAT(bm_orders.order_date,'%d/%m/%Y (%H:%i)') AS order_date,
                UNIX_TIMESTAMP(bm_orders.actual_period) AS finish_date, UNIX_TIMESTAMP(bm_orders.order_end) AS close_date,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region,
(6371 * acos (
      cos ( radians(?) )
      * cos( radians( bm_orders_places.lat ) )
      * cos( radians( bm_orders_places.lng ) - radians(?) )
      + sin ( radians(?) )
      * sin( radians( bm_orders_places.lat ) )
    )
) AS distance
                FROM bm_orders
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
                WHERE bm_orders.order_wrk_type = ? AND bm_orders.order_wrk_spec = ? AND bm_orders.order_status = 1 AND bm_orders.id > (SELECT id FROM bm_orders ORDER by id DESC LIMIT 1) - 500 AND CURRENT_TIMESTAMP < bm_orders.actual_period
                HAVING distance < {$distance} ORDER BY distance ASC, bm_orders.order_date DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql, array($lat, $lng, $lat, $rub, $sub))->fetchAll();
        }

        public function getSubOrdersRefListByCoords($rub, $sub, $lat, $lng, $start, $limit, $distance = 25)
        {
            $sql = 'SELECT ref_id, parent_ref_id FROM bm_reference_spec WHERE spec_id = ?';
            $res = $this->db->query($sql, array($sub_id))->fetchAll();
            if(!$res)
                return false;
            $sids = array();
            foreach ($res as $r) {
                $sids[] = $r['ref_id'];
            }
            $sids = implode(',', $sids);

            $sql = "SELECT bm_orders.id, bm_orders.order_brief AS descr_short, work_types.type_name, specializations.spec_name,
                DATE_FORMAT(bm_orders.order_date,'%d/%m/%Y (%H:%i)') AS order_date,
                UNIX_TIMESTAMP(bm_orders.actual_period) AS finish_date, UNIX_TIMESTAMP(bm_orders.order_end) AS close_date,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region,
(6371 * acos (
      cos ( radians(?) )
      * cos( radians( bm_orders_places.lat ) )
      * cos( radians( bm_orders_places.lng ) - radians(?) )
      + sin ( radians(?) )
      * sin( radians( bm_orders_places.lat ) )
    )
) AS distance
                FROM bm_orders
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
                WHERE bm_orders.order_wrk_spec IN ({$sids}) AND bm_orders.order_status = 1 AND bm_orders.id > (SELECT id FROM bm_orders ORDER by id DESC LIMIT 1) - 500 AND CURRENT_TIMESTAMP < bm_orders.actual_period
                HAVING distance < {$distance} ORDER BY distance ASC, bm_orders.order_date DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql, array($lat, $lng, $lat, $rub, $sub))->fetchAll();
        }

        public function getWorkersList($start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $sql = 'SELECT bm_users.user_name, bm_users.id AS uid, avatar, bm_perfs_info.brief,
                (SELECT COUNT(id) FROM bm_files WHERE ftype = "image" AND user_id = bm_users.id)!=0 AS images
                FROM bm_users
                LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = bm_users.id
                WHERE bm_users.user_role_id > 3 AND bm_users.user_role_id < 7
                        AND bm_perfs_info.brief IS NOT NULL AND bm_perfs_info.brief <> "" AND bm_users.is_active = 1';
            $sql .= " GROUP BY uid ORDER BY images DESC LIMIT {$start}, {$limit}";

            $res = $this->db->query($sql)->fetchAll(); //var_dump($sql); die;
            if(!$res)
                return false;

            return $this->getWPlaces($res);
        }

        public function getTopWorkersList($top_id, $start, $limit, $rating = true)
        {

            ($rating) ? $add = "stars" : $add = "bm_users.user_reg_date";

            $start = $start ? ($limit * ($start - 1)) : 0;
            $sql = 'SELECT bm_users.user_email, avatar, bm_users.user_phone,bm_users.stars AS stars, bm_users.user_name, bm_users.id AS uid, bm_perfs_info.brief
                FROM builder_specializations
                LEFT JOIN bm_users ON bm_users.id = builder_specializations.user_id
                LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = builder_specializations.user_id
                WHERE builder_specializations.wrk_type_id = ? AND bm_perfs_info.brief IS NOT NULL AND bm_perfs_info.brief <> "" AND bm_users.is_active = 1';
            $limit++;
            $sql .= " GROUP BY uid ORDER BY " . $add . "  DESC LIMIT {$start}, {$limit}"; //var_dump($sql); die;
            $res = $this->db->query($sql, array($top_id))->fetchAll();
            if(!$res)
                return false;

            $CTR_filter = $this->getCTRUsers($res, $top_id);

            if($CTR_filter) {
                $res = array_merge($res, $CTR_filter);
                shuffle($res);
            }

            /* Обновляем счетчик показов */
            if(is_array($res) and count($res) > 0) {
                $all_id = $this->getUsIdsFromQuery($res, 'uid');
                $this->updateUserViews($all_id);
            }
            /* Обновляем счетчик показов */

            return $this->getWPlaces($res);
        }

        public function getUsIdsFromQuery($res, $field_name)
        {
            if($res and count($res) > 0) {
                foreach ($res as $value) {
                    $all_id[] = $value[$field_name];
                }
                $all_id = implode(",", $all_id);
            } else {
                $all_id = "";
            }
            return $all_id;
        }

        public function getTopWorkersListByCoords($rub = 0, $lat, $lng, $start, $limit, $distance = 25)
        {
            if(!$lat || !$lng)
                return false;
            $start = $start ? ($limit * ($start - 1)) : 0;

            ($rub > 0) ? $rub_ins = "=" : $rub_ins = ">";

            $sql = " SELECT 
                    user_id
                FROM
                    builder_specializations
                WHERE 
                    wrk_type_id $rub_ins ?
        ";

            $res = $this->db->query($sql, array($rub))->fetchAll();
            unset($all_id_add);
            if(is_array($res) and count($res) > 0) {
                $all_id = $this->getUsIdsFromQuery($res, 'user_id');
                $all_id_add = "WHERE 
                    user_id IN ($all_id) ";
            }


            $sql = " SELECT 
                    user_id,
                    (6371 * acos (
                            cos ( radians(?) )
                            * cos( radians( bm_users_places.lat ) )
                            * cos( radians( bm_users_places.lng ) - radians(?) )
                            + sin ( radians(?) )
                            * sin( radians( bm_users_places.lat ) )
                          )
                      ) AS distance
                FROM
                    bm_users_places
                $all_id_add
                HAVING distance < ?
        ";

            $res = $this->db->query($sql, array($lat, $lng, $lat, $distance))->fetchAll();

            unset($all_id_add);
            if(is_array($res) and count($res) > 0) {
                $all_id = $this->getUsIdsFromQuery($res, 'user_id');
                $all_id_add = "AND bm_users.id IN ($all_id)";
            }

            $sql = "SELECT
                    bm_users.user_name,
                    bm_users.avatar,
                    bm_users.user_phone,
                    bm_users.stars AS stars,
                    bm_users.user_email,
                    bm_users.id AS uid, 
                    bm_perfs_info.brief
                FROM        
                    bm_users
                LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = bm_users.id
                WHERE 
                       bm_users.user_role_id > 3
                       $all_id_add                        
                       AND bm_users.user_role_id < 7
                       AND bm_perfs_info.brief IS NOT NULL
                       AND bm_perfs_info.brief <> ''
                       AND bm_users.is_active = 1
                GROUP BY uid ORDER BY stars DESC, uid DESC LIMIT {$start}, {$limit}
        ";

            $res = $this->db->query($sql, array())->fetchAll();

            $CTR_filter = $this->getCTRUsersByCoords($res, $lat, $lng, $rub);
            if($CTR_filter) {
                $res = array_merge($res, $CTR_filter);
                shuffle($res);
            }

            unset($all_id_add);
            if(is_array($res) and count($res) > 0) {
                $all_id = $this->getUsIdsFromQuery($res, 'uid');
                $all_id_add = "WHERE 
                bm_users_places.user_id IN ($all_id)";
            }

            $sql = "
            SELECT
                bm_users_places.user_id,
                bm_users_places.route,
                bm_users_places.town,
                bm_users_places.district,
                bm_users_places.region
            FROM    
                bm_users_places
                $all_id_add
        ";
            $res_place = $this->db->query($sql, array())->fetchAll();

            /* merge arrays */

            foreach ($res as $key => &$value) {
                foreach ($res_place as $k => $v) {
                    if($value['uid'] == $v['user_id']) {
                        $value['route'] = $v['route'];
                        $value['town'] = $v['town'];
                        $value['district'] = $v['district'];
                        $value['region'] = $v['region'];
                    }
                }
            }


            /* Обновляем счетчик показов */
            if(strlen($all_id) > 0) {
                $this->updateUserViews($all_id);
            }
            /* Обновляем счетчик показов */
            return $res;
        }

        public function updateUserViews($all_id)
        {
            $sql = "UPDATE bm_users SET user_views=user_views+1 WHERE id IN ($all_id)";
            return $this->db->query($sql, []);
        }

        public function getBlockedWorkers($usID)
        {
            $sql = "SELECT * FROM blocked_workers WHERE customer_id=?";
            return $this->db->query($sql, array($usID))->fetch();
        }

        public function getBlockedOrders($usID)
        {
            $sql = "SELECT blocked_orders FROM blocked_orders WHERE user_id=?";
            return $this->db->query($sql, array($usID))->fetch();
        }

        public function addBlockedWorkers($usID, $blocked)
        {

            $sql = "SELECT * FROM blocked_workers WHERE customer_id=?";
            $query = $this->db->query($sql, array($usID))->fetch();

            if($query and $query['blocked_workers'] != "") {
                $block_old = json_decode($query['blocked_workers'], true);
            }
            if(count($block_old)) {
                if(!in_array($blocked, $block_old)) {
                    $block_old[] = $blocked;
                }
            } else {
                $block_old[] = $blocked;
            }


            $block_old = json_encode($block_old);

            $sql = 'INSERT INTO blocked_workers(customer_id, blocked_workers) VALUES(?,?)
                ON DUPLICATE KEY UPDATE blocked_workers=?';

            return $this->db->query($sql, array($usID, $block_old, $block_old));
        }

        public function addBlockedOrders($usID, $blocked)
        {

            $sql = "SELECT * FROM blocked_orders WHERE user_id=?";
            $query = $this->db->query($sql, array($usID))->fetch();

            if($query and $query['blocked_orders'] != "") {
                $block_old = json_decode($query['blocked_orders'], true);
            }
            if(count($block_old)) {
                if(!in_array($blocked, $block_old)) {
                    $block_old[] = $blocked;
                }
            } else {
                $block_old[] = $blocked;
            }


            $block_old = json_encode($block_old);

            $sql = 'INSERT INTO blocked_orders(user_id, blocked_orders) VALUES(?,?)
                ON DUPLICATE KEY UPDATE blocked_orders=?';

            return $this->db->query($sql, array($usID, $block_old, $block_old));
        }

        public function getSubWorkersListByCoords($rub, $sub, $lat, $lng, $start, $limit, $stars = false, $distance = 25, $blocked_workers = false)
        {

            if(!$lat || !$lng)
                return false;
            $start = $start ? ($limit * ($start - 1)) : 0;

            if($stars) {
                $stars = " AND bm_users.stars = $stars ";
            }

            //Если поиск только по координатам
            if(!$rub or $rub == 0) {
                $sql = "SELECT bm_users.user_name, bm_users.user_phone, bm_users.user_email, bm_users.stars AS stars, bm_users.scale_trust, bm_users.scale_prof, bm_users.scale_cost, bm_users.id AS uid, bm_perfs_info.brief,
                       bm_users_places.route, bm_users_places.town, bm_users_places.district, bm_users_places.region,
(6371 * acos (
      cos ( radians({$lat}) )
      * cos( radians( bm_users_places.lat ) )
      * cos( radians( bm_users_places.lng ) - radians({$lng}) )
      + sin ( radians({$lat}) )
      * sin( radians( bm_users_places.lat ) )
    )
) AS distance
                FROM bm_users_places
                LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = bm_users_places.user_id
                LEFT JOIN bm_users ON bm_users.id = bm_users_places.user_id
               
            WHERE 
                bm_users.user_role_id IN (4,5,6)
                AND bm_perfs_info.brief IS NOT NULL
                {$stars}
                AND bm_perfs_info.brief <> '' ";
                if($blocked_workers) {
                    $blocked_workers = json_decode($blocked_workers['blocked_workers'], true);
                    $blocked_workers = implode(",", $blocked_workers);
                    $sql .= " AND bm_users.id NOT IN ({$blocked_workers})";
                }
                $sql .= " HAVING distance < {$distance} ORDER BY stars DESC, uid DESC LIMIT {$start}, {$limit}";
            } else {
                if($sub and $sub != 0) {
                    $sql = "SELECT user_id FROM builder_specializations WHERE builder_specializations.wrk_type_id = {$rub} AND builder_specializations.wrk_spec_id = {$sub}";
                } else {
                    $sql = "SELECT user_id FROM builder_specializations WHERE builder_specializations.wrk_type_id = {$rub}";
                }
                $res = $this->db->query($sql)->fetchAll();

                //если не удовлетворяет поиску прерываем
                if(!$res) {
                    return false;
                }
                foreach ($res as $r) {
                    $tmp[] = $r['user_id'];
                }
                $ids = implode(',', $tmp);

                $sql = "SELECT bm_users.user_name, bm_users.user_phone, bm_users.user_email, bm_users.stars AS stars,  bm_users.id AS uid, bm_perfs_info.brief, bm_users_places.route, bm_users_places.town, bm_users_places.district, bm_users_places.region,
(6371 * acos (
      cos ( radians({$lat}) )
      * cos( radians( bm_users_places.lat ) )
      * cos( radians( bm_users_places.lng ) - radians({$lng}) )
      + sin ( radians({$lat}) )
      * sin( radians( bm_users_places.lat ) )
    )
) AS distance
                FROM bm_users_places
                LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = bm_users_places.user_id
                LEFT JOIN bm_users ON bm_users.id = bm_users_places.user_id                
                WHERE
                    bm_users.user_role_id IN (4,5,6)
                AND bm_users.id IN ({$ids})                
                AND bm_perfs_info.brief IS NOT NULL
                AND bm_perfs_info.brief <> ''
                {$stars}";
                if($blocked_workers) {
                    $blocked_workers = json_decode($blocked_workers['blocked_workers'], true);
                    $blocked_workers = implode(",", $blocked_workers);
                    $sql .= " AND bm_users.id NOT IN ({$blocked_workers})";
                }
                $sql .= " HAVING distance < {$distance} ORDER BY distance ASC, stars DESC, uid DESC LIMIT {$start}, {$limit}"; //var_dump($sql); die;
            }

            $res = $this->db->query($sql)->fetchAll();

            $CTR_filter = $this->getCTRUsersByCoords($res, $lat, $lng, $rub, $sub);

            if($CTR_filter) {
                $res = array_merge($res, $CTR_filter);
                shuffle($res);
            }

            /* Обновляем счетчик показов */
            if(is_array($res) and count($res) > 0) {
                $all_id = $this->getUsIdsFromQuery($res, 'uid');
                $this->updateUserViews($all_id);
            }
            /* Обновляем счетчик показов */

            return $res;
        }

        public function getSubWorkersRefListByCoords($rub, $sub, $lat, $lng, $start, $limit, $distance = 25)
        {
            if(!$lat || !$lng)
                return false;
            $start = $start ? ($limit * ($start - 1)) : 0;

            $sql = 'SELECT ref_id, parent_ref_id FROM bm_reference_spec WHERE spec_id = ?';
            $res = $this->db->query($sql, array($sub_id))->fetchAll();
            if(!$res)
                return false;
            $sids = array();
            foreach ($res as $r) {
                $sids[] = $r['ref_id'];
            }
            $sids = implode(',', $sids);

            $sql = 'SELECT bm_users.user_name, bm_users.user_phone, bm_users.user_email, bm_users.id AS uid, bm_perfs_info.brief,
                       bm_users_places.route, bm_users_places.town, bm_users_places.district, bm_users_places.region,
(6371 * acos (
      cos ( radians(?) )
      * cos( radians( bm_users_places.lat ) )
      * cos( radians( bm_users_places.lng ) - radians(?) )
      + sin ( radians(?) )
      * sin( radians( bm_users_places.lat ) )
    )
) AS distance
                FROM bm_users_places
                LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = bm_users_places.user_id
                LEFT JOIN bm_users ON bm_users.id = bm_users_places.user_id
                LEFT JOIN builder_specializations ON builder_specializations.user_id = bm_users_places.user_id
                WHERE bm_users.user_role_id > 3 AND bm_users.user_role_id < 7 AND bm_perfs_info.brief IS NOT NULL AND bm_perfs_info.brief <> "" AND bm_users.is_active = 1';
            $limit++;
            $sql .= " AND builder_specializations.wrk_spec_id IN ({$sids}) HAVING distance < {$distance} ORDER BY distance ASC, uid DESC LIMIT {$start}, {$limit}"; //var_dump($sql); die;
            return $this->db->query($sql, array($lat, $lng, $lat, $rub, $sub))->fetchAll();
        }

        public function getSubOrdersList($top_id, $sub_id, $start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $brief_letters_limit = $this->config->lists->big_brief;
            $sql = 'SELECT bm_orders.id, bm_orders.order_brief AS descr_short, work_types.type_name, specializations.spec_name,
                DATE_FORMAT(bm_orders.order_date,"%d/%m/%Y (%H:%i)") AS order_date,
                UNIX_TIMESTAMP(bm_orders.actual_period) AS finish_date, UNIX_TIMESTAMP(bm_orders.order_end) AS close_date,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region
                FROM bm_orders
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                WHERE bm_orders.order_wrk_type = ? AND bm_orders.order_wrk_spec = ? AND bm_orders.order_status = 1';
            $limit++;
            $sql .= " ORDER BY bm_orders.order_date DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql, array($top_id, $sub_id))->fetchAll();
        }

        public function getadmin_info($uID)
        {
            $sql = 'SELECT description FROM bm_admin_info WHERE user_id = ?';
            return $this->db->query($sql, [$uID])->fetch();
        }

        public function getSubOrdersRefList($top_id, $sub_id, $start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $brief_letters_limit = $this->config->lists->big_brief;

            $sql = 'SELECT ref_id, parent_ref_id FROM bm_reference_spec WHERE spec_id = ?';
            $res = $this->db->query($sql, array($sub_id))->fetchAll();
            if(!$res)
                return false;
            $sids = array();
            foreach ($res as $r) {
                $sids[] = $r['ref_id'];
            }
            $sids = implode(',', $sids);

            $sql = 'SELECT bm_orders.id, bm_orders.order_brief AS descr_short, work_types.type_name, specializations.spec_name,
                DATE_FORMAT(bm_orders.order_date,"%d/%m/%Y (%H:%i)") AS order_date,
                UNIX_TIMESTAMP(bm_orders.actual_period) AS finish_date, UNIX_TIMESTAMP(bm_orders.order_end) AS close_date,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region
                FROM bm_orders
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                WHERE bm_orders.order_status = 1';
            $limit++;
            $sql .= " AND bm_orders.order_wrk_spec IN ({$sids}) ORDER BY bm_orders.order_date DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql, array($top_id, $sub_id))->fetchAll();
        }

        public function getSubWorkersList($top_id, $sub_id, $start, $limit, $stars = false)
        {
            if($stars) {
                $stars = " AND bm_users.stars = $stars ";
            }

            $start = $start ? ($limit * ($start - 1)) : 0;
            $sql = "SELECT bm_users.user_name,bm_users.user_phone, bm_users.stars AS stars, bm_users.scale_trust, bm_users.scale_prof, bm_users.scale_cost, bm_users.user_email, bm_users.id AS uid, bm_perfs_info.brief
                FROM builder_specializations
                LEFT JOIN bm_users ON bm_users.id = builder_specializations.user_id
                LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = builder_specializations.user_id
                WHERE 
                bm_users.user_role_id > 3
                AND bm_users.user_role_id < 7
                AND builder_specializations.wrk_type_id = ?
                AND builder_specializations.wrk_spec_id = ?
                {$stars}
                AND bm_perfs_info.brief IS NOT NULL
                AND bm_perfs_info.brief <> ''                
                AND bm_users.is_active = 1";
            // where user role check?

            $limit++;
            $sql .= " GROUP BY uid ORDER BY stars DESC LIMIT {$start}, {$limit}";
            $res = $this->db->query($sql, array($top_id, $sub_id))->fetchAll();
            if(!$res)
                return false;


            $CTR_filter = $this->getCTRUsers($res, $top_id, $sub_id);
            if($CTR_filter) {
                $res = array_merge($res, $CTR_filter);
                shuffle($res);
            }

            /* Обновляем счетчик показов */
            if(is_array($res) and count($res) > 0) {
                $all_id = $this->getUsIdsFromQuery($res, 'uid');
                $this->updateUserViews($all_id);
            }
            /* Обновляем счетчик показов */
            return $this->getWPlaces($res);
        }

        public function getSubWorkersRefList($top_id, $sub_id, $start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;

            $sql = 'SELECT ref_id, parent_ref_id FROM bm_reference_spec WHERE spec_id = ?';
            $res = $this->db->query($sql, array($sub_id))->fetchAll();
            if(!$res)
                return false;
            $sids = array();
            foreach ($res as $r) {
                $sids[] = $r['ref_id'];
            }
            $sids = implode(',', $sids);

            $sql = 'SELECT bm_users.user_name, bm_users.user_email, bm_users.id AS uid, bm_perfs_info.brief
                FROM builder_specializations
                LEFT JOIN bm_users ON bm_users.id = builder_specializations.user_id
                LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = builder_specializations.user_id
                WHERE bm_users.user_role_id > 3 AND bm_users.user_role_id < 7 AND bm_perfs_info.brief IS NOT NULL AND bm_perfs_info.brief <> "" AND bm_users.is_active = 1';
            // where user role check?

            $limit++;
            $sql .= " AND builder_specializations.wrk_spec_id IN ({$sids}) GROUP BY uid ORDER BY RAND() LIMIT {$start}, {$limit}";
            $res = $this->db->query($sql)->fetchAll();
            if(!$res)
                return false;
            return $this->getWPlaces($res);
        }

        public function getShops($start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $sql = 'SELECT bm_users.user_name, bm_users.id AS uid, DATE_FORMAT(bm_users.user_reg_date, "%d/%m/%Y (%H:%i)") AS user_reg_date, bm_shops_info.brief
                FROM bm_users
                LEFT JOIN bm_shops_info ON bm_shops_info.shop_id = bm_users.id
                WHERE bm_users.user_role_id = 7';
            $sql .= " ORDER BY bm_users.user_reg_date DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql)->fetchAll();
        }

        public function getShopNetworks()
        {
            $sql = 'SELECT * FROM bm_shop_networks ORDER BY net_name DESC';
            return $this->db->query($sql)->fetchAll();
        }

        public function getOrder($order_id, $user_id = false)
        {
            $sql = 'SELECT bm_orders.id, bm_orders.informed_users, bm_users.user_phone, bm_orders.user_id, bm_orders.order_description, bm_orders.order_admin_description, work_types.slug AS wts, work_types.type_name, specializations.slug AS sps, specializations.spec_name,
                DATE_FORMAT(bm_orders.order_date,"%d/%m/%Y") AS order_date, bm_budget_list.budget_name, bm_orders.user_id,
                bm_call_count.count_name, bm_orders.order_wrk_type, bm_orders.order_wrk_spec, bm_orders.order_budget,
                bm_orders.when_start, bm_orders.order_status, bm_when_list.when_name,
                DATE_FORMAT(bm_orders.actual_period,"%d/%m/%Y") AS actual_period, bm_orders.place_district_id,
                bm_orders.order_call_count, UNIX_TIMESTAMP(bm_orders.actual_period) AS finish_date, UNIX_TIMESTAMP(bm_orders.order_end) AS close_date, UNIX_TIMESTAMP(bm_orders.order_date) AS create_date,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region,
                bm_orders_places.lat, bm_orders_places.lng, bm_orders.compare_materials, compare_materials_notes
                FROM bm_orders
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
                LEFT JOIN bm_budget_list ON bm_budget_list.id = bm_orders.order_budget
                LEFT JOIN bm_call_count ON bm_call_count.id = bm_orders.order_call_count
                LEFT JOIN bm_when_list ON bm_when_list.id = bm_orders.when_start
                LEFT JOIN bm_users ON bm_users.id = bm_orders.user_id
                WHERE bm_orders.id = ?';
            $data = array($order_id);
            if($user_id) {
                $sql .= ' AND bm_orders.user_id = ?';
                $data[] = $user_id;
            }
            return $this->db->query($sql, $data)->fetch();
        }

        public function getLastOrder_new($user_id)
        {
            $sql = "SELECT bm_orders.id, bm_orders.user_id, bm_orders.order_description, work_types.slug AS wts, work_types.type_name, specializations.slug AS sps, specializations.spec_name,
                DATE_FORMAT(bm_orders.order_date,\"%d/%m/%Y\") AS order_date, bm_budget_list.budget_name, bm_orders.user_id,
                bm_call_count.count_name, bm_orders.order_wrk_type, bm_orders.order_wrk_spec, bm_orders.order_budget,
                bm_orders.when_start, bm_orders.order_status, bm_when_list.when_name,
                DATE_FORMAT(bm_orders.actual_period,\"%d/%m/%Y\") AS actual_period, bm_orders.place_district_id,
                bm_orders.order_call_count, UNIX_TIMESTAMP(bm_orders.actual_period) AS finish_date, UNIX_TIMESTAMP(bm_orders.order_end) AS close_date,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region,
                bm_orders_places.lat, bm_orders_places.lng
                FROM bm_orders
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                LEFT JOIN work_types ON work_types.id = bm_orders.order_wrk_type
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec
                LEFT JOIN bm_budget_list ON bm_budget_list.id = bm_orders.order_budget
                LEFT JOIN bm_call_count ON bm_call_count.id = bm_orders.order_call_count
                LEFT JOIN bm_when_list ON bm_when_list.id = bm_orders.when_start
                WHERE bm_orders.id = (SELECT id FROM bm_orders WHERE user_id = {$user_id} ORDER BY id DESC LIMIT 1)";

            return $this->db->query($sql)->fetch();
        }

        public function getRandom($order_id, $sid)
        {
            $time = time();
            $sql = 'SELECT bm_orders.id, bm_orders.order_brief AS descr_short,
                specializations.spec_name, DATE_FORMAT(bm_orders.order_date,"%d/%m/%Y (%H:%i)") AS order_date,
                UNIX_TIMESTAMP(bm_orders.actual_period) AS finish_date, UNIX_TIMESTAMP(bm_orders.order_end) AS close_date,
                bm_orders_places.route, bm_orders_places.town, bm_orders_places.district, bm_orders_places.region
                FROM bm_orders
                LEFT JOIN bm_orders_places ON bm_orders_places.order_id = bm_orders.id
                LEFT JOIN specializations ON specializations.id = bm_orders.order_wrk_spec';
            $sql .= " WHERE bm_orders.order_wrk_spec = {$sid} AND bm_orders.order_status = 1
                AND UNIX_TIMESTAMP(bm_orders.actual_period) < {$time} AND bm_orders.id <> {$order_id}";
            $sql .= " ORDER BY RAND() DESC LIMIT 0, 3"; //var_dump($sql);
            return $this->db->query($sql)->fetchAll();
        }

        public function closeOrder($order_id)
        {
            $level = $this->getDi()->getShared('session')->get('usr_level');
            $sql = 'UPDATE bm_orders SET order_end = NOW() WHERE id = ?';
            $data = array($order_id);
            if($level > 2) {
                $my_id = $this->getDi()->getShared('session')->get('usr_id');
                $sql .= ' AND user_id = ?';
                $data[] = $my_id;
            }
            $this->db->query($sql, $data);
        }

        public function openOrder($order_id)
        {
            $new_period = date('Y-m-d H:i:s', strtotime('+7 days'));
            $level = $this->getDi()->getShared('session')->get('usr_level');
            $sql = 'UPDATE bm_orders SET order_end = NULL, actual_period = ? WHERE id = ?';
            $data = array($new_period, $order_id);
            if($level > 2) {
                $my_id = $this->getDi()->getShared('session')->get('usr_id');
                $sql .= ' AND user_id = ?';
                $data[] = $my_id;
            }
            $this->db->query($sql, $data);
        }

        public function setConnect($order_id, $user_id, $type, $text = false)
        {

            if(!$user_id)
                return;

            $my_id = $this->getDi()->getShared('session')->get('usr_id');
            switch ($type)
            {
                case 'customer':
                    $sql = 'SELECT bm_orders_connect.* FROM bm_orders_connect
                        LEFT JOIN bm_orders ON bm_orders.id = bm_orders_connect.order_id
                        WHERE bm_orders_connect.order_id = ? AND bm_orders_connect.choice_id = ?
                        AND bm_orders.user_id = ?';
                    $res = $this->db->query($sql, array($order_id, $user_id, $my_id))->fetch();
                    if(!$res) {
                        $sql = 'INSERT bm_orders_connect (order_id, choice_id) VALUES (?, ?)';
                        $this->db->query($sql, array($order_id, $user_id));
                        return true;
                    } else {
                        $sql = 'DELETE FROM bm_orders_connect WHERE order_id = ? AND choice_id = ?';
                        $this->db->query($sql, array($order_id, $user_id));
                        return false;
                    }
                    break;
                case 'performer':
                    if(!$text)
                    //return false;
                        $text = $this->config->other->connect_text;
                    $sql = 'SELECT * FROM bm_orders_connect WHERE order_id = ? AND offer_id = ?';
                    $res = $this->db->query($sql, array($order_id, $user_id))->fetch();
                    if(!$res) {
                        $sql = 'INSERT bm_orders_connect (order_id, offer_id, offer_text) VALUES (?, ?, ?)';
                        $text = nl2br($text);
                        $this->db->query($sql, array($order_id, $user_id, $text));
                        return true;
                    } else {
                        $sql = 'DELETE FROM bm_orders_connect WHERE order_id = ? AND offer_id = ?';
                        $this->db->query($sql, array($order_id, $user_id));
                        return false;
                    }
                    break;
                default:
                    return false;
                    break;
            }
        }

        public function deleteConnect($connect_id, $user_id)
        {
            $sql = 'SELECT * FROM bm_orders_connect WHERE id =?';
            $res = $this->db->query($sql, array($connect_id))->fetch();
            $order_id = $res['order_id'];
            if(!$res)
                return false;

            if($res['offer_id'] == $user_id) {
                $sql = 'DELETE FROM bm_orders_connect WHERE id = ? AND offer_id = ?';
                $this->db->query($sql, array($connect_id, $user_id));
            } else {
                $sql = 'SELECT user_id FROM bm_orders WHERE id = ?';
                $order_owner = $this->db->query($sql, array($res['order_id']))->fetch()['user_id'];
                if(!$order_owner)
                    return false;
                if($order_owner != $user_id)
                    return false;

                $sql = 'DELETE FROM bm_orders_connect WHERE id = ?';
                $this->db->query($sql, array($connect_id));
            }
            return $order_id;
        }

        public function getMyOrderConnect($order_id)
        {
            $sql = 'SELECT bm_users.* FROM
                    bm_orders_connect
                LEFT JOIN
                    bm_users ON bm_users.id = bm_orders_connect.choice_id
                WHERE 
                    bm_orders_connect.order_id IN (' . implode(',', is_array($order_id) ? $order_id : [$order_id]) . ')
                AND
                    bm_orders_connect.offer_id IS NULL';

            $res = $this->db->query($sql)->fetchAll();
            return $res;
        }

        public function getMyOrderOffer($order_id)
        {
            $sql = 'SELECT bm_orders_connect.*, bm_users.user_name, bm_users.user_phone, bm_users.id AS uid
                FROM bm_orders_connect
                LEFT JOIN bm_users ON bm_users.id = bm_orders_connect.offer_id
                WHERE bm_orders_connect.order_id = ? AND bm_orders_connect.offer_id IS NOT NULL';
            return $this->db->query($sql, array($order_id))->fetchAll();
        }

        public function getOrderConnect($order_id)
        {
            $sql = 'SELECT bm_orders_connect.*, bm1.user_name AS c_user_name, bm1.id AS c_uid, bm2.user_name AS o_user_name, bm2.id AS o_uid
                FROM bm_orders_connect
                LEFT JOIN bm_users bm1 ON bm1.id = bm_orders_connect.choice_id
                LEFT JOIN bm_users bm2 ON bm2.id = bm_orders_connect.offer_id
                WHERE bm_orders_connect.order_id = ?';
            $res = $this->db->query($sql, array($order_id))->fetchAll();
            $count = count($res);
            $choice = array();
            $offer = array();
            for ($i = 0; $i < $count; $i++) {
                if($res[$i]['choice_id'])
                    $choice[] = $res[$i];
                else
                    $offer[] = $res[$i];
            }
            return array('choice' => $choice, 'offer' => $offer);
        }

        public function getCountConnectsByUser($user_id)
        {
            $sql = 'SELECT COUNT(id) FROM bm_orders_connect WHERE bm_orders_connect.offer_id = ?';
            return $this->db->query($sql, array($user_id))->fetch();
        }

        public function createOrder($d)
        {
            $ip = $_SERVER['REMOTE_ADDR'];
            $te = $this->getDi()->getShared('textengine');
            $brief = $te->getBrief($d['description'], $this->config->lists->big_brief);
            $sql = 'INSERT INTO bm_orders
                    (order_brief, order_description, user_id, user_ip)
                    VALUES (?, ?, ?, ?)';
            $data = array($brief, $d['description'], $d['user_id'], $ip);
            $this->db->query($sql, $data);
            return $this->db->lastInsertId();
            // NEED REFACT
        }

        public function createOrderByWorker($d)
        {
            $ip = $_SERVER['REMOTE_ADDR'];
            $te = $this->getDi()->getShared('textengine');
            $brief = $te->getBrief($d['description'], $this->config->lists->big_brief);
            $sql = 'INSERT INTO bm_orders
                    (order_brief, order_description, user_id, user_ip,premium_block)
                    VALUES (?, ?, ?, ?, ?)';
            $data = array($brief, $d['description'], $d['user_id'], $ip, '0');
            $this->db->query($sql, $data);
            return $this->db->lastInsertId();
            // NEED REFACT
        }

        public function createNewOrder($d)
        {
            $ip = $_SERVER['REMOTE_ADDR'];
            $te = $this->getDi()->getShared('textengine');
            $brief = $te->getBrief($d['description'], $this->config->lists->big_brief);
            $sql = 'INSERT INTO bm_orders
                (order_brief, order_description, user_id, user_ip)
                VALUES (?, ?, ?, ?)';
            $data = array($brief, $d['description'], $d['user_id'], $ip);
            $this->db->query($sql, $data);
            return $this->db->lastInsertId();
        }

        public function updateOrder($d)
        {
            $te = $this->getDi()->getShared('textengine');
            $brief = $te->getBrief($d['description'], $this->config->lists->big_brief);
            $sql = 'UPDATE bm_orders SET
                order_brief = ?, order_description = ?, order_admin_description = ?, order_budget = ?, order_call_count = ?, when_start = ?, order_wrk_type = ?, order_wrk_spec = ?,
                order_status = 1, compare_materials=?, compare_materials_notes=?';
            $data = array($brief, $d['description'], $d['admin_description'], $d['bugdet'], $d['limcall'], $d['start'], $d['speciality'], $d['subspeciality'], $d['compare_materials'], $d['compare_materials_notes']);

            if($d['actual_period']) {
                $sql .= ', actual_period = ?';
                $data[] = $d['actual_period'];
            }

            if(isset($d['user_level']) && $d['user_level'] < 3) {
                $sql .= ' WHERE id = ?';
                $data[] = $d['order_id'];
            } else {
                $sql .= ' WHERE user_id = ? AND id = ?';
                $data[] = $d['user_id'];
                $data[] = $d['order_id'];
            }

            $this->db->execute($sql, $data);
            return $this->db->affectedRows();
        }

        public function getBudgetList()
        {
            $sql = 'SELECT id, budget_name FROM bm_budget_list';
            return $this->db->query($sql)->fetchAll();
        }

        public function getCallCountList()
        {
            $sql = 'SELECT id, count_name FROM bm_call_count';
            return $this->db->query($sql)->fetchAll();
        }

        public function getWhenList()
        {
            $sql = 'SELECT id, when_name FROM bm_when_list';
            return $this->db->query($sql)->fetchAll();
        }

        public function getPromoWorkers($user_id)
        {
            $sql = 'SELECT user_place_id, user_region_id, user_district_id FROM bm_users WHERE id = ?';
            $usr = $this->db->query($sql, array($user_id))->fetch();
            if($usr['user_place_id']) {
                $sql = 'SELECT bm_users.user_name, bm_users.id AS uid, bm_perfs_info.brief,
                           bm_places.place_town, bm_places.place_district, bm_places.place_region, bm_places.is_district
                    FROM bm_users
                    LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = bm_users.id
                    LEFT JOIN bm_places ON bm_places.place_id = bm_users.user_place_id
                    WHERE bm_places.place_id = ? AND bm_users.user_role_id > 3 AND bm_users.user_role_id < 7 AND bm_perfs_info.brief IS NOT NULL AND bm_perfs_info.brief <> "" AND bm_users.is_active = 1
                    LIMIT 5';
                $res = $this->db->query($sql, array($usr['user_place_id']))->fetchAll();
            }

            $sql = 'SELECT bm_users.user_name, bm_users.id AS uid, bm_perfs_info.brief,
                       bm_places.place_town, bm_places.place_district, bm_places.place_region, bm_places.is_district
                FROM bm_users
                LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = bm_users.id
                LEFT JOIN bm_places ON bm_places.place_id = bm_users.user_place_id
                WHERE bm_users.user_role_id > 3 AND bm_users.user_role_id < 7 AND bm_perfs_info.brief IS NOT NULL AND bm_perfs_info.brief <> "" AND bm_users.is_active = 1
                LIMIT 5';
            $res2 = $this->db->query($sql)->fetchAll();

            return isset($res) ? array_merge($res, $res2) : $res2;
        }

        private function getWPlaces($res)
        {
            $uids = array();
            $res_count = count($res);
            foreach ($res as $r) {
                $uids[] = $r['uid'];
            }
            $uids = implode(',', $uids);

            $tmp = array();
            $sql = "SELECT user_id, place_id, bm_users_places.route, bm_users_places.town, bm_users_places.district, bm_users_places.region
                FROM bm_users_places WHERE user_id IN ({$uids}) GROUP BY user_id";
            $places = $this->db->query($sql)->fetchAll();
            for ($i = 0; $i < $res_count; $i++) {
                foreach ($places as $p) {
                    if($p['user_id'] == $res[$i]['uid']) {
                        $res[$i]['route'] = $p['route'];
                        $res[$i]['town'] = $p['town'];
                        $res[$i]['district'] = $p['district'];
                        $res[$i]['region'] = $p['region'];
                    }
                }
            }
            return $res;
        }

        public function set_calback_data($data)
        {
            $sql = "INSERT INTO callback_form SET phone=?, name=?, locality=?, description=? ";
            $val = array($data['phone'], $data['name'], $data['locality'], $data['description']);
            $this->db->query($sql, $val);
            return $this->db->lastInsertId();
        }

        public function setBlockSMS($order_id)
        {
            $sql = "INSERT INTO cron_blocked SET order_id=?";
            return $this->db->query($sql, array($order_id));
        }

        public function deleteBlockSMS($order_id)
        {
            $sql = "DELETE FROM cron_blocked WHERE order_id=?";
            return $this->db->query($sql, array($order_id));
        }

        public function setIsOldStatus($order_id, $status)
        {
            $sql = "UPDATE bm_orders SET show_is_old = ? WHERE id = ?";
            return $this->db->query($sql, array($status, $order_id));
        }

        public function setPremiumStatusByOrder($order_id, $status)
        {
            $sql = "UPDATE bm_orders SET premium_block = ? WHERE id=?";
            return $this->db->query($sql, array($status, $order_id));
        }

        public function getStatusSMSByOrder($order_id)
        {
            $sql = "SELECT * FROM cron_blocked WHERE order_id=?";
            return $this->db->query($sql, array($order_id))->fetch();
        }

        public function getStatusPremiumBlockByOrder($order_id)
        {
            $sql = "SELECT premium_block FROM bm_orders WHERE id=?";
            return $this->db->query($sql, array($order_id))->fetch()['premium_block'];
        }

        public function getCTRUsers($selected_users, $spec_id, $sub_spec_id = false, $lat = false, $lng = false)
        {

            if(is_array($selected_users)) {
                foreach ($selected_users as $key => $user) {
                    if($u_ids != '')
                        $u_ids .= ',';
                    $u_ids .= $user['uid'];
                }
            }
            
            if($u_ids!='')
                $selected_users = "AND bm_users.id NOT IN ($u_ids)";
            else
                $selected_users = '';

            if(!$spec_id)
                $spec_id = 0;

            $filter = $this->getCTR_filter($spec_id);

            if($filter['limit_max'] > 0) {
                $limit = "LIMIT " . $filter['limit_max'];
            } else {
                $limit = "LIMIT 10";
            }

            if($filter['photo_priority'] > 0) {
                $photo_join = "LEFT JOIN bm_files ON bm_files.user_id = builder_specializations.user_id";
                $add .= " AND bm_files.user_id = bm_users.id
                    AND bm_files.ftype = 'image' ";
            }

            if($filter['stars_min'] > 0 and $filter['stars_max'] > 0) {
                $add .= "AND stars BETWEEN " . $filter['stars_min'] . " AND " . $filter['stars_max'];
            } else if($filter['stars_min'] > 0) {
                $add .= " AND bm_users.stars >=" . $filter['stars_min'];
            } else if($filter['stars_max'] > 0) {
                $add .= " AND bm_users.stars <=" . $filter['stars_max'];
            }

            if($filter['views_min'] > 0) {
                $add .= " AND bm_users.user_views >=" . $filter['views_min'];
            }
            if($filter['views_max'] > 0) {
                $add .= " AND bm_users.user_views <=" . $filter['views_max'];
            }

            if($filter['logins_min'] > 0) {
                $add .= " AND bm_users.user_logins >=" . $filter['logins_min'];
            }
            if($filter['logins_max'] > 0) {
                $add .= " AND bm_users.user_logins <=" . $filter['logins_max'];
            }

            if($spec_id and $sub_spec_id) {
                $select_add = " builder_specializations.wrk_spec_id = " . $sub_spec_id;
            } else {
                $select_add = " builder_specializations.wrk_type_id = " . $spec_id;
            }

            $sql = "SELECT 
                    bm_users.user_email,
                    bm_users.avatar,
                    bm_users.user_phone,
                    bm_users.stars AS stars,
                    bm_users.user_name,
                    bm_users.id AS uid,
                    bm_perfs_info.brief 
                FROM builder_specializations
                LEFT JOIN bm_users ON bm_users.id = builder_specializations.user_id
                LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = builder_specializations.user_id
                $photo_join
                WHERE                    
                    $select_add
                    $selected_users
                    AND bm_perfs_info.brief <> ''
                    $add
                ";
            $sql .= "GROUP BY uid ORDER BY RAND() " . $limit;

            $res = $this->db->query($sql, array())->fetchAll();

            if(!$res)
                return false;

            return $res;
        }

        public function getCTRUsersByCoords($selected_users, $lat, $lng, $spec_id = false, $sub_spec_id = false)
        {

            if(is_array($selected_users)) {
                foreach ($selected_users as $key => $user) {
                    if($u_ids != '')
                        $u_ids .= ',';
                    $u_ids .= $user['uid'];
                }
            }

            if($u_ids!='')
                $selected_users = "AND bm_users.id NOT IN ($u_ids)";
            else
                $selected_users = '';
            
            if(!$lat || !$lng)
                return false;

            if(!$spec_id)
                $spec_id = 0;

            $filter = $this->getCTR_filter($spec_id);
            if($filter['limit_max'] > 0) {
                $limit = "LIMIT " . $filter['limit_max'];
            } else {
                $limit = "LIMIT 10";
            }


            if($filter['photo_priority'] > 0) {
                $photo_join = "LEFT JOIN bm_files ON bm_files.user_id = builder_specializations.user_id";
                $add .= " AND bm_files.user_id = bm_users.id
                    AND bm_files.ftype = 'image' ";
            }

            if($filter['stars_min'] > 0 and $filter['stars_max'] > 0) {
                $add .= "AND stars BETWEEN " . $filter['stars_min'] . " AND " . $filter['stars_max'];
            } else if($filter['stars_min'] > 0) {
                $add .= " AND bm_users.stars >=" . $filter['stars_min'];
            } else if($filter['stars_max'] > 0) {
                $add .= " AND bm_users.stars <=" . $filter['stars_max'];
            }


            if($filter['views_min'] > 0) {
                $add .= " AND bm_users.user_views >=" . $filter['views_min'];
            }
            if($filter['views_max'] > 0) {
                $add .= " AND bm_users.user_views <=" . $filter['views_max'];
            }

            if($filter['logins_min'] > 0) {
                $add .= " AND bm_users.user_logins >=" . $filter['logins_min'];
            }
            if($filter['logins_max'] > 0) {
                $add .= " AND bm_users.user_logins <=" . $filter['logins_max'];
            }

            if($spec_id > 0 and $sub_spec_id > 0) {
                $select_add = " wrk_spec_id = " . $sub_spec_id;
            } else {
                $select_add = " wrk_type_id = " . $spec_id;
            }

            if($spec_id > 0) {
                $sql = " SELECT 
                        user_id
                    FROM
                        builder_specializations
                    WHERE 
                        $select_add
            ";

                $res = $this->db->query($sql, array())->fetchAll();
                $all_id = $this->getUsIdsFromQuery($res, 'user_id');
                $add_In = " bm_users_places.user_id IN ($all_id) ";
            } else {
                $add_In = " 1 = 1 ";
            }


            $sql = " SELECT 
                    bm_users.user_name,
                    bm_users.user_phone,
                    bm_users.avatar,
                    bm_users.stars AS stars,
                    bm_users.user_email,
                    bm_users.id AS uid, 
                    bm_perfs_info.brief,
                    bm_users_places.route,
                    bm_users_places.town,
                    bm_users_places.district,
                    bm_users_places.region,
                    (6371 * acos (
                            cos ( radians(?) )
                            * cos( radians( bm_users_places.lat ) )
                            * cos( radians( bm_users_places.lng ) - radians(?) )
                            + sin ( radians(?) )
                            * sin( radians( bm_users_places.lat ) )
                          )
                      ) AS distance
                FROM bm_users_places
                    LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = bm_users_places.user_id
                    LEFT JOIN bm_users ON bm_users.id = bm_users_places.user_id
                    $photo_join
                WHERE 
                    $add_In
                    $add
                    $selected_users    
                HAVING distance < 40
                ORDER BY RAND() $limit
            ";

            $res = $this->db->query($sql, array($lat, $lng, $lat))->fetchAll();

            return $res;
        }

        public function getCTR_filter($spec_id)
        {

            /* Получаем значения в фильтре */
            $sql = "SELECT * FROM ctr_filter WHERE spec_id =?";
            $filter = $this->db->query($sql, array($spec_id))->fetch();

            if(!$filter) {
                $sql = "SELECT * FROM ctr_filter WHERE spec_id =0";
                $filter = $this->db->query($sql, array())->fetch();
            }

            /* Получаем минимальные и максимальные значения в фильтре */
            if($spec_id > 0) {
                $add = "LEFT JOIN 
                builder_specializations ON builder_specializations.user_id = bm_users.id
              WHERE 
                wrk_type_id =$spec_id";
            }

            $sql = "SELECT
                MIN(user_views) as views_min,
                MAX(user_views) as views_max,
                MIN(user_logins) as logins_min,
                MAX(user_logins) as logins_max
              FROM
                bm_users
              $add
            ";
            $val = $this->db->query($sql, array())->fetch();

            /* Высчитываем процентные значения из фильтра */

            $filter['views_min'] = ceil($val['views_min'] / 100 * $filter['views_min']);
            $filter['views_max'] = ceil($val['views_max'] / 100 * $filter['views_max']);
            $filter['logins_min'] = ceil($val['logins_min'] / 100 * $filter['logins_min']);
            $filter['logins_max'] = ceil($val['logins_max'] / 100 * $filter['logins_max']);


            return $filter;
        }

        public function getCommercialsListByCoords($user_id = false)
        {
            if(!$user_id)
                $user_id = $this->session->get('usr_id');
            /* Получаем данные пользователя */
            $sql = "SELECT 
                        *
                FROM
                        bm_users 
                LEFT JOIN
                        bm_users_places ON bm_users_places.user_id = bm_users.id
                 WHERE
                        bm_users.id = ?";

            $user = $this->db->query($sql, array($user_id))->fetch();



            if(!$user)
                return false;
            $user['spec'] = Catalog::getUsetCategories($user['user_id']);

            if(count($user['spec']['wrk_spec_id']) > 0) {

                foreach ($user['spec']['wrk_spec_id'] as $key => $value) {

                    if($key == 0) {
                        $add = 'AND (';
                    } else {
                        $add .= ' OR ';
                    }
                    $add .= " wrk_spec_id LIKE '%[" . $value . "]%' ";
                }
                $add .= ' ) ';
            }


            /* Выбираем все рекламыне блоки подходящие под юзера */
            $sql = "SELECT 
                        *,
                       (6371 * acos (
                    cos ( radians(?) )
                    * cos( radians( bm_shop_places.lat ) )
                    * cos( radians( bm_shop_places.lng ) - radians(?) )
                    + sin ( radians(?) )
                    * sin( radians( bm_shop_places.lat ) )
                    )
                  ) AS distance_to_shop,
                  shop_commercial.id as commercial_id
                FROM
                        shop_commercial
                LEFT JOIN
                        bm_shop_places ON bm_shop_places.shop_id = shop_commercial.shop_id
                LEFT JOIN
                        bm_shops_info ON bm_shops_info.shop_id = shop_commercial.shop_id
                WHERE
                         shop_commercial.active = '1'
                    AND
                        user_role LIKE  '%[" . $user['user_role_id'] . "]%'
                    AND
                        displayed_users NOT LIKE '%[" . $user['user_id'] . "]%'
                        $add                        
                HAVING 
                        distance_to_shop < shop_commercial.distance
                ORDER BY
                        `create` ASC
        ";

            $commercials = $this->db->query($sql, [$user['lat'], $user['lng'], $user['lat']])->fetchALL();


            if(count($commercials) < 1) {
                /* Выбираем по верхней специальности */

                if(count($user['spec']['wrk_type_id']) > 0) {

                    foreach ($user['spec']['wrk_type_id'] as $key => $value) {
                        if($key == 0) {
                            $add = 'AND (';
                        } else {
                            $add .= ' OR ';
                        }
                        $add .= " wrk_type_id LIKE '%[" . $value . "]%' ";
                    }
                    $add .= ' ) ';
                }

                /* Выбираем все рекламыне блоки подходящие под юзера */
                $sql = "SELECT 
                        *,
                       (6371 * acos (
                    cos ( radians(?) )
                    * cos( radians( bm_shop_places.lat ) )
                    * cos( radians( bm_shop_places.lng ) - radians(?) )
                    + sin ( radians(?) )
                    * sin( radians( bm_shop_places.lat ) )
                    )
                  ) AS distance_to_shop,
                  shop_commercial.id as commercial_id
                FROM
                        shop_commercial
                LEFT JOIN
                        bm_shop_places ON bm_shop_places.shop_id = shop_commercial.shop_id
                LEFT JOIN
                        bm_shops_info ON bm_shops_info.shop_id = shop_commercial.shop_id
                WHERE
                        shop_commercial.active = '1'
                    AND
                        user_role LIKE  '%[" . $user['user_role_id'] . "]%'
                    AND
                        displayed_users NOT LIKE '%[" . $user['user_id'] . "]%' 
                        $add                        
                HAVING 
                        distance_to_shop < shop_commercial.distance                        
                ORDER BY
                        `create` ASC
        ";

                $commercials = $this->db->query($sql, [$user['lat'], $user['lng'], $user['lat']])->fetchALL();
            }


            return $commercials[0];
        }

        public function getUsetCategories($user_id)
        {

            $sql = 'SELECT wrk_type_id AS w_t_id, wrk_spec_id AS w_s_id
                FROM builder_specializations
                LEFT JOIN work_types ON work_types.id = builder_specializations.wrk_type_id
                LEFT JOIN specializations ON specializations.id = builder_specializations.wrk_spec_id
                WHERE builder_specializations.user_id = ? ORDER BY work_types.type_name';
            $res = $this->db->query($sql, array($user_id))->fetchAll();

            if(!$res)
                return false;

            foreach ($res as $key => $value) {
                $wrk_type_id[] = $value['w_t_id'];
                $wrk_spec_id[] = $value['w_s_id'];
            }
            $spec['wrk_type_id'] = array_values(array_unique($wrk_type_id));
            $spec['wrk_spec_id'] = array_values(array_unique($wrk_spec_id));

            return $spec;
        }

        public function setCommercialDisplayedUser($commercial_id, $user_id)
        {
            $sql = "UPDATE shop_commercial SET displayed_users = CONCAT(displayed_users,'[$user_id]') WHERE id= ?";

            $this->db->execute($sql, [$commercial_id]);
            return $this->db->affectedRows();
        }

        public function setUserShops($commercial_id, $user_id)
        {

            $sql = " SELECT * FROM bm_users_shop WHERE user_id = ? AND shop_id = (SELECT shop_id FROM shop_commercial WHERE id = ?) ";
            $res = $this->db->query($sql, [$user_id, $commercial_id])->fetchAll();

            if(count($res) > 0) {

                $sql = " 
            
            UPDATE
                bm_users_shop 
            SET                
                last_commercial_id = ?
            WHERE 
                user_id = ? AND shop_id = (SELECT shop_id FROM shop_commercial WHERE id = ?)

            ";
                $this->db->execute($sql, [$commercial_id, $user_id, $commercial_id]);
                $res = $this->db->affectedRows();
            } else {

                $sql = " 
            
            INSERT INTO 
                bm_users_shop
            SET    
                user_id = ?,
                shop_id = (SELECT shop_id FROM shop_commercial WHERE id = ?),
                last_commercial_id = ?

            ";
                $this->db->query($sql, [$user_id, $commercial_id, $commercial_id]);
                $res = $this->db->lastInsertId();
            }

            return $res;
        }

        public function get_user_shops($user_id)
        {
            $sql = "SELECT  *
            FROM
                bm_users_shop
            LEFT JOIN
                bm_shops_info ON bm_shops_info.shop_id = bm_users_shop.shop_id
            LEFT JOIN
                bm_shop_places ON bm_shop_places.shop_id = bm_users_shop.shop_id
            LEFT JOIN
                shop_commercial ON shop_commercial.id = bm_users_shop.last_commercial_id
            WHERE 
                bm_users_shop.user_id =?";
            $res = $this->db->query($sql, [$user_id])->fetchAll();
            return $res;
        }

        public function get_single_shop($shop_id, $user_id)
        {
            $sql = "
            SELECT *, shop_commercial.id AS commercial_id
            FROM
                shop_commercial
            LEFT JOIN
                bm_shop_places ON bm_shop_places.shop_id = shop_commercial.shop_id
            LEFT JOIN
                bm_shops_info ON bm_shops_info.shop_id = shop_commercial.shop_id 
            WHERE
                shop_commercial.shop_id = ?     
            AND 
                shop_commercial.id = (SELECT last_commercial_id FROM bm_users_shop WHERE shop_id=? AND user_id = ?)
        ";

            $res = $this->db->query($sql, [$shop_id, $shop_id, $user_id])->fetch();
            return $res;
        }

        public function setPremiumBlock($order, $user_id, $duration = 60)
        {

            $sql = "SELECT * FROM order_premium_block WHERE order_id=? AND status =?";
            $res = $this->db->query($sql, [$order['id'], "1"])->fetchAll();

            /* Если есть текущая блокировка - ставим в очередь, иначе ставим текущую премиум блокировку */
            if($res) {
                $sql = " INSERT INTO order_premium_series SET order_id=? , user_id=?, duration=?";
                $this->db->query($sql, [$order['id'], $user_id, $duration]);
                $q = $this->db->lastInsertId();
            } else {
                $sql = " INSERT INTO order_premium_block SET order_id=? , user_id=?, block_start=?, block_end=?";
                $this->db->query($sql, [$order['id'], $user_id, date("Y-m-d H:i:s", time()), date("Y-m-d H:i:s", time() + $duration)]);
                $q = $this->db->lastInsertId();
            }
            switch ($duration)
            {
                case 86400:
                    $duration = 18000;
                    break;
                case 172800:
                    $duration = 36000;
                    break;
                case 604800:
                    $duration = 90000;
                    break;
                default:
                    break;
            }
            $sql = "UPDATE bm_users SET premium_hours = premium_hours - ?, premium_hours_used = premium_hours_used + ? WHERE id = ?";
            $this->db->execute($sql, [$duration, $duration, $user_id]);



            return $this->db->affectedRows();
        }

        public function updatePremiumBlock($order, $user_id, $duration)
        {

            $sql = "SELECT UNIX_TIMESTAMP(block_end) AS block_end FROM order_premium_block WHERE order_id=? AND user_id=?";
            $res = $this->db->query($sql, [$order['id'], $user_id])->fetch();

            $new_end = date("Y-m-d H:i:s", $res['block_end'] + $duration);

            $sql = "UPDATE order_premium_block SET block_end = ? WHERE order_id=? AND user_id=?";
            $this->db->execute($sql, [$new_end, $order['id'], $user_id]);

            switch ($duration)
            {
                case 86400:
                    $duration = 18000;
                    break;
                case 172800:
                    $duration = 36000;
                    break;
                case 604800:
                    $duration = 90000;
                    break;
                default:
                    break;
            }

            $sql = "UPDATE bm_users SET premium_hours = premium_hours - ?, premium_hours_used = premium_hours_used + ? WHERE id = ?";
            $this->db->execute($sql, [$duration, $duration, $user_id]);
            return $this->db->affectedRows();
        }

        public function returnPremiumHours($user_id, $duration)
        {
            $sql = "UPDATE bm_users SET premium_hours = premium_hours + ?, premium_hours_used = premium_hours_used - ? WHERE id = ?";
            $this->db->execute($sql, [$duration, $duration, $user_id]);
        }

        public function deleteAllPremiumBlocks($order_id)
        {
            $sql = "DELETE FROM order_premium_block WHERE order_id=?";
            $this->db->execute($sql, [$order_id]);

            $sql = "DELETE FROM order_premium_block WHERE order_id=?";
            $this->db->execute($sql, [$order_id]);

            $sql = "DELETE FROM bm_orders_eternal_freeze WHERE order_id=?";
            $this->db->execute($sql, [$order_id]);
        }

        public function getCurrentPremiumBlockByOrder($order_id, $status = "1")
        {

            $sql = "SELECT * FROM order_premium_block WHERE order_id=? AND status = ? ";

            return $this->db->query($sql, [$order_id, $status])->fetch();
        }

        public function checkInformed($user, $order)
        {

            $sql = "SELECT * FROM cron WHERE phone=? AND order_id=? AND status = '1' ";

            return $this->db->query($sql, [$user['user_phone'], $order['id']])->fetch();
        }

        public function returnPremiumTime($user_id, $duration)
        {

            $sql = "UPDATE bm_users SET premium_hours = premium_hours + ?, premium_hours_used = premium_hours_used - ? WHERE id = ?";

            $this->db->execute($sql, [$duration, $duration, $user_id]);
            return $this->db->affectedRows();
        }

        public function deleteCurrentPremiumBlock($order_id)
        {
            $sql = "UPDATE order_premium_block SET status = ? WHERE order_id=?";
            $this->db->execute($sql, ["0", $order_id]);

            return $this->db->affectedRows();
        }

        public function deleteEndedPremiumBlock()
        {

            $sql = "UPDATE order_premium_block SET status = ? WHERE UNIX_TIMESTAMP(block_end) < ?";
            return $this->db->execute($sql, ["0", time()]);
        }

        public function getEndedPremiumBlockByOrderId($order_id)
        {

            $sql = "SELECT * FROM order_premium_block WHERE status = ? AND order_id=?";
            return $this->db->query($sql, ["0", $order_id])->fetchAll();
        }

        public function getSeriesPremiumBlock()
        {
            $sql = "SELECT * FROM order_premium_series";
            return $this->db->query($sql, [])->fetchAll();
        }

        public function getSeriesPremiumBlockById($order_id)
        {
            $sql = "SELECT * FROM order_premium_series WHERE order_id=?";
            return $this->db->query($sql, [$order_id])->fetchAll();
        }

        public function getSeriesPremiumBlockByOrderId($order_id)
        {
            $sql = "SELECT * FROM order_premium_series WHERE order_id = ?";
            return $this->db->query($sql, [$order_id])->fetchAll();
        }

        public function deleteSeries($id)
        {
            $sql = "DELETE FROM order_premium_series WHERE id=?";
            return $this->db->execute($sql, [$id]);
        }

        public function is_old_order($order_id, $duration = 5)
        {
            $time = time() - 3600 * 24 * $duration;
            $sql = "SELECT * FROM bm_orders WHERE show_is_old > '0' AND id=?";
            $res = $this->db->query($sql, [$order_id])->fetch();

            if(!$res)
                return 0;

            if($res['show_is_old'] > 0 or strtotime($res['order_date']) < $time)
                return $res['show_is_old'];
            else
                return 0;
        }

        public function show_is_old($order_id)
        {

            $sql = "SELECT show_is_old FROM bm_orders WHERE id=?";
            $res = $this->db->query($sql, [$order_id])->fetch();

            return $res['show_is_old'];
        }

        public function getOffersToWorker($user_id)
        {
            $sql = "
            SELECT *                 
                FROM
                    work_offers                
                LEFT JOIN
                    bm_users ON bm_users.id = work_offers.id_worker                
                WHERE id_customer=?
            
        ";
            $res = $this->db->query($sql, [$user_id])->fetchAll();

            return $res;
        }

        public function setOrderChoice($data)
        {
            $sql = "INSERT INTO bm_orders_choices SET order_id=?, customer_id=?, worker_id=?";
            $this->db->query($sql, [$data['order_id'], $data['customer_id'], $data['worker_id']]);

            return $this->db->lastInsertId();
        }

        public function countChoicesToday($order_id)
        {

            $sql = "SELECT * FROM bm_orders_connect WHERE bm_orders_connect.order_id = ? AND DATE(bm_orders_connect.cnct_date) = CURRENT_DATE AND bm_orders_connect.choice_id IS NOT NULL";

            return $this->db->query($sql, [$order_id])->fetchAll();
        }

        public function getLimitChoices($user_id)
        {
            $sql = "SELECT limit_choices FROM bm_users WHERE id=?";

            return $this->db->query($sql, [$user_id])->fetch()['limit_choices'];
        }

        public function setLimitChoices($user_id)
        {
            $sql = "UPDATE bm_users SET  limit_choices= (limit_choices-1) WHERE id=?";

            $this->db->execute($sql, [$user_id]);

            return $this->db->affectedRows();
        }

        public function setEternalFreeze($us_id, $order_id)
        {
            $sql = "SELECT id FROM bm_orders_eternal_freeze WHERE us_id=? and order_id=?";
            $q = $this->db->query($sql, [$us_id, $order_id])->fetchAll();
            if($q)
                return 1;

            $sql = "INSERT INTO bm_orders_eternal_freeze SET us_id=?, order_id=?";
            $this->db->query($sql, [$us_id, $order_id]);

            return $this->db->lastInsertId();
        }

        public function getEternalFreeze($order_id)
        {
            $sql = "SELECT * FROM bm_orders_eternal_freeze LEFT JOIN bm_users ON bm_users.id = bm_orders_eternal_freeze.us_id WHERE order_id=?";
            return $this->db->query($sql, [$order_id])->fetchAll();
        }

        public function deleteEternalFreeze($order_id)
        {
            
        }

        public function getSMSOrders($id = false)
        {
            $sql = "SELECT * FROM sms_orders ";
            if($id) {
                $sql .= "WHERE id=?";
            }

            return $this->db->query($sql, [$id ? $id : ''])->fetchAll();
        }

        public function setSMSOrderData($name, $text)
        {
            $sql = "INSERT INTO sms_orders SET name=?, text=? ";
            $this->db->execute($sql, [$name, $text]);

            return $this->db->lastInsertId();
        }

        public function updateSMSOrderData($id, $name, $text)
        {

            $sql = "UPDATE sms_orders SET name=?, text=? WHERE id=?";
            $this->db->execute($sql, [$name, $text, $id]);

            return $this->db->affectedRows();
        }

        public function deleteSMSOrder($id)
        {
            $sql = "DELETE FROM sms_orders WHERE id=?";
            $this->db->execute($sql, [$id]);

            return $this->db->affectedRows();
        }

        public function updateSMSOrderUsers($id, $users)
        {
            $sql = "UPDATE sms_orders SET sended_to=? WHERE id=?";
            $this->db->execute($sql, [$users, $id]);

            return $this->db->affectedRows();
        }

        public function getSubSpecInfo($id)
        {
            $sql = 'SELECT * FROM specializations WHERE id = ? ORDER BY id';
            return $this->db->query($sql, array($id))->fetch();
        }

    }
    