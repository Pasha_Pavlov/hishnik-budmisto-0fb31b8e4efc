<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/front/models/Users.php
     *
     * Users model
     *
     */

    namespace Budmisto\Modules\Front\Models;

    class Users extends \Phalcon\Mvc\Model
    {

        private $db;
        //private $lang;
        private $security;
        private $session;

        public function initialize()
        {
            $this->db = $this->getDi()->getShared('db');
            $this->security = $this->getDi()->getShared('security');
            $this->session = $this->getDi()->getShared('session');
        }

        /* public function setLang($lang)
          {
          $this->lang = $lang;
          } */

        public function getUserProfileByAlt($user_alt)
        {
            $sql = 'SELECT id  FROM bm_users WHERE user_alt = ? AND user_role_id < 7';
            return $this->db->query($sql, array($user_alt))->fetch();
        }

        public function getUserProfile($user_id)
        {
            $sql = 'SELECT id, user_phone, user_email, user_alt, soc_vk, soc_ok, soc_fb, stars, scale_trust ,scale_cost, limit_choices, scale_prof, scale_count, user_lang, user_role_id, is_confirmed, avatar, user_name, is_active, premium_hours,
                DATE_FORMAT(user_reg_date, "%d/%m/%Y (%H:%i)") AS user_reg_date, TO_DAYS(NOW()) - TO_DAYS(bm_users.user_reg_date) AS days_from_reg
                FROM bm_users WHERE id =? AND user_role_id < 7';
            return $this->db->query($sql, array($user_id))->fetch();
        }

        public function updateUserAlt($alt)
        {

            $sql = "SELECT id FROM bm_users WHERE user_alt = ?";
            $res = $this->db->query($sql, array($alt))->fetch();
            if($res)
                return false;

            $user_id = $this->session->get('usr_id');

            $sql = 'UPDATE bm_users SET user_alt = ? WHERE id = ?';

            return $this->db->execute($sql, [$alt, $user_id]);
        }

        public function checkUserProfile($user_id)
        {
            $sql = 'SELECT
                        bm_users.id as usID, bm_users.user_role_id, bm_users.user_phone, bm_users.user_email, bm_users.user_name, bm_users_places.town, bm_perfs_info.description
                    FROM 
                        bm_users
                LEFT JOIN
                    bm_users_places ON bm_users.id = bm_users_places.user_id 
                LEFT JOIN
                    bm_perfs_info ON bm_users.id = bm_perfs_info.user_id 
                WHERE
                    bm_users.id =? AND user_role_id < 7';
            return $this->db->query($sql, array($user_id))->fetch();
        }

        public function getUserPlaces($user_id)
        {
            $sql = 'SELECT * FROM bm_users_places WHERE user_id = ?';
            return $this->db->query($sql, array($user_id))->fetchAll();
        }

        public function delUserPlaces($user_id, $place_id)
        {
            $sql = 'DELETE FROM bm_users_places WHERE user_id = ? AND id = ?';
            $this->db->query($sql, array($user_id, $place_id));
        }

        public function recoveryByEmail($email)
        {
            $sql = 'SELECT user_email FROM bm_users WHERE user_email =?';
            $email = $this->db->query($sql, array($email))->fetch()['user_email'];
            if($email) {
                //$passwd = $this->getDi()->getShared('generator')->genRandomString(4);
                $passwd = rand(1111, 9999);
                $passwd_to_db = $this->security->hash($passwd);
                $sql = 'UPDATE bm_users SET user_password = ? WHERE user_email = ?';
                $this->db->query($sql, array($passwd_to_db, $email));
                return $passwd;
            } else
                return false;
        }

        public function getUserByPhone($phone)
        {
            $sql = 'SELECT * FROM bm_users WHERE user_phone =?';
            $user = $this->db->query($sql, array($phone))->fetch();
            return $user;
        }

        public function recoveryByPhone($phone)
        {
            $sql = 'SELECT * FROM bm_users WHERE user_phone =?';
            $user = $this->db->query($sql, array($phone))->fetch();
            if($user) {
                //$passwd = $this->getDi()->getShared('generator')->genRandomString(4);
                $passwd = rand(1111, 9999);
                $passwd_to_db = $this->security->hash($passwd);
                $sql = 'UPDATE bm_users SET user_password = ? WHERE id = ?';
                $this->db->query($sql, array($passwd_to_db, $user['id']));
                return $passwd;
            } else
                return false;
        }

        public function confirmed($user_id)
        {
            $sql = 'UPDATE bm_users SET is_confirmed = 1 WHERE id = ?';
            $this->db->query($sql, array($user_id));
        }

        public function getUserInfo($user_id)
        {
            $sql = 'SELECT email, phone1, phone2, orgname, edrpou, chief, address, description, website
                FROM bm_perfs_info WHERE user_id =?';
            return $this->db->query($sql, array($user_id))->fetch();
        }

        public function getUserFiles($type, $user_id)
        {
            $sql = 'SELECT * FROM bm_files WHERE ftype = ? AND user_id = ? ORDER BY id DESC';
            return $this->db->query($sql, array($type, $user_id))->fetchAll();
        }

        public function getBigSliderData()
        {
            $sql = 'SELECT bm_files.*, bm_users.user_name
                    FROM
                            bm_files
                    LEFT JOIN 
                            bm_users ON bm_users.id = bm_files.user_id
                    WHERE
                            bm_files.ftype = "image"                   
                    AND 
                             bm_users.user_role_id > 3 
                    AND
                            bm_users.user_role_id < 7 
                    ORDER BY RAND()
                    LIMIT 20';
            return $this->db->query($sql)->fetchAll();
        }

        public function addSpec($speciality, $subspeciality, $user_id = false)
        {
            if(!$user_id)
                $user_id = $this->session->get('usr_id');
            $sql = 'INSERT IGNORE INTO builder_specializations (user_id, wrk_type_id, wrk_spec_id, speckey) VALUES (?, ?, ?, ?)';
            $speckey = $user_id . '-' . $speciality . '-' . $subspeciality;
            $this->db->query($sql, array($user_id, $speciality, $subspeciality, $speckey));
            return $this->db->lastInsertId();
        }

        public function delSpec($spec_id, $user_id = false)
        {
            if(!$user_id)
                $user_id = $this->session->get('usr_id');
            $sql = 'DELETE FROM builder_specializations WHERE id = ? AND user_id = ?';
            $this->db->query($sql, array($spec_id, $user_id));
        }

        public function getUserSpecs($user_id)
        {
            $sql = 'SELECT work_types.id AS wid, work_types.type_name, specializations.spec_name, builder_specializations.id
                FROM builder_specializations
                LEFT JOIN work_types ON work_types.id = builder_specializations.wrk_type_id
                LEFT JOIN specializations ON specializations.id = builder_specializations.wrk_spec_id
                WHERE builder_specializations.user_id = ? ORDER BY work_types.type_name';
            return $this->db->query($sql, array($user_id))->fetchAll();
        }

        public function delPlace($place_id, $user_id = false)
        {
            if(!$user_id)
                $user_id = $this->session->get('usr_id');
            $sql = 'DELETE FROM bm_users_places WHERE id = ? AND user_id = ?';
            $this->db->query($sql, array($place_id, $user_id));
        }

        public function addPrice($item_name, $price, $spec_item)
        {
            $user_id = $this->session->get('usr_id');
            $sql = 'INSERT INTO bm_perfs_prices (name, price, spec_item_id, user_id) VALUES (?, ?, ?, ?)';
            $this->db->query($sql, array($item_name, $price, $spec_item, $user_id));
            return $this->db->lastInsertId();
        }

        public function delPrice($item_id)
        {
            $user_id = $this->session->get('usr_id');
            $sql = 'DELETE FROM bm_perfs_prices WHERE id = ? AND user_id = ?';
            $this->db->query($sql, array($item_id, $user_id));
        }

        public function getUserPrices($user_id)
        {
            $sql = 'SELECT work_types.type_name, specializations.spec_name, bm_perfs_prices.id, bm_perfs_prices.name, bm_perfs_prices.price, bm_perfs_prices.spec_item_id
                FROM bm_perfs_prices
                LEFT JOIN builder_specializations ON builder_specializations.id = bm_perfs_prices.spec_item_id
                LEFT JOIN work_types ON work_types.id = builder_specializations.wrk_type_id
                LEFT JOIN specializations ON specializations.id = builder_specializations.wrk_spec_id
                WHERE builder_specializations.user_id = ? ORDER BY work_types.type_name';
            return $this->db->query($sql, array($user_id))->fetchAll();
        }

        public function createUser($name, $email, $phone, $role, $passwd, $admin = false)
        {
            $is_mobile = preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $_SERVER['HTTP_USER_AGENT']) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($_SERVER['HTTP_USER_AGENT'], 0, 4)) ? "mobile" : "desktop";

            $ip = $_SERVER['REMOTE_ADDR'];
            /* if ($email) {
              $name = explode('@', $email)[0];
              } else {
              $email = NULL;
              $name = '';
              } */
            if(!$email) {
                $add = str_replace([' ', '-', '(', ')'], '', $phone);
                $email = "$add@budmisto.com.ua";
            }
            $passwd = $this->security->hash($passwd);
            $sql = 'INSERT IGNORE INTO bm_users (user_name, user_email, user_phone, user_password, user_role_id, user_reg_ip, user_reg_device) VALUES (?, ?, ?, ?, ?, ?, ?)';
            $this->db->query($sql, array($name, $email, $phone, $passwd, $role, $ip, $is_mobile));
            $user_id = $this->db->lastInsertId();

            if($user_id && !$admin) {
                $this->setLastActivity($user_id);
                $config = $this->getDi()->getShared('config');
                $this->setUsrSession($user_id, $email, $phone, $role, $config->application->default_lang);
            }
            return $user_id;
        }

        public function createByOrderForm($email, $phone, $passwd)
        {
            $is_mobile = preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $_SERVER['HTTP_USER_AGENT']) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($_SERVER['HTTP_USER_AGENT'], 0, 4)) ? "mobile" : "desktop";

            $ip = $_SERVER['REMOTE_ADDR'];
            $name = 'Заказчик';
            $passwd_to_db = $this->security->hash($passwd);
            $sql = 'INSERT IGNORE INTO bm_users (user_name, user_email, user_phone, user_password, user_role_id, user_reg_ip, user_reg_device) VALUES (?, ?, ?, ?, 3, ?, ?)';
            $this->db->query($sql, array($name, $email, $phone, $passwd_to_db, $ip, $is_mobile));
            $user_id = $this->db->lastInsertId();
            if($user_id) {
                $this->setLastActivity($user_id);
                $config = $this->getDi()->getShared('config');
                $this->setUsrSession($user_id, $email, $phone, 3, $config->application->default_lang);
                $this->session->set('just_registered', 1);
            }
            return $user_id;
        }

        public function updateUserProfile($d, $user_id = false)
        {
            if(!$user_id)
                $user_id = $this->session->get('usr_id');
            $data = array();
            $inner_sql = '';
            if($d['phone']) {
                $inner_sql .= 'user_phone =?, ';
                $data[] = $d['phone'];
            }
            if($d['email']) {
                $inner_sql .= 'user_email = ?, ';
                $data[] = $d['email'];
            }

            $sql = 'UPDATE bm_users SET ' . $inner_sql . ' user_lang = ?, user_name = ?, soc_vk = ?, soc_ok = ?, soc_fb = ? WHERE id = ?';

            $data[] = $d['lang'];
            $data[] = $d['name'];
            $data[] = $d['soc_vk'];
            $data[] = $d['soc_ok'];
            $data[] = $d['soc_fb'];
            $data[] = $user_id;
            $this->db->execute($sql, $data);
            //$error = $this->db->getErrorInfo(); var_dump($error); die;
            return $this->db->affectedRows();
        }

        public function updateUserInfo($d, $user_id = false, $usr_level = false)
        {
            if(!$user_id)
                $user_id = $this->session->get('usr_id');
            if(!$usr_level)
                $usr_level = $this->session->get('usr_level');
            if($usr_level == 4 || $usr_level == 5) {
                $sql = 'INSERT INTO bm_perfs_info (email, phone1, phone2,  brief, website, user_id) VALUES (?, ?, ?,  ?, ?, ?)
                    ON DUPLICATE KEY UPDATE email = ?, phone1 = ?, phone2 = ?,  brief =?, website = ?';
                $data = array($d['email'], $d['phone1'], $d['phone2'], $d['brief'], $d['website'], $user_id,
                    $d['email'], $d['phone1'], $d['phone2'], $d['brief'], $d['website']);
            }
            if($usr_level == 6) {
                $sql = 'INSERT INTO bm_perfs_info (email, phone1, phone2, brief, website, orgname, edrpou, chief, address, user_id)
                    VALUES (?, ?, ?, ?,  ?, ?, ?, ?, ?, ?)
                    ON DUPLICATE KEY UPDATE email = ?, phone1 = ?, phone2 = ?,  brief =?, website = ?, orgname = ?, edrpou = ?, chief = ?, address = ?';
                $data = array($d['email'], $d['phone1'], $d['phone2'], $d['brief'], $d['website'], $d['name'], $d['edrpou'], $d['chief'], $d['address'], $user_id,
                    $d['email'], $d['phone1'], $d['phone2'], $d['brief'], $d['website'], $d['name'], $d['edrpou'], $d['chief'], $d['address']);
            }
            /* if ($usr_level == 7) {
              $sql = 'INSERT INTO bm_perfs_info (email, phone1, phone2, description, brief, website, orgname, chief, address, user_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
              ON DUPLICATE KEY UPDATE email = ?, phone1 = ?, phone2 = ?, description = ?, brief =?, website = ?, orgname = ?, chief = ?, address = ?';
              $data = array($d['email'], $d['phone1'], $d['phone2'], $d['text'], $d['brief'], $d['website'], $d['name'], $d['chief'], $d['address'], $user_id,
              $d['email'], $d['phone1'], $d['phone2'], $d['text'], $d['brief'], $d['website'], $d['name'], $d['chief'], $d['address']);
              } */
            $this->db->execute($sql, $data);
            return $this->db->affectedRows();
        }

        public function updateUserDescr($d)
        {

            $user_id = $this->session->get('usr_id');

            $sql = 'INSERT INTO bm_perfs_info (description, brief, user_id) VALUES (?, ?, ?)
                    ON DUPLICATE KEY UPDATE description = ?, brief =?';
            $data = array($d['text'], $d['brief'], $user_id, $d['text'], $d['brief']);

            $this->db->execute($sql, $data);
            return $this->db->affectedRows();
        }

        public function updateUserStars($s, $user_id = false)
        {

            $user_id = $user_id ? $user_id : $this->session->get('usr_id');

            $sql = "SELECT stars from bm_users WHERE id = {$user_id}";
            $res = $this->db->query($sql)->fetch();


            if($res['stars'] >= $s)
                return;

            $sql = "UPDATE bm_users SET stars ={$s} WHERE  id = {$user_id}";

            return $this->db->execute($sql);
        }

        // NEED REFACT!
        public function addUserFile($title, $filename)
        {
            $user_id = $this->session->get('usr_id');
            $sql = 'INSERT INTO bm_files (user_id, ftype, fname, ftitle) VALUES (?, ?, ?, ?)';
            $this->db->query($sql, array($user_id, 'file', $filename, $title));
        }

        public function deleteUserFile($file_id)
        {
            $user_id = $this->session->get('usr_id');
            $sql = 'SELECT fname FROM bm_files WHERE user_id = ? AND id = ?';
            $filename = $this->db->query($sql, array($user_id, $file_id))->fetch()['fname'];
            if($filename) {
                $sql = 'DELETE FROM bm_files WHERE user_id = ? AND id = ?';
                $this->db->query($sql, array($user_id, $file_id));
                return $filename;
            } else
                return false;
        }

        public function updatePassword($oldpasswd, $newpasswd)
        {
            $user_id = $this->session->get('usr_id');
            $just_registered = $this->session->get('just_registered');
            if($just_registered) {
                $passwd = $this->security->hash($newpasswd);
                $sql = 'UPDATE bm_users SET user_password = ? WHERE id = ?';
                $this->db->query($sql, array($passwd, $user_id));
                return true;
            }

            $sql = 'SELECT user_password FROM bm_users WHERE id = ?';
            $result = $this->db->query($sql, array($user_id))->fetch();
            if(!$result)
                return false;

            if($this->security->checkHash($oldpasswd, $result['user_password'])) {
                $passwd = $this->security->hash($newpasswd);
                $sql = 'UPDATE bm_users SET user_password = ? WHERE id = ?';
                $this->db->query($sql, array($passwd, $user_id));
                return true;
            } else
                return false;
        }

        public function signinUser($email, $phone, $passwd, $fast_login = false)
        {
            if(!$fast_login) {
                $this->signoutUser();
            }

            if($email && !$phone) {
                $sql = 'SELECT id, user_password, user_email, user_phone, user_role_id, user_lang, avatar
                    FROM bm_users WHERE is_active = 1 AND LOWER(user_email) = LOWER(?)';
                $result = $this->db->query($sql, array($email))->fetch();
            }

            if($phone) {
                $sql = 'SELECT id, user_password, user_email, user_phone, user_role_id, user_lang, avatar
                    FROM bm_users WHERE is_active = 1 AND user_phone = ?';
                $result = $this->db->query($sql, array($phone))->fetch();
            }
            if(!$result)
                return false;
            //$logger = $this->getDi()->getShared('logger');
            //$logger->log(serialize($this->security->checkHash($passwd, $result['user_password'])), \Phalcon\Logger::ERROR);

            if($this->security->checkHash($passwd, $result['user_password'])) {
                $this->setLastActivity($result['id']);
                $this->updateLoginsCount($result['id']);
                $this->setUsrSession($result['id'], $result['user_email'], $result['user_phone'], $result['user_role_id'], $result['user_lang'], $result['avatar']);
                return true;
            } else if($fast_login) {
                Users::updateLoginsCount($result['id']);
                Users::setUsrSession($result['id'], $result['user_email'], $result['user_phone'], $result['user_role_id'], $result['user_lang'], $result['avatar']);
                return "success";
            } else
                return false;
        }

        public function setLastActivity($user_id)
        {
            $timestamp = date('Y-m-d G:i:s');
            $sql = 'UPDATE bm_users SET last_activity = ? WHERE id = ?';
            $this->db->query($sql, array($timestamp, $user_id));
        }

        public function updateLoginsCount($user_id)
        {
            $sql = 'UPDATE bm_users SET user_logins = user_logins +1, last_activity=CURRENT_TIMESTAMP()   WHERE id = ?';
            $this->db->query($sql, array($user_id));
        }

        private function setUsrSession($user_id, $email, $phone, $role, $lang, $avatar = 'empty.jpg')
        {
            $this->session->set('usr_id', $user_id);
            $this->session->set('usr_email', $email);
            $this->session->set('usr_phone', $phone);
            $this->session->set('usr_level', $role);
            $this->session->set('language', $lang);
            $this->session->set('avatar', $avatar);
        }

        public function signoutUser()
        {
            $user_id = $this->session->get('usr_id');
            $this->setLastActivity($user_id);

            $this->session->remove('usr_id');
            $this->session->remove('usr_level');
            $this->session->remove('usr_email');
            $this->session->remove('usr_phone');
            $this->session->remove('language');
            $this->session->remove('avatar');
        }

        public function checkEmailExists($data, $type = 'email')
        {
            $return = false;
            $user_id = $this->session->get('usr_id');
            if(!$user_id && $type == 'email') {
                $sql = 'SELECT id FROM bm_users WHERE LOWER(user_email) = LOWER(?)';
                $result = $this->db->query($sql, array($data))->fetch()['id'];
                $return = $result;
            }
            if(!$user_id && $type == 'phone') {
                $sql = 'SELECT id FROM bm_users WHERE user_phone = ?';
                $result = $this->db->query($sql, array($data))->fetch()['id'];
                $return = $return ? $return : $result;
            }
            if($user_id && $type == 'email') {
                $sql = 'SELECT id FROM bm_users WHERE LOWER(user_email) = LOWER(?)';
                $result = $this->db->query($sql, array($data))->fetch()['id'];
                $return = ($result != $user_id) ? $result : false;
            }
            if($user_id && $type == 'phone') {
                $sql = 'SELECT id FROM bm_users WHERE user_phone = ?';
                $result = $this->db->query($sql, array($data))->fetch()['id'];
                if(!$return)
                    $return = ($result != $user_id) ? $result : false;
            }
            return $return ? $return : false;
        }

        public function getPlaces($user_id)
        {
            $sql = 'SELECT * FROM bm_users_places WHERE user_id = ?';
            return $this->db->query($sql, array($user_id))->fetchAll();
        }

        public function addShop($d)
        {
            $shop_id = $this->createUser($d['name'], $d['email'], $d['phone1'], 7, $d['passwd'], true);
            if(!$shop_id)
                return false;

            $d['lang'] = 'ru';
            $d['email'] = $d['email2'];
            $this->updateUserProfile($d, $shop_id);
            $this->updateShopInfo($d, $shop_id);
            $this->addShopSpec($d['speciality'], $shop_id);

            return $shop_id;
        }

        public function editShop($d, $shop_id)
        {
            $d['lang'] = 'ru';
            $d['email'] = $d['email2'];
            $this->updateUserProfile($d, $shop_id);
            $this->updateShopInfo($d, $shop_id);
            if(isset($d['tid']) && $d['tid'] && isset($d['sid']) && $d['sid'])
                $this->addShopSpec($d['tid'], $d['sid'], $shop_id);
            if(isset($d['passwd']) && $d['passwd']) {
                $passwd = $this->security->hash($d['passwd']);
                $sql = 'UPDATE bm_users SET user_password = ? WHERE id = ?';
                $this->db->query($sql, array($passwd, $shop_id));
            }

            return $shop_id;
        }

        public function TEMP()
        {
            $sql = "SELECT * FROM shop_specializations";
            return $this->db->query($sql)->fetchAll();
        }

        public function TEMP_1($work_type)
        {
            $sql = "SELECT id, spec_name FROM specializations WHERE type_id = ? and slug!= 'vse-vidy-rabot'";
            return $this->db->query($sql, [$work_type])->fetchAll();
        }

        public function TEMP_2($tid, $sid, $shop_id)
        {
            $sql = 'INSERT IGNORE INTO shop_specializations_new (shop_id, wrk_type_id, wrk_spec_id, speckey) VALUES (?, ?, ?, ?)';
            $speckey = $shop_id . '-' . $tid . '-' . $sid;
            $this->db->query($sql, array($shop_id, $tid, $sid, $speckey));
            return $this->db->lastInsertId();
        }

        public function addShopSpec($tid, $sid, $shop_id = false)
        {
            if(!$shop_id)
                $shop_id = $this->session->get('usr_id');

            if($sid == 1000) {
                $sub_list = (new Catalog())->getSubSpecListById($tid);
                if(!$sub_list)
                    return false;

                foreach ($sub_list as $key => $value) {
                    $speckey = $shop_id . '-' . $tid . '-' . $value['id'];
                    $sql = 'INSERT IGNORE INTO shop_specializations (shop_id, wrk_type_id, wrk_spec_id, speckey) VALUES (?, ?, ?, ?)';
                    $this->db->query($sql, array($shop_id, $tid, $value['id'], $speckey));
                }
                return $this->db->lastInsertId();
            } else {
                $sql = 'INSERT IGNORE INTO shop_specializations (shop_id, wrk_type_id, wrk_spec_id, speckey) VALUES (?, ?, ?, ?)';
                $speckey = $shop_id . '-' . $tid . '-' . $sid;
                $this->db->query($sql, array($shop_id, $tid, $sid, $speckey));
                return $this->db->lastInsertId();
            }
        }

        public function updateShopInfo($d, $shop_id = false)
        {
            if(!$shop_id)
                $shop_id = $this->session->get('usr_id');
            $d['net_id'] = $d['net_id'] ? $d['net_id'] : 0;
            $sql = 'INSERT INTO bm_shops_info (net_id, email, phone1, phone2, description, brief, website, orgname, chief, address, brands, shop_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                ON DUPLICATE KEY UPDATE net_id = ?, email = ?, phone1 = ?, phone2 = ?, description = ?, brief =?, website = ?, orgname = ?, chief = ?, address = ?, brands = ?';
            $data = array($d['net_id'], $d['email'], $d['phone1'], $d['phone2'], $d['text'], $d['brief'], $d['website'], $d['name'], $d['chief'], $d['address'], $d['brands'], $shop_id,
                $d['net_id'], $d['email'], $d['phone1'], $d['phone2'], $d['text'], $d['brief'], $d['website'], $d['name'], $d['chief'], $d['address'], $d['brands']);
            $this->db->execute($sql, $data);
            return $this->db->affectedRows();
        }

        public function getShop($shop_id)
        {
            $sql = 'SELECT bm_users.*, bm_shops_info.phone1, bm_shops_info.phone2, bm_shops_info.description, bm_shops_info.brands,
                bm_shops_info.website, bm_shops_info.orgname, bm_shops_info.chief, bm_shops_info.email AS email2,
                bm_shops_info.net_id
                FROM bm_users
                LEFT JOIN bm_shops_info ON bm_shops_info.shop_id = bm_users.id
                WHERE bm_users.id = ?';
            return $this->db->query($sql, array($shop_id))->fetch();
        }

        public function getShopPlaces($shop_id)
        {
            $sql = 'SELECT * FROM bm_shop_places WHERE shop_id = ?';
            return $this->db->query($sql, array($shop_id))->fetchAll();
        }

        public function getShopSpecs($shop_id)
        {
            $sql = 'SELECT shop_specializations.id, work_types.id AS tid, specializations.id AS sid, work_types.type_name, specializations.spec_name
                FROM shop_specializations
                LEFT JOIN work_types ON work_types.id = shop_specializations.wrk_type_id
                LEFT JOIN specializations ON specializations.id = shop_specializations.wrk_spec_id
                WHERE shop_specializations.shop_id = ? ORDER BY work_types.type_name';
            return $this->db->query($sql, array($shop_id))->fetchAll();
        }

        public function shopDelSpec($sid, $shop_id)
        {
            $sql = 'DELETE FROM shop_specializations WHERE wrk_spec_id = ? AND shop_id = ?';
            $this->db->query($sql, array($sid, $shop_id));
        }

        public function shopDelPlace($id, $shop_id)
        {
            $sql = 'DELETE FROM bm_shop_places WHERE id = ? AND shop_id = ?';
            $this->db->query($sql, array($id, $shop_id));
        }

        public function getCallData($user_id, $user_call_id)
        {
            $sql = 'SELECT * FROM bm_users WHERE id = ?';
            $res1 = $this->db->query($sql, array($user_call_id))->fetch();
            $sql = 'SELECT * FROM bm_users WHERE id = ?';
            $res2 = $this->db->query($sql, array($user_id))->fetch();
            if(!$res1 || !$res2)
                return false;
            return array('user' => $res2, 'caller' => $res1);
        }

        public function setWorkOffer($data)
        {
            $sql = "SELECT * FROM work_offers WHERE id_customer=? AND name_customer=? AND id_worker=? AND text=? AND phone=?";
            $exist = $this->db->query($sql, [$data['id_customer'], $data['name_customer'], $data['id_worker'], $data['text'], $data['phone']])->fetch();

            if($exist)
                return 'exist';

            $sql = 'INSERT INTO work_offers SET id_customer=?, name_customer=?, id_worker=?, text=?, phone=?';
            return $this->db->query($sql, [$data['id_customer'], $data['name_customer'], $data['id_worker'], $data['text'], $data['phone']]);
        }

        public function getWorkkOffer($usID, $start = 0)
        {

            $sql = "SELECT * FROM work_offers WHERE id_worker=? AND status='0' ";
            return $this->db->query($sql, [$usID])->fetchAll();
        }

        public function getOffersFromCustomer($usID)
        {
            $sql = "SELECT * FROM work_offers 
                LEFT JOIN bm_users ON work_offers.id_worker=bm_users.id
                WHERE id_customer=?";
            return $this->db->query($sql, [$usID])->fetchAll();
        }

        public function deleteWorkkOffer($usID, $id)
        {
            $sql = "UPDATE work_offers SET status='2' WHERE id_worker=? AND id=?";
            return $this->db->query($sql, [$usID, $id]);
        }

        public function get_informed_users($order_id)
        {
            $sql = "SELECT phone FROM cron WHERE order_id=? AND status=?";
            return $this->db->query($sql, [$order_id, "1"])->fetchAll();
        }

        public function getComercials($shop_id)
        {
            $sql = "SELECT * FROM shop_commercial WHERE shop_id=?";
            return $this->db->query($sql, [$shop_id])->fetchAll();
        }

        public function getSingleComercial($id)
        {
            $sql = "SELECT * FROM shop_commercial WHERE id=?";
            return $this->db->query($sql, [$id])->fetch();
        }

        public function deleteComercials($id)
        {
            $sql = "DELETE FROM shop_commercial WHERE id=?";
            return $this->db->query($sql, [$id]);
        }

        public function update_comercial($data)
        {

            if($data['comercial_id'] and $data['comercial_id'] > 0) {

                if($data['comercial_image'] != '') {
                    $add = "img='{$data['comercial_image']}',";
                }

                $sql = "UPDATE shop_commercial 
                    SET 
                        name='{$data['comercial_name']}' , 
                        shop_id={$data['shop_id']} , 
                        user_role='{$data['user_role']}' ,
                        wrk_type_id='{$data['wrk_type_id']}',
                        wrk_spec_id='{$data['wrk_spec_id']}',
                        $add
                        descr='{$data['comercial_descr']}',
                        active='{$data['comercial_active']}'
                    WHERE id={$data['comercial_id']}";
            } else {
                $sql = "INSERT INTO shop_commercial                   
                    SET 
                        name='{$data['comercial_name']}' , 
                        shop_id={$data['shop_id']} , 
                        user_role='{$data['user_role']}' ,
                        wrk_type_id='{$data['wrk_type_id']}',
                        wrk_spec_id='{$data['wrk_spec_id']}',
                        img='{$data['comercial_image']}',
                        descr='{$data['comercial_descr']}',
                        active='{$data['comercial_active']}',
                        displayed_users=''
                    ";
            }

            return $this->db->query($sql);
        }

        public function getUsersByStarAndSpec($stars, $data, $limit = 10, $distance = 50)
        {
            /* Вытягиваем координаты заказа */
            $sql = "SELECT 
                    lat,
                    lng,
                    order_wrk_spec,
                    order_wrk_type
                FROM
                    bm_orders_places
                LEFT JOIN
                    bm_orders ON bm_orders.id=bm_orders_places.order_id
                WHERE
                    order_id =?
            ";

            $order_data = $this->db->query($sql, [$data['order_id']])->fetch();

            /* Отбираем телефоны строиетелей по звездам, дистанции и нижней специализации */

            $sql = "  SELECT
                    user_phone,
                    (6371 * acos (
                            cos ( radians({$order_data['lat']}) )
                            * cos( radians( bm_users_places.lat ) )
                            * cos( radians( bm_users_places.lng ) - radians({$order_data['lng']}) )
                            + sin ( radians({$order_data['lat']}) )
                            * sin( radians( bm_users_places.lat ) )
                          )
                      ) AS distance,
                    user_views,
                    bm_users.id AS uid,
                    stars,
                    (SELECT user_phone FROM bm_users WHERE bm_users.id=(SELECT user_id FROM bm_orders WHERE id = {$data['order_id']}))AS order_phone,
                    (SELECT type_name FROM work_types WHERE id = ?) AS order_specialization
                FROM
                    bm_users_places
                LEFT JOIN
                    bm_users ON bm_users.id = bm_users_places.user_id
                LEFT JOIN
                    builder_specializations ON builder_specializations.user_id = bm_users_places.user_id
                WHERE bm_users.user_role_id > 3 
                    AND bm_users.user_role_id < 7 
                    AND bm_users.is_active = 1
                    AND bm_users.stars = $stars
                    AND builder_specializations.wrk_spec_id = {$order_data['order_wrk_spec']}  
                GROUP BY user_views
                HAVING distance < {$distance}
                LIMIT 2                
            ";
            $res = $this->db->query($sql, [$data['speciality']])->fetchAll();


            if(count($res) < 5) {

                foreach ($res as $key => $value) {
                    $all_id[] = $value['uid'];
                }

                if(count($all_id) > 0) {
                    $add = 'AND bm_users.id NOT IN (' . implode(',', $all_id) . ')';
                    $limit = $limit - count($all_id);
                }


                $sql = "  SELECT
                    user_phone,
                    (6371 * acos (
                            cos ( radians({$order_data['lat']}) )
                            * cos( radians( bm_users_places.lat ) )
                            * cos( radians( bm_users_places.lng ) - radians({$order_data['lng']}) )
                            + sin ( radians({$order_data['lat']}) )
                            * sin( radians( bm_users_places.lat ) )
                          )
                      ) AS distance,
                    user_views,
                    bm_users.id AS uid,
                    stars,
                    (SELECT user_phone FROM bm_users WHERE bm_users.id=(SELECT user_id FROM bm_orders WHERE id = {$data['order_id']}))AS order_phone,
                    (SELECT type_name FROM work_types WHERE id = ?) AS order_specialization
                FROM
                    bm_users_places
                LEFT JOIN
                    bm_users ON bm_users.id = bm_users_places.user_id
                LEFT JOIN
                    builder_specializations ON builder_specializations.user_id = bm_users_places.user_id
                WHERE bm_users.user_role_id > 3 
                    AND bm_users.user_role_id < 7 
                    $add
                    AND bm_users.is_active = 1
                    AND bm_users.stars = $stars
                    AND builder_specializations.wrk_type_id = {$order_data['order_wrk_type']}  
                HAVING distance < {$distance}
                ORDER BY RAND()
                LIMIT $limit               
            ";
                $res2 = $this->db->query($sql, [$data['speciality']])->fetchAll();
                $res = array_merge($res, $res2);
            }

            return $res;
        }

        public function setInformedUsers($users, $order_id)
        {
            foreach ($users as $key => $value) {
                $id[] = $value['uid'];
            }

            $sql = "UPDATE bm_orders SET informed_users = ? WHERE id=?";
            return $this->db->query($sql, [json_encode($id), $order_id]);
        }

        public function setAutoChoice($users, $order_id)
        {
            foreach ($users as $key => $value) {
                $sql = "INSERT INTO bm_orders_connect SET order_id = ?, choice_id=?";
                $this->db->query($sql, [$order_id, $value['uid']]);
            }
            return;
        }

        public function setCronSeries($data)
        {
            $sql = 'INSERT INTO cron_series SET data=?';
            return $this->db->query($sql, [$data]);
        }

        public function getCronSeries($duration = 5)
        {
            $time = time() - 60 * $duration;
            $sql = "SELECT * FROM cron_series WHERE status = '0' AND UNIX_TIMESTAMP(created) < $time";

            return $this->db->query($sql)->fetchAll();
        }

        public function updateCronSeriesStatus($id)
        {
            $sql = "UPDATE cron_series SET status = '1' WHERE id=?";
            return $this->db->query($sql, [$id]);
        }

        public function updateOfferStatus($offer_id, $status)
        {
            $sql = "UPDATE work_offers SET status = ? WHERE id=?";
            return $this->db->query($sql, [$status, $offer_id]);
        }

    }
    