<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/front/models/Leftovers.php
     *
     * Leftovers model
     *
     */

    namespace Budmisto\Modules\Front\Models;

    class Leftovers extends \Phalcon\Mvc\Model
    {

        private $db;
        private $security;
        private $session;

        public function initialize()
        {
            $this->db = $this->getDi()->getShared('db');
            $this->security = $this->getDi()->getShared('security');
            $this->session = $this->getDi()->getShared('session');
        }

        public function addPost($d)
        {
            if(!$d['placextra']) {
                $d['placedistrict'] = ($d['placedistrict2']) ? $d['placedistrict2'] : $d['placedistrict3'];
                $d['placedistrict'] = str_replace('горсовет', 'регион', $d['placedistrict']);
                $d['placedistrict'] = str_replace('міськрада', 'громада', $d['placedistrict']);
            } else {
                $d['placedistrict'] = $d['placextra'];
            }

            $sql = 'SELECT id FROM bm_regions WHERE ru_region_name = ? OR uk_region_name = ?';
            $region_id = $this->db->query($sql, array($d['placeregion'], $d['placeregion']))->fetch()['id'];
            if($region_id == 26)
                $region_id = 10;
            // NEED REFACT

            $sql = 'INSERT INTO bm_leftovers (place_id, place_lat, place_lng, lo_wrk_type, lo_actual_period, lo_shipping, lo_brief,
                            lo_description, route, town, district, region, country, region_id, lo_name, lo_phone, lo_email, lo_type)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
            $data = array($d['placeid'], $d['placelat'], $d['placelng'], $d['speciality'], $d['actual_period'], $d['shipping'], $d['brief'],
                $d['description'], $d['placeroute'], $d['placetown'], $d['placedistrict'], $d['placeregion'],
                $d['placecountry'], $region_id, $d['name'], $d['phone'], $d['email'], $d['type']);
            $this->db->query($sql, $data);
            return $this->db->lastInsertId();
        }

        public function getPost($post_id)
        {
            $sql = 'SELECT bm_leftovers.id, bm_leftovers.lo_description, work_types.slug AS wts, work_types.type_name,
                DATE_FORMAT(bm_leftovers.lo_date,"%d/%m/%Y") AS lo_date, bm_leftovers.lo_wrk_type,
                DATE_FORMAT(bm_leftovers.lo_actual_period,"%d/%m/%Y") AS actual_period,
                DATE_FORMAT(bm_leftovers.lo_close_date,"%d/%m/%Y") AS close_date,
                UNIX_TIMESTAMP(bm_leftovers.lo_actual_period) AS finish_date, bm_leftovers.lo_shipping,
                bm_leftovers.route, bm_leftovers.town, bm_leftovers.district, bm_leftovers.region,
                bm_leftovers.lo_phone, bm_leftovers.lo_email, bm_leftovers.lo_name, bm_leftovers.lo_type
                FROM bm_leftovers
                LEFT JOIN work_types ON work_types.id = bm_leftovers.lo_wrk_type
                WHERE bm_leftovers.id = ?';
            return $this->db->query($sql, array($post_id))->fetch();
        }

        public function getLoList($type, $start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            $sql = "SELECT bm_leftovers.id, bm_leftovers.lo_brief AS descr_short, work_types.type_name,
                DATE_FORMAT(bm_leftovers.lo_date,'%d/%m/%Y (%H:%i)') AS lo_date, lo_shipping,
                UNIX_TIMESTAMP(bm_leftovers.lo_actual_period) AS finish_date, UNIX_TIMESTAMP(bm_leftovers.lo_close_date) AS close_date,
                bm_leftovers.route, bm_leftovers.town, bm_leftovers.district, bm_leftovers.region,
                DATE_FORMAT(bm_leftovers.lo_close_date,'%d/%m/%Y (%H:%i)') AS lo_close_date
                FROM bm_leftovers
                LEFT JOIN work_types ON work_types.id = bm_leftovers.lo_wrk_type
                WHERE bm_leftovers.lo_type = '{$type}'
                ORDER BY bm_leftovers.lo_date DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql)->fetchAll();
        }

        public function getTopLoList($top_id, $shipping, $type, $start, $limit)
        {
            $start = $start ? ($limit * ($start - 1)) : 0;
            switch ($shipping)
            {
                case 'pickup': $shipping = 1;
                    break;
                case 'delivery': $shipping = 2;
                    break;
                default: $shipping = false;
                    break;
            }
            $sql = "SELECT bm_leftovers.id, bm_leftovers.lo_brief AS descr_short, work_types.type_name,
                DATE_FORMAT(bm_leftovers.lo_date,'%d/%m/%Y (%H:%i)') AS lo_date, lo_shipping,
                UNIX_TIMESTAMP(bm_leftovers.lo_actual_period) AS finish_date, UNIX_TIMESTAMP(bm_leftovers.lo_close_date) AS close_date,
                bm_leftovers.route, bm_leftovers.town, bm_leftovers.district, bm_leftovers.region
                FROM bm_leftovers
                LEFT JOIN work_types ON work_types.id = bm_leftovers.lo_wrk_type
                WHERE bm_leftovers.lo_wrk_type = ? AND lo_type = ?";
            if($shipping)
                $sql .= " AND lo_shipping = {$shipping}";
            $sql .= " ORDER BY bm_leftovers.lo_date DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql, array($top_id, $type))->fetchAll();
        }

        public function getLoListByCoords($rub, $shipping, $lat, $lng, $start, $limit, $distance = 25)
        {
            if(!$lat || !$lng)
                return false;
            $start = $start ? ($limit * ($start - 1)) : 0;
            $limit++;
            switch ($shipping)
            {
                case 'pickup': $shipping = 1;
                    break;
                case 'delivery': $shipping = 2;
                    break;
                default: $shipping = false;
                    break;
            }
            $sql = "SELECT bm_leftovers.id, bm_leftovers.lo_brief AS descr_short, work_types.type_name,
                DATE_FORMAT(bm_leftovers.lo_date,'%d/%m/%Y (%H:%i)') AS lo_date, lo_shipping,
                UNIX_TIMESTAMP(bm_leftovers.lo_actual_period) AS finish_date, UNIX_TIMESTAMP(bm_leftovers.lo_close_date) AS close_date,
                bm_leftovers.route, bm_leftovers.town, bm_leftovers.district, bm_leftovers.region,
(6371 * acos (
      cos ( radians(?) )
      * cos( radians( bm_leftovers.place_lat ) )
      * cos( radians( bm_leftovers.place_lng ) - radians(?) )
      + sin ( radians(?) )
      * sin( radians( bm_leftovers.place_lat ) )
    )
) AS distance
                FROM bm_leftovers
                LEFT JOIN work_types ON work_types.id = bm_leftovers.lo_wrk_type
                WHERE  bm_leftovers.lo_wrk_type = ?";
            if($shipping)
                $sql .= " AND lo_shipping = {$shipping}";
            $sql .= " HAVING distance < {$distance} ORDER BY distance ASC, bm_leftovers.lo_date DESC LIMIT {$start}, {$limit}";
            return $this->db->query($sql, array($lat, $lng, $lat, $rub))->fetchAll();
        }

        public function countTopLO($top_id)
        {
            $sql = 'SELECT count(id) AS cnt FROM bm_leftovers WHERE lo_wrk_type = ?';
            return $this->db->query($sql, array($top_id))->fetch()['cnt'];
        }

        public function countLO()
        {
            $sql = 'SELECT count(id) AS cnt FROM bm_leftovers';
            return $this->db->query($sql)->fetch()['cnt'];
        }

    }
    