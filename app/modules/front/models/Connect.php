<?php

    /*

     *     Author:  Hishnik  
     *     E-mail:  arenovec@gmail.com    
     *     skype:   kidw810i 
     * 
     *     Copyright (C) 2017 Hishnik
     */

    namespace Budmisto\Modules\Front\Models;

    class Connect extends Catalog
    {

        public function setLang($lang)
        {
            $this->lang = $lang;
        }

        public function setTask($data)
        {

            /* Позиция отправки, если заявка создана до 2 дня- тип 1, иначе - тип 2 */

            if(date("H", time()) <= 10) {
                $position = 2;
            } elseif(date("H", time()) > 10 and date("H", time()) <= 14) {
                $position = 1;
            } else {
                $position = 2;
            }


            $sql = "SELECT * FROM cron WHERE type=? AND phone=? AND sender=? AND order_id=? AND position=?";
            $res = $this->db->query($sql, [$data['type'], $data['phone'], $data['sender'], $data['order_id'], $position])->fetch();

            if($res)
                return;

            $sql = 'INSERT INTO cron
                    (type, phone, sender,order_id,position)
                    VALUES (?,?,?,?,?)';
            $val = array($data['type'], $data['phone'], $data['sender'], $data['order_id'], $position);
            $this->db->query($sql, $val);
            return $this->db->lastInsertId();
        }

        public function getPhoneOrderUser($order_id)
        {
            $sql = 'SELECT user_phone FROM bm_users WHERE id = (SELECT user_id FROM bm_orders WHERE id=?)';
            return $this->db->query($sql, [$order_id])->fetch();
        }

        public function get_all_task($position)
        {

            $sql = 'SELECT * FROM cron WHERE status = "0" AND position= ? ';

            return $this->db->query($sql, [$position])->fetchAll();
        }

        public function getTextSMS($type)
        {
            $type = "SMS_TEXT_TYPE_" . $type;
            $sql = 'SELECT value FROM config WHERE param = "' . $type . '" ';

            return $this->db->query($sql, [])->fetch();
        }

        public function updateStatusMsg($id, $status)
        {

            $id = is_array($id) ? implode(",", $id) : $id;

            $sql = "UPDATE `cron` SET `status` = '{$status}' WHERE `id` IN ($id)";

            return $this->db->query($sql);
        }

        public function get_config($param)
        {

            $sql = 'SELECT value FROM config WHERE param = ? ';

            return $this->db->query($sql, [$param])->fetch();
        }

        public function add_msg_info($type, $phone, $sender, $order_id)
        {

            $sql = 'INSERT INTO cron SET type=?, phone=?, sender=?, status="1", order_id = ?';
            return $this->db->query($sql, [$type, $phone, $sender, $order_id]);
        }

        public function get_msg_check($phone, $sender, $order_id)
        {
            $sql = "SELECT * FROM cron WHERE phone=? AND sender=? AND order_id=?";
            return $this->db->query($sql, [$phone, $sender, $order_id])->fetch();
        }

        public function get_blocked_orders()
        {
            $sql = "SELECT * FROM cron_blocked";
            return $this->db->query($sql, [$phone, $sender, $order_id])->fetchAll();
        }

        public function get_premium_orders()
        {
            $sql = "SELECT * FROM order_premium_block WHERE status = ?";
            $res = $this->db->query($sql, ["1"])->fetchAll();
            return $res;
        }

    }
    