<?php

    /*

     *     Author:  Hishnik  
     *     E-mail:  arenovec@gmail.com    
     *     skype:   kidw810i 
     * 
     *     Copyright (C) 2016 Hishnik
     */

    namespace Budmisto\Modules\Front\Models;

    class Rotator extends \Phalcon\Mvc\Model
    {

        private $db;
        private $lang;
        private $config;

        public function initialize()
        {
            $this->db = $this->getDi()->getShared('db');
            $this->config = $this->getDi()->getShared('config');
        }

        public function setLang($lang)
        {
            $this->lang = $lang;
        }

        public function get_spec_id($main, $sub = FALSE)
        {

            $sql = "SELECT id FROM work_types WHERE slug='{$main}'";

            $arr = $this->db->query($sql)->fetchAll();

            if($sub and isset($arr[0])) {
                $sql = "SELECT id FROM specializations WHERE slug='{$sub}' and type_id = {$arr[0]['id']}";
                $res = $this->db->query($sql)->fetchAll();
                if($res)
                    return $res;
            }

            return $arr;
        }

        public function get_main_spec()
        {

            $sql = "SELECT * FROM work_types";
            $main_spec = $this->db->query($sql)->fetchAll();

            $main_spec[] = [
                'id' => '1000',
                'type_name' => 'Страница всех заказов',
                'is_active' => 1,
                'slug' => 'raboty-pod-klyuch',
                'description' => '',
            ];
            return $main_spec;
        }

        public function get_sub_spec()
        {

            $sql = "SELECT * FROM specializations WHERE is_active=1";

            return $this->db->query($sql)->fetchAll();
        }

        public function get_banner($type = 'banner', $controller, $city = false, $specialization = false)
        {
            $distance = 50;

            if($city and $type != 'text') {
                if($specialization)
                    $sql = "SELECT *,
                        (6371 * acos (
                                cos ( radians({$city['lat']}) )
                                * cos( radians( city_lat ) )
                                * cos( radians( city_lng ) - radians({$city['lng']}) )
                                + sin ( radians({$city['lat']}) )
                                * sin( radians( city_lat ) )
                              )
                        ) AS distance  FROM rotator_info                          
                        WHERE active=1
                        HAVING distance <  rotator_info.city_distance
                        AND `type`='{$type}' 
                        AND `controller` LIKE '%{$controller}%' 
                        AND (`spec_main` LIKE '%[{$specialization}]%' 
                            OR `spec_sub` LIKE '%[{$specialization}]%') ";
                else
                    $sql = "SELECT *,
                        (6371 * acos (
                                cos ( radians({$city['lat']}) )
                                * cos( radians( city_lat ) )
                                * cos( radians( city_lng ) - radians({$city['lng']}) )
                                + sin ( radians({$city['lat']}) )
                                * sin( radians( city_lat ) )
                              )
                        ) AS distance 
                        FROM rotator_info 
                        WHERE active=1 
                        HAVING distance <  rotator_info.city_distance
                        AND `type`='{$type}' 
                        AND `controller` LIKE '%{$controller}%' 
                        AND `spec_main`='' 
                        AND `spec_sub`='' ";
                return $this->db->query($sql)->fetchAll();
            }

            if($specialization)
                $sql = "SELECT * FROM rotator_info  WHERE active=1 AND `type`='{$type}' AND `controller` LIKE '%{$controller}%' AND (`spec_main` LIKE '%[{$specialization}]%' OR `spec_sub` LIKE '%[{$specialization}]%') AND (`city_name`='' OR `city_name` is null)";
            else
                $sql = "SELECT * FROM rotator_info  WHERE active=1 AND `type`='{$type}' AND `controller` LIKE '%{$controller}%' AND `spec_main`='' AND `spec_sub`='' AND (`city_name`='' OR `city_name` is null) ";

            return $this->db->query($sql)->fetchAll();
        }

        public function get_all_banners($type = 'banner')
        {

            $sql = "SELECT * FROM rotator_info WHERE type='{$type}' AND controller !='text' ORDER BY id DESC";

            return $this->db->query($sql)->fetchAll();
        }

        public function get_banner_by_id($id, $type = 'banner')
        {

            $sql = "SELECT * FROM rotator_info WHERE type='{$type}' AND id={$id}";

            return $this->db->query($sql)->fetch();
        }

        public function update_banner($type = 'banner', $data, $id = false)
        {

            $data['text'] = str_replace("'", "\'", $data['text']);
            $data['text'] = str_replace('"', '\"', $data['text']);
            $data['city_distance'] = (int) $data['city_distance'] > 0 ? $data['city_distance'] : 0;


            if($data['placeaddr'] != '') {
                $city = "city_name='{$data['placeaddr']}',
                                    city_lat='{$data['placelat']}',
                                    city_lng='{$data['placelng']}',";
            }else
                $city = "city_name='',
                                    city_lat=null,
                                    city_lng=null,";
            
            
            if($id) {
                $sql = "UPDATE rotator_info 
                    SET 
                                    name='{$data['name']}' , 
                                    active={$data['active']} , 
                                    controller='{$data['controller']}' ,
                                    spec_main='{$data['main_spec']}',
                                    spec_sub='{$data['sub_spec']}',
                                    text='{$data['text']}',
                                    url='{$data['url']}',
                                    position='{$data['position']}',
                                    $city
                                    city_distance='{$data['city_distance']}'
                    WHERE type='{$type}' AND id={$id}";
            } else {
                $sql = "INSERT INTO rotator_info
                    SET
                    name='{$data['name']}' , 
                    active={$data['active']} , 
                    controller='{$data['controller']}' ,
                    spec_main='{$data['main_spec']}', 
                    spec_sub='{$data['sub_spec']}',
                    text='{$data['text']}',
                    url='{$data['url']}',
                    position='{$data['position']}',
                    type='{$type}',
                    $city
                    city_distance='{$data['city_distance']}'
                    ";
            }
            return $this->db->query($sql);
        }

        public function delete_banner($type = 'banner', $id)
        {
            $sql = "DELETE FROM rotator_info WHERE type='{$type}' AND id={$id}";
            return $this->db->query($sql);
        }

        public function get_active_callback()
        {
            $sql = "SELECT * FROM callback_form WHERE 'status' = 0 ORDER BY  `time` DESC  ";
            return $this->db->query($sql)->fetchAll();
        }

        public function update_callback($id)
        {
            $sql = "UPDATE callback_form SET status=1 WHERE id={$id}";

            return $this->db->query($sql);
        }

        public function get_order_text($spec_id)
        {

            $sql = "SELECT * FROM rotator_orders_text WHERE spec_main={$spec_id}";

            return $this->db->query($sql)->fetch();
            ;
        }

        public function update_order_text($data)
        {

            if($data['text_block_id'] > 0) {
                $sql = "UPDATE rotator_orders_text
            SET 
                text='{$data['text']}',
                active={$data['active']} 
            WHERE 
                spec_main={$data['spec_id']}
            ";
            } else {
                $sql = "INSERT INTO   rotator_orders_text
                SET
                    text='{$data['text']}',
                    active={$data['active']} ,
                    spec_main = {$data['spec_id']}
                ";
            }


            // print_r($sql);exit();
            return $this->db->query($sql);
        }

        public function get_order_text_front($spec_id)
        {

            $sql = "SELECT text FROM rotator_orders_text WHERE active=1 AND spec_main={$spec_id}";

            return $this->db->query($sql)->fetch();
            ;
        }

        public function get_CTR_filter_data($spec_id)
        {
            $sql = "SELECT * FROM ctr_filter WHERE spec_id =?";
            return $this->db->query($sql, array($spec_id))->fetch();
        }

        public function set_CTR_filter_data($data)
        {

            $sql = 'INSERT INTO ctr_filter (spec_id, stars_min,stars_max, views_min,views_max, logins_min,logins_max,limit_max) VALUES(?,?,?,?,?,?,?,?)
                ON DUPLICATE KEY UPDATE 
                spec_id=? , stars_min= ?,stars_max=?, views_min=?,views_max=?, logins_min=?,logins_max=?,limit_max=?, photo_priority=?
        ';

            return $this->db->query($sql, array($data['spec_id'], $data['stars_min'], $data['stars_max'], $data['views_min'], $data['views_max'], $data['logins_min'], $data['logins_max'], $data['limit_max'], $data['spec_id'], $data['stars_min'], $data['stars_max'], $data['views_min'], $data['views_max'], $data['logins_min'], $data['logins_max'], $data['limit_max'], $data['photo_priority']));
        }

    }
    