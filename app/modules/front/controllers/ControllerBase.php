<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/front/controllers/BaseController.php
     *
     * Base controller
     *
     */

    namespace Budmisto\Modules\Front\Controllers;

    use Phalcon\Mvc\Dispatcher;
    use Budmisto\Modules\Common\Controllers\RotatorController as RotatorController;

    class ControllerBase extends \Phalcon\Mvc\Controller
    {

        public $lang;
        public $siteUrl;
        public $actionName;
        public $myCache;
        public $user_level;
        public $user_id;
        public $rotator_img_data;
        public $rotator_text_data;
        public $commonModel;

        public function onConstruct()
        {
            $this->commonModel = new \Budmisto\Modules\Common\Models\Common();

            if($this->session->has('language')) {
                $this->lang = $this->session->get('language');
            } else {
                $this->lang = $this->config->application->default_lang;
                $this->session->set('language', $this->lang);
            }

            if($this->rotator_img_data == "") {
                $this->rotator_img_data = $this->get_rotator_data();
                $rotator = $this->rotator_img_data;
            } else {
                $rotator = $this->rotator_img_data;
            }

            if($this->rotator_text_data == "") {
                $this->rotator_text_data = $this->get_rotator_text_data();
                $rotator = $this->rotator_text_data;
            } else {
                $rotator = $this->rotator_text_data;
            }

            $this->siteUrl = $this->config->application->site_url;
            $this->user_level = $this->session->get('usr_level');
            $this->view->setVar('banner1', $this->config->other->banner1);
            //$this->view->setVar('banner2', $this->config->other->banner2);
            $this->view->setVar('banner2', $this->rotator_text_data);
            $this->view->setVar('banner3', $this->rotator_img_data);


            // print_r($_SESSION);die;
        }

        public function beforeExecuteRoute(Dispatcher $dispatcher)
        {
            $path = $dispatcher->getActionName();

            if($this->user_level && $path == 'signin') {
                /* $this->response->redirect($this->siteUrl);
                  $this->response->send(); */
                die;
            }
        }

        public function get_rotator_data()
        {
            $url = explode('/', $_GET[_url]);

            if(isset($url[1])) {
                $controller = $url[1];
            }
//Если выбран город
            $tmp = $this->dispatcher->getParam('place');

            if($tmp) {

                $tmp = explode('-', $tmp);
                $place['id'] = array_shift($tmp);
                $place['slug'] = implode('-', $tmp);

                if($place['id'] < 161)
                    $city = $this->commonModel->getCitiesNew($place);
                else
                    $city = $this->commonModel->getPlacesNew($place);
            }

            switch ($controller)
            {
                case 'catalog':

                    if($url[2] == 'order')
                        break;

                    if($url[2] == 'orders') {
                        $data[0]['id'] = 1000;
                    } else {
                        $data = $this->get_rotator_catalog_data($url[2], $url[3]);
                    }


                    if(isset($data[0]['id']) and ! isset($data[1]['id'])) {
                        $rotator_info = \Budmisto\Modules\Front\Models\Rotator::get_banner('banner', $controller, $city ? $city : false, $data[0]['id']);
                    } else {
                        $rotator_info = \Budmisto\Modules\Front\Models\Rotator::get_banner('banner', $controller, $city ? $city : false);
                    }

                    return $this->get_rotator_html($rotator_info);
                    break;

                case 'leftovers':

                    $data = $this->get_rotator_leftovers_data($url[2]);

                    if(isset($data[0]['id'])) {
                        $rotator_info = \Budmisto\Modules\Front\Models\Rotator::get_banner('banner', $controller, $city ? $city : false, $data[0]['id']);
                    } else {
                        $rotator_info = \Budmisto\Modules\Front\Models\Rotator::get_banner('banner', $controller, $city ? $city : false);
                    }

                    return $this->get_rotator_html($rotator_info);
                    break;
                case 'my':

                    $rotator_info = \Budmisto\Modules\Front\Models\Rotator::get_banner('banner', $controller);


                    return $this->get_rotator_html($rotator_info, true);
                    break;
                default:
                    $rotator_info = \Budmisto\Modules\Front\Models\Rotator::get_banner('banner', 'main');

                    return $this->get_rotator_html($rotator_info);
                    break;
            }
        }

        public function get_rotator_text_data()
        {

            $url = explode('/', $_GET[_url]);

            if(isset($url[1])) {
                $controller = $url[1];
            }

            //Если выбран город
            $tmp = $this->dispatcher->getParam('place');

            if($tmp) {

                $tmp = explode('-', $tmp);
                $place['id'] = array_shift($tmp);
                $place['slug'] = implode('-', $tmp);

                if($place['id'] < 161)
                    $city = $this->commonModel->getCitiesNew($place);
                else
                    $city = $this->commonModel->getPlacesNew($place);
            }

            switch ($controller)
            {
                case 'catalog':

                    if($url[2] == 'order')
                        break;
                 
                    if($url[2] == 'orders') {
                        $data[0]['id'] = 1000;
                    } else {
                        $data = $this->get_rotator_catalog_data($url[2], $url[3]);
                    }

                    if(isset($data[0]['id'])) {
                        $rotator_info = \Budmisto\Modules\Front\Models\Rotator::get_banner('text', $controller, $city ? $city : false, $data[0]['id']);
                    } else {
                        $rotator_info = \Budmisto\Modules\Front\Models\Rotator::get_banner('text', $controller, $city ? $city : false);
                    }

                    return $this->get_rotator_text_html($rotator_info);
                    break;

                case 'leftovers':

                    $data = $this->get_rotator_leftovers_data($url[2]);

                    if(isset($data[0]['id'])) {
                        $rotator_info = \Budmisto\Modules\Front\Models\Rotator::get_banner('text', $controller, $city ? $city : false, $data[0]['id']);
                    } else {
                        $rotator_info = \Budmisto\Modules\Front\Models\Rotator::get_banner('text', $controller, $city ? $city : false);
                    }

                    return $this->get_rotator_text_html($rotator_info);
                    break;

                case 'my':

                    $rotator_info = \Budmisto\Modules\Front\Models\Rotator::get_banner('text', $controller);

                    return $this->get_rotator_text_html($rotator_info);
                    break;


                default:
                    $rotator_info = \Budmisto\Modules\Front\Models\Rotator::get_banner('text', 'main');

                    return $this->get_rotator_text_html($rotator_info);
                    break;
            }

            return;
        }

        public function get_rotator_catalog_data($main_spec, $sub_spec)
        {
            return \Budmisto\Modules\Front\Models\Rotator::get_spec_id($main_spec, $sub_spec);
        }

        public function get_rotator_leftovers_data($main_spec)
        {
            return \Budmisto\Modules\Front\Models\Rotator::get_spec_id($main_spec);
        }

        public function get_rotator_html($data, $my_controller = false)
        {
            /*
              $count=count($data);
              if($count==0)return;

              //$html="<div style=' width:100%;'><div class='row'>";
              $html="<div style=' width:100%;'>";
              if($my_controller)
              $html="<div style=' width:100%;'>";

              if($count>0){
              for($i=0;$i<2;$i++){

              if(!strpos($data[$i]['text'],"img") and $i==1){
              $add_style="padding-left: 10px;";
              }

              if($data[$i]['url']!=''){
              $cursor="cursor:pointer;";
              $onclick="onclick=\"window.location.href='{$data[$i]['url']}'\"";
              }
              $html.="<div style='width:50%; float:{$data[$i]['position']}; {$cursor} {$add_style}'{$onclick} >{$data[$i]['text']}</div>";
              }
              }

              if($my_controller){
              $html.="</div>";
              }else{
              //$html.="</div></div>";
              $html.="</div><div class='blank5'></div>";
              }
             */

            $count = count($data);
            if($count == 0)
                return;

            $html = '<div class="row"><div class="col-md-12">';

            if($count > 0) {
                for ($i = 0; $i < 2; $i++) {

                    if(!isset($data[$i]))
                        continue;

                    if($data[$i]['url'] != '') {
                        $cursor = "cursor:pointer;";
                        $onclick = preg_match('|\(|', $data[$i]['url']) ? "onclick='{$data[$i]['url']}'" : "onclick=\"window.location.href='{$data[$i]['url']}'\"";
                    }

                    $pos = $data[$i]['position'] == "left" ? "float: left;" : "float: right;";

                    $html .= "<div class='col-md-6' style='{$pos}'>
                            <div class='bg_top_{$data[$i]['position']}'>
                                <div class='top_info'>
                                    <div class='info' style='{$cursor} border: 2px solid #F5CF46; position: relative;' {$onclick} >
										{$data[$i]['text']}
                                    </div>
                                </div>
                                <div class='blank5'></div>  
                            </div>
                        </div>";
                }
            }


            $html .= "</div></div>";

            return $html;
        }

        public function get_rotator_text_html($data)
        {
            $data = $data[0];
            if($data['text'] == "")
                return;
            $html .= "<div class='container pcatalog size100' id='info_text'><div style=''>";
            $html .= $data['text'];
            $html .= "</div></div>";

            return $html;
        }

        public function initialize()
        {
            $this->view->setVar('t', $this->_getTranslation());
            $this->view->setVar('lang', $this->lang);
            $this->view->setVar('url', $this->siteUrl);
            $this->view->setVar('is_logged', false);




            if($this->session->get('usr_level')) {
                $this->user_id = $this->session->get('usr_id');
                $this->view->setVar('usr_email', $this->session->get('usr_email'));
                $this->view->setVar('usr_id', $this->user_id);
                $this->view->setVar('usr_phone', $this->session->get('usr_phone'));
                $this->view->setVar('usr_level', $this->user_level);
                $this->view->setVar('is_logged', true);
                if($this->session->get('usr_email'))
                    $this->view->setVar('usr_label', $this->session->get('usr_email'));
                else
                    $this->view->setVar('usr_label', $this->session->get('usr_phone'));
            } else {
                $this->view->setVar('usr_level', false);
                $this->view->setVar('usr_id', false);
            }
            $this->view->setVar('stat_track', $this->config->other->stat_track);




            if(!$this->request->isAjax()) {

                //Проверка для строителя
                if(!$this->checkAllFieldsByWorker() and ! strpos($_SERVER['REQUEST_URI'], 'profile')) {
                    $this->response->redirect($this->siteUrl . '/my/profile?action=submit');
                    $this->response->send();
                }

                //Проверка для заказчика
                $this->checkAllFieldsByCustomer();
            }
        }

        public function checkAllFieldsByCustomer()
        {
            if(!$this->user_id)
                return true;

            $us = \Budmisto\Modules\Front\Models\Users::checkUserProfile($this->user_id);

            if($us['user_role_id'] == 3) {
                $last_order = \Budmisto\Modules\Front\Models\Catalog::getLastUserOrder($us['usid']);

                if(!$last_order)
                    return;

                if((int) $last_order['order_wrk_type'] < 1 and ! preg_match('/(my\/order\/)[0-9]{1,}/', $_SERVER['REQUEST_URI'])) {
                    $this->response->redirect($this->siteUrl . '/my/order/' . $last_order['id'] . '?action=submit');
                    $this->response->send();
                }
            }
        }

        public function checkAllFieldsByWorker()
        {

            if(!$this->user_id)
                return true;

            $us = \Budmisto\Modules\Front\Models\Users::checkUserProfile($this->user_id);

            if(!$us)
                return false;

            if($us['user_role_id'] == 1 or $us['user_role_id'] == 3)
                return true;

            foreach ($us as $key => $value) {
                if(!$value or $value == "") {

                    return false;
                }
            }

            return true;
        }

        protected function _getTranslation()
        {
            if(file_exists(APP_DIR . 'app/lang/' . $this->lang . '.php')) {
                require APP_DIR . 'app/lang/' . $this->lang . '.php';
            } else {
                require APP_DIR . 'app/lang/ru.php';
            }

            return new \Phalcon\Translate\Adapter\NativeArray(array(
                'content' => array_merge_recursive($navLabels, $commonLabels, $footerLabels, $userLabels, $blockLabels, $titlesLabels, $workLabels)
            ));
        }

        public function getInputFilter()
        {
            $filter = new \Phalcon\Filter();
            $filter->add('user_input', function($value)
            {
                $value = html_entity_decode($value, ENT_QUOTES, 'UTF-8');
                return htmlentities($value, ENT_QUOTES, 'UTF-8');
            });
            return $filter;
        }

        public function createPaginator($quantity, $current, $limit, $url)
        {
            if($quantity == 0)
                return;
            $current = (!$current) ? $current + 1 : $current;
            $list_class = "pagination pagination-lg";
            $links = 3;

            //var_dump($quantity, $current, $limit, $url); die;
            $last = ceil($quantity / $limit);
            $start = ( ( $current - $links ) > 0 ) ? $current - $links : 1;
            $end = ( ( $current + $links ) < $last ) ? $current + $links : $last;
            $html = '<ul class="' . $list_class . '">';
            $class = ( $current == 1 ) ? "disabled" : "";
            $prev = ( ($current - 1) <= 1) ? $url : $url . '?page=' . ( $current - 1 );
            $html .= '<li class="' . $class . '"><a href="' . $prev . '">&laquo;</a></li>';

            if($start > 1) {
                $html .= '<li><a href="' . $url . '">1</a></li>';
                $html .= '<li class="disabled"><span>...</span></li>';
            }

            for ($i = $start; $i <= $end; $i++) {
                $class = ( $current - 1 == $i - 1 ) ? "active" : "";
                $curr = ( $i == 1 ) ? $url : $url . '?page=' . $i;
                $html .= '<li class="' . $class . '"><a href="' . $curr . '">' . $i . '</a></li>';
            }

            if($end < $last) {
                $html .= '<li class="disabled"><span>...</span></li>';
                $html .= '<li><a href="' . $url . '?page=' . $last . '">' . $last . '</a></li>';
            }

            $class = ( $current == $last ) ? "disabled" : "";
            //$next = ( $current == $last )?$url . '?page=' . ( $current ):$url . '?page=' . ( $current + 1 );
            $next = ( $current == $last ) ? '#' : $url . '?page=' . ( $current + 1 );
            $html .= '<li class="' . $class . '"><a href="' . $next . '">&raquo;</a></li>';
            $html .= '</ul>';

            return $html;
        }

        public function wunderCache($key, $action = 'undef')
        {
            $key = $action . '-' . $key . '-' . $this->lang . '.txt';
            $this->view->cache(array('service' => 'viewCache', 'key' => $key));
            return $this->viewCache->start($key);
        }

        public function dumbError($message = "<h1>Unexpected error! Try again latter, please.</h1>", $redirect = false, $show404 = false)
        {
            $ip = $_SERVER['REMOTE_ADDR'];
            $uri = $_SERVER['REQUEST_URI'];
            $this->logger->log($message . ': ' . $ip . ' | ' . $uri, \Phalcon\Logger::ERROR);
            if(!$redirect) {
                echo $message;
            } else {
                if($show404)
                    $this->response->redirect($this->siteUrl . '/404');
                else
                    $this->response->redirect($this->siteUrl . '/helpcenter');
                $this->response->send();
            }
            die;
        }

        public function checkLogged($logged = false, $redirect = true)
        {
            if($this->user_level && $this->user_level < 3) {
                $this->response->redirect($this->siteUrl . '/workz/board');
                $this->response->send();
                die;
            }

            $ok = false;
            if(!$this->user_level && !$logged)
                $ok = true;
            if($this->user_level && $logged)
                $ok = true;

            if($redirect && $ok) {
                $this->response->redirect($this->siteUrl);
                $this->response->send();
                die;
            }

            return ($this->user_level) ? true : false;
        }

    }
    