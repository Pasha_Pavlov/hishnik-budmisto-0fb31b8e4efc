<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/front/controllers/UsersController.php
     *
     * Users controller
     *
     */

    namespace Budmisto\Modules\Front\Controllers;

    use Budmisto\Modules\Front\Models\Users as BmUsers;
    use Budmisto\Modules\Front\Models\Catalog as BmCatalog;
    use Budmisto\Modules\Common\Models\Common as Common;
    use Budmisto\Lib\Mailer\Mail as BMail;

    class UsersController extends ControllerBase
    {

        public $users;

        public function initialize()
        {
            parent::initialize();
            $this->users = new BmUsers();
            //$this->users->setLang($this->lang);               
        }

        private function setMarkIfHasOrders($catalog)
        {
            if($this->user_level > 3 && $this->user_level < 7) {
                $has_orders = $this->session->has('has_orders');
                if($has_orders)
                    $has_orders = $this->session->get('has_orders');
                else {
                    $has_orders = $catalog->countUserOrders($this->user_id);
                    $this->session->set('has_orders', $has_orders);
                }
            } else
                $has_orders = false;
            $this->view->setVar('has_orders', $has_orders);
        }

        public function setCommercialsOptionsAction()
        {

            $add_shop = $this->request->getPost('add_shop', 'int', false);

            $commercial_id = $this->request->getPost('commercial_id', 'int', false);
            $res = BmCatalog::setCommercialDisplayedUser($commercial_id, $this->user_id);

            if($add_shop == 1 and $res) {
                $res = BmCatalog::setUserShops($commercial_id, $this->user_id);
            }


            print_r($res);
            exit();
        }

        public function delCommercialFromSessionAction()
        {
            $this->session->remove("no_commercial");
            exit();
        }

        public function shopInfoAction()
        {
            $shop_id = $this->request->getPost('shop_id', 'int', false);
            $shop = BmCatalog::get_single_shop($shop_id, $this->user_id);

            print_r(json_encode($shop));
            exit();
        }

        public function indexAction()
        {

            if(isset($_GET['referer_login'])) {
                $this->flashSession->success('referer_login');
                $this->response->redirect("/my");
            }

            if(isset($_GET['selected_commercial'])) {
                $this->flashSession->message("selected_commercial", $_GET['selected_commercial']);
                $this->response->redirect("/my");
            }

            $user_shops = BmCatalog::get_user_shops($this->user_id);
            $this->view->setVar('user_shops', $user_shops);

            $this->checkLogged();
            $catalog = new BmCatalog();

            if($this->request->isAjax()) {

                /* ajax подгрузка заказов на основе существующей выборки */
                if(isset($_POST['ajax_query_orders']) or isset($_GET['ajax_query_orders_object'])) {

                    if(isset($_POST['ajax_query_orders'])) {
                        $start = $this->request->getPost('start', 'int', false);
                    } else {
                        $start = $this->request->getQuery('start', 'int', false);
                    }

                    ///////////////////////////////////
                    $last_orders = $catalog->getLastUserOrdersList($this->user_id, $start * 10, 10);
                    ///////////////////////////////////
                    $blocked_orders = $catalog->getBlockedOrders((int) $this->session->get('usr_id'));


                    $this->view->setVar('promo', $last_orders);

                    if($blocked_orders['blocked_orders'] != "") {
                        $this->view->setVar('blocked_orders', json_decode($blocked_orders['blocked_orders'], true));
                    } else {
                        $this->view->setVar('blocked_orders', false);
                    }
                    if(isset($_POST['ajax_query_orders'])) {
                        return $this->view->pick('users/ajax_orders');
                        exit();
                    }
                    if(isset($_GET['ajax_query_orders_object'])) {

                        foreach ($last_orders as $key => &$value) {
                            if(in_array($value['id'], $blocked_orders) or isset($value['close_date'])) {
                                unset($last_orders[$key]);
                            } else {
                                $all_ord[] = $value['id'];
                            }
                        }

                        $all_ord = json_encode($all_ord);

                        print_r($all_ord);
                        exit();
                    }
                }

                $blocked_order = (int) $_POST['blocked_order_id'];
                $query = $catalog->addBlockedOrders((int) $this->session->get('usr_id'), $blocked_order);

                if($query) {
                    print_r('success');
                }
                exit();
            }

            if($this->user_level == 3) {
                $order_id = $catalog->getLastUserOrder($this->user_id)['id'];

                if($order_id) {
                    $this->response->redirect($this->siteUrl . '/my/' . $order_id);
                    $this->response->send();
                    die;
                }
                $this->view->pick('users/index_c');
            } elseif($this->user_level < 7) {


                $user_info = $this->users->getUserProfile($this->user_id);
                $this->view->setVar('user_info', $user_info);

                $this->setMarkIfHasOrders($catalog);
                $last_orders = $catalog->getLastUserOrdersList($this->user_id, 0, 10, 150);


                if(!$last_orders) {
                    $last_orders = $catalog->getLastOrdersList(0, 10);
                    $this->view->setVar('promo_error', true);
                }

                $this->view->setVar('promo', $last_orders);
                $requests = $catalog->getLastConnectList($this->user_id, 0, 10);


                if(is_array($requests)) {
                    foreach ($requests as $key => &$value) {
                        $value['premium_block'] = $catalog->getCurrentPremiumBlockByOrder($value['id']);

                        if(isset($new_req[$value['id']])) {
                            if($new_req[$value['id']]['choice_id'] == "" and $value['choice_id'] != "")
                                $new_req[$value['id']]['choice_id'] = $value['choice_id'];

                            if($new_req[$value['id']]['offer_id'] == "" and $value['offer_id'] != "")
                                $new_req[$value['id']]['offer_id'] = $value['offer_id'];
                        }
                        else {
                            $new_req[$value['id']] = $value;
                        }
                        $connect = $catalog->getOrderConnect($value['id']);
                        foreach ($connect['choice_id'] as $o) {
                            if($this->session->get('usr_id') == $o['o_uid']) {
                                $informed[] = $value['id'];
                            }
                        }

                        $user_informed = $catalog->checkInformed(['user_phone' => $this->session->get('usr_phone')], $value);

                        if($user_informed)
                            $informed[] = $value['id'];
                    }
                }



                $requests = array_values($new_req);

                $this->view->setVar('current_premium_block', $current_premium_block);

                $this->view->setVar('requests', $requests);
                $this->view->setVar('informed', $informed);


                $this->view->pick('users/index_p');

                $blocked_orders = $catalog->getBlockedOrders((int) $this->session->get('usr_id'));
                if($blocked_orders['blocked_orders'] != "") {
                    $this->view->setVar('blocked_orders', json_decode($blocked_orders['blocked_orders'], true));
                } else {
                    $this->view->setVar('blocked_orders', false);
                }
            }
        }

        public function offersAction()
        {

            if(isset($_GET['no_commercial'])) {
                $this->session->set("no_commercial", $_GET['no_commercial']);
                $this->response->redirect("/my/offers");
                $this->response->send();
            }

            if($this->session->has("no_commercial")) {
                $this->view->setVar('no_commercial', $this->session->get("no_commercial"));
            }


            $user_shops = BmCatalog::get_user_shops($this->user_id);
            $this->view->setVar('user_shops', $user_shops);

            if($this->request->isAjax()) {

                $id = (int) $_POST['offer'];
                $req = $this->users->deleteWorkkOffer((int) $this->session->get('usr_id'), $id);
                ($req) ? print_r('success') : print_r('error');
                exit();
            }

            $offers = $this->users->getWorkkOffer((int) $this->session->get('usr_id'));

            $catalog = new BmCatalog();
            foreach ($offers as $key => &$value) {
                $user_id = $value['id_customer'];
                $last_order = $catalog->getLastOrder_new($user_id);
                if($last_order)
                    $value['last_order'] = $last_order;
                $this->users->updateOfferStatus($value['id'], "1");
            }

            $this->view->setVar('offers', $offers);
        }

        public function activAction()
        {
            if($this->request->isAjax()) {

                $blocked_user = (int) $_POST['blocked_user'];

                $catalog = new BmCatalog();

                $query = $catalog->addBlockedWorkers((int) $this->session->get('usr_id'), $blocked_user);

                if($query)
                    print_r('success');

                exit();
            }

            $order_id = $this->dispatcher->getParam('id');
            if(!$order_id)
                $this->dumbError('(Active order) bad order ID');

            $this->checkLogged();

            $catalog = new BmCatalog();
            $this->setMarkIfHasOrders($catalog);
            //$common = new Common()

            $list = $catalog->getUserOrdersList($this->user_id, 0, 20, true);
            $this->view->setVar('orders', $list);

            $order = $catalog->getOrder($order_id, $this->user_id);
            if(!$order) {
                $this->dumbError('(Order) Not found', true, true);
            }


            $blocked_workers = $catalog->getBlockedWorkers((int) $this->session->get('usr_id'));

            $page = (int) $_GET['page'];

            if(!$page)
                $page = 1;

            if($page and $page > 1)
                $this->view->setVar('prev_page', strip_tags($_GET['_url']) . '?page=' . --$page);
            $this->view->setVar('next_page', strip_tags($_GET['_url']) . '?page=' . ++$page);

            $top_catalog = $catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top_catalog);
            if($this->request->isPost()) {
                $order['order_wrk_type'] = (int) $this->request->getPost('new_speciality', 'int', false);
            }

            $this->view->setVar('order', $order);

            $workers = $catalog->getSubWorkersListByCoords($order['order_wrk_type'], $order['order_wrk_spec'], $order['lat'], $order['lng'], $page, $this->config->lists->limit, false, 50, $blocked_workers);

            if(!$workers) {
                $workers = $catalog->getTopWorkersListByCoords($order['order_wrk_type'], $order['lat'], $order['lng'], 0, $this->config->lists->limit);
                $this->view->setVar('workers_not_found_sub', true);
            }
            $offer_workers = $catalog->getOffersToWorker((int) $this->session->get('usr_id'));
            $this->view->setVar('offer_workers', $offer_workers);


            //Добавляем юзера, с которого перешли на регистрацию
            if(isset($_SESSION['referer_user']) and $_SESSION['referer_user'] > 0) {
                $ref_user = (int) $_SESSION['referer_user'];
                unset($_SESSION['referer_user']);
                $this->view->setVar('ref_user', $ref_user);
            }

            $this->view->setVar('workers', $workers);

            $countChoicesToday = count($catalog->countChoicesToday($order_id));
            $limit = $catalog->getLimitChoices($this->session->get('usr_id'));

            if($countChoicesToday < 10)
                $this->view->setVar('limit', 10 - $countChoicesToday + $limit);
            else
                $this->view->setVar('limit', $limit);


            if($blocked_workers['blocked_workers'] != "") {
                $this->view->setVar('blocked_workers', json_decode($blocked_workers['blocked_workers'], true));
            } else {
                $this->view->setVar('blocked_workers', false);
            }


            $selected = $catalog->getMyOrderConnect($order_id);
            $this->view->setVar('selected', $selected);

            $LastPremiumBlock = $catalog->getCurrentPremiumBlockByOrder($order_id, "0");
            $this->view->setVar('LastPremiumBlock', $LastPremiumBlock);

            $CurrentPremiumBlock = $catalog->getCurrentPremiumBlockByOrder($order_id);
            $this->view->setVar('CurrentPremiumBlock', $CurrentPremiumBlock);


            $offer = $catalog->getMyOrderOffer($order_id);
            $this->view->setVar('offer', $offer);

            $this->view->pick('users/activ_c');

            // разницу я не увидел и не понимаю смысла в другом шаблоне,
            // Олег тоже не смог объяснить для чего он нужен
            /*
              if ($this->user_level == 3) {
              $this->view->pick('users/activ_c');
              } elseif ($this->user_level < 7) {
              $this->view->pick('users/activ_p');
              }
             */
        }

        public function ordersAction()
        {
            $this->checkLogged();

            $catalog = new BmCatalog();
            $this->setMarkIfHasOrders($catalog);

            /* $list = $catalog->getUserOrdersList($this->user_id, 0, 20);
              $this->view->setVar('orders', $list); */
            $order_id = $catalog->getLastUserOrder($this->user_id)['id'];
            if($order_id) {
                $this->response->redirect($this->siteUrl . '/my/' . $order_id);
                $this->response->send();
                die;
            }
        }

        public function orderAction()
        {
            if($this->request->isAjax()) {

                $spec_main = (int) $_POST['spec_main'];
                $text = \Budmisto\Modules\Front\Models\Rotator::get_order_text_front($spec_main);
                print_r($this->textengine->convertBB2Html($text['text']));
                exit();
            }

            $this->checkLogged();

            $order_id = $this->dispatcher->getParam('id');
            if(!$order_id)
                $this->dumbError('(Edit order) ID is 0');

            $catalog = new BmCatalog();

            if($this->request->isPost() && $this->security->checkToken()) {
                //echo '<pre>';

                $data['placelat'] = $this->request->getPost('lat', 'float', 0);
                $data['placelng'] = $this->request->getPost('lng', 'float', 0);

                $district = $this->request->getPost('district', array('trim', 'string'), false);
                if($district)
                    $data['placeid'] = $district;

                $chplace = $this->request->getPost('chplace', 'int', 0);
                if($chplace) {
                    $data['placelat'] = $this->request->getPost('placelat', 'float', false);
                    $data['placelng'] = $this->request->getPost('placelng', 'float', false);
                    $data['placeid'] = $this->request->getPost('placeid', array('trim', 'string'), false);
                    $data['placeaddr'] = $this->request->getPost('placeaddr', array('trim', 'string'), false);
                    $data['placetown'] = $this->request->getPost('placetown', array('trim', 'string'), false);
                    $data['placedistrict2'] = $this->request->getPost('placedistrict2', array('trim', 'string'), false);
                    $data['placedistrict3'] = $this->request->getPost('placedistrict3', array('trim', 'string'), false);
                    $data['placeregion'] = $this->request->getPost('placeregion', array('trim', 'string'), false);
                    $data['placecountry'] = $this->request->getPost('placecountry', array('trim', 'string'), false);
                    $data['placeroute'] = $this->request->getPost('placeroute', array('trim', 'string'), false);
                    $data['placextra'] = $this->request->getPost('placextra', array('trim', 'string'), false);

                    if($data['placetown'] && $data['placeid'] && $data['placelat'] && $data['placelng'] && $data['placeregion']) {
                        //$this->dumbError('(Create order) Wrong location', false, true);

                        //$common = new Common()
                        //$has_place = $this->commonModelcheckPlace($data['placeid']);
                        if($data['placeroute'] && !$data['placextra']) {
                            $key = $this->config->application->server_key;
                            $ch = curl_init();
                            $url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query=' . urlencode($data['placetown'] . ',' . $data['placeregion']);
                            $url .= '&key=' . $key . '&language=ru';
                            $cert = APP_DIR . 'var/cacerts.pem';

                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HEADER, false);
                            curl_setopt($ch, CURLOPT_CAINFO, $cert);

                            session_write_close();
                            $responce = curl_exec($ch);
                            session_start();
                            curl_close($ch);

                            $res = json_decode($responce);
                            $d['placetown'] = $res->results[0]->name;
                            $d['placeid'] = $res->results[0]->place_id;
                            $d['placelat'] = $res->results[0]->geometry->location->lat;
                            $d['placelng'] = $res->results[0]->geometry->location->lng;
                            $d['placeregion'] = $data['placeregion'];

                            $this->commonModeladdNewPlace($d);
                        } else
                            $this->commonModeladdNewPlace($data);
                        $this->commonModeleditOrderPlace($order_id, $data);
                    }
                }

                if($data['placelat'] && $data['placelng'] && !$chplace) {
                    //$common = new Common()
                    $this->commonModeleditOrderCoords($order_id, $data);
                }

                $data['speciality'] = $this->request->getPost('speciality', 'int', false);
                $data['subspeciality'] = $this->request->getPost('subspeciality', 'int', false);
                $data['bugdet'] = $this->request->getPost('budget', 'int', false);
                $data['bugdet'] = !$data['bugdet'] ? 1 : $data['bugdet'];
                $data['limcall'] = $this->request->getPost('limcall', 'int', false);
                $data['limcall'] = !$data['limcall'] ? 12 : $data['limcall'];
                $data['start'] = $this->request->getPost('start', 'int', false);
                $data['start'] = !$data['start'] ? 1 : $data['start'];
                $actual = (int) $this->request->getPost('period', 'int', false);
                $actual = $actual < 1 ? 30 : $actual;
                $data['description'] = $this->request->getPost('desc', array('trim', 'string'), false);
                $data['user_id'] = $this->user_id;
                $data['order_id'] = $order_id;
                $data['compare_materials'] = $this->request->getPost('compare_materials', array('trim', 'string'), false) == "on" ? 1 : 0;
                $data['compare_materials_notes'] = $this->request->getPost('compare_materials_notes', array('trim', 'string'), '');
                
                $ok = false;

                if($data['speciality'] && $data['subspeciality'] && $data['bugdet'] && $data['limcall'] && $data['start'] && $actual && $data['description']) {

                    $data['actual_period'] = date('Y-m-d H:i:s', strtotime('+' . $actual . ' days'));
                    $filter = $this->getInputFilter();
                    $data['description'] = $filter->sanitize($data['description'], 'user_input');
                    $ok = $catalog->updateOrder($data);
                }

                if(!$ok) {
                    $this->view->setVar('result', $this->_getTranslation()->_('e_edit'));
                    $this->view->setVar('error', true);
                } else {
                    //$this->view->setVar('result', $this->_getTranslation()->_('ok_edit'));
                    //$this->view->setVar('error', false);

                    /* Метод выборки специалистов с 10 звездами и отправкой им СМС о создании заказа */
                    $this->inform_workers($data);

                    $this->response->redirect($this->siteUrl . '/my/' . $order_id);
                    $this->response->send();
                    die;
                }
            }

            $order = $catalog->getOrder($order_id, $this->user_id);
            if(!$order) {
                $this->dumbError('(Order) Not found', true, true);
            }
            $this->view->setVar('order', $order);
            $this->view->setVar('map_key', $this->config->application->map_key);

            /* if ($order['extra_place_id']) {
              $xplace = $this->commonModelgetExtraPlace($order['extra_place_id']);
              $this->view->setVar('xplace', $xplace);
              } */
            $top = $catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);
            if($order['order_wrk_type']) {
                $sub = $catalog->getSubSpecListById($order['order_wrk_type']);
                $this->view->setVar('sub_catalog', $sub);
            }
            $budget_list = $catalog->getBudgetList();
            $this->view->setVar('budget_list', $budget_list);
            $call_count = $catalog->getCallCountList();
            $this->view->setVar('call_count', $call_count);
            $when_list = $catalog->getWhenList();
            $this->view->setVar('when_list', $when_list);

            /* $districts = $this->commonModelgetDistricts($order['place_id']);
              $this->view->setVar('districts', $districts); */

            if($this->user_level == 3) {
                $this->view->setVar('just_registered', $this->session->get('just_registered'));
                $this->view->pick('users/order_c');
            } elseif($this->user_level < 7) {
                $this->view->pick('users/order_p');
            }
        }

        public function inform_workers($data)
        {

            $req = BmCatalog::getStatusPremiumBlockByOrder($data['order_id']);

            if($req != 1)
                return;
            $users = $this->users->getUsersByStarAndSpec(10, $data, 5, 39);

            $phone = ConnectController::clearPhone($this->config->other->admin_phone);
            ConnectController::sendMSG($phone, "Новый заказ на сайте budmisto");

            if($users) {
                $connect = new ConnectController();
                $this->users->setInformedUsers($users, $data['order_id']);
                // $this->users->setAutoChoice($users,$data['order_id']);

                foreach ($users as $key => $value) {

                    $d['getter_phone'] = $value['user_phone'];
                    $d['sender_phone'] = $value['order_phone'];
                    $d['order_id'] = $data['order_id'];
                    $d['order_specialization'] = $value['order_specialization'];
                    $d['type'] = 3;
                    $d = json_encode($d);
                    $this->users->setCronSeries($d);   //Отправка с задержкой
                    unset($d);
                    //$connect->sendOneAction($data);     //отправка без задержки           
                }
            } else {
                return;
            }
        }

        public function orderCloseAction()
        {
            $this->checkLogged();

            $this->view->disable();
            $order_id = $this->dispatcher->getParam('id');
            if($order_id) {
                $catalog = new BmCatalog();
                $catalog->closeOrder($order_id);
            }
            $this->response->redirect($this->siteUrl . '/my/order/' . $order_id);
            $this->response->send();
            die;
        }

        public function orderOpenAction()
        {
            $this->checkLogged();
            $this->view->disable();

            $order_id = $this->dispatcher->getParam('id');
            if($order_id) {
                $catalog = new BmCatalog();
                $catalog->openOrder($order_id);
            }
            $this->response->redirect($this->siteUrl . '/my/order/' . $order_id);
            $this->response->send();
            die;
        }

        public function profileAction()
        {

            $this->checkLogged();

            $submit_type = $this->request->getPost('submit_type', 'int', false);

            $profile = $this->users->getUserProfile($this->user_id);

            if($this->request->isPost() && $this->security->checkToken()) {
                // update profile

                if($submit_type == 1) {

                    $data['text'] = $this->request->getPost('desc', array('trim', 'string'), '');

                    if($data['text']) {
                        $filter = $this->getInputFilter();
                        $data['text'] = $this->textengine->convertBB2Html($filter->sanitize($data['text'], 'user_input'));
                        $data['brief'] = $this->textengine->getBrief($data['text'], $this->config->lists->big_brief);
                    } else {
                        $data['brief'] = '';
                    }
                    $res = $this->users->updateUserDescr($data);
                    if($data['text'] and $data['text'] != "") {
                        $this->users->updateUserStars(3);
                    }


                    $chplace = $this->request->getPost('chplace', 'int', 0);
                    if($chplace) {
                        $data['placelat'] = $this->request->getPost('placelat', 'float', false);
                        $data['placelng'] = $this->request->getPost('placelng', 'float', false);
                        $data['placeid'] = $this->request->getPost('placeid', array('trim', 'string'), false);
                        $data['placeaddr'] = $this->request->getPost('placeaddr', array('trim', 'string'), false);
                        $data['placetown'] = $this->request->getPost('placetown', array('trim', 'string'), false);
                        $data['placedistrict2'] = $this->request->getPost('placedistrict2', array('trim', 'string'), false);
                        $data['placedistrict3'] = $this->request->getPost('placedistrict3', array('trim', 'string'), false);
                        $data['placeregion'] = $this->request->getPost('placeregion', array('trim', 'string'), false);
                        $data['placecountry'] = $this->request->getPost('placecountry', array('trim', 'string'), false);
                        $data['placeroute'] = $this->request->getPost('placeroute', array('trim', 'string'), false);
                        $data['placextra'] = $this->request->getPost('placextra', array('trim', 'string'), false);

                        if($data['placetown'] && $data['placeid'] && $data['placelat'] && $data['placelng'] && $data['placeregion']) {

                            ////$common = new Common()
                            if($data['placeroute'] && !$data['placextra']) {
                                $key = $this->config->application->server_key;
                                $ch = curl_init();
                                $url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query=' . urlencode($data['placetown'] . ',' . $data['placeregion']);
                                $url .= '&key=' . $key . '&language=ru';
                                $cert = APP_DIR . 'var/cacerts.pem';

                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_HEADER, false);
                                curl_setopt($ch, CURLOPT_CAINFO, $cert);

                                session_write_close();
                                $responce = curl_exec($ch);
                                session_start();
                                curl_close($ch);

                                $res = json_decode($responce);
                                $d['placetown'] = $res->results[0]->name;
                                $d['placeid'] = $res->results[0]->place_id;
                                $d['placelat'] = $res->results[0]->geometry->location->lat;
                                $d['placelng'] = $res->results[0]->geometry->location->lng;
                                $d['placeregion'] = $data['placeregion'];
                                $this->commonModel->addNewPlace($d);
                            } else
                                $this->commonModel->addNewPlace($data);

                            if($this->user_level > 3 && $this->user_level < 7) {
                                $this->commonModel->addUserPlace($this->user_id, $data);
                            }
                            if($this->user_level == 3) {
                                $this->commonModel->editUserPlace($this->user_id, $data);
                            }
                        }
                    }

                    $d['name'] = $this->request->getPost('name', array('trim', 'string'), false);
                    $d['user_alt'] = $this->request->getPost('user_alt', array('trim', 'string'), false);



                    $d['soc_vk'] = $this->request->getPost('soc_vk', array('string')) == 'on' ? 1 : 0;
                    $d['soc_fb'] = $this->request->getPost('soc_fb', array('string')) == 'on' ? 1 : 0;
                    $d['soc_ok'] = $this->request->getPost('soc_ok', array('string')) == 'on' ? 1 : 0;

                    $d['phone'] = $this->request->getPost('phone', array('trim', 'string'), false);
                    $d['email'] = $this->request->getPost('email', array('trim', 'email'), false);
                    $d['lang'] = $this->request->getPost('lang', array('trim', 'string'), false);


                    if($d['phone'] || $d['lang'] || $d['name'] || $d['email']) {
                        $res = $this->users->updateUserProfile($d);
                        if($res) {
                            $this->view->setVar('result', $this->_getTranslation()->_('l_ok_profile'));
                            $this->view->setVar('error', false);
                        }
                    }

                    if($profile['user_alt'] != $d['user_alt']) {
                        if(!$this->users->updateUserAlt($d['user_alt'])) {
                            $this->view->setVar('result', 'Эта ссылка уже занята');
                            $this->view->setVar('error', true);
                        } else {
                            $this->view->setVar('result', $this->_getTranslation()->_('l_ok_profile'));
                            $this->view->setVar('error', false);
                        }
                    }


                    $data['email'] = $this->request->getPost('email', array('trim', 'email'), '');
                    $data['phone1'] = $this->request->getPost('phone1', array('trim', 'string'), '');
                    $data['phone2'] = $this->request->getPost('phone2', array('trim', 'string'), '');


                    $data['website'] = $this->request->getPost('website', array('trim', 'string'), '');
                    if($this->user_level == 6) {
                        $data['name'] = $this->request->getPost('name', array('trim', 'string'), '');
                        $data['edrpou'] = $this->request->getPost('edrpou', array('trim', 'string'), '');
                        $data['chief'] = $this->request->getPost('chief', array('trim', 'string'), '');
                        $data['address'] = $this->request->getPost('address', array('trim', 'string'), '');
                    }


                    $not_empty = false;
                    $res = false;
                    foreach ($data as $key => $val) {
                        if($val)
                            $not_empty = true;
                    }
                    if($not_empty)
                        $res = $this->users->updateUserInfo($data);

                    if(!$res) {
                        $this->view->setVar('result', $this->_getTranslation()->_('e_add_text'));
                    }
                }
                if($submit_type == 3) {

                    if($this->request->hasFiles() == true) {
                        $files = $this->request->getUploadedFiles();
                        $titles = $this->request->getPost('titles');
                        $count = sizeof($files);

                        for ($i = 0; $i < $count; $i++) {
                            $fsize = $files[$i]->getSize();

                            if($titles[$i] && $fsize > 0 && $fsize < 2097152) {
                                $title = $titles[$i];
                                $file = $files[$i];
                                $res_filename = $this->fileswork->moveToUserFolder($file, $this->user_id);
                                // NEED REFACT!
                                $this->users->addUserFile($title, $res_filename);

                                if($profile['stars'] > 2 and $profile['stars'] < 6) {
                                    BmUsers::updateUserStars(6);
                                }
                            }
                        }
                        $this->fileswork->updateUserGallery($this->user_id);
                    }
                }

                $this->response->redirect($this->siteUrl . '/my/profile');
                $this->response->send();
            }


            $this->view->setVar('profile', $profile);
            $this->view->setVar('map_key', $this->config->application->map_key);
            $places = $this->users->getPlaces($this->user_id);
            $this->view->setVar('places', $places);

            if($this->user_level > 3 && $this->user_level < 7) {
                $info = $this->users->getUserInfo($this->user_id);
                if($info['description'])
                    $info['description'] = $this->textengine->convertHtml2BB($info['description']);
                $this->view->setVar('info', $info);
            }

            $img_files = $this->users->getUserFiles('image', $this->user_id);
            $doc_files = $this->users->getUserFiles('file', $this->user_id);
            $this->view->setVar('img_files', $img_files);
            $this->view->setVar('doc_files', $doc_files);
            $this->view->setVar('fpath', '/data/users/');

            $this->fileswork->clearUserTempFolder($this->user_id);
            if($this->user_level == 3) {
                $this->view->pick('users/profile_c');
            } elseif($this->user_level < 7) {
                $catalog = new BmCatalog();
                $this->setMarkIfHasOrders($catalog);
                $top_catalog = $catalog->getTopSpecList();
                $sub_catalog = $catalog->getSubSpecList();
                $this->view->setVar('top_catalog', $top_catalog);
                $this->view->setVar('sub_catalog', $sub_catalog);

                $user_specs = $this->users->getUserSpecs($this->user_id);
                $this->view->setVar('user_specs', $user_specs);
                $this->view->pick('users/profile_p');
            }
        }

        public function delPlaceAction()
        {
            $this->checkLogged();

            $place_id = $this->dispatcher->getParam('id');
            $this->users->delUserPlaces($this->user_id, $place_id);
            $this->response->redirect($this->siteUrl . '/my/profile');
            $this->response->send();
            die;
        }

        public function changeRoleAction()
        {
            $this->checkLogged();

            if($this->request->isPost() && $this->request->isAjax() && $this->security->checkToken()) {


                $curent_role = $this->request->getPost('curent_role', 'int', false);
                $new_role = $this->request->getPost('newrole', 'int', false);

                if($curent_role == $new_role) {

                    print_r("not_changed");
                    exit();
                }

                if(!in_array($newrole, array(1, 2, 3, 4)))
                    $this->dumbError('(Change role) Bad role number ' . $newrole);

                switch ($newrole)
                {
                    case 3: $new_role = 'ЗАКАЗЧИК';
                        break;
                    case 4: $new_role = 'МАСТЕР';
                        break;
                    case 5: $new_role = 'БРИГАДА СТРОИТЕЛЕЙ';
                        break;
                    case 6: $new_role = 'СТРОИТЕЛЬНАЯ ФИРМА';
                        break;
                }

                $admin_email = $this->config->application->admin_email;
                $mail = new BMail('common', 'info');
                $mail->setOptions($this->siteUrl);
                $data['title'] = 'Пользователь ' . $this->session->get('usr_email') . ' желает изменить роль!';
                $data['message'] = 'Новая роль: ' . $new_role . ', внесите изменения.';
                $mail->send('Изменить роль', $admin_email, $data);
            } else {
                $this->dumbError('(Change role) Bad request');
            }
            // NEED REFACT
        }

        public function viewAction()
        {
            $user_id = $this->dispatcher->getParam('id');

            if($this->request->isAjax()) {
                if(!$user_id) {
                    exit();
                }

                if($this->request->getPost('set_referer_user', 'int', false)) {
                    $_SESSION['referer_user'] = $user_id;
                    exit('success');
                } else {
                    exit();
                }
            }

            if(!$user_id) {
                $this->response->redirect($this->siteUrl . '/404');
                $this->response->send();
                die;
            }
            $user = $this->users->getUserProfile($user_id);

            if(!$user) {
                $this->response->redirect($this->siteUrl . '/404');
                $this->response->send();
                die;
            }

            if($user['is_active'] == 0) {
                $this->view->pick('users/notactive');
            }

            if($user['user_role_id'] < 3) {
                $this->response->redirect($this->siteUrl . '/404');
                $this->response->send();
                die;
            }

            if($user['user_role_id'] > 3 && $user['user_role_id'] < 7) {
                $user_info = $this->users->getUserInfo($user_id);
                $this->view->setVar('info', $user_info);

                $user_files = $this->users->getUserFiles('file', $user_id);
                $user_images = $this->users->getUserFiles('image', $user_id);
                $this->view->setVar('img_files', $user_images);
                $this->view->setVar('doc_files', $user_files);
                $this->view->setVar('fpath', '/data/users/');

                $user_specs = $this->users->getUserSpecs($user_id);
                $this->view->setVar('user_specs', $user_specs);
                if($user_specs) {
                    $user_prices = $this->users->getUserPrices($user_id);
                    $this->view->setVar('user_prices', $user_prices);
                }
            }

            $places = $this->users->getUserPlaces($user_id);
            $this->view->setVar('places', $places);

            $this->view->setVar('profile', $user);
            $this->view->setVar('user_id', $this->user_id);
            $r = $this->request->getHTTPReferer();

            $this->view->setVar('map_key', $this->config->application->map_key);
            /* //$common = new Common()
              $place = $this->commonModelgetPlace($user['user_place_id']);
              $this->view->setVar('place', $place); */


            if($user['user_role_id'] > 3 && $user['user_role_id'] < 7) {

                $catalog = new BmCatalog;
                $list = $catalog->getUserOrdersList($this->user_id, 0, 20, true);

                if(count($list)) {
                    foreach ($list as $key => $value) {
                        $order_ids[] = $value['id'];
                    }
                

					$selected_users = $catalog->getMyOrderConnect($order_ids);
					if(is_array($selected_users)) {
						foreach ($selected_users as $key => $value) {
							$selected[] = $value['id'];
						}
					}
				}
                $this->view->setVar('selected', $selected);
                $this->view->setVar('last_order', $list[0]);

                $this->view->pick('users/view_p');
            }
            if($user['user_role_id'] == 3) {
                $this->view->pick('users/view_c');
            }
        }

        public function altViewAction()
        {
            $user_alt = $this->dispatcher->getParam('alt');
            if(!$user_alt) {
                $this->response->redirect($this->siteUrl . '/404');
                $this->response->send();
                die;
            }
            $user = $this->users->getUserProfileByAlt($user_alt);
            if(!$user) {
                $this->response->redirect($this->siteUrl . '/404');
                $this->response->send();
                die;
            }

            $this->response->redirect($this->siteUrl . '/user/' . $user['id']); //var_dump($go); die;
            $this->response->send();
        }

        public function deleteAction($file_id)
        {
            if(!$this->checkLogged(true, false) || !is_numeric($file_id)) {
                $this->response->redirect($this->siteUrl);
                $this->response->send();
                die;
            }
            // NEED REFACT

            $filename = $this->users->deleteUserFile($file_id);
            if($filename)
                $this->fileswork->deleteUserFileByName($filename, $this->user_id);
            $this->response->redirect($this->request->getHTTPReferer());
            $this->response->send();
            die;
        }

        public function securityAction()
        {
            $this->checkLogged();
            $this->setMarkIfHasOrders(new BmCatalog());

            if($this->request->isPost() && $this->security->checkToken()) {
                $oldpasswd = $this->request->getPost('oldpasswd', array('trim', 'string'), false);
                $newpasswd = $this->request->getPost('passwd1', array('trim', 'string'), false);
                $res = $this->users->updatePassword($oldpasswd, $newpasswd);
                if(!$res) {
                    $this->view->setVar('result', $this->_getTranslation()->_('l_fail_reset'));
                    $this->view->setVar('error', true);
                } else {
                    $this->view->setVar('result', $this->_getTranslation()->_('l_ok_reset'));
                    $this->view->setVar('error', false);
                }
            }
            $just_registered = $this->session->get('just_registered');
            $this->view->setVar('just_registered', $just_registered);
            // show attention for change password
        }

        public function specAction()
        {
            if($this->request->isAjax()) {
                $this->checkLogged();
                if($this->user_level <= 3) {
                    print_r("error");
                    exit();
                }

                $res = $this->addSpec();
                if($res) {
                    $user_specs = $this->users->getUserSpecs($this->user_id);
                    $this->view->setVar('user_specs', $user_specs);
                    return $this->view->pick('users/ajax_specializations');
                    exit();
                } else {
                    print_r("error");
                    exit();
                }
            }

            $this->view->disable();
            $this->checkLogged();
            if($this->user_level == 3) {
                $this->response->redirect($this->siteUrl . '/my');
                $this->response->send();
                die;
            }

            if($this->request->isPost() && $this->security->checkToken()) {
                $submit_type = $this->request->getPost('submit_type', 'int', false);
                switch ($submit_type)
                {
                    case 1: $res = $this->addSpec();
                        $error = $this->_getTranslation()->_('e_add_spec');
                        break;
                    case 2: $res = $this->addPrice();
                        $error = $this->_getTranslation()->_('e_add_price');
                        break;
                    default: $res = false;
                        $error = 'SYSTEM FAILURE';
                        break;
                }
                if(!$res) {
                    $this->view->setVar('result', $error);
                    $this->view->setVar('error', $submit_type);
                }
            }
            $this->response->redirect($this->siteUrl . '/my/profile');
            $this->response->send();
            die;

            /* $catalog = new BmCatalog();
              $top_catalog = $catalog->getTopSpecList();
              $sub_catalog = $catalog->getSubSpecList();
              $this->view->setVar('top_catalog', $top_catalog);
              $this->view->setVar('sub_catalog', $sub_catalog);

              $user_specs = $this->users->getUserSpecs($this->user_id);
              $this->view->setVar('user_specs', $user_specs);
              if ($user_specs) {
              $user_prices = $this->users->getUserPrices($this->user_id);
              $this->view->setVar('user_prices', $user_prices);
              } */
        }

        private function addSpec()
        {
            $speciality = $this->request->getPost('speciality', 'int', false);
            $subspeciality = $this->request->getPost('subspeciality', 'int', false);

            if(!$speciality || !$subspeciality)
                return false;

            if($subspeciality == 1000) {
                $sub_list = BmCatalog::getSubSpecListById($speciality);
                if(!$sub_list)
                    return false;

                foreach ($sub_list as $key => $value) {
                    $res = $this->users->addSpec($speciality, $value['id']);
                }
                return $res;
            }

            return $this->users->addSpec($speciality, $subspeciality);
        }

        public function delspecAction($spec_id)
        {
            if(!$spec_id)
                die('Error!');
            $this->checkLogged();

            $this->users->delSpec($spec_id);
            $this->response->redirect($this->siteUrl . '/my/profile');
            $this->response->send();
            die;
        }

        private function addPrice()
        {
            $item_name = $this->request->getPost('item', array('trim', 'string'), false);
            $price = $this->request->getPost('price', 'float', false);
            $spec_item = $this->request->getPost('spec', 'int', false);
            if(!$item_name || !$price || !$spec_item)
                return false;

            $price = round($price, 2);
            return $this->users->addPrice($item_name, $price, $spec_item);
        }

        public function delpriceAction($item_id)
        {
            $this->checkLogged();

            $this->users->delPrice($item_id);
            $this->response->redirect($this->siteUrl . '/my/spec');
            $this->response->send();
            die;
        }

        public function signinAction()
        {
            $this->checkLogged(true);

            $ret = '0';
            $this->view->disable();
            if($this->request->isPost() && $this->request->isAjax()) {
                if($this->security->checkToken()) {
                    $email = $this->request->getPost('email', array('trim', 'email'), false);
                    $phone = $this->request->getPost('phone', array('trim', 'string'), false);
                    $passwd = $this->request->getPost('passwd', array('trim', 'string'), false);
                    if(($email || $phone) && $passwd) {
                        $login = $this->users->signinUser($email, $phone, $passwd);
                        $ret = ($login) ? 1 : '0';

                        $not_viewed_offers = $this->users->getWorkkOffer($this->session->get('usr_id'));
                        if($login and count($not_viewed_offers) > 0) {
                            $ret = 2;
                        }
                    }
                }
            }
            echo $ret;
        }

        function dsCrypt($input, $decrypt = false)
        {

            $input = preg_replace("|^bud|", '', $input);

            $o = $s1 = $s2 = array(); // Arrays for: Output, Square1, Square2
            // формируем базовый массив с набором символов
            $basea = array('(', '@', ';', '$', "]", '*', '!!', ')', '_', '|', '['); // base symbol set
            $basea = array_merge($basea, range('a', 'z'), range('A', 'Z'), range(0, 9));

            $dimension = 8; // of squares
            for ($i = 0; $i < $dimension; $i++) { // create Squares
                for ($j = 0; $j < $dimension; $j++) {
                    $s1[$i][$j] = $basea[$i * $dimension + $j];
                    $s2[$i][$j] = str_rot13($basea[($dimension * $dimension - 1) - ($i * $dimension + $j)]);
                }
            }
            unset($basea);
            $m = floor(strlen($input) / 2) * 2; // !strlen%2
            $symbl = $m == strlen($input) ? '' : $input[strlen($input) - 1]; // last symbol (unpaired)
            $al = array();
            // crypt/uncrypt pairs of symbols
            for ($ii = 0; $ii < $m; $ii += 2) {
                $symb1 = $symbn1 = strval($input[$ii]);
                $symb2 = $symbn2 = strval($input[$ii + 1]);
                $a1 = $a2 = array();
                for ($i = 0; $i < $dimension; $i++) { // search symbols in Squares
                    for ($j = 0; $j < $dimension; $j++) {
                        if($decrypt) {
                            if($symb1 === strval($s2[$i][$j]))
                                $a1 = array($i, $j);
                            if($symb2 === strval($s1[$i][$j]))
                                $a2 = array($i, $j);
                            if(!empty($symbl) && $symbl === strval($s2[$i][$j]))
                                $al = array($i, $j);
                        }
                        else {
                            if($symb1 === strval($s1[$i][$j]))
                                $a1 = array($i, $j);
                            if($symb2 === strval($s2[$i][$j]))
                                $a2 = array($i, $j);
                            if(!empty($symbl) && $symbl === strval($s1[$i][$j]))
                                $al = array($i, $j);
                        }
                    }
                }
                if(sizeof($a1) && sizeof($a2)) {
                    $symbn1 = $decrypt ? $s1[$a1[0]][$a2[1]] : $s2[$a1[0]][$a2[1]];
                    $symbn2 = $decrypt ? $s2[$a2[0]][$a1[1]] : $s1[$a2[0]][$a1[1]];
                }
                $o[] = $symbn1 . $symbn2;
            }
            if(!empty($symbl) && sizeof($al)) // last symbol
                $o[] = $decrypt ? $s1[$al[1]][$al[0]] : $s2[$al[1]][$al[0]];

            if($decrypt)
                return implode('', $o);
            return "bud" . implode('', $o);
        }

        public function signupAction()
        {
            $this->checkLogged(true);

            if($this->request->isPost()) {
                //if ($this->security->checkToken()) {
                if(true) {
                    $name = $this->request->getPost('name', array('trim', 'string'), false);
                    $email = $this->request->getPost('email', array('trim', 'email'), '');
                    $phone = $this->request->getPost('phone', array('trim', 'string'), false);
                    $role = $this->request->getPost('type', 'int', false);
                    $passwd = $this->request->getPost('passwd1', array('trim', 'string'), false);
                    $agree = $this->request->getPost('agree', 'int', false);

                    if($role < 3 || $role > 6) {
                        $this->dumbError('(User reg) Wrong role (' . $role . ')');
                    }


                    if($name && $phone && $role && $passwd && $agree) {
                        $user_id = $this->users->createUser($name, $email, $phone, $role, $passwd);
                        if($user_id) {
                            if($email) {
                                $mail = new BMail('users', 'registration');
                                $mail->setOptions($this->siteUrl);
                                $data['email'] = $email;
                                $data['passwd'] = $passwd;
                                $data['fast_login'] = $this->dsCrypt($email);
                                $mail->send('Регистрация на сайте "Будмісто"', $email, $data);
                            }
                            if($role == 3 && $email)
                                $this->response->redirect($this->siteUrl . '/my');
                            else
                                $this->response->redirect($this->siteUrl . '/my/profile');
                            $this->response->send();
                            die;
                        } else {
                            $this->dumbError('(User reg) Failed to register user');
                        }
                    } else {
                        $this->dumbError('(User reg) Not completed required fields');
                    }
                } else {
                    $this->dumbError('(User reg) Token error', true);
                }
            }
            $type = $this->request->get('type', array('trim', 'email'), false);
            $builders = ($type == 'builders') ? true : false;
            $this->view->setVar('builders', $builders);
        }

        public function signoutAction()
        {
            $this->view->disable();
            $this->users->signoutUser();
            $this->session->destroy();
            $this->response->redirect($this->siteUrl);
        }

        public function recoveryAction()
        {
            $this->checkLogged(true);

            if($this->request->isAjax()) {

                $email = $this->request->getPost('email', array('trim', 'email'), false);
                $phone = $this->request->getPost('phone', array('trim', 'string'), false);

                if(!$email && !$phone)
                    exit("error");

                if($email) {
                    $newpasswd = $this->users->recoveryByEmail($email);
                    if($newpasswd) {
                        $mail = new BMail('users', 'recovery');
                        $mail->setOptions($this->siteUrl);
                        $data['email'] = $email;
                        $data['passwd'] = $newpasswd;
                        $data['fast_login'] = $this->dsCrypt($email);
                        $mail->send('Восстановление доступа', $email, $data);
                        $this->view->setVar('result', true);
                        $this->view->setVar('error', false);
                    } else {
                        $this->view->setVar('result', true);
                        $this->view->setVar('error', true);
                        $this->view->setVar('email', $email);
                    }

                    exit("success_email");
                }
                if($phone) {

                    $user = $this->users->getUserByPhone($phone);
                    if(!$user)
                        exit("error");

                    $newpasswd = $this->users->recoveryByPhone($user['user_phone']);

                    if($newpasswd) {
                        $mail = new BMail('users', 'recovery');
                        $mail->setOptions($this->siteUrl);
                        $data['email'] = $user['user_email'];
                        $data['passwd'] = $newpasswd;
                        $data['fast_login'] = $this->dsCrypt($user['user_email']);
                        $mail->send('Восстановление доступа', $user['user_email'], $data);
                        $this->view->setVar('result', true);
                        $this->view->setVar('error', false);

                        $phone = ConnectController::clearPhone($user['user_phone']);
                        ConnectController::sendMSG($phone, "Новый пароль на сайте budmisto: " . $newpasswd);
                        exit("success_phone");
                    } else {
                        $this->view->setVar('result', true);
                        $this->view->setVar('error', true);
                        $this->view->setVar('email', $email);
                    }


                    exit("success_phone");
                    /*
                      $admin_email = $this->config->application->admin_email;
                      $mail = new BMail('common', 'info');
                      $mail->setOptions($this->siteUrl);
                      $data['title'] = 'Пользователь забыл пароль';
                      $data['message'] = 'Пользователь хочет восстановить пароль по телефону';
                      $data['message'] .= '<br />Телефон: ' . $phone;
                      @$mail->send('Восстановление пароля', $admin_email, $data);
                      $this->view->setVar('result', true);
                      $this->view->setVar('error', false);
                      $this->view->setVar('by_phone', true);
                     * 
                     */
                }
            }
        }

        public function confirmAction()
        {
            $this->checkLogged();
            $r = $this->request->getHTTPReferer();
            if(strpos($r, $this->siteUrl) === false)
                die;

            if($this->request->isPost() && $this->request->isAjax()) {
                $this->view->disable();
                $email = $this->users->getUserProfile($this->user_id)['user_email'];
                $code = $this->generator->genEmailCode($email);
                $mail = new BMail('users', 'confirm');
                $mail->setOptions($this->siteUrl);
                $data['url'] = $this->siteUrl . '/emailconfirm?code=' . $code;
                $mail->send('Подтверждение E-mail', $email, $data);
            }
        }

        public function emailConfirmAction()
        {
            if($this->user_level) {
                $getcode = $this->request->get('code', array('trim', 'string'), false);
                if($getcode) {
                    $email = $this->users->getUserProfile($this->user_id)['user_email'];
                    $code = $this->generator->genEmailCode($email);
                    if($getcode === $code) {
                        $this->users->confirmed($this->user_id);
                        $this->view->setVar('result', 'confirmed');
                    } else {
                        $this->view->setVar('result', 'badmail');
                    }
                } else {
                    $this->view->setVar('result', 'notcode');
                }
            } else {
                $this->view->setVar('result', 'notlogged');
            }
        }

        public function availableAction()
        {
            /* if ($this->user_level && $this->user_level > 1) {
              echo 'AllOk';
              die;
              } */
            $r = $this->request->getHTTPReferer();
            if(strpos($r, $this->siteUrl) === false)
                die;

            $ret = 'AllOk';
            $this->view->disable();
            if($this->request->isPost() && $this->request->isAjax()) {
                //$this->_dependencyInjector['session']->get('$PHALCON/CSRF$');
                if($this->security->checkToken(null, null, false)) {
                    $email = $this->request->getPost('email', array('trim', 'email'), false);
                    $phone = $this->request->getPost('phone', array('trim', 'string'), false);
                    if($email) {
                        $res = $this->users->checkEmailExists($email, 'email');
                        if($res) {
                            $ret = $email;
                            //header('Content-Type: text/plain');
                            /* if (!$this->user_level)
                              $ret = $this->_getTranslation()->_('l_email_exist');
                              else
                              $ret = $this->_getTranslation()->_('l_email_exist_logged');
                             *
                             */
                        }
                    }
                    if($phone) {
                        $res = $this->users->checkEmailExists($phone, 'phone');

                        if($res) {

                            $ret = $phone;
                            //header('Content-Type: text/plain');
                            /* if (!$this->user_level)
                              $ret = $this->_getTranslation()->_('l_phone_exist');
                              else
                              $ret = $this->_getTranslation()->_('l_phone_exist_logged');
                             * 
                             */
                        }
                    }
                }
            }
            echo $ret;
        }

        public function needHelpAction()
        {
            $this->view->disable();
            if($this->request->isPost() && $this->request->isAjax()) {
                $order_id = $this->request->getPost('id', 'int', false);
                $admin_email = $this->config->application->admin_email;
                $mail = new BMail('common', 'info');
                $mail->setOptions($this->siteUrl);
                $data['title'] = 'Пользователю требуется помощь';
                $data['message'] = 'по подбору строителей для заявки ' . $order_id;
                $data['message'] .= '<br />Ссылка на заявку: ' . $this->siteUrl . '/catalog/order/' . $order_id;
                @$mail->send('Требуется помощь', $admin_email, $data);
                echo 'Ok';
            }
        }

        public function saveDataAction()
        {
            $this->view->disable();
            if($this->request->isPost() && $this->request->isAjax()) {
                $placelat = $this->request->getPost('placelat', 'float', '');
                $placelng = $this->request->getPost('placelng', 'float', '');
                $placeid = $this->request->getPost('placeid', array('trim', 'string'), '');
                $placeaddr = $this->request->getPost('placeaddr', array('trim', 'string'), '');
                $placetown = $this->request->getPost('placetown', array('trim', 'string'), '');
                $placedistrict2 = $this->request->getPost('placedistrict2', array('trim', 'string'), '');
                $placedistrict3 = $this->request->getPost('placedistrict3', array('trim', 'string'), '');
                $placeregion = $this->request->getPost('placeregion', array('trim', 'string'), '');
                $placecountry = $this->request->getPost('placecountry', array('trim', 'string'), '');
                $placeroute = $this->request->getPost('placeroute', array('trim', 'string'), '');
                $placextra = $this->request->getPost('placextra', array('trim', 'string'), '');
                $description = $this->request->getPost('desc', array('trim', 'string'), '');
                $email = $this->request->getPost('email', array('trim', 'email'), '');
                $phone = $this->request->getPost('phone', array('trim', 'string'), '');

                $this->session->set('placelat', $placelat);
                $this->session->set('placelng', $placelng);
                $this->session->set('placeid', $placeid);
                $this->session->set('placeaddr', $placeaddr);
                $this->session->set('placetown', $placetown);
                $this->session->set('placedistrict2', $placedistrict2);
                $this->session->set('placedistrict3', $placedistrict3);
                $this->session->set('placeregion', $placeregion);
                $this->session->set('placecountry', $placecountry);
                $this->session->set('placeroute', $placeroute);
                $this->session->set('placextra', $placextra);
                $this->session->set('description', $description);
                $this->session->set('email', $email);
                $this->session->set('phone', $phone);
            }
        }

        public function getPlacesAction()
        {
            $this->view->disable();
            $r = $this->request->getHTTPReferer();
            if(strpos($r, $this->siteUrl) === false)
                die;

            $user_id = $this->dispatcher->getParam('id');
            if(!$user_id)
                die;

            $res = $this->users->getPlaces($user_id);
            if(!$res) {
                echo 'Not found';
                die;
            }
            foreach ($res as $r) {
                echo $r['route'], ' ', $r['town'], ' ', $r['district'], ' ', $r['region'], '<br />';
            }
        }

        public function callRequestAction()
        {
            if($this->request->isPost() && $this->request->isAjax() && $this->security->checkToken()) {

                $user_call_id = $this->request->getPost('user_call_id', 'int', false);
                $callphone = $this->request->getPost('callphone', array('trim', 'string'), false);
                $message = $this->request->getPost('message', array('trim', 'string'), false);
                $user_id = $this->session->get('usr_id');
                if(!$user_id || !$callphone || !$user_call_id)
                    $this->logError('(User call request) Wrong parameters');

                $data = $this->users->getCallData($user_id, $user_call_id);
                if(!$data)
                    $this->logError('(User call request) Wrong data');

                $user_name = $data['user']['user_name'];
                $user_link = $this->siteUrl . '/user/' . $data['user']['id'];
                $caller_name = $data['caller']['user_name'];
                $caller_link = $this->siteUrl . '/user/' . $data['caller']['id'];
                $caller_phone = $data['caller']['user_phone'];

                $DB_data = [
                    'id_customer' => $data['user']['id'],
                    'name_customer' => $data['user']['user_name'],
                    'id_worker' => $data['caller']['id'],
                    'text' => $message,
                    'phone' => $callphone,
                ];

                $q = $this->users->setWorkOffer($DB_data);
                if($q == 'exist')
                    exit('exist');

                $admin_email = $this->config->application->admin_email;
                $mail = new BMail('common', 'info');
                $mail->setOptions($this->siteUrl);
                $mail_data['title'] = 'Пользователь запросил номер телефона';
                $mail_data['message'] = 'Пользователь "' . $user_name . '" профиль: ' . $user_link . '<br />';
                $mail_data['message'] .= 'запросил номер телефона пользователя "' . $caller_name . '" профиль: ' . $caller_link;
                $mail_data['message'] .= ' телефон: ' . $caller_phone;
                @$mail->send('Запрос номера телефона', $this->config->other->admin_email, $mail_data);

                $mail_data['title'] = ' Новое предложение работы!';
                $fast_login = UsersController::dsCrypt($data['caller']['user_email']);
                $mail_data['message'] = " У Вас появилось новое предложение работы<br>Быстрый вход: <a href=\"http://{$_SERVER[HTTP_HOST]}/index_offer/{$fast_login}\"><button>Личный кабинет</button></a></span>";
                @$mail->send('Новое предложение работы!', $data['caller']['user_email'], $mail_data);

                $customer = $this->users->getUserProfile($data['user']['id']);
                $worker = $this->users->getUserProfile($data['caller']['id']);

                $catalog = new BmCatalog();
                $last_order = $catalog->getLastUserOrder($data['user']['id'])['id'];
                $order_info = $catalog->getOrder($last_order);

                $d['getter_phone'] = $worker['user_phone'];
                $d['sender_phone'] = $customer['user_phone'];
                $d['order_id'] = $last_order;
                $d['order_specialization'] = $last_order['type_name'];
                $d['set_to_crone'] = 1;
                $d['type'] = 3;

                $connect = new ConnectController();
                $connect->sendOneAction($d);

                exit();
            } else
                $this->logError('(User call request) Wrong Request');
        }

        public function premiumBlockAction()
        {
            $catalog = new BmCatalog();

            $res = $catalog->deleteEndedPremiumBlock();

            /* Получаем очередь блокировок */
            $get_series = $catalog->getSeriesPremiumBlock();

            if(count($get_series)) {
                /* Работаем с очередью, отбираем те, где нет блокировок по заказу */
                foreach ($get_series as $key => $value) {
                    $q = $catalog->getCurrentPremiumBlockByOrder($value['order_id']);
                    if(!$q) {
                        /* Ставим из очереди в прпемиум блокировку */
                        $res = $catalog->setPremiumBlock(['id' => $value['order_id']], $value['user_id'], $value['duration']);
                        $catalog->deleteSeries($value['id']);

                        $worker_info = $this->users->getUserProfile($value['user_id']);
                        $order_info = $catalog->getOrder($value['order_id']);
                        $customer_info = $this->users->getUserProfile($order_info['user_id']);

                        $data['getter_phone'] = $worker_info['user_phone'];
                        $data['sender_phone'] = $customer_info['user_phone'];
                        $data['order_id'] = $value['order_id'];
                        $data['order_specialization'] = $order_info['type_name'];


                        $connect = new ConnectController();
                        $connect->sendOneAction($data);
                    }
                }
            }
        }

        public function cronSeriesAction()
        {

            $tasks = $this->users->getCronSeries();

            $connect = new ConnectController();
            foreach ($tasks as $value) {
                $connect->sendOneAction(json_decode($value['data'], true));
                $this->users->updateCronSeriesStatus($value['id']);
            }
        }

    }
    