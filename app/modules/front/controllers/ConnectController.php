<?php

    /*

     *     Author:  Hishnik  
     *     E-mail:  arenovec@gmail.com    
     *     skype:   kidw810i 
     * 
     *     Copyright (C) 2017 Hishnik
     */

    namespace Budmisto\Modules\Front\Controllers;

    class ConnectController extends ControllerBase
    {

        public function initialize()
        {
            parent::initialize();
        }

        /* Сообщение заказчику */

        public function setTaskSMSByOrderId($order_id, $sender_phone, $type = 1)
        {

            $user_phone = \Budmisto\Modules\Front\Models\Connect::getPhoneOrderUser($order_id)['user_phone'];

            if(!$user_phone) {
                return false;
            } else {

                $data['type'] = $type;
                $data['phone'] = $user_phone;
                $data['sender'] = $sender_phone;
                $data['order_id'] = $order_id;
            }

            return \Budmisto\Modules\Front\Models\Connect::setTask($data);
        }

        public function sendAllAction()
        {

            $position = $this->dispatcher->getParam('position');

            $allow_sms = \Budmisto\Modules\Front\Models\Connect::get_config("SMS_ACTIVE")['value'];

            if($allow_sms != 1) {
                print_r("Module disabled");
                exit();
            }


            $blocked_orders = \Budmisto\Modules\Front\Models\Connect::get_blocked_orders();
            $premium_orders = \Budmisto\Modules\Front\Models\Connect::get_premium_orders();

            if($blocked_orders) {
                foreach ($blocked_orders as $value) {
                    $blocked_ord[] = $value['order_id'];
                }
            }
            if($premium_orders) {
                foreach ($premium_orders as $value) {
                    $blocked_ord[] = $value['order_id'];
                }
            }

            $tasks = \Budmisto\Modules\Front\Models\Connect::get_all_task($position);

            foreach ($tasks as $key => $value) {
                if(in_array($value['order_id'], $blocked_ord)) {
                    unset($tasks[$key]);
                }
            }

            if(count($tasks) > 0) {

                /* Разложим задания по типам и по получателям */


                foreach ($tasks as $value) {
                    $task[$this->clearPhone($value['phone'])]['type'][$value['type']][] = [
                        'id' => $value['id'],
                        'sender' => $value['sender'],
                        'order_id' => $value['order_id']
                    ];
                }

                /* Пройдемся по каждому получателю */

                foreach ($task as $user_phone => $value) {

                    /* Обрабатываем 1 тип сообщений */
                    if($value['type'][1] != '') {
                        $this->sendType1($user_phone, $value);
                    } elseif($value['type'][2] != '') {
                        $this->sendType2($user_phone, $value);
                    } elseif($value['type'][3] != '') {
                        $this->sendType3($user_phone, $value);
                    }
                }
            } else {
                echo 'no task';
            }
        }

        public function sendOneAction($data = false)
        {

            if($data) {
                $getter_phone = $data['getter_phone'];
                $sender_phone = $data['sender_phone'];
                $order_id = $data['order_id'];
                $order_specialization = $data['order_specialization'];
                $type = $data['type'];
                $to_crone = $data['set_to_crone']; // Если отправляем через крон то устанавливаем значение
            } else {
                $getter_phone = $this->request->getPost('getter_phone', 'string', false);
                $sender_phone = $this->request->getPost('sender_phone', 'string', false);
                $order_id = $this->request->getPost('order_id', 'int', false);
                $order_specialization = $this->request->getPost('order_specialization', 'string', false);
                $type = $this->request->getPost('type', 'int', false);
                $to_crone = $this->request->getPost('set_to_crone', 'int', false); // Если отправляем через крон то устанавливаем значение
            }


            $to_crone = ((int) $to_crone > 0) ? true : false;

            $phones['getter_phone'] = str_replace(" ", "", $this->clearPhone($getter_phone));
            $phones['sender_phone'] = str_replace(" ", "", $this->clearPhone($sender_phone));

            switch ($type)
            {
                case 2:
                    $phones['order_specialization'] = trim(mb_substr($order_specialization, 0, 19));
                    break;

                case 3:
                    $phones['order_specialization'] = trim(mb_substr($order_specialization, 0, 25));
                    break;

                default:
                    $phones['order_specialization'] = trim($order_specialization);
                    break;
            }


            if($phones['getter_phone'] == '' or $phones['sender_phone'] == '' or $phones['order_specialization'] == '') {
                print_r('error data');
                exit();
            }

            $check = \Budmisto\Modules\Front\Models\Connect::get_msg_check($getter_phone, $sender_phone, $order_id);

            if($check and $check['status'] != 0) {

                if($data)
                    return;
                print_r('already_send');
                exit();
            }

            $type = $type ? $type : 2;
            $msg = $this->createMSG($phones, $type);

            if($to_crone) {

                $data['type'] = $type;
                $data['phone'] = $getter_phone;
                $data['sender'] = $sender_phone;
                $data['order_id'] = $order_id;

                if(\Budmisto\Modules\Front\Models\Connect::setTask($data)) {
                    echo 'success';
                } else {
                    echo 'not_send';
                }
            } else {
                if($this->sendMSG($phones['getter_phone'], $msg)) {
                    if($check) {
                        \Budmisto\Modules\Front\Models\Connect::updateStatusMsg($check['id'], "1");
                        echo 'success';
                    } else {
                        \Budmisto\Modules\Front\Models\Connect::add_msg_info($type, $getter_phone, $sender_phone, $order_id);
                        echo 'success';
                    }
                } else {
                    echo 'not_send';
                }
            }
        }

        public function sendType1($user_phone, $value)
        {

            foreach ($value['type'][1] as $k => $v) {
                $id[] = $v['id'];
                $phones[] = $this->clearPhone($v['sender']);
            }

            $msg = $this->createMSG($phones, 1);

            if($this->sendMSG($user_phone, $msg)) {
                $res = \Budmisto\Modules\Front\Models\Connect::updateStatusMsg($id, 1);
                if($res) {
                    echo 'success';
                } else {
                    echo 'error';
                }
            }
        }

        public function sendType2($user_phone, $value)
        {

            foreach ($value['type'][2] as $k => $v) {
                $id[] = $v['id'];
                $phones['sender_phone'] = $this->clearPhone($v['sender']);
                $order_id = $v['order_id'];
            }

            $order = \Budmisto\Modules\Front\Models\Catalog::getOrder($order_id);

            $phones['order_specialization'] = $order['type_name'];

            $msg = $this->createMSG($phones, 2);

            if($this->sendMSG($user_phone, $msg)) {
                $res = \Budmisto\Modules\Front\Models\Connect::updateStatusMsg($id, 1);
                if($res) {
                    echo 'success';
                } else {
                    echo 'error';
                }
            }
        }

        public function sendType3($user_phone, $value)
        {

            foreach ($value['type'][3] as $k => $v) {
                $id[] = $v['id'];
                $phones['sender_phone'] = $this->clearPhone($v['sender']);
                $order_id = $v['order_id'];
            }

            $order = \Budmisto\Modules\Front\Models\Catalog::getOrder($order_id);

            $phones['order_specialization'] = $order['type_name'];

            $msg = $this->createMSG($phones, 3);

            if($this->sendMSG($user_phone, $msg)) {
                $res = \Budmisto\Modules\Front\Models\Connect::updateStatusMsg($id, 1);
                if($res) {
                    echo 'success';
                } else {
                    echo 'error';
                }
            }
        }

        public function sendMSG($phone, $msg)
        {

            // If need set test mode uncommented
            if($_SERVER['SERVER_ADDR'] == "127.0.0.1") {
                return true;
            }

            $client = new \SoapClient('http://turbosms.in.ua/api/wsdl.html');

            $auth = [
                'login' => 'olegwww',
                'password' => 'budsms211'
            ];

            // Авторизируемся на сервере   
            $result = $client->Auth($auth);

            $result = $client->GetCreditBalance();

            if((float) $result->GetCreditBalanceResult > 0.5) {
                // Отправляем сообщение на один номер.   
                // Подпись отправителя может содержать английские буквы и цифры. Максимальная длина - 11 символов.   
                // Номер указывается в полном формате, включая плюс и код страны   
                //!!!!сервис не отвечает на этот номер!!!
                if(in_array($phone, ['0670000000']))
                    return true;

                $sms = [
                    'sender' => 'Budmisto',
                    'destination' => '+38' . $phone,
                    'text' => $msg
                ];


                $result = $client->SendSMS($sms);

                if($result->SendSMSResult->ResultArray[0] == "Сообщения успешно отправлены") {

                    if(count($result->SendSMSResult->ResultArray) - 1 == count($phone)) {
                        foreach ($result->SendSMSResult->ResultArray as $key => $value) {

                            if($key == 0)
                                continue;

                            if(preg_match("/([а-яА-Я]+)/", $value)) {
                                $this->setMsgError($phone);
                            }
                        }
                    }

                    return true;
                } else {
                    return false;
                }
            }
        }
        
        public function setMsgError($phone)
        {
            $us_id = $this->admin->checkPhone($phone);
            $this->admin->setSMSError($us_id);
        }

        public function createMSG($phones, $type)
        {

            $text = $this->getTextSMS($type)['value'];

            switch ($type)
            {
                case 1:
                    $msg = (sprintf($text, implode(" ", $phones)));
                    break;
                case 2:
                    $msg = (sprintf($text, $phones['order_specialization'], $phones['sender_phone']));
                    break;
                case 3:
                    $msg = (sprintf($text, $phones['order_specialization']));
                    break;
            }

            return $msg;
        }

        public function getTextSMS($type)
        {
            return \Budmisto\Modules\Front\Models\Connect::getTextSMS($type);
        }

        public function clearPhone($phone)
        {
            return str_replace(["(", ")", " ", "_", "-"], "", $phone);
        }

    }
    