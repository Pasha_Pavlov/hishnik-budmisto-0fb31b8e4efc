<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/front/controllers/IndexController.php
     *
     * Main controller
     *
     */

    namespace Budmisto\Modules\Front\Controllers;

    use Budmisto\Modules\Front\Models\Catalog as BmCatalog;
    use Budmisto\Lib\Mailer\Mail as BMail;

    class IndexController extends ControllerBase
    {

        public $catalog;

        public function initialize()
        {
            parent::initialize();
            $this->catalog = new BmCatalog();
            $this->catalog->setLang($this->lang);
        }

        public function fastLoginAction()
        {
            $crypt = $this->dispatcher->getParam('crypt');

            $email = UsersController::dsCrypt($crypt, 1);


            $auth = false;

            $auth = \Budmisto\Modules\Front\Models\Users::signinUser($email, '', '', true);
            if($auth == "success") {
                $this->response->redirect($this->siteUrl . '/my');
                $this->response->send();
                die;
            } else {
                $this->response->redirect($this->siteUrl . '/');
                $this->response->send();
                die;
            }
        }

        public function fastLoginOfferAction()
        {
            $crypt = $this->dispatcher->getParam('crypt');
            $email = UsersController::dsCrypt($crypt, 1);
            $auth = false;

            $auth = \Budmisto\Modules\Front\Models\Users::signinUser($email, '', '', true);
            if($auth == "success") {
                $this->response->redirect($this->siteUrl . '/my/offers');
                $this->response->send();
                die;
            } else {
                $this->response->redirect($this->siteUrl . '/');
                $this->response->send();
                die;
            }
        }

        public function indexAction()
        {

            //var_dump($this->session->get('email')); die;
            if($this->session->has('placetown')) {
                $this->setOrderSessionData();
            }
            $content = $this->wunderCache('index', 'index');

            if($content === null) {
                $top_catalog = $this->catalog->getTopSpecList();
                $sub_catalog = $this->catalog->getSubSpecList();
                $last_orders = $this->catalog->getLastOrdersList(0, 10);

                $this->view->setVar('top_catalog', $top_catalog);
                $this->view->setVar('sub_catalog', $sub_catalog);
                $this->view->setVar('last_orders', $last_orders);
                $this->view->setVar('map_key', $this->config->application->map_key);

                //$this->viewCache->save();
            } else
                echo $content;
        }

        public function confidencialAction()
        {
            
        }

        public function callbackAction()
        {
            if($this->request->isPost() && $this->request->isAjax()) {

                $data['name'] = $this->request->getPost('callback_name', array('trim', 'string'), false);
                $data['phone'] = $this->request->getPost('callback_phone', array('trim', 'string'), false);
                $data['description'] = $this->request->getPost('callback_desc', array('trim', 'string'), false);
                $data['locality'] = $this->request->getPost('locality', array('trim', 'string'), false);
             
                $req = $this->catalog->set_calback_data($data);

                if((int) $req > 0) {
                    print_r('success');
                } else {
                    print_r('error');
                }

                exit();
            }
        }

        private function setOrderSessionData()
        {
            if(!$this->session->has('usr_email'))
                $this->view->setVar('usr_email', $this->session->get('email'));
            if(!$this->session->has('usr_phone'))
                $this->view->setVar('usr_phone', $this->session->get('phone'));
            $this->view->setVar('placelat', $this->session->get('placelat'));
            $this->view->setVar('placelng', $this->session->get('placelng'));
            $this->view->setVar('placeid', $this->session->get('placeid'));
            $this->view->setVar('placeaddr', $this->session->get('placeaddr'));
            $this->view->setVar('placetown', $this->session->get('placetown'));
            $this->view->setVar('placedistrict2', $this->session->get('placedistrict2'));
            $this->view->setVar('placedistrict3', $this->session->get('placedistrict3'));
            $this->view->setVar('placeregion', $this->session->get('placeregion'));
            $this->view->setVar('placecountry', $this->session->get('placecountry'));
            $this->view->setVar('placeroute', $this->session->get('placeroute'));
            $this->view->setVar('placextra', $this->session->get('placextra'));
            $this->view->setVar('description', $this->session->get('description'));
        }

        public function helpCenterAction()
        {
            if($this->request->isPost()) {
                if($this->security->checkToken()) {
                    $name = $this->request->getPost('name', array('trim', 'string'), false);
                    $contact = $this->request->getPost('contact', array('trim', 'string'), false);
                    $message = $this->request->getPost('message', array('trim', 'string'), false);
                    if($name && $contact && $message) {
                        $admin_email = $this->config->application->admin_email;
                        $mail = new BMail('common', 'info');
                        $mail->setOptions($this->siteUrl);
                        $data['title'] = 'Пользователь ' . $name . ' спрашивает и предлагает';
                        $data['message'] = $message . '<br /><br />Связаться с пользователем:' . $contact;
                        $mail->send('Обратная связь', $admin_email, $data);
                        $this->view->setVar('error', false);
                    } else
                        $this->view->setVar('error', true);
                } else
                    $this->view->setVar('error', true);
            }
        }

        public function contactsAction()
        {
            $this->response->redirect($this->siteUrl . '/helpcenter');
            $this->response->send();
            die;
        }

        public function dberrorAction()
        {
            $this->response->setStatusCode(404, 'Not Found');
        }

        public function route404Action()
        {
            $this->response->setStatusCode(404, 'Not Found');
        }

        public function route403Action()
        {
            $this->view->enable();
            $this->response->setStatusCode(403, 'Forbidden');
            $return = $this->request->getHTTPReferer();
            $return = $return ? $return : '/';
            $this->view->setVar('return', $return);
        }

        public function adminAction()
        {

            if($this->request->isPost() && $this->security->checkToken()) {
                $login = $this->request->getPost('login', array('trim', 'string'), false);
                $passwd = $this->request->getPost('passwd', array('trim', 'string'), false);
                if($login && $passwd)
                    $this->view->setVar('fuck', true);
            }
            if($this->user_level && $this->user_level < 3) {
                $this->response->redirect($this->siteUrl . '/workz');
                $this->response->send();
                die;
            }
        }

        public function testAction()
        {
            $this->view->disable();
            $this->catalog->doTest();
            echo 'done.';
        }

    }
    