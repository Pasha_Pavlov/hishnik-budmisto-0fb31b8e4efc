<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/front/controllers/CatalogController.php
     *
     * Catalog controller
     *
     */

    namespace Budmisto\Modules\Front\Controllers;

    use Budmisto\Modules\Front\Models\Catalog as BmCatalog;
    use Budmisto\Modules\Front\Models\Users as BmUsers;
    use Budmisto\Lib\Mailer\Mail as BMail;

    class CatalogController extends ControllerBase
    {

        public $catalog;

        public function initialize()
        {

            parent::initialize();
            $this->catalog = new BmCatalog();
            $this->catalog->setLang($this->lang);
            $this->view->setVar('map_key', $this->config->application->map_key);
            //$this->view->setVar('banner1', $this->config->other->banner1);
        }

        public function sortWorkersByPhotos($workers)
        {
            $users = new BmUsers();
            $result = [];
            if(is_array($workers)) {
                foreach ($workers as $key => $worker) {
                    $user_images = $users->getUserFiles('image', $worker['uid']);
                    if(count($user_images)) {
                        $worker['images'] = $user_images;
                        array_unshift($result, $worker);
                    } else {
                        $result[] = $worker;
                    }
                }
            }

            return $result;
        }

        public function indexAction()
        {
            $top_catalog = $this->catalog->getTopSpecList();
            $sub_catalog = $this->catalog->getSubSpecList();
            $last_orders = $this->catalog->getLastOrdersList(0, 20);
            $workers = $this->catalog->getRandomWorkersList(0, 20);
            $workers = $this->sortWorkersByPhotos($workers);

            $this->view->setVar('workers', $workers);

            $this->view->setVar('top_catalog', $top_catalog);
            $this->view->setVar('sub_catalog', $sub_catalog);
            $this->view->setVar('orders', $last_orders);


            $cities = $this->commonModel->getCatCities();
            $this->view->setVar('cities', $cities);

            /* $count = $this->catalog->countMain();
              $this->view->setVar('count', $count);
              $this->view->setVar('limit', $this->config->lists->limit); */
        }

        public function indexCityAction()
        {
            $top_catalog = $this->catalog->getTopSpecList();
            $sub_catalog = $this->catalog->getSubSpecList();


            $tmp = $this->dispatcher->getParam('place');
            $this->view->setVar('slug', $tmp);
            $tmp = explode('-', $tmp);
            $place['id'] = array_shift($tmp);
            $place['slug'] = implode('-', $tmp);


            if($place['id'] < 161)
                $city = $this->commonModel->getCitiesNew($place);
            else
                $city = $this->commonModel->getPlacesNew($place);

            $orders = $this->catalog->getOrdersListByCoords($city['lat'], $city['lng'], 0, 20);
            $workers = $this->catalog->getWorkersListByCoords($city['lat'], $city['lng'], 0, 20);
            $workers = $this->sortWorkersByPhotos($workers);

            if($place['id'] > 77 && $place['id'] < 161) {
                $cities = $this->commonModel->getCatDistricts($city['parent_id']);
                $cities = $cities['sub' . $city['parent_id']];
                $this->view->setVar('parent_id', $city['parent_id']);
                $this->view->setVar('parent_name', $city['parent_name']);
                $this->view->setVar('parent_slug', $city['parent_slug']);
            }
            if($place['id'] < 78) {
                $cities = $this->commonModel->getCatDistricts($place['id']);
                $cities = $cities['sub' . $place['id']];
                if($cities)
                    $this->view->setVar('to_top', true);
            }
            if(!isset($cities) || !$cities) {
                $cities = $this->commonModel->getCatCities();
            }
            $this->view->setVar('cities', $cities);

            $this->view->setVar('top_catalog', $top_catalog);
            $this->view->setVar('sub_catalog', $sub_catalog);
            $this->view->setVar('orders', $orders);
            $this->view->setVar('workers', $workers);
            $this->view->setVar('city', $city);

            /* $count = $this->catalog->countMain();
              $this->view->setVar('count', $count);
              $this->view->setVar('limit', $this->config->lists->limit); */
        }

        public function topRubricAction()
        {
            $top_rubric = $this->dispatcher->getParam('top_rubric');

            $place = $this->dispatcher->getParam('place');
            $this->view->setVar('place_slug', $place);


            $cities = false;
            $city = false;
            $this->view->setVar('slug', false);
            if($place) {
                $place = '';
                $tmp = $this->dispatcher->getParam('place');
                $this->view->setVar('slug', $tmp);
                $tmp = explode('-', $tmp);
                $place['id'] = array_shift($tmp);
                $place['slug'] = implode('-', $tmp);
                if($place['id'] < 161)
                    $city = $this->commonModel->getCitiesNew($place);
                else
                    $city = $this->commonModel->getPlacesNew($place);

                if(!$city) {
                    $this->dumbError('(Rubrics top) Wrong city', true, true);
                }

                if($place['id'] > 77 && $place['id'] < 161) {
                    $cities = $this->commonModel->getCatDistricts($city['parent_id']);
                    $cities = $cities['sub' . $city['parent_id']];
                    $this->view->setVar('parent_id', $city['parent_id']);
                    $this->view->setVar('parent_name', $city['parent_name']);
                    $this->view->setVar('parent_slug', $city['parent_slug']);
                }
                if($place['id'] < 78) {
                    $cities = $this->commonModel->getCatDistricts($place['id']);
                    $cities = $cities['sub' . $place['id']];
                    if($cities)
                        $this->view->setVar('to_top', true);
                }
            }


            if(!isset($cities) || !$cities) {
                $cities = $this->commonModel->getCatCities();
            }
            $this->view->setVar('cities', $cities);
            $this->view->setVar('city', $city);

            $filter = $this->getInputFilter();
            $top_rubric = $filter->sanitize($top_rubric, 'user_input');
            $rub = $this->catalog->getSLbyS($top_rubric);

            if(!$rub) {
                $this->dumbError('(Rubrics top) Wrong top rubric', true, true);
            }

            $this->view->setVar('rub_name', $rub['type_name']);
            $this->view->setVar('top_id', $rub['tid']);
            $this->view->setVar('top_slug', $top_rubric);

            $top = $this->catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);
            $sub = $this->catalog->getSubSpecListById($rub['tid']);
            $this->view->setVar('sub_catalog', $sub);

            if($place) {
                $workers = $this->catalog->getTopWorkersListByCoords($rub['tid'], $city['lat'], $city['lng'], 0, $this->config->lists->limit, 40);
                $orders = $this->catalog->getTopOrdersListByCoords($rub['tid'], $city['lat'], $city['lng'], 0, $this->config->lists->limit, 50);
            } else {
                $workers = $this->catalog->getTopWorkersList($rub['tid'], 0, $this->config->lists->limit);
                $orders = $this->catalog->getTopOrdersList($rub['tid'], 0, $this->config->lists->limit);
            }

            if(count($workers) > $this->config->lists->limit)
                array_pop($workers);
            if(count($orders) > $this->config->lists->limit)
                array_pop($orders);

            $workers = $this->sortWorkersByPhotos($workers);
            $this->view->setVar('workers', $workers); //var_dump($workers); die;
            $this->view->setVar('orders', $orders);
        }

        public function topOrdersAction()
        {
            $tmp = $this->request->get('page', 'int');
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            $top_rubric = $this->dispatcher->getParam('top_rubric');
            $place = $this->dispatcher->getParam('place');
            $this->view->setVar('place_slug', $place);


            $cities = false;
            $city = false;
            $this->view->setVar('slug', false);
            if($place) {
                $place = '';
                $tmp = $this->dispatcher->getParam('place');
                $this->view->setVar('slug', $tmp);
                $tmp = explode('-', $tmp);
                $place['id'] = array_shift($tmp);
                $place['slug'] = implode('-', $tmp);
                if($place['id'] < 161)
                    $city = $this->commonModel->getCitiesNew($place);
                else
                    $city = $this->commonModel->getPlacesNew($place);

                if(!$city) {
                    $this->dumbError('(Rubrics top) Wrong city', true, true);
                }

                if($place['id'] > 77 && $place['id'] < 161) {
                    $cities = $this->commonModel->getCatDistricts($city['parent_id']);
                    $cities = $cities['sub' . $city['parent_id']];
                    $this->view->setVar('parent_id', $city['parent_id']);
                    $this->view->setVar('parent_name', $city['parent_name']);
                    $this->view->setVar('parent_slug', $city['parent_slug']);
                }
                if($place['id'] < 78) {
                    $cities = $this->commonModel->getCatDistricts($place['id']);
                    $cities = $cities['sub' . $place['id']];
                    if($cities)
                        $this->view->setVar('to_top', true);
                }
            }

            if(!isset($cities) || !$cities) {
                $cities = $this->commonModel->getCatCities();
            }
            $this->view->setVar('cities', $cities);
            $this->view->setVar('city', $city);

            $filter = $this->getInputFilter();
            $top_rubric = $filter->sanitize($top_rubric, 'user_input');
            $rub = $this->catalog->getSLbyS($top_rubric);

            if(!$rub) {
                $this->dumbError('(Rubrics top) Wrong top rubric', true, true);
            }

            $this->view->setVar('rub_name', $rub['type_name']);
            $this->view->setVar('top_id', $rub['tid']);
            $this->view->setVar('top_slug', $top_rubric);

            $top = $this->catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);
            $sub = $this->catalog->getSubSpecListById($rub['tid']);
            $this->view->setVar('sub_catalog', $sub);

            if($place) {
                $orders = $this->catalog->getTopOrdersListByCoords($rub['tid'], $city['lat'], $city['lng'], $page, $this->config->lists->limit * 2);
            } else {
                $orders = $this->catalog->getTopOrdersList($rub['tid'], $page, $this->config->lists->limit * 2);
            }
            if(!$orders) {
                $this->response->setStatusCode(404, 'Not Found');
            }
            /*
              if(count($orders) > $this->config->lists->limit * 2) {
              $next = ($page == 0) ? 2 : ($page + 1);
              array_pop($orders);
              } else
              $next = 0;
              $this->view->setVar('nextpage', $next);
             */
            $this->view->setVar('orders', $orders);

            $count = $this->catalog->countTopOW($rub['tid']);
            $this->view->setVar('count', $count);
            $this->view->setVar('limit', $this->config->lists->limit);

            $links_html = $this->createPaginator($count['orders'], $page, $this->config->lists->limit * 2, $this->siteUrl . '/catalog/' . $top_rubric . '/orders');
            $this->view->setVar('pager_html', $links_html);
        }

        public function topUsersAction()
        {
            $tmp = $this->request->get('page', 'int');
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            $top_rubric = $this->dispatcher->getParam('top_rubric');
            $place = $this->dispatcher->getParam('place');
            $this->view->setVar('place_slug', $place);


            $cities = false;
            $city = false;
            $this->view->setVar('slug', false);
            if($place) {
                $place = '';
                $tmp = $this->dispatcher->getParam('place');
                $this->view->setVar('slug', $tmp);
                $tmp = explode('-', $tmp);
                $place['id'] = array_shift($tmp);
                $place['slug'] = implode('-', $tmp);
                if($place['id'] < 161)
                    $city = $this->commonModel->getCitiesNew($place);
                else
                    $city = $this->commonModel->getPlacesNew($place);

                if(!$city) {
                    $this->dumbError('(Rubrics top) Wrong city', true, true);
                }

                if($place['id'] > 77 && $place['id'] < 161) {
                    $cities = $this->commonModel->getCatDistricts($city['parent_id']);
                    $cities = $cities['sub' . $city['parent_id']];
                    $this->view->setVar('parent_id', $city['parent_id']);
                    $this->view->setVar('parent_name', $city['parent_name']);
                    $this->view->setVar('parent_slug', $city['parent_slug']);
                }
                if($place['id'] < 78) {
                    $cities = $this->commonModel->getCatDistricts($place['id']);
                    $cities = $cities['sub' . $place['id']];
                    if($cities)
                        $this->view->setVar('to_top', true);
                }
            }

            if(!isset($cities) || !$cities) {
                $cities = $this->commonModel->getCatCities();
            }
            $this->view->setVar('cities', $cities);
            $this->view->setVar('city', $city);

            $filter = $this->getInputFilter();
            $top_rubric = $filter->sanitize($top_rubric, 'user_input');
            $rub = $this->catalog->getSLbyS($top_rubric);

            if(!$rub) {
                $this->dumbError('(Rubrics top) Wrong top rubric', true, true);
            }

            $this->view->setVar('rub_name', $rub['type_name']);
            $this->view->setVar('top_id', $rub['tid']);
            $this->view->setVar('top_slug', $top_rubric);

            $top = $this->catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);
            $sub = $this->catalog->getSubSpecListById($rub['tid']);
            $this->view->setVar('sub_catalog', $sub);

            /* $workers = $this->catalog->getTopWorkersList($rub['tid'], $place_id, $page, $this->config->lists->limit*2);
              if (!$workers && $place_id) {
              $workers = $this->catalog->getTopWorkersListOnPlace($rub['tid'], $place, $page, $this->config->lists->limit);
              $this->view->setVar('workers_not_found_place', true);
              }
              if (!$workers) {
              $this->response->setStatusCode(404, 'Not Found');
              }
              $this->view->setVar('workers', $workers); */
            if($place) {

                $workers = $this->catalog->getTopWorkersListByCoords($rub['tid'], $city['lat'], $city['lng'], $page, $this->config->lists->limit * 2);
            } else {
                $workers = $this->catalog->getTopWorkersList($rub['tid'], $page, $this->config->lists->limit * 2);
            }
            $workers = $this->sortWorkersByPhotos($workers);

            /*
              if(count($workers) > $this->config->lists->limit * 2) {
              $next = ($page == 0) ? 2 : ($page + 1);
              array_pop($workers);
              } else
              $next = 0;
              $this->view->setVar('nextpage', $next);
             */

            $this->view->setVar('workers', $workers); //var_dump($next); die;

            if(!$place) {
                $count = $this->catalog->countTopOW($rub['tid']);
                $this->view->setVar('count', $count);
                $this->view->setVar('limit', $this->config->lists->limit);

                $links_html = $this->createPaginator($count['workers'], $page, $this->config->lists->limit * 2, $this->siteUrl . '/catalog/' . $top_rubric . '/users');
                $this->view->setVar('pager_html', $links_html);
            }
        }

        public function subRubricAction()
        {
            $top_rubric = $this->dispatcher->getParam('top_rubric');
            $sub_rubric = $this->dispatcher->getParam('sub_rubric');
            $place = $this->dispatcher->getParam('place');
            $this->view->setVar('place_slug', $place);


            $cities = false;
            $city = false;
            $this->view->setVar('slug', false);
            if($place) {
                $place = '';
                $tmp = $this->dispatcher->getParam('place');
                $this->view->setVar('slug', $tmp);
                $tmp = explode('-', $tmp);
                $place['id'] = array_shift($tmp);
                $place['slug'] = implode('-', $tmp);
                if($place['id'] < 161)
                    $city = $this->commonModel->getCitiesNew($place);
                else
                    $city = $this->commonModel->getPlacesNew($place);

                if(!$city) {
                    $this->dumbError('(Rubrics top) Wrong city', true, true);
                }

                if($place['id'] > 77 && $place['id'] < 161) {
                    $cities = $this->commonModel->getCatDistricts($city['parent_id']);
                    $cities = $cities['sub' . $city['parent_id']];
                    $this->view->setVar('parent_id', $city['parent_id']);
                    $this->view->setVar('parent_name', $city['parent_name']);
                    $this->view->setVar('parent_slug', $city['parent_slug']);
                }
                if($place['id'] < 78) {
                    $cities = $this->commonModel->getCatDistricts($place['id']);
                    $cities = $cities['sub' . $place['id']];
                    if($cities)
                        $this->view->setVar('to_top', true);
                }
            }

            if(!isset($cities) || !$cities) {
                $cities = $this->commonModel->getCatCities();
            }
            $this->view->setVar('cities', $cities);
            $this->view->setVar('city', $city);

            $filter = $this->getInputFilter();
            $top_rubric = $filter->sanitize($top_rubric, 'user_input');
            $sub_rubric = $filter->sanitize($sub_rubric, 'user_input');
            $rub = $this->catalog->getSLbyS2($top_rubric, $sub_rubric);

            if(!$rub) {
                $this->dumbError('(Rubrics sub) Wrong sub rubric', true, true);
            }

            $this->view->setVar('top_name', $rub['type_name']);
            $this->view->setVar('sub_name', $rub['spec_name']);
            $this->view->setVar('top_id', $rub['tid']);
            $this->view->setVar('sub_id', $rub['sid']);
            $this->view->setVar('top_slug', $top_rubric);
            $this->view->setVar('sub_slug', $sub_rubric);

            $top = $this->catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);
            $sub = $this->catalog->getSubSpecListById($rub['tid']);
            $this->view->setVar('sub_catalog', $sub);

            if($place) {
                $workers = $this->catalog->getSubWorkersListByCoords($rub['tid'], $rub['sid'], $city['lat'], $city['lng'], 0, $this->config->lists->limit, 50);
                if(!$workers) {
                    $workers = $this->catalog->getTopWorkersListByCoords($rub['tid'], $city['lat'], $city['lng'], 0, $this->config->lists->limit, 50);
                    $this->view->setVar('workers_not_found_sub', true);
                }
                $orders = $this->catalog->getSubOrdersListByCoords($rub['tid'], $rub['sid'], $city['lat'], $city['lng'], 0, $this->config->lists->limit, 50);
                if(!$orders) {
                    $orders = $this->catalog->getTopOrdersListByCoords($rub['tid'], $city['lat'], $city['lng'], 0, $this->config->lists->limit, 50);
                    $this->view->setVar('orders_not_found_sub', true);
                }
            } else {
                $workers = $this->catalog->getSubWorkersList($rub['tid'], $rub['sid'], 0, $this->config->lists->limit);
                if(!$workers) {
                    $workers = $this->catalog->getTopWorkersList($rub['tid'], 0, $this->config->lists->limit);
                    $this->view->setVar('workers_not_found_sub', true);
                }
                $orders = $this->catalog->getSubOrdersList($rub['tid'], $rub['sid'], 0, $this->config->lists->limit);
                if(!$orders) {
                    $orders = $this->catalog->getTopOrdersList($rub['tid'], 0, $this->config->lists->limit);
                    $this->view->setVar('orders_not_found_sub', true);
                }
            }

            if(count($workers) > $this->config->lists->limit)
                array_pop($workers);
            if(count($orders) > $this->config->lists->limit)
                array_pop($orders);

            $workers = $this->sortWorkersByPhotos($workers);

            $this->view->setVar('workers', $workers);
            $this->view->setVar('orders', $orders);
        }

        public function subOrdersAction()
        {
            $tmp = $this->request->get('page', 'int');
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            $top_rubric = $this->dispatcher->getParam('top_rubric');
            $sub_rubric = $this->dispatcher->getParam('sub_rubric');
            $place = $this->dispatcher->getParam('place');
            $this->view->setVar('place_slug', $place);


            $cities = false;
            $city = false;
            $this->view->setVar('slug', false);
            if($place) {
                $place = '';
                $tmp = $this->dispatcher->getParam('place');
                $this->view->setVar('slug', $tmp);
                $tmp = explode('-', $tmp);
                $place['id'] = array_shift($tmp);
                $place['slug'] = implode('-', $tmp);
                if($place['id'] < 161)
                    $city = $this->commonModel->getCitiesNew($place);
                else
                    $city = $this->commonModel->getPlacesNew($place);

                if(!$city) {
                    $this->dumbError('(Rubrics top) Wrong city', true, true);
                }

                if($place['id'] > 77 && $place['id'] < 161) {
                    $cities = $this->commonModel->getCatDistricts($city['parent_id']);
                    $cities = $cities['sub' . $city['parent_id']];
                    $this->view->setVar('parent_id', $city['parent_id']);
                    $this->view->setVar('parent_name', $city['parent_name']);
                    $this->view->setVar('parent_slug', $city['parent_slug']);
                }
                if($place['id'] < 78) {
                    $cities = $this->commonModel->getCatDistricts($place['id']);
                    $cities = $cities['sub' . $place['id']];
                    if($cities)
                        $this->view->setVar('to_top', true);
                }
            }

            if(!isset($cities) || !$cities) {
                $cities = $this->commonModel->getCatCities();
            }
            $this->view->setVar('cities', $cities);
            $this->view->setVar('city', $city);

            $filter = $this->getInputFilter();
            $top_rubric = $filter->sanitize($top_rubric, 'user_input');
            $sub_rubric = $filter->sanitize($sub_rubric, 'user_input');
            $rub = $this->catalog->getSLbyS2($top_rubric, $sub_rubric);


            if(!$rub) {
                $this->dumbError('(Rubrics top) Wrong top rubric', true, true);
            }

            $this->view->setVar('top_name', $rub['type_name']);
            $this->view->setVar('sub_name', $rub['spec_name']);
            $this->view->setVar('top_id', $rub['tid']);
            $this->view->setVar('sub_id', $rub['sid']);
            $this->view->setVar('top_slug', $top_rubric);
            $this->view->setVar('sub_slug', $sub_rubric);

            $top = $this->catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);
            $sub = $this->catalog->getSubSpecListById($rub['tid']);
            $this->view->setVar('sub_catalog', $sub);

            if($place) {
                $orders = $this->catalog->getSubOrdersListByCoords($rub['tid'], $rub['sid'], $city['lat'], $city['lng'], $page, $this->config->lists->limit * 2);
            } else {
                $orders = $this->catalog->getSubOrdersList($rub['tid'], $rub['sid'], $page, $this->config->lists->limit * 2);
            }

            if(!$orders) {
                $this->response->setStatusCode(404, 'Not Found');
            }

            /*
              if(count($orders) > $this->config->lists->limit * 2) {
              $next = ($page == 0) ? 2 : ($page + 1);
              array_pop($orders);
              } else
              $next = 0;
              $this->view->setVar('nextpage', $next);
             */

            $this->view->setVar('count', count($orders));
            $this->view->setVar('limit', $this->config->lists->limit);

            $place = $place ? '/' . $place : '';
            $links_html = $this->createPaginator(count($orders), $page, $this->config->lists->limit * 2, $this->siteUrl . '/catalog/' . $top_rubric . '/' . $sub_rubric . $place . '/orders');

            $this->view->setVar('pager_html', $links_html);

            $this->view->setVar('orders', $orders);
        }

        public function subUsersAction()
        {
            $tmp = $this->request->get('page', 'int');
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            $top_rubric = $this->dispatcher->getParam('top_rubric');
            $sub_rubric = $this->dispatcher->getParam('sub_rubric');
            $place = $this->dispatcher->getParam('place');
            $this->view->setVar('place_slug', $place);


            $cities = false;
            $city = false;
            $this->view->setVar('slug', false);
            if($place) {
                $place = '';
                $tmp = $this->dispatcher->getParam('place');
                $this->view->setVar('slug', $tmp);
                $tmp = explode('-', $tmp);
                $place['id'] = array_shift($tmp);
                $place['slug'] = implode('-', $tmp);
                if($place['id'] < 161)
                    $city = $this->commonModel->getCitiesNew($place);
                else
                    $city = $this->commonModel->getPlacesNew($place);

                if(!$city) {
                    $this->dumbError('(Rubrics top) Wrong city', true, true);
                }

                if($place['id'] > 77 && $place['id'] < 161) {
                    $cities = $this->commonModel->getCatDistricts($city['parent_id']);
                    $cities = $cities['sub' . $city['parent_id']];
                    $this->view->setVar('parent_id', $city['parent_id']);
                    $this->view->setVar('parent_name', $city['parent_name']);
                    $this->view->setVar('parent_slug', $city['parent_slug']);
                }
                if($place['id'] < 78) {
                    $cities = $this->commonModel->getCatDistricts($place['id']);
                    $cities = $cities['sub' . $place['id']];
                    if($cities)
                        $this->view->setVar('to_top', true);
                }
            }

            if(!isset($cities) || !$cities) {
                $cities = $this->commonModel->getCatCities();
            }
            $this->view->setVar('cities', $cities);
            $this->view->setVar('city', $city);

            $filter = $this->getInputFilter();
            $top_rubric = $filter->sanitize($top_rubric, 'user_input');
            $sub_rubric = $filter->sanitize($sub_rubric, 'user_input');
            $rub = $this->catalog->getSLbyS2($top_rubric, $sub_rubric);

            if(!$rub) {
                $this->dumbError('(Rubrics top) Wrong top rubric', true, true);
            }

            $this->view->setVar('top_name', $rub['type_name']);
            $this->view->setVar('sub_name', $rub['spec_name']);
            $this->view->setVar('top_id', $rub['tid']);
            $this->view->setVar('sub_id', $rub['sid']);
            $this->view->setVar('top_slug', $top_rubric);
            $this->view->setVar('sub_slug', $sub_rubric);

            $top = $this->catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);
            $sub = $this->catalog->getSubSpecListById($rub['tid']);
            $this->view->setVar('sub_catalog', $sub);

            /* if (!$workers && $place_id) {
              $workers = $this->catalog->getSubWorkersListOnPlace($rub['tid'], $rub['sid'], $page, $this->config->lists->limit);
              $this->view->setVar('workers_not_found_place', true);
              } */

            if($place) {
                $workers = $this->catalog->getSubWorkersListByCoords($rub['tid'], $rub['sid'], $city['lat'], $city['lng'], $page, $this->config->lists->limit * 2);
            } else {
                $workers = $this->catalog->getSubWorkersList($rub['tid'], $rub['sid'], $page, $this->config->lists->limit * 2);
            }
            /*
              if(!$workers) {
              $this->response->setStatusCode(404, 'Not Found');
              }
              if(count($workers) > $this->config->lists->limit * 2) {
              $next = ($page == 0) ? 2 : ($page + 1);
              array_pop($workers);
              } else
              $next = 0;
              $this->view->setVar('nextpage', $next);
             */
            $this->view->setVar('count', count($workers));
            $this->view->setVar('limit', $this->config->lists->limit);

            $place = $place ? '/' . $place['id'] . '-' . $place['slug'] : '';

            $links_html = $this->createPaginator(count($workers), $page, $this->config->lists->limit * 2, $this->siteUrl . '/catalog/' . $top_rubric . '/' . $sub_rubric . $place . '/users');
            $this->view->setVar('pager_html', $links_html);

            $workers = $this->sortWorkersByPhotos($workers);
            $this->view->setVar('workers', $workers);
        }

        public function selectAction()
        {
            $this->view->disable();
            if($this->request->isPost() && $this->security->checkToken()) {
                $data['placelat'] = $this->request->getPost('placelat', 'float', false);
                $data['placelng'] = $this->request->getPost('placelng', 'float', false);
                $data['placeid'] = $this->request->getPost('placeid', array('trim', 'string'), false);
                $data['placeaddr'] = $this->request->getPost('placeaddr', array('trim', 'string'), false);
                $data['placetown'] = $this->request->getPost('placetown', array('trim', 'string'), false);
                $data['placedistrict2'] = $this->request->getPost('placedistrict2', array('trim', 'string'), false);
                $data['placedistrict3'] = $this->request->getPost('placedistrict3', array('trim', 'string'), false);
                $data['placeregion'] = $this->request->getPost('placeregion', array('trim', 'string'), false);
                $data['placecountry'] = $this->request->getPost('placecountry', array('trim', 'string'), false);

                if($data['placetown'] || $data['placeid']) {

                    $place = $this->commonModel->getCatPlace($data['placeid']);
                    if(!$place)
                        $this->commonModel->addNewPlace($data);
                }
                $location = $this->request->getPost('location', array('trim', 'string'), false);

                $speciality = $this->request->getPost('speciality', 'int', false);
                $subspeciality = $this->request->getPost('subspeciality', 'int', false);

                if(!$speciality && !$subspeciality) {
                    //$this->response->redirect($this->siteUrl . '/catalog');
                    $go = '/catalog';
                } else {
                    if($subspeciality > 0) {
                        $rub = $this->catalog->selectById2($speciality, $subspeciality);
                        $go = '/catalog/' . $rub['top_slug'] . '/' . $rub['sub_slug'];
                    } else {
                        $rub = $this->catalog->selectById($speciality);
                        $go = '/catalog/' . $rub['top_slug'];
                    }
                    if(!$rub) {
                        $this->dumbError('(Catalog select) Wrong select parameters', false, true);
                    }
                }

                if(isset($place['id']))
                    $go .= '/' . $place['id'] . '-' . $place['slug'];
                elseif($location)
                    $go .= '/' . $location;

                $show_only = $this->request->getPost('show_only', array('trim', 'string'), false);
                if(in_array($show_only, ['users', 'orders']))
                    $show_only = '/' . $show_only;
                else
                    $show_only = false;

                $this->response->redirect($this->siteUrl . $go . $show_only); //var_dump($go); die;
                $this->response->send();
            } else {
                $this->dumbError('(Catalog select) Wrong token', true);
            }
        }

        public function viewOrderAction($order_id = false, $from_list = false)
        {

            if(!$order_id)
                $order_id = $this->dispatcher->getParam('id');

            $this->view->setVar('order_id', $order_id);

            $order = $this->catalog->getOrder($order_id);

            if(!$order) {
                //$this->dumbError('(View order) Order not found ' . $order_id);
                $this->dispatcher->forward(array('controller' => 'index', 'action' => 'route404'));
            }

            $users = new BmUsers();
            $user = $users->getUserProfile($order['user_id']);
            $this->view->setVar('profile', $user);

            $user2 = $users->getUserProfile($this->session->get('usr_id'));
            $this->view->setVar('my_profile', $user2);

            /* AJAX */
            if($this->request->isAjax() and ! $from_list) {

                $duration = $this->request->get('duration', 'int', false);

                $add = $this->request->get('add', 'int', false);
                if($add == 1) {
                    if($user2['premium_hours'] > 3600) {
                        $duration = $duration * 3600;
                        $res = $this->catalog->updatePremiumBlock($order, $this->session->get('usr_id'), $duration);
                        print_r($res);
                    }
                } else {
                    if($user2['premium_hours'] > 3600) {
                        $duration = $duration * 3600;
                        $res = $this->catalog->setPremiumBlock($order, $this->session->get('usr_id'), $duration);
                        print_r($res);
                    }
                }

                exit();
            }

            $premium_block = $this->catalog->getStatusPremiumBlockByOrder($order_id);
            $this->view->setVar('premium_block_access', $premium_block);

            $is_old_order = $this->catalog->is_old_order($order_id);
            if(!$is_old_order and $user2['stars'] == 10) {
                $is_old_order = $order['create_date'] < (time() - 3600 * 24 * 3) ? 3 : 0;
            }
            $this->view->setVar('is_old_order', $is_old_order);

            $series_premium_block = $this->catalog->getSeriesPremiumBlockById($order_id);

            if(is_array($series_premium_block)) {
                foreach ($series_premium_block as $key => $value) {
                    $series_premium_block[$key]['user'] = $users->getUserProfile($value['user_id']);
                }
            }

            $this->view->setVar('series_premium_block', $series_premium_block);

            $current_premium_block = $this->catalog->getCurrentPremiumBlockByOrder($order_id);
            $this->view->setVar('current_premium_block', $current_premium_block);

            $user_informed = $this->catalog->checkInformed($user2, $order);
            $user_informed = $user_informed ? 1 : 0;

            $this->view->setVar('user_informed', $user_informed);

            $this->view->setVar('order', $order);
            if($order['town']) {

                $tmp = $this->commonModel->getPlaceByTown($order['town']);
                $slug = $tmp['id'] . '-' . $tmp['slug'];
                $this->view->setVar('slug', $slug);
            }

            $connect = $this->catalog->getOrderConnect($order_id);
            $this->view->setVar('connect', $connect);

            foreach ($connect['offer'] as $o) {
                $exists_ids[] = $o['o_uid'];
            }
            $this->view->setVar('exists_ids', $exists_ids);

            $OffersToWorker = $this->catalog->getOffersToWorker($order['user_id']);
            $this->view->setVar('OffersToWorker', $OffersToWorker);

            $this->view->setVar('OffersToWorker', $OffersToWorker);

            /* не используется */
            //$random = $this->catalog->getRandom($order_id, $order['order_wrk_spec']);
            //$this->view->setVar('random', $random);  

            $blocked_orders = $this->catalog->getBlockedOrders((int) $this->session->get('usr_id'));
            $blocked_orders = json_decode($blocked_orders['blocked_orders'], 1);

            $requests = $this->catalog->getLastConnectList($this->session->get('usr_id'));

            if(is_array($requests)) {
                foreach ($requests as $key => $value) {
                    $requests_id[] = $value['id'];
                }
            }

            $last_orders = $this->catalog->getLastUserOrdersList($this->session->get('usr_id'), 0, 50, 150);

            if(!$last_orders) {
                $last_orders = $this->catalog->getLastOrdersList(0, 50);
            }

            //убираем повторы в выборках
            if(is_array($last_orders)) {
                foreach ($last_orders as $key => $value) {
                    if(in_array($value['id'], $requests_id))
                        unset($last_orders[$key]);
                }
            }

            $all_orders = array_merge($requests, $last_orders);

            if(is_array($all_orders)) {
                foreach ($all_orders as $key => $value) {
                    if(in_array($value['id'], $blocked_orders)) {
                        unset($all_orders[$key]);
                    }
                }
            }
            $all_orders = array_values($all_orders);


            if(is_array($all_orders)) {
                foreach ($all_orders as $key => $value) {
                    if($value['id'] == $order_id) {
                        $current_order_id = $key;
                        //предыдущий
                        if($key > 0) {
                            $this->view->setVar('last', $all_orders[$key - 1]['id']);
                        }

                        //следующий
                        if(isset($all_orders[$key + 1])) {
                            $this->view->setVar('next', $all_orders[$key + 1]['id']);
                        }
                    }
                }
            }



            if($from_list) {
                return $this->view->getRender('catalog', 'order_info', []);
            }
        }

        public function createOrderAction()
        {
            $this->view->disable();

            if($this->request->isPost() && $this->security->checkToken()) {

                $email = $this->request->getPost('email', array('trim', 'email'), false);
                $phone = $this->request->getPost('phone', array('trim', 'string'), false);

                if(!$email and ! $phone)
                    die;
                if(!$email) {
                    $add = str_replace([' ', '-', '(', ')'], '', $phone);
                    $email = "$add@budmisto.com.ua";
                }

                $data['placelat'] = $this->request->getPost('placelat', 'float', false);
                $data['placelng'] = $this->request->getPost('placelng', 'float', false);
                $data['placeid'] = $this->request->getPost('placeid', array('trim', 'string'), false);
                $data['placeaddr'] = $this->request->getPost('placeaddr', array('trim', 'string'), false);
                $data['placetown'] = $this->request->getPost('placetown', array('trim', 'string'), false);
                $data['placedistrict2'] = $this->request->getPost('placedistrict2', array('trim', 'string'), false);
                $data['placedistrict3'] = $this->request->getPost('placedistrict3', array('trim', 'string'), false);
                $data['placeregion'] = $this->request->getPost('placeregion', array('trim', 'string'), false);
                $data['placecountry'] = $this->request->getPost('placecountry', array('trim', 'string'), false);
                $data['placeroute'] = $this->request->getPost('placeroute', array('trim', 'string'), false);
                $data['placextra'] = $this->request->getPost('placextra', array('trim', 'string'), false);

                $data['description'] = $this->request->getPost('desc', array('trim', 'string'), false);

                /* Глючит гугл, не выдает киевскую область! */
                if($data['placetown'] == "Киев")
                    $data['placeregion'] = 'Киевская область';

                if(!$this->user_level) {
                    $passwd = $this->generator->genRandomString(6);
                    $users = new BmUsers();
                    $user_id = $users->createByOrderForm($email, $phone, $passwd);
                    if(!$user_id) {
                        $this->dumbError('(Create order form) Failed to create a new user');
                    }
                    if($email) {
                        $mail = new BMail('users', 'registration');
                        $mail->setOptions($this->siteUrl);
                        $data['email'] = $email;
                        $data['passwd'] = $passwd;
                        $data['fast_login'] = UsersController::dsCrypt($email);
                        $mail->send('Регистрация на сайте "Будмісто"', $email, $data);
                    }
                } else {
                    $user_id = $this->user_id;
                }


                $this->session->remove('placelat');
                $this->session->remove('placelng');
                $this->session->remove('placeid');
                $this->session->remove('placeaddr');
                $this->session->remove('placetown');
                $this->session->remove('placedistrict2');
                $this->session->remove('placedistrict3');
                $this->session->remove('placeregion');
                $this->session->remove('placecountry');
                $this->session->remove('placeroute');
                $this->session->remove('placextra');
                $this->session->remove('description');
                $this->session->remove('email');
                $this->session->remove('phone');

                $filter = $this->getInputFilter();
                $data['description'] = $filter->sanitize($data['description'], 'user_input');
                $data['user_id'] = $user_id;
                if($this->session->usr_level > 3 and $this->session->usr_level < 7) {
                    $order_id = $this->catalog->createOrderByWorker($data);
                } else {
                    $order_id = $this->catalog->createOrder($data);
                }

                if(!$order_id) {
                    $this->dumbError('(Create order form) Failed to create order');
                } else {
                    
                    $phones = file_get_contents('order_phones.txt');
                    $phones = explode(';', $phones);
                    $arr = is_array($phones) ? $phones : [];
                    foreach ($arr as $key => $value) {
                        if($value != $phone and $value!='')
                            $html .= $value . ';';
                    }

                    file_put_contents('order_phones.txt', $html);
                }

                if($data['placetown'] && $data['placeid'] && $data['placelat'] && $data['placelng'] && $data['placeregion']) {
                    //$this->dumbError('(Create order) Wrong location', false, true);
                    //$has_place = $this->commonModel->checkPlace($data['placeid']);
                    if($data['placeroute'] && !$data['placextra']) {
                        $key = $this->config->application->server_key;
                        $ch = curl_init();
                        $url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query=' . urlencode($data['placetown'] . ',' . $data['placeregion']);
                        $url .= '&key=' . $key . '&language=ru';
                        $cert = APP_DIR . 'var/cacerts.pem';

                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_HEADER, false);
                        curl_setopt($ch, CURLOPT_CAINFO, $cert);

                        session_write_close();
                        $responce = curl_exec($ch);
                        session_start();
                        curl_close($ch);

                        $res = json_decode($responce);
                        $d['placetown'] = $res->results[0]->name;
                        $d['placeid'] = $res->results[0]->place_id;
                        $d['placelat'] = $res->results[0]->geometry->location->lat;
                        $d['placelng'] = $res->results[0]->geometry->location->lng;
                        $d['placeregion'] = $data['placeregion'];
                        $this->commonModel->addNewPlace($d);
                    } else
                        $this->commonModel->addNewPlace($data);
            
                    $this->commonModel->addOrderPlace($order_id, $data);
                }

                $this->response->redirect($this->siteUrl . '/my/order/' . $order_id);
                $this->response->send();
                die;
            } else {
                $this->response->redirect($this->siteUrl . '/my');
                $this->response->send();
                die;
            }
        }

        public function setEternalFreezeAction()
        {
            $order_id = $this->request->getPost('order_id', 'int', false);
            $user_id = $this->session->get('usr_id');

            $res = $this->catalog->setEternalFreeze($user_id, $order_id);
            print_r($res);
            exit();
        }

        public function subDataAction()
        {
            $r = $this->request->getHTTPReferer();
            $ref_page = $this->request->get('ref', 'string', false);

            if(strpos($r, $this->siteUrl) === false)
                die;

            $this->view->disable();
            //$this->logger->log('WTF?!', \Phalcon\Logger::ERROR);
            if($this->request->isAjax()) {
                $top_id = $this->request->get('speciality', 'int', false);
                $tmp = array();
                $text = $this->_getTranslation()->_('blk_select2');
                echo '{';
                if($top_id) {
                    $content = $this->wunderCache($top_id, 'subdata');
                    if($content === null) {
                        $sub_list = $this->catalog->getSubSpecListById($top_id);

                        if($sub_list) {

                            $tmp[] = '"0" : "-- ' . $text . ' --"';
                            foreach ($sub_list as $sl)
                                $tmp[] = '"' . $sl['id'] . '" : "' . $sl['spec_name'] . ' (' . $sl['num'] . ')"';

                            if($ref_page == 'profile' or $ref_page == 'admin' or $ref_page == 'shops') {
                                $tmp[] = '"1000" : "Добавить все"';
                            }

                            echo implode(', ', $tmp);

                            $this->viewCache->save();
                        }
                    } else
                        echo $content;
                }
                if(empty($tmp)) {
                    $tmp[] = '"" : "-- ' . $text . ' --"';
                    echo implode(', ', $tmp);
                }
                echo '}';
            }
        }

        public function choiceAction()
        {
            $this->checkLogged();

            if($this->user_level < 3 || $this->user_level > 6)
                return false;

            if(!$this->request->isAjax() || !$this->security->checkToken())
                return false;

            $this->view->disable();
            $worker_id = $this->request->getPost('worker', 'int', false);
            $order_id = $this->request->getPost('order', 'int', false);
            if(!$worker_id || !$order_id)
                $this->dumbError('(Choice worker) Failed input data');

            $countChoicesToday = count($this->catalog->countChoicesToday($order_id));

            if($countChoicesToday >= 10) {
                $limit = $this->catalog->getLimitChoices($this->session->get('usr_id'));
                if($limit < 1)
                    exit("blocked");
                else
                    $this->catalog->setLimitChoices($this->session->get('usr_id'));
            }

            $res = $this->catalog->setConnect($order_id, $worker_id, 'customer');
            if($res) {
                echo 'selected';
                $users = new BmUsers();
                $user = $users->getUserProfile($worker_id);
                $mail = new BMail('common', 'info');
                $mail->setOptions($this->siteUrl);
                $data['title'] = 'Вас выбрали для выполнения заказа, свяжитесь с заказчиком';
                $data['message'] = 'Зайдите в <a href="http://' . $_SERVER[HTTP_HOST] . '/index/' . UsersController::dsCrypt($user['user_email']) . '"><button>Личный кабинет</button></a> для ознакомления с новой информацией!';
                $mail->send('Новый заказ', $user['user_email'], $data);

                /* Создаем задание для крона на отправку смс */

                $order = BmCatalog::getOrder($order_id);

                if($user['stars'] == 10) {
                    $data['type'] = 3;
                } else {
                    $data['type'] = 2;
                }

                $data['phone'] = $user['user_phone'];
                $data['sender'] = $order['user_phone'];
                $data['order_id'] = $order_id;

                \Budmisto\Modules\Front\Models\Connect::setTask($data);
            } else {
                echo 'removed';
            }
            // NEED REFACT
        }

        public function offerAction()
        {
            $this->checkLogged();

            if($this->user_level < 4 || $this->user_level > 6)
                return false;

            $order_id = $this->request->getPost('order_id', 'int', false);
            $text = $this->request->getPost('message', array('trim', 'string'), false);
            if(!$order_id)
                $this->dumbError('(Offer worker) Failed input data');
            if(!$text)
                $text = 'Предлагаем свои услуги';

            if(($this->request->isPost() or $this->request->isAjax() ) && $this->security->checkToken()) {


                $res = $this->catalog->setConnect($order_id, $this->user_id, 'performer', $text);

                if($res) {
                    $users = new BmUsers();
                    $user = $users->getUserProfile($this->session->get('usr_id'));

                    /**
                     * Даем 5 звезд, если добавился сам не в день регистрации
                     */
                    if($user['stars'] < 5 and $user['stars'] > 1 and $user['stars'] != 4 and $user['days_from_reg'] > 0) {
                        $users->updateUserStars(5, $this->session->get('usr_id'));
                    }

                    /**
                     * Даем 6 звезд, если строитель имеет  от 3х собственных добавлений к заказам
                     * в разные дни будучи 5кой
                     */
                    if($user['stars'] < 6 and $user['stars'] != 4 and $user['stars'] != 1) {

                        $count = $this->catalog->getCountConnectsByUser($this->session->get('usr_id'));
                        if($count > 2 and $user['stars'] == 5)
                            $users->updateUserStars(6, $this->session->get('usr_id'));
                    }


                    if($user['stars'] > 2) {
                        ConnectController::setTaskSMSByOrderId($order_id, $this->session->get('usr_phone'));


                        $order_info = $this->catalog->getOrder($order_id);
                        $customer_id = $order_info['user_id'];


                        $customer = BmUsers::getUserProfile($customer_id);

                        $d['getter_phone'] = $user['user_phone']; //
                        $d['sender_phone'] = $customer['user_phone'];
                        $d['order_id'] = $order_id;
                        $d['order_specialization'] = 'not need'; //
                        $d['set_to_crone'] = 1;
                        $d['type'] = 2;

                        $connect = new ConnectController();
                        @$connect->sendOneAction($d);
                    }
                    $admin_email = $this->config->application->admin_email;
                    $mail = new BMail('common', 'info');
                    $mail->setOptions($this->siteUrl);
                    $data['title'] = 'Интерес строителя';
                    $data['message'] = "Строитель <a href='" . $this->siteUrl . "/workz/users/view/" . $this->user_id . "'>" . $this->session->get('usr_email') . "</a> заинтересовался <a href='" . $this->siteUrl . "/workz/orders/view/$order_id'>заказом</a> ";
                    $mail->send('Интерес строителя', $admin_email, $data);
                }

                if($this->request->isAjax()) {
                    print_r("success");
                    exit();
                } else {
                    $this->response->redirect($this->siteUrl . '/catalog/order/' . $order_id);
                }
            }
        }

        public function delOfferAction()
        {
            $this->checkLogged();

            $connect_id = $this->dispatcher->getParam('id');
            if(!$connect_id)
                $this->dumbError('(Delete offer) Failed input data');

            /* if ($this->user_level < 4 || $this->user_level > 5) {
              $this->response->redirect($this->siteUrl.'/catalog/');
              $this->response->send();
              die;
              } */

            $r = $this->request->getHTTPReferer();
            if(strpos($r, $this->siteUrl) === false) {
                $this->response->redirect($this->siteUrl);
                $this->response->send();
                die;
            }

            $order_id = $this->catalog->deleteConnect($connect_id, $this->user_id);
            if($order_id)
                $this->response->redirect($this->siteUrl . '/catalog/order/' . $order_id);
            else
                $this->response->redirect($this->siteUrl . '/catalog/');
            $this->response->send();
            die;
        }

        public function usersAction()
        {
            $tmp = $this->request->get('page', 'int');
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            $workers = $this->catalog->getWorkersList($page, $this->config->lists->limit * 2);

            if(!$workers) {
                $this->response->setStatusCode(404, 'Not Found');
            }

            $workers = $this->sortWorkersByPhotos($workers);

            $this->view->setVar('workers', $workers); //var_dump($workers); die;

            $count = $this->catalog->countWorkers();
            $this->view->setVar('count', $count);
            $this->view->setVar('limit', $this->config->lists->limit);

            $links_html = $this->createPaginator($count, $page, $this->config->lists->limit * 2, $this->siteUrl . '/catalog/users');
            $this->view->setVar('pager_html', $links_html);

            $top = $this->catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);
        }

        public function ordersAction()
        {
            $tmp = $this->request->get('page', 'int');
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            $orders = $this->catalog->getOrdersList($page, $this->config->lists->limit * 2);
            if(!$orders) {
                $this->response->setStatusCode(404, 'Not Found');
            }
            $this->view->setVar('orders', $orders); //var_dump($orders); die;

            $count = $this->catalog->countOrders();
            $this->view->setVar('count', $count);
            $this->view->setVar('limit', $this->config->lists->limit);

            $links_html = $this->createPaginator($count, $page, $this->config->lists->limit * 2, $this->siteUrl . '/catalog/orders');
            $this->view->setVar('pager_html', $links_html);

            $top = $this->catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);
        }

        public function AjaxOrderAction()
        {
            $order_id = $this->request->getPost('id', 'int', 3657); /////////TODOOOO

            if(!$order_id)
                die;

            $order = $this->catalog->getOrder($order_id);

            print_r(json_encode((object) [
                                'order' => $order,
                                'add_info' => $this->viewOrderAction($order_id, true),
            ]));
            exit();
        }

        public function tempPhoneAction()
        {

            $phone = $this->request->getPost('phone');

            file_put_contents('order_phones.txt', $phone . ";", FILE_APPEND);
        }

    }
    