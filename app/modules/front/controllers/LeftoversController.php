<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/front/controllers/leftoversController.php
     *
     * SurplusesController controller
     *
     */

    namespace Budmisto\Modules\Front\Controllers;

    use Budmisto\Modules\Front\Models\Catalog as BmCatalog;
    use Budmisto\Modules\Front\Models\Leftovers as BmLO;
    use Budmisto\Lib\Mailer\Mail as BMail;
    use Budmisto\Modules\Common\Models\Common as Common;

    class LeftoversController extends ControllerBase
    {

        public $catalog;
        public $lo;

        public function initialize()
        {
            parent::initialize();
            $this->catalog = new BmCatalog();
            $this->lo = new BmLO();
            $this->catalog->setLang($this->lang);
        }

        public function indexAction()
        {
            $limit = $this->config->lists->limit;

            $tmp = $this->request->get('page', 'int');
            if($this->request->isAjax()) {
                $tmp = $this->request->getPost('start', 'int', false);
            }
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            $top_catalog = $this->catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top_catalog);
            $this->view->setVar('map_key', $this->config->application->map_key);

            $leftovers = $this->lo->getLoList('sell', $page, $limit);
            /* if (!$leftovers) {
              $this->dispatcher->forward(array('controller' => 'index', 'action' => 'route404'));
              } */



            $this->view->setVar('sellleftovers', $leftovers);

            $leftovers = $this->lo->getLoList('buy', $page, $limit);
            $this->view->setVar('buyleftovers', $leftovers);

            if($this->request->isAjax()) {
                return $this->view->pick('leftovers/ajax_leftovers');
                exit();
            }


            if(!$leftovers) {
                $count = $this->lo->countLO();
                $this->view->setVar('count', $count);
                $this->view->setVar('limit', $limit);

                $links_html = $this->createPaginator($count, $page, $limit, $this->siteUrl . '/leftovers');
                $this->view->setVar('pager_html', $links_html);
            } else {
                $this->view->pick('leftovers/indexnew');
            }
        }

        public function addSellAction()
        {
            $top_catalog = $this->catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top_catalog);
            $this->view->setVar('map_key', $this->config->application->map_key);
        }

        public function addBuyAction()
        {
            $top_catalog = $this->catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top_catalog);
            $this->view->setVar('map_key', $this->config->application->map_key);
        }

        public function addAction()
        {
            if($this->request->isPost() && $this->security->checkToken()) {
                $desc = $this->request->getPost('desc', array('trim', 'string'), false);
                $placelat = $this->request->getPost('placelat', 'float', 0);
                $placelng = $this->request->getPost('placelng', 'float', 0);
                if($placelat && $placelng) {
                    $data['placelat'] = $placelat;
                    $data['placelng'] = $placelng;
                    $data['placeid'] = $this->request->getPost('placeid', array('trim', 'string'), false);
                    $data['placeaddr'] = $this->request->getPost('placeaddr', array('trim', 'string'), false);
                    $data['placetown'] = $this->request->getPost('placetown', array('trim', 'string'), false);
                    $data['placedistrict2'] = $this->request->getPost('placedistrict2', array('trim', 'string'), false);
                    $data['placedistrict3'] = $this->request->getPost('placedistrict3', array('trim', 'string'), false);
                    $data['placeregion'] = $this->request->getPost('placeregion', array('trim', 'string'), false);
                    $data['placecountry'] = $this->request->getPost('placecountry', array('trim', 'string'), false);
                    $data['placeroute'] = $this->request->getPost('placeroute', array('trim', 'string'), false);
                    $data['placextra'] = $this->request->getPost('placextra', array('trim', 'string'), false);

                    if($data['placetown'] && $data['placeid'] && $data['placeregion']) {

                        //$common = new Common()
                        if($data['placeroute'] && !$data['placextra']) {
                            $key = $this->config->application->server_key;
                            $ch = curl_init();
                            $url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query=' . urlencode($data['placetown'] . ',' . $data['placeregion']);
                            $url .= '&key=' . $key . '&language=ru';
                            $cert = APP_DIR . 'var/cacerts.pem';

                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HEADER, false);
                            curl_setopt($ch, CURLOPT_CAINFO, $cert);

                            session_write_close();
                            $responce = curl_exec($ch);
                            session_start();
                            curl_close($ch);

                            $res = json_decode($responce);
                            $d['placetown'] = $res->results[0]->name;
                            $d['placeid'] = $res->results[0]->place_id;
                            $d['placelat'] = $res->results[0]->geometry->location->lat;
                            $d['placelng'] = $res->results[0]->geometry->location->lng;
                            $d['placeregion'] = $data['placeregion'];
                            $this->commonModeladdNewPlace($d);
                        } else
                            $this->commonModeladdNewPlace($data);
                    }
                } else
                    die;

                $data['speciality'] = $this->request->getPost('speciality', 'int', false);
                $actual = $this->request->getPost('period', 'int', false);
                $data['description'] = $this->request->getPost('desc', array('trim', 'string'), false);
                $data['user_id'] = $this->user_id;
                $data['shipping'] = $this->request->getPost('shipping', 'int', false);

                $data['name'] = $this->request->getPost('name', array('trim', 'string'), '');
                $data['phone'] = $this->request->getPost('phone', array('trim', 'string'), false);
                $data['type'] = $this->request->getPost('type', array('trim', 'string'), false);
                $data['email'] = $this->request->getPost('email', 'email', '');

                if($data['speciality'] && $actual && $data['description'] && $data['shipping'] && $data['phone'] && $data['type']) {
                    $data['actual_period'] = date('Y-m-d H:i:s', strtotime('+' . $actual . ' days'));
                    $filter = $this->getInputFilter();
                    $data['description'] = $filter->sanitize($data['description'], 'user_input');
                    $data['brief'] = $this->textengine->getBrief($data['description'], $this->config->lists->big_brief);
                    $post_id = $this->lo->addPost($data);
                    if($post_id) {
                        $this->response->redirect($this->siteUrl . '/leftovers/view/' . $post_id);
                        $this->response->send();
                        die;
                    }
                } else {
                    $this->response->redirect($this->request->getHTTPReferer());
                    $this->response->send();
                    die;
                }
            } else {
                $this->response->redirect($this->siteUrl . '/leftovers/');
                $this->response->send();
                die;
            }
        }

        public function viewAction()
        {
            $post_id = $this->dispatcher->getParam('id');
            if(!$post_id)
                $this->dumbError('(Post view) wring id');

            $lo = $this->lo->getPost($post_id);
            if(!$lo) {
                $this->dispatcher->forward(array('controller' => 'index', 'action' => 'route404'));
            }
            $this->view->setVar('lo', $lo);
            if($lo['lo_type'] == 'sell')
                $this->view->pick('leftovers/viewsell');
            else
                $this->view->pick('leftovers/viewbuy');
        }

        public function topRubricAction()
        {
            $limit = $this->config->lists->limit;
            $this->view->setVar('map_key', $this->config->application->map_key);
            $top_rubric = $this->dispatcher->getParam('top_rubric');
            $place = $this->dispatcher->getParam('place');
            $this->view->setVar('place_slug', $place);
            $shipping = $this->dispatcher->getParam('shipping');
            $this->view->setVar('shipping_slug', $shipping);

            //$common = new Common()
            $cities = false;
            $city = false;
            $this->view->setVar('slug', false);
            if($place) {
                $place = '';
                $tmp = $this->dispatcher->getParam('place');
                $this->view->setVar('slug', $tmp);
                $tmp = explode('-', $tmp);
                $place['id'] = array_shift($tmp);
                $place['slug'] = implode('-', $tmp);
                if($place['id'] < 161)
                    $city = $this->commonModelgetCitiesNew($place);
                else
                    $city = $this->commonModelgetPlacesNew($place);

                if(!$city) {
                    $this->dumbError('(Rubrics leftovers) Wrong city', true, true);
                }

                if($place['id'] > 77 && $place['id'] < 161) {
                    $cities = $this->commonModelgetCatDistricts($city['parent_id']);
                    $cities = $cities['sub' . $city['parent_id']];
                    $this->view->setVar('parent_id', $city['parent_id']);
                    $this->view->setVar('parent_name', $city['parent_name']);
                    $this->view->setVar('parent_slug', $city['parent_slug']);
                }
                if($place['id'] < 78) {
                    $cities = $this->commonModelgetCatDistricts($place['id']);
                    $cities = $cities['sub' . $place['id']];
                    if($cities)
                        $this->view->setVar('to_top', true);
                }
            }

            if(!isset($cities) || !$cities) {
                $cities = $this->commonModelgetCatCities();
            }
            $this->view->setVar('cities', $cities);
            $this->view->setVar('city', $city);

            $filter = $this->getInputFilter();
            $top_rubric = $filter->sanitize($top_rubric, 'user_input');
            $rub = $this->catalog->getSLbyS($top_rubric);

            if(!$rub) {
                $this->dumbError('(Rubrics leftovers) Wrong top rubric', true, true);
            }

            $this->view->setVar('rub_name', $rub['type_name']);
            $this->view->setVar('top_id', $rub['tid']);
            $this->view->setVar('top_slug', $top_rubric);

            $top = $this->catalog->getTopSpecList();
            $this->view->setVar('top_catalog', $top);

            $tmp = $this->request->get('page', 'int');
            $page = $tmp ? $tmp : 0;
            $this->view->setVar('page', $page);

            if($place) {
                $leftovers = $this->lo->getLoListByCoords($rub['tid'], $shipping, $city['lat'], $city['lng'], $page, $limit);
                if(count($leftovers) > $this->config->lists->limit) {
                    $next = ($page == 0) ? 2 : ($page + 1);
                    array_pop($leftovers);
                } else
                    $next = 0;
                $this->view->setVar('nextpage', $next);
            } else {
                $leftovers = $this->lo->getTopLoList($rub['tid'], $shipping, 'sell', $page, $limit);
                $this->view->setVar('sellleftovers', $leftovers);

                $leftovers = $this->lo->getTopLoList($rub['tid'], $shipping, 'buy', $page, $limit);
                $this->view->setVar('buyleftovers', $leftovers);

                if(!$shipping)
                    $pager_url = $this->siteUrl . '/leftovers/' . $top_rubric;
                else
                    $pager_url = $this->siteUrl . '/leftovers/' . $top_rubric . '/' . $shipping;
                $this->view->setVar('pager_url', $pager_url);

                if(!$leftovers) {
                    $count = $this->lo->countTopLO($rub['tid'], $shipping);
                    $this->view->setVar('count', $count);
                    $this->view->setVar('limit', $limit);

                    $links_html = $this->createPaginator($count, $page, $limit, $pager_url);
                    $this->view->setVar('pager_html', $links_html);
                    $this->view->pick('leftovers/toprubric');
                } else
                    $this->view->pick('leftovers/toprubricnew');
            }
            /* if (!$leftovers) {
              $this->response->setStatusCode(404, 'Not Found');
              } */
        }

        public function selectAction()
        {
            $this->view->disable();
            if($this->request->isPost() && $this->security->checkToken()) {
                $data['placelat'] = $this->request->getPost('placelat', 'float', false);
                $data['placelng'] = $this->request->getPost('placelng', 'float', false);
                $data['placeid'] = $this->request->getPost('placeid', array('trim', 'string'), false);
                $data['placeaddr'] = $this->request->getPost('placeaddr', array('trim', 'string'), false);
                $data['placetown'] = $this->request->getPost('placetown', array('trim', 'string'), false);
                $data['placedistrict2'] = $this->request->getPost('placedistrict2', array('trim', 'string'), false);
                $data['placedistrict3'] = $this->request->getPost('placedistrict3', array('trim', 'string'), false);
                $data['placeregion'] = $this->request->getPost('placeregion', array('trim', 'string'), false);
                $data['placecountry'] = $this->request->getPost('placecountry', array('trim', 'string'), false);

                if($data['placetown'] || $data['placeid']) {
                    //$common = new Common()
                    $place = $this->commonModelgetCatPlace($data['placeid']);
                    if(!$place)
                        $this->commonModeladdNewPlace($data);
                }
                $location = $this->request->getPost('location', array('trim', 'string'), false);
                $speciality = $this->request->getPost('speciality', 'int', false);
                $shipping = $this->request->getPost('shipping', 'int', false);
                if(!$speciality && !$shipping) {
                    $go = '/leftovers';
                } else {
                    $rub = $this->catalog->selectById($speciality);
                    if(!$rub) {
                        $this->dumbError('(Leftovers select) Wrong select parameters', false, true);
                    }
                    $go = '/leftovers/' . $rub['top_slug'];
                }
                if($shipping) {
                    switch ($shipping)
                    {
                        case '1': $go .= '/pickup';
                            break;
                        case '2': $go .= '/delivery';
                            break;
                    }
                }

                if(isset($place['id']))
                    $go .= '/' . $place['id'] . '-' . $place['slug'];
                elseif($location)
                    $go .= '/' . $location;

                $this->response->redirect($this->siteUrl . $go);
                $this->response->send();
            } else {
                $this->dumbError('(Leftovers select) Wrong token', true);
            }
        }

    }
    