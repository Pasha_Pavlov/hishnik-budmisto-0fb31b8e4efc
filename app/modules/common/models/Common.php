<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/common/models/Common.php
     *
     * Common model
     *
     */

    namespace Budmisto\Modules\Common\Models;

    class Common extends \Phalcon\Mvc\Model
    {

        private $db;
        private $lang;
        private $te;

        public function initialize()
        {
            $this->db = $this->getDi()->getShared('db');
            $this->te = $this->getDi()->getShared('textengine');
        }

        public function setLang($lang)
        {
            $this->lang = $lang;
        }

        public function getUser($user_id)
        {
            $sql = 'SELECT bm_users.user_role_id, bm_users.id, bm_users.user_name, bm_users.user_phone,
                bm_perfs_info.description, bm_perfs_info.phone1, bm_perfs_info.phone2, bm_perfs_info.website
                FROM bm_users
                LEFT JOIN bm_perfs_info ON bm_perfs_info.user_id = bm_users.id
                WHERE bm_users.id =?';
            return $this->db->query($sql, array($user_id))->fetch();
        }

        public function setAvatar($user_id, $ava_name)
        {
            $sql = 'UPDATE bm_users SET avatar = ? WHERE id = ?';
            $this->db->query($sql, array($ava_name, $user_id));
        }

        public function addPlace($d)
        {
            $d['placedistrict'] = ($d['placedistrict2']) ? $d['placedistrict2'] : $d['placedistrict3'];
            $d['placedistrict'] = str_replace('горсовет', 'регион', $d['placedistrict']);
            $d['placedistrict'] = str_replace('міськрада', 'громада', $d['placedistrict']);

            $sql = 'SELECT id FROM bm_regions WHERE ru_region_name = ? OR uk_region_name = ?';
            $region = $this->db->query($sql, array($d['placeregion'], $d['placeregion']))->fetch()['id'];
            if($region) {
                // NEED REFACT
                $district = str_replace('регион', '', $d['placedistrict']);
                $district = str_replace('район', '', $district);
                $district .= 'район';
                $sql = 'SELECT id FROM bm_districts WHERE region_id = ? AND district_name = ?';
                $district = $this->db->query($sql, array($region, $district))->fetch()['id'];
                if(!$district)
                    $district = 'NULL';
            } else {
                $region = 'NULL';
                $district = 'NULL';
            }

            $sql = 'INSERT IGNORE INTO bm_places (place_town, place_district, place_region, place_country, place_lat, place_lng, place_id, region_id, district_id)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
            $data = array($d['placetown'], $d['placedistrict'], $d['placeregion'], $d['placecountry'], $d['placelat'], $d['placelng'], $d['placeid'], $region, $district);
            $this->db->query($sql, $data);
        }

        public function addCatPlace($d)
        {
            $d['placedistrict'] = ($d['placedistrict2']) ? $d['placedistrict2'] : $d['placedistrict3'];
            $d['placedistrict'] = str_replace('горсовет', 'регион', $d['placedistrict']);
            $d['placedistrict'] = str_replace('міськрада', 'громада', $d['placedistrict']);

            $sql = 'SELECT id FROM bm_regions WHERE ru_region_name = ? OR uk_region_name = ?';
            $region = $this->db->query($sql, array($d['placeregion'], $d['placeregion']))->fetch()['id'];
            if($region) {
                // NEED REFACT
                $district = str_replace('регион', '', $d['placedistrict']);
                $district = str_replace('район', '', $district);
                $district .= 'район';
                $sql = 'SELECT id FROM bm_districts WHERE region_id = ? AND district_name = ?';
                $district = $this->db->query($sql, array($region, $district))->fetch()['id'];
                if(!$district)
                    $district = 'NULL';
            } else {
                $region = 'NULL';
                $district = 'NULL';
            }

            $sql = 'INSERT IGNORE INTO bm_places (place_town, place_district, place_region, place_country, place_lat, place_lng, place_id, region_id, district_id)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
            $data = array($d['placetown'], $d['placedistrict'], $d['placeregion'], $d['placecountry'], $d['placelat'], $d['placelng'], $d['placeid'], $region, $district);
            $this->db->query($sql, $data);
        }

        public function addUserPlace($user_id, $d)
        {
            if(!$d['placextra']) {
                $d['placedistrict'] = ($d['placedistrict2']) ? $d['placedistrict2'] : $d['placedistrict3'];
                $d['placedistrict'] = str_replace('горсовет', 'регион', $d['placedistrict']);
                $d['placedistrict'] = str_replace('міськрада', 'громада', $d['placedistrict']);
            } else {
                $d['placedistrict'] = $d['placextra'];
            }

            $sql = 'SELECT id FROM bm_regions WHERE ru_region_name = ? OR uk_region_name = ?';
            $region_id = $this->db->query($sql, array($d['placeregion'], $d['placeregion']))->fetch()['id'];
            if($region_id == 26)
                $region_id = 10;
            // NEED REFACT

            $sql = 'INSERT INTO bm_users_places (route, town, district, region, country, lat, lng, place_id, user_id, region_id)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
            $data = array($d['placeroute'], $d['placetown'], $d['placedistrict'], $d['placeregion'], $d['placecountry'], $d['placelat'], $d['placelng'], $d['placeid'], $user_id, $region_id);
            $this->db->query($sql, $data);
        }

        public function addShopPlace($shop_id, $d)
        {
            if(!$d['placextra']) {
                $d['placedistrict'] = ($d['placedistrict2']) ? $d['placedistrict2'] : $d['placedistrict3'];
                $d['placedistrict'] = str_replace('горсовет', 'регион', $d['placedistrict']);
                $d['placedistrict'] = str_replace('міськрада', 'громада', $d['placedistrict']);
            } else {
                $d['placedistrict'] = $d['placextra'];
            }

            $sql = 'SELECT id FROM bm_regions WHERE ru_region_name = ? OR uk_region_name = ?';
            $region_id = $this->db->query($sql, array($d['placeregion'], $d['placeregion']))->fetch()['id'];
            if($region_id == 26)
                $region_id = 10;
            // NEED REFACT

            $sql = 'INSERT INTO bm_shop_places (budnum, route, town, district, region, country, lat, lng, place_id, shop_id, region_id)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
            $data = array($d['budnum'], $d['placeroute'], $d['placetown'], $d['placedistrict'], $d['placeregion'], $d['placecountry'], $d['placelat'], $d['placelng'], $d['placeid'], $shop_id, $region_id);
            $this->db->query($sql, $data);
        }

        public function editUserPlace($user_id, $d)
        {
            if(!$d['placextra']) {
                $d['placedistrict'] = ($d['placedistrict2']) ? $d['placedistrict2'] : $d['placedistrict3'];
                $d['placedistrict'] = str_replace('горсовет', 'регион', $d['placedistrict']);
                $d['placedistrict'] = str_replace('міськрада', 'громада', $d['placedistrict']);
            } else {
                $d['placedistrict'] = $d['placextra'];
            }

            $sql = 'SELECT id FROM bm_regions WHERE ru_region_name = ? OR uk_region_name = ?';
            $region_id = $this->db->query($sql, array($d['placeregion'], $d['placeregion']))->fetch()['id'];
            if($region_id == 26)
                $region_id = 10;
            // NEED REFACT

            $sql = 'UPDATE bm_users_places SET route = ?, town = ?, district = ?, region = ?, country = ?,
                lat = ?, lng = ?, place_id = ?, region_id = ? WHERE user_id = ?';
            $data = array($d['placeroute'], $d['placetown'], $d['placedistrict'], $d['placeregion'], $d['placecountry'],
                $d['placelat'], $d['placelng'], $d['placeid'], $region_id, $user_id);
            $this->db->execute($sql, $data);
            $res = $this->db->affectedRows();

            if(!$res) {
                $sql = 'INSERT INTO bm_users_places (route, town, district, region, country, lat, lng, place_id, user_id, region_id)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
                $data = array($d['placeroute'], $d['placetown'], $d['placedistrict'], $d['placeregion'], $d['placecountry'], $d['placelat'], $d['placelng'], $d['placeid'], $user_id, $region_id);
                $this->db->query($sql, $data);
            }
        }

        public function addOrderPlace($order_id, $d)
        {
            if(!$d['placextra']) {
                $d['placedistrict'] = ($d['placedistrict2']) ? $d['placedistrict2'] : $d['placedistrict3'];
                $d['placedistrict'] = str_replace('горсовет', 'регион', $d['placedistrict']);
                $d['placedistrict'] = str_replace('міськрада', 'громада', $d['placedistrict']);
            } else {
                $d['placedistrict'] = $d['placextra'];
            }

            $sql = 'SELECT id FROM bm_regions WHERE ru_region_name = ? OR uk_region_name = ?';
            $region_id = $this->db->query($sql, array($d['placeregion'], $d['placeregion']))->fetch()['id'];
            if($region_id == 26)
                $region_id = 10;
            // NEED REFACT

            $sql = 'INSERT INTO bm_orders_places (route, town, district, region, country, lat, lng, place_id, order_id, region_id)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
            $data = array($d['placeroute'], $d['placetown'], $d['placedistrict'], $d['placeregion'], $d['placecountry'], $d['placelat'],
                $d['placelng'], $d['placeid'], $order_id, $region_id);
            $this->db->query($sql, $data);
        }

        public function editOrderPlace($order_id, $d)
        {
            if(!$d['placextra']) {
                $d['placedistrict'] = ($d['placedistrict2']) ? $d['placedistrict2'] : $d['placedistrict3'];
                $d['placedistrict'] = str_replace('горсовет', 'регион', $d['placedistrict']);
                $d['placedistrict'] = str_replace('міськрада', 'громада', $d['placedistrict']);
            } else {
                $d['placedistrict'] = $d['placextra'];
            }

            $sql = 'SELECT id FROM bm_regions WHERE ru_region_name = ? OR uk_region_name = ?';
            $region_id = $this->db->query($sql, array($d['placeregion'], $d['placeregion']))->fetch()['id'];
            if($region_id == 26)
                $region_id = 10;
            // NEED REFACT

            $sql = "SELECT * FROM bm_orders_places WHERE order_id = ?";
            $res = $this->db->query($sql, [$order_id])->fetch();

            if($res) {
                $sql = 'UPDATE bm_orders_places SET route = ?, town = ?, district = ?, region = ?, country = ?, lat = ?, lng = ?,
                place_id = ?, region_id = ? WHERE order_id = ?';
                $data = array($d['placeroute'], $d['placetown'], $d['placedistrict'], $d['placeregion'], $d['placecountry'], $d['placelat'],
                    $d['placelng'], $d['placeid'], $region_id, $order_id);
                $this->db->query($sql, $data);
            } else {
                $sql = 'INSERT INTO bm_orders_places SET route = ?, town = ?, district = ?, region = ?, country = ?, lat = ?, lng = ?,
                place_id = ?, region_id = ?, order_id = ?';
                $data = array($d['placeroute'], $d['placetown'], $d['placedistrict'], $d['placeregion'], $d['placecountry'], $d['placelat'],
                    $d['placelng'], $d['placeid'], $region_id, $order_id);
                $this->db->query($sql, $data);
            }
        }

        public function editOrderCoords($order_id, $d)
        {
            $sql = 'UPDATE bm_orders_places SET lat = ?, lng = ? WHERE order_id = ?';
            $data = array($d['placelat'], $d['placelng'], $order_id);
            $this->db->query($sql, $data);
        }

        public function addNewPlace($d)
        {
            $sql = 'SELECT id FROM bm_cat_cities WHERE place_id = ?';
            $res = $this->db->query($sql, array($d['placeid']))->fetch()['id'];
            if($res)
                return false;

            $sql = 'SELECT id FROM bm_regions WHERE ru_region_name = ? OR uk_region_name = ?';
            $region_id = $this->db->query($sql, array($d['placeregion'], $d['placeregion']))->fetch()['id'];
            if($region_id == 26)
                $region_id = 10;
            // NEED REFACT

            $slug = $this->te->translit($d['placetown']);
            $sql = 'INSERT IGNORE INTO bm_cat_places (name, lat, lng, place_id, region_id, slug)
                VALUES (?, ?, ?, ?, ?, ?)';
            $data = array($d['placetown'], $d['placelat'], $d['placelng'], $d['placeid'], $region_id, $slug);
            $this->db->query($sql, $data);
        }

        /* public function addExtraPlace($d)
          {
          if ($d['placextra']) {
          $d['placedistrict'] = $d['placextra'];
          } else {
          $d['placedistrict'] = ($d['placedistrict2'])?$d['placedistrict2']:$d['placedistrict3'];
          $d['placedistrict'] = str_replace('горсовет', 'регион', $d['placedistrict']);
          }
          $sql = 'INSERT IGNORE INTO bm_places_extra (place_addr, place_town, place_district, place_region, place_country, place_lat, place_lng, place_id)
          VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
          $data = array($d['placeaddr'], $d['placetown'], $d['placedistrict'], $d['placeregion'], $d['placecountry'], $d['placelat'], $d['placelng'], $d['placeid']);
          $this->db->query($sql, $data);
          } */

        public function getPlace($place_id)
        {
            if(!$place_id) {
                return array('is_district' => '', 'place_lat' => '', 'place_lng' => '', 'place_id' => '', 'place_town' => '', 'place_district' => '', 'place_region' => '', 'place_country' => '', 'region_id' => '', 'district_id' => '');
            } else {
                $sql = 'SELECT * FROM bm_places WHERE place_id = ?';
                return $this->db->query($sql, array($place_id))->fetch();
            }
        }

        /* public function getExtraPlace($place_id)
          {
          $sql = 'SELECT * FROM bm_places_extra WHERE place_id = ?';
          return $this->db->query($sql, array($place_id))->fetch();
          } */

        public function getCitiesNew($place)
        {
            $sql = 'SELECT * FROM bm_cat_cities WHERE id = ? AND slug = ?';
            return $this->db->query($sql, array($place['id'], $place['slug']))->fetch();
        }

        public function getPlacesNew($place)
        {
            $sql = 'SELECT * FROM bm_cat_places WHERE id = ? AND slug = ?';
            return $this->db->query($sql, array($place['id'], $place['slug']))->fetch();
        }

        public function getCatPlace($place_id)
        {
            $sql = 'SELECT * FROM bm_cat_places WHERE place_id = ?';
            $res = $this->db->query($sql, array($place_id))->fetch();
            if(!$res) {
                $sql = 'SELECT * FROM bm_cat_cities WHERE place_id = ?';
                $res = $this->db->query($sql, array($place_id))->fetch();
            }
            return $res;
        }

        public function getPlaceByTown($town)
        {
            $sql = 'SELECT * FROM bm_cat_places WHERE name = ?';
            $res = $this->db->query($sql, array($town))->fetch();
            if(!$res) {
                $sql = 'SELECT * FROM bm_cat_cities WHERE name = ?';
                $res = $this->db->query($sql, array($town))->fetch();
            }
            return $res;
        }

        public function getCatCities()
        {
            $sql = 'SELECT * FROM bm_cat_cities WHERE parent_id IS NULL ORDER BY name';
            return $this->db->query($sql)->fetchAll();
        }

        public function checkPlace($place_id)
        {
            $sql = 'SELECT * FROM bm_cat_places WHERE place_id = ?';
            $res = $this->db->query($sql, array($place_id))->fetch()['id'];
            if($res)
                return true;
            $sql = 'SELECT * FROM bm_cat_cities WHERE place_id = ?';
            $res = $this->db->query($sql, array($place_id))->fetch()['id'];
            if($res)
                return true;

            return false;
        }

        public function getCatDistricts($parent_id = false)
        {
            $sql = 'SELECT * FROM bm_cat_cities WHERE parent_id IS NOT NULL';
            if($parent_id)
                $sql .= ' AND parent_id = ' . $parent_id;
            $result = $this->db->query($sql);
            $count = $result->numRows();
            $result = $result->fetchAll();
            $ret = false;
            if($count) {
                $ret = array();
                foreach ($result as $r) {
                    $indx = 'sub' . $r['parent_id'];
                    if(!isset($ret[$indx]))
                        $ret[$indx] = array();
                    array_push($ret[$indx], array('name' => $r['name'], 'id' => $r['id'], 'slug' => $r['slug']));
                }
            }
            return $ret;
        }

        public function getDistricts($place_id)
        {
            $sql = 'SELECT oc_code FROM bm_places WHERE place_id = ?';
            $oc_code = $this->db->query($sql, array($place_id))->fetch()['oc_code'];
            if($oc_code) {
                $sql = 'SELECT * FROM bm_places WHERE oc_code = ? AND is_district > 0';
                $districts = $this->db->query($sql, array($oc_code))->fetchAll();
                if($districts)
                    return $districts;
            }
            return false;
        }

        public function getCities()
        {
            $sql = 'SELECT * FROM bm_places WHERE oc_code > 0 AND is_district = 0';
            return $this->db->query($sql)->fetchAll();
        }

        public function getCitiesList()
        {
            $sql = 'SELECT * FROM bm_places WHERE oc_code > 0 AND is_district = 0';
            return $this->db->query($sql)->fetchAll();
        }

        public function getPlaceById($id)
        {
            $sql = 'SELECT * FROM bm_places WHERE id = ?';
            return $this->db->query($sql, array($id))->fetch();
        }

        public function getPlacesByCode($code)
        {
            $sql = 'SELECT * FROM bm_places WHERE oc_code = ?';
            return $this->db->query($sql, array($code))->fetchAll();
        }

        public function addToMailQueue($subject, $receiver, $data, $user_id)
        {
            $sql = 'INSERT IGNORE INTO bm_mail_queue (mail_to, mail_title, mail_data, mail_type, mail_template, mail_lang, user_id)
                VALUES (?, ?, ?, ?, ?, ?, ?)';
            $data = serialize($data);
            $this->db->query($sql, array($receiver, $subject, $data, $this->type, $this->template, $this->lang, $user_id));
        }

    }
    