<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/common/Module.php
     *
     * Common module autoloader
     *
     */

    namespace Budmisto\Modules\Common;

    class Module
    {

        public function registerAutoloaders()
        {
            $loader = new \Phalcon\Loader();
            $loader->registerNamespaces(array(
                'Budmisto\Modules\Common\Controllers' => __DIR__ . '/controllers/',
                'Budmisto\Modules\Common\Models' => __DIR__ . '/models/'
            ));
            $loader->register();
        }

        public function registerServices($di)
        {
            $di->set('view', function()
            {
                $view = new \Phalcon\Mvc\View();
                $view->setViewsDir(__DIR__ . '/views/');
                return $view;
            });
        }

    }
    