<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/common/controllers/IndexController.php
     *
     * Common controller
     *
     */

    namespace Budmisto\Modules\Common\Controllers;

    use Budmisto\Modules\Common\Models\Common as BmCommon;
    use Budmisto\Modules\Front\Models\Catalog as BCatalog;

    class IndexController extends ControllerBase
    {

        public $catalog;

        public function initialize()
        {
            parent::initialize();
            $r = $this->request->getHTTPReferer();
            if(strpos($r, $this->config->application->site_url) === false)
                $this->logError('(Common initialize) Wrong referer');
            $this->view->disable();
        }

        public function indexAction()
        {
            $this->logError('(Common index) Blank request');
        }

        public function tokenAction()
        {
            if($this->request->isPost() && $this->request->isAjax()) {
                $csrf_name = $this->security->getTokenKey();
                $csrf_value = $this->security->getToken();
                echo json_encode((object) array(
                            'csrf_name' => $csrf_name,
                            'csrf_value' => $csrf_value
                ));
            } else
                $this->logError('(Common) Error token generate');
        }

        public function avatarAction()
        {
            if(!$this->session->get('usr_level')) {
                $this->logError('(Common avatar) Unauthorized access');
            }

            if($this->request->isPost() && $this->request->isAjax()) {
                if($this->request->hasFiles() == true) {
                    $x1 = $this->request->getPost('x1', 'int', false);
                    $x2 = $this->request->getPost('x2', 'int', false);
                    $y1 = $this->request->getPost('y1', 'int', false);
                    $y2 = $this->request->getPost('y2', 'int', false);
                    $clientWidth = $this->request->getPost('client_width', 'int', false);
                    $clientHeight = $this->request->getPost('client_height', 'int', false);

                    $file = $this->request->getUploadedFiles()[0];
                    if($file->getError()) {
                        $this->logError('(Common) Error load avatar', $file->getError());
                    }
                    try
                    {
                        $image = new \Phalcon\Image\Adapter\GD($file->getTempName());
                        $image->background('EFECE9');
                        $realWidth = $image->getWidth();
                        $realHeight = $image->getHeight();

                        if($realWidth > $this->config->images->max_width || $realHeight > $this->config->images->max_height) {
                            echo 1;
                            die;
                        }
                        if($realWidth < 50 || $realHeight < 50) {
                            echo 1;
                            die;
                        }

                        $user_id = $this->session->get('usr_id');
                        $realX1 = (int) $realWidth * $x1 / $clientWidth;
                        $realX2 = (int) $realWidth * $x2 / $clientWidth;
                        $width = $realX2 - $realX1;
                        $realY1 = (int) $realHeight * $y1 / $clientHeight;
                        $realY2 = (int) $realHeight * $y2 / $clientHeight;
                        $height = $realY2 - $realY1;
                        $image->crop($width, $height, $realX1, $realY1);
                        $image->resize(150, 150);
                        $ava_dir = $this->config->images->ava_dir;
                        $ava_name = $this->generator->genRandom($user_id);
                        $ava_name .= '.jpg';
                        //$image->save($ava_dir . 'profile/' . $ava_name, 99);
                        //$image->resize(50, 50);
                        $image->save($ava_dir . $ava_name, 99);

                        $c = new BmCommon();
                        $c->setAvatar($user_id, $ava_name);
                        $this->session->set('avatar', $ava_name);

                        echo $ava_name;
                    } catch (\Phalcon\Exception $e)
                    {
                        $this->logError('(Common avatar) Error avatar creating', $e->getMessage());
                    }
                }
            } else
                echo 0;
        }

        public function uploadAction()
        {

            if(!$this->session->get('usr_level')) {
                $this->logError('(Common upload) Unauthorized access');
            }

            if(!$this->request->isPost() && !$this->request->isAjax()) {
                $this->logError('(Common upload) Wrong Request');
            }

            if(!$this->security->checkToken(null, null, false)) {
                $this->logError('(Common upload) Token error');
            }

            if($this->request->hasFiles() != true) {
                $this->logError('(Common upload) Empty file');
            }

            $file = $this->request->getUploadedFiles()[0];
            if($file->getError()) {
                $this->logError('(Common upload) Error load file');
            }

            $user_id = $this->session->get('usr_id');
            $tempCode = $this->generator->genRandom($user_id);
            $configPath = $this->config->files->tempDir;
            $path = $configPath . $user_id . '-' . $tempCode . '/';
            if(!file_exists($path))
                mkdir($path);

            $allow_types = array('image/jpeg', 'image/png', 'image/gif');
            $file_type = $file->getType();
            if(!in_array($file_type, $allow_types)) {
                $this->logError('(Common upload) Wrong file type');
            }

            $path_parts = pathinfo($file->getName());
            $flName = $this->textengine->translit($path_parts['filename']);
            $flExt = $path_parts['extension'];
            $newFileName = $flName . '.' . $flExt;

            $file->moveTo($path . $newFileName);

            $img = new \Phalcon\Image\Adapter\Imagick($path . $newFileName);

            $realWidth = $img->getWidth();
            $realHeight = $img->getHeight();

            if($realWidth > $realHeight)
                $img->resize(640, 480);
            else
                $img->resize(480, 640);


            $img->save($path . $newFileName, 99);


            try
            {

                //$image = new \Phalcon\Image\Adapter\GD($path . $newFileName);
                $image = new \Phalcon\Image\Adapter\Imagick($path . $newFileName);
                $image->background('EFECE9');
                $realWidth = $image->getWidth();
                $realHeight = $image->getHeight();
                $thumb_size = $this->config->images->thumb_size;
                if($realWidth > ($thumb_size * 2) || $realHeight > ($thumb_size * 2)) {
                    if($realWidth < $realHeight)
                        $ratio = ($thumb_size + 30) / $realWidth;
                    else
                        $ratio = ($thumb_size + 30) / $realHeight;
                } else
                    $ratio = 1;
                $newWidth = ceil($ratio * $realWidth);
                $newHeight = ceil($ratio * $realHeight);
                $image->resize($newWidth, $newHeight);
                $cropX = floor(($newWidth - $thumb_size) / 2);
                $cropY = floor(($newHeight - $thumb_size) / 2);
                $image->crop($thumb_size, $thumb_size, $cropX, $cropY);
                $newFileName = 'thumb@' . $flName . '.' . $flExt;
                $image->save($path . $newFileName, 99);
            } catch (\Phalcon\Exception $e)
            {
                $this->logError('(Common upload) Error create thumbnail', $e->getMessage());
            }
        }

        public function deleteAction()
        {
            if(!$this->session->get('usr_level')) {
                $this->logError('(Common delete) Unauthorized access');
            }
            if(!$this->request->isPost() && !$this->request->isAjax()) {
                $this->logError('(Common upload) Wrong Request');
            }

            $file = $this->request->getPost('file', array('trim', 'string'), false);
            if(!$file) {
                $this->logError('(Common delete) Empty filename');
            }
            $path_parts = pathinfo($file);
            $flName = $this->textengine->translit($path_parts['filename']);
            $flExt = $path_parts['extension'];
            $fileName = $flName . '.' . $flExt;
            $thumbName = 'thumb@' . $flName . '.' . $flExt;

            $user_id = $this->session->get('usr_id');
            $tempCode = $this->generator->genRandom($user_id);
            $configPath = $this->config->files->tempDir;
            $path = $configPath . $user_id . '-' . $tempCode . '/';

            if(file_exists($path . $fileName)) {
                unlink($path . $fileName);
            }
            if(file_exists($path . $thumbName)) {
                unlink($path . $thumbName);
            }

            if(!is_dir($path))
                return false;

            $dir = scandir($path);
            if(!$dir)
                return false;

            $files = array_diff($dir, array('.', '..'));
            if(!$files && is_dir($path))
                rmdir($path);
        }

        public function updateAction()
        {
            //////////////////////////////////
        }

        public function chkMailAction()
        {
            $email = $this->request->getPost('mail', array('trim', 'string'), false);
            if(!$email)
                die;

            $tmp = explode('@', $email);
            $domain = end($tmp);
            if(strpos($domain, '.') === false)
                die;
            $res = dns_get_record($domain);
            if($res)
                echo 'AllOk';
            die;
        }

        public function getUserAction()
        {
            $user_id = $this->request->getPost('id', 'int', false);
            if(!$user_id)
                die;

            $c = new BmCommon();
            $info = $c->getUser($user_id);
            switch ($info['user_role_id'])
            {
                case 4: $role_name = 'Строитель';
                    break;
                case 5: $role_name = 'Бригада строителей';
                    break;
                case 6: $role_name = 'Строительная фирма';
                    break;
                default: $role_name = 'Не указано';
                    break;
            }
            $desc = $info['description'] ? $info['description'] : 'Нет описания';
            $web = $info['website'] ? $info['website'] : 'Нет сайта';
            $phones = array();
            if($info['user_phone'])
                $phones[] = $info['user_phone'];
            if($info['phone1'])
                $phones[] = $info['phone1'];
            if($info['phone2'])
                $phones[] = $info['phone2'];
            $phones = implode(', ', $phones);
            echo json_encode((object) array(
                        'role' => $role_name,
                        'worker' => $info['id'],
                        'name' => $info['user_name'],
                        'desc' => $desc,
                        'phones' => $phones,
                        'web' => $web
            ));
            // NEED REFACT
        }

        public function fixLocAction()
        {
            
        }

        private function logError($message, $phalcon_message = '', $todie = true)
        {
            $this->response->setStatusCode(400, 'Bad Request');
            $this->response->send();
            $user_id = $this->session->get('usr_id');
            $ip = $_SERVER['REMOTE_ADDR'];
            $uri = $_SERVER['REQUEST_URI'];
            if($phalcon_message)
                $phalcon_message = ' (Phalcon) ' . $phalcon_message;
            $this->logger->log($message . ': [id]' . $user_id . ' ' . $ip . $phalcon_message . ' | ' . $uri, \Phalcon\Logger::ERROR);
            if($todie)
                die;
        }

    }
    