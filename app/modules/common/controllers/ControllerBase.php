<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/modules/common/controllers/ControllerBase.php
     *
     * Base controller
     *
     */

    namespace Budmisto\Modules\Common\Controllers;

    use Phalcon\Mvc\Dispatcher;

    class ControllerBase extends \Phalcon\Mvc\Controller
    {

        public function onConstruct()
        {
            
        }

        public function beforeExecuteRoute(Dispatcher $dispatcher)
        {
            $path = $dispatcher->getActionName();

            if($path == 'fixloc') {
                $this->response->redirect($this->config->application->site_url . '/catalog');
                $this->response->send();
                die;
            }
        }

        public function initialize()
        {
            
        }

        public function getInputFilter()
        {
            $filter = new \Phalcon\Filter();
            $filter->add('user_input', function($value)
            {
                $value = html_entity_decode($value, ENT_QUOTES, 'UTF-8');
                return htmlentities($value, ENT_QUOTES | ENT_IGNORE, 'UTF-8');
            });
            return $filter;
        }

    }
    