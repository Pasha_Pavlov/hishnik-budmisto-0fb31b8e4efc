<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/helpers/fileswork.php
     *
     * Work with files helper
     *
     */

    namespace Budmisto\Helpers;

    use Phalcon\DI;

    class FilesWork
    {

        private $config;
        private $generator;
        private $te;
        private $db;

        public function __construct()
        {
            $this->config = DI::getDefault()->getShared('config');
            $this->generator = DI::getDefault()->getShared('generator');
            $this->te = DI::getDefault()->getShared('textengine');
            $this->db = DI::getDefault()->getShared('db');
        }

        public function clearUserTempFolder($user_id)
        {
            $tempCode = $this->generator->genRandom($user_id);
            $configPath = $this->config->files->tempDir;
            $path = $configPath . $user_id . '-' . $tempCode . '/';
            $this->deleteDir($path);
        }

        public function moveToUserFolder($file, $user_id)
        {
            $configPath = $this->config->files->users;
            $path = $configPath . $user_id . '/';
            if(!file_exists($path))
                mkdir($path);

            $path_parts = pathinfo($file->getName());
            $flName = $this->te->translit($path_parts['filename']);
            $tmp = $this->generator->getBigNumber();
            $flName = $flName . '-' . $tmp;
            $flExt = $path_parts['extension'];

            $newFileName = $flName . '.' . $flExt;
            $file->moveTo($path . $newFileName);
            return $newFileName;
            // NEED ERROR HANDLER
            // NEED MIME TYPES CHECK
        }

        public function updateUserGallery($user_id)
        {
            $tempCode = $this->generator->genRandom($user_id);
            $configPath = $this->config->files->tempDir;
            $path = $configPath . $user_id . '-' . $tempCode . '/';
            $not_empty = $this->deleteEmptyDir($path);
            if(!$not_empty)
                return false;

            $configPath = $this->config->files->users;
            $path2 = $configPath . $user_id . '/';
            if(!file_exists($path2))
                mkdir($path2);
            $files = array_diff(scandir($path), array('.', '..'));
            foreach ($files as $file) {
                rename($path . $file, $path2 . $file);
                if(strpos($file, 'thumb@') === false)
                    $this->storeDB($user_id, $file);
            }
            $this->deleteDir($path);
        }

        public function deleteUserFileByName($filename, $user_id)
        {
            $configPath = $this->config->files->users;
            $path = $configPath . $user_id . '/';
            if(file_exists($path . $filename)) {
                unlink($path . $filename);
            }
            if(file_exists($path . 'thumb@' . $filename)) {
                unlink($path . 'thumb@' . $filename);
            }
            $this->deleteEmptyDir($path);
            // NEED REFACT
        }

        private function deleteDir($path)
        {
            if(file_exists($path)) {
                array_map('unlink', glob($path . '*.*'));
                rmdir($path);
            }
        }

        private function deleteEmptyDir($path)
        {
            if(!file_exists($path))
                return false;
            $files = array_diff(scandir($path), array('.', '..'));
            if(!$files) {
                rmdir($path);
                return false;
            } else
                return true;
        }

        private function storeDB($user_id, $filename)
        {
            $sql = 'INSERT INTO bm_files (user_id, ftype, fname) VALUES (?, ?, ?)';
            $this->db->query($sql, array($user_id, 'image', $filename));
        }

        // NEED REFACT!
    }

    return new \Budmisto\Helpers\FilesWork();
    