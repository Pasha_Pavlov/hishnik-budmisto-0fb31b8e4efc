<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/helpers/textengine.php
     *
     * Code text helper
     *
     */

    namespace Budmisto\Helpers;

    use Phalcon\DI;

    class TextEngine
    {
        /*
          public function convertBB2Html($bbtext)
          {
          $preg = array(
          "~\[color=(.+?)\](.+?)\[\/color\]~is"=>"<span style='color:\\1'>\\2</span>",
          '~\[list\](.*?)\[\/list\]~si' => '<ul>$1</ul>',
          '~\[list=1\](.*?)\[\/list\]~si' => '<ol>$1</ol>',
          '~\[\*\](.*?)\[\/\*\]~si' => '<li>$1</li>',
          '~\[b\](.*?)\[\/b\]~si' => '<strong>$1</strong>',
          '~\[i\](.*?)\[\/i\]~si' => '<em>$1</em>',
          '~\[u\](.*?)\[\/u\]~si' => '<u>$1</u>',
          '~\[center\](.*?)\[\/center\]~si' => '<p style=text-align:center>$1</p>',
          '~\[left\](.*?)\[\/left\]~si' => '<p style=text-align:left>$1</p>',
          '~\[right\](.*?)\[\/right\]~si' => '<p style=text-align:right>$1</p>',
          '~\n~' => '<br />'
          );
          return preg_replace(array_keys($preg), array_values($preg), $bbtext);
          }
         */

        public function convertHtml2BB($html)
        {
            $preg = array(
                '~<ul>(.*?)<\/ul>~si' => '[list]$1[/list]',
                '~<ol>(.*?)<\/ol>~si' => '[list=1]$1[/list]',
                '~<li>(.*?)<\/li>~si' => '[*]$1[/*]',
                '~<strong>(.*?)<\/strong>~si' => '[b]$1[/b]',//
                '~<em>(.*?)</em>~si' => '[i]$1[/i]',//
                '~<u>(.*?)</u>~si' => '[u]$1[/u]',//
                '~<p style=text-align:center>(.*?)<\/p>~si' => '[center]$1[/center]',
                '~<p style=text-align:left>(.*?)<\/p>~si' => '[left]$1[/left]',
                '~<p style=text-align:right>(.*?)<\/p>~si' => '[right]$1[/right]',
                '~<br />~' => "\n",
                '~<b>(.*?)<\/b>~si' => '[b]$1[/b]',
                '~<i>(.*?)<\/i>~si' => '[i]$1[/i]',
                '~<u>(.*?)<\/u>~si' => '[u]$1[/u]',
                '~<code>(.*?)<\/code>~si' => '[code]$1[/code]',
                '~<table width = [\'|"]95%[\'|"]><tr><td>Цитата</td></tr><tr><td class=[\'|"]quote[\'|"]>(.+?)</td></tr></table>~si' => '[quote]$1[/quote]',
                '~<span style=[\'|"]font-size:(.+?)\%[\'|"]>(.+?)<\/span>~si' => '[size=$1]$2[/size]',
                "~<span style=[\'|\"]color:(.+?)[\'|\"]>(.+?)<\/span>~si" => "[color=$1]$2[/color]"
                
            );
            return preg_replace(array_keys($preg), array_values($preg), $html);
        }

        function convertBB2Html($text_post)
        {
            $str_search = array(
                "#\\\n#is",
                "#\[b\](.+?)\[\/b\]#is",
                "#\[i\](.+?)\[\/i\]#is",
                "#\[u\](.+?)\[\/u\]#is",
                "#\[code\](.+?)\[\/code\]#is",
                "#\[quote\](.+?)\[\/quote\]#is",
                "#\[url=(.+?)\](.+?)\[\/url\]#is",
                "#\[url\](.+?)\[\/url\]#is",
                "#\[img\](.+?)\[\/img\]#is",
                "#\[size=(.+?)\](.+?)\[\/size\]#is",
                "#\[color=(.+?)\](.+?)\[\/color\]#is",
                "#\[list\](.+?)\[\/list\]#is",
                "#\[listn](.+?)\[\/listn\]#is",
                "#\[\*\](.+?)\[\/\*\]#"
            );
            $str_replace = array(
                "<br />",
                "<b>\\1</b>",
                "<i>\\1</i>",
                "<span style='text-decoration:underline'>\\1</span>",
                "<code class='code'>\\1</code>",
                "<table width = '95%'><tr><td>Цитата</td></tr><tr><td class='quote'>\\1</td></tr></table>",
                "<a href='\\1'>\\2</a>",
                "<a href='\\1'>\\1</a>",
                "<img src='\\1' alt = 'Изображение' />",
                "<span style='font-size:\\1%'>\\2</span>",
                "<span style='color:\\1'>\\2</span>",
                "<ul>\\1</ul>",
                "<ol>\\1</ol>",
                "<li>\\1</li>"
            );
            return preg_replace($str_search, $str_replace, $text_post);
        }

        public function translit($string)
        {
            $converter = array(
                'а' => 'a', 'б' => 'b', 'в' => 'v',
                'г' => 'g', 'д' => 'd', 'е' => 'e',
                'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
                'и' => 'i', 'й' => 'y', 'к' => 'k',
                'л' => 'l', 'м' => 'm', 'н' => 'n',
                'о' => 'o', 'п' => 'p', 'р' => 'r',
                'с' => 's', 'т' => 't', 'у' => 'u',
                'ф' => 'f', 'х' => 'h', 'ц' => 'c',
                'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
                'ь' => '', 'ы' => 'y', 'ъ' => '',
                'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
                'є' => 'e', 'ї' => 'i', '\'' => '',
                'і' => 'i',
                'А' => 'A', 'Б' => 'B', 'В' => 'V',
                'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
                'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
                'И' => 'I', 'Й' => 'Y', 'К' => 'K',
                'Л' => 'L', 'М' => 'M', 'Н' => 'N',
                'О' => 'O', 'П' => 'P', 'Р' => 'R',
                'С' => 'S', 'Т' => 'T', 'У' => 'U',
                'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
                'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
                'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
                'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
                'Є' => 'E', 'Ї' => 'I', 'І' => 'I'
            );

            $string = strtr($string, $converter);
            $string = strtolower($string);
            $string = preg_replace('~[^-a-z0-9_]+~u', '-', $string);
            $string = trim($string, "-");
            return $string;
        }

        public function getBrief($text, $maxchar = 150)
        {
            if(!$text)
                return '';
            $text = str_replace("\n", ' ', $text); // ?
            $text = str_replace("\r", ' ', $text); // ?
            $text = str_replace("\t", ' ', $text); // ?
            $text = str_replace('&nbsp;', ' ', $text);
            $text = preg_replace('!\s+!', ' ', $text);

            $text = strip_tags($text);
            if(mb_strlen($text, 'UTF-8') > $maxchar) {
                $text = mb_substr($text, 0, $maxchar, 'UTF-8');
                $words = mb_split(' ', $text);
                $tmp = sizeof($words);
                $text = join(' ', array_slice($words, 0, $tmp - 2));
                return $text;
            } else
                return $text;
        }

    }

    return new \Budmisto\Helpers\TextEngine();
    