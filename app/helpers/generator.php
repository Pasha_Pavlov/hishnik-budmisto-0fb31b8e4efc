<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * app/helpers/generator.php
     *
     * Code generator helper
     *
     */

    namespace Budmisto\Helpers;

    use Phalcon\DI;

    class Generator
    {

        private $code;
        private $salt;

        // class constructor?!

        public function genRandom($in)
        {
            $code = hash('md5', $in);
            $tmp1 = substr(preg_replace('/[W\.\/\$]/', null, crypt($in, $code)), 1, 12);
            $code = hash('md5', $tmp1);
            $tmp2 = substr(preg_replace('/[W\.\/\$]/', null, crypt($code, $in)), 1, 12);
            return $tmp1 . $tmp2;
        }

        public function genRandomString($length)
        {
            $characters = 'X123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ987654321';
            $charactersLength = 71;
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        public function getBigNumber()
        {
            $tmp = explode(' ', microtime());
            $return = $tmp[1] . ($tmp[0] * 1000000);
            return $return;
        }

        public function genCryptCode($string)
        {
            $config = DI::getDefault()->getShared('config');
            $this->code = $config->application->crypt_code;
            $this->salt = $config->application->crypt_salt;
            return $this->_encode($string);
        }

        public function decodeCryptCode($string)
        {
            $config = DI::getDefault()->getShared('config');
            $this->code = $config->application->crypt_code;
            $this->salt = $config->application->crypt_salt;
            return $this->_decode($string);
        }

        /* public function genEmailCode($email, $need_time = true)
          {
          $config = DI::getDefault()->getShared('config');
          $this->code = $config->application->crypt_code;
          $this->salt = $config->application->crypt_salt;

          if ($need_time) {
          $rand = rand(1,60) + time();
          $data = serialize(array('email'=>$email, 'time'=>$rand));
          } else {
          $data = serialize(array('email'=>$email));
          }
          return $this->_encode($data);
          } */

        public function genEmailCode($email)
        {
            $config = DI::getDefault()->getShared('config');
            $this->code = $config->application->crypt_code;
            $this->salt = $config->application->crypt_salt;

            return $this->_encode($email);
        }

        public function decodeEmailCode($code)
        {
            $config = DI::getDefault()->getShared('config');
            $this->code = $config->application->crypt_code;
            $this->salt = $config->application->crypt_salt;

            $tmp = $this->_decode($code);
            $result = @unserialize($tmp); // we not need notice
            return (!empty($result)) ? $result : false;
        }

        private function _encode($string)
        {
            $tmp = $this->_doEncode($string);
            return $this->_raw2hex($tmp);
        }

        private function _decode($string)
        {
            $tmp = $this->_hex2raw($string);
            return $this->_doEncode($tmp);
        }

        private function _doEncode($string)
        {
            $code = $this->code;
            $salt = $this->salt;
            $strLen = strlen($string);
            $seq = $code;
            $gamma = '';
            while (strlen($gamma) < $strLen)
            {
                $seq = pack('H*', sha1($gamma . $seq . $salt));
                $gamma .= substr($seq, 0, 8);
            }
            return $string ^ $gamma;
        }

        private function _raw2hex($raw)
        {
            $m = unpack('H*', $raw);
            return $m[1];
        }

        private function _hex2raw($hex)
        {
            return pack('H*', $hex);
        }

    }

    return new \Budmisto\Helpers\Generator();
    