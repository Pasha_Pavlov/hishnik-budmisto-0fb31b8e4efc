<?php

/**
 * BudMisto
 * 
 * @package      BudMisto
 * @author       Budmisto
 * @copyright    Budmisto
 * @license      http://opensource.org/licenses/MIT
 * @link         http://budmisto.com.ua
 * @since        Version 1.0.0
 *
 */


/**
 * html/index.php
 *
 * Application starting point
 *
 */


//print_r($_GET); die;

set_time_limit(15);

ini_set('display_errors',1);
error_reporting(E_ERROR);

$req = $_SERVER['REQUEST_URI'];
$res = strpos($req, 'index.php');

if ($res!==false) {
    header('Location: https://budmisto.com.ua/');
    die;
}
$res = strpos($req, 'soft/arch/');
if ($res!==false) {
    header('Location: https://budmisto.com.ua/');
    die;
}



if (!extension_loaded('phalcon')) {
    die('Install Phalcon framework');
}

//define('APP_DIR', realpath('..') . '/');
define('APP_DIR', '../');

//echo APP_DIR . 'app/library'; die;

try {

    $application = new \Phalcon\Mvc\Application();

    $di = new \Phalcon\DI\FactoryDefault();
    require APP_DIR . 'config/services.php';
    
    $application->setDI($di);

    $modules = require APP_DIR . 'config/modules.php';
  
    $application->registerModules($modules);

    header('Content-Type: text/html; charset=utf-8');
   // echo $application->handle()->getContent();
    $content = $application->handle()->getContent();
    $content = str_replace("budmisto.js", "budmisto.js?".time(),$content);
    $content = str_replace("login.js", "login.js?".time(),$content);
    $content = str_replace("jquery.maskedinput.min.js", "jquery.maskedinput.min.js?".time(),$content);
    $content = str_replace('</head>', "<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TJJXMWZ');</script>
<!-- End Google Tag Manager -->
</head>", $content);
    
    $content = str_replace('<body>',"<body><!-- Google Tag Manager (noscript) -->
<noscript><iframe src='https://www.googletagmanager.com/ns.html?id=GTM-TJJXMWZ'
height='0' width='0' style='display:none;visibility:hidden'></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->",$content);
    echo $content;	   

} catch (\Phalcon\Exception $e) {
    echo get_class($e), ': ', $e->getMessage(), '<br />';
    echo 'File=', $e->getFile(), '<br />';
    echo 'Line=', $e->getLine(), '<br />';
    echo $e->getTraceAsString();
    //$application->getDI()->get('logger')->log($e->getMessage(), \Phalcon\Logger::ERROR);

}
