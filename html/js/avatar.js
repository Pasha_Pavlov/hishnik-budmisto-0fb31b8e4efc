var jcrop_api = false;

$('#ava-form').on('submit', function(e) {
    e.preventDefault();
    var x1 = $('#x1').val();
    var y1 = $('#y1').val();
    var x2 = $('#x2').val();
    var y2 = $('#y2').val();
    var w = x2-x1; var h = y2-y1;
    if ((w<50 || h<50)) { 
        if (w>0) alertBM.alert('Фото или выделенная область<br />меньше минимума');
        return false; 
    }
    $('#rw').val($('#ava-area').width());
    $('#rh').val($('#ava-area').height());

    var ava_form = new FormData(this);

    $.ajax({
        url: '/avatar',
        type: 'post',
        data: ava_form,
        contentType: false,
        cache: false,
        processData: false,
        success: function (returndata) {
            console.log(returndata);
            if(returndata) {
                var time = new Date().getTime();
                $('#avatar').attr('src', url + '/data/avatars/'+returndata+'?'+time);
                $('#avafilename').val(returndata);
                $('#chmess').html('<strong class="red">Фото профиля обновлено!</strong>');
                $('#cancel').click();
            } else
                alertBM.alert('Ошибка сервера');
        },
        error: function(e){
            alertBM.alert('SYSTEM FAILURE');
            console.log(e);
            return false;
        }
    });
});

$('#file').on('change', function(){
    var file = this.files[0];
    var imagefile = file.type;
    var match= ['image/jpeg','image/gif','image/png','image/jpg'];
    if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3]))) {
        alertBM.alert('Недопустимый тип изображения!');
        return false;
    } else {
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
    }
});

function imageIsLoaded(e) {
    $('button#save').removeClass('btn btn-primary disabled').addClass('btn btn-primary').fadeIn();
    $('button#cancel').removeClass('btn btn-default disabled').addClass('btn btn-default').fadeIn();
    $('#ava').attr('src', e.target.result);

    if (jcrop_api) {
        jcrop_api.release();
        jcrop_api.destroy();
        jcrop_api = false;
    }
    $('#ava').Jcrop({
        setSelect: [1,1,40,40],
        allowMove: true,
        onChange: setCoords,
        onSelect: setCoords,
        onRelease: clearCoords,
        aspectRatio:1,
        keySupport: false
        }, function(){
        jcrop_api = this;
    });
    $('#file').css('display', 'none');
    var tmpImage = new Image();
    tmpImage.src = e.target.result;
    tmpImage.onload = function() {
        $('#ava').css('height', '0px');
        $('#ava').css('width', '0px');
        $('#rw').val($('#ava-area').width());
        $('#rh').val($('#ava-area').height());
    };
    $('#ava').css('display', 'block');
};

function setCoords(c) {
    $('#x1').val(c.x);
    $('#y1').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
};

function clearCoords() {
    $('#x1').val('');
    $('#y1').val('');
    $('#x2').val('');
    $('#y2').val('');
};

$('#cancel').on('click', function (e) {
    e.preventDefault();
    jcrop_api.destroy();
    $('#ava').attr('src', '');
    $('#ava').css('display', 'none');
    $('#ava').css('height', '');
    $('#ava').css('width', '');
    $('button#save').removeClass('btn btn-primary').addClass('btn btn-primary disabled').hide();
    $('button#cancel').removeClass('btn btn-default').addClass('btn btn-default disabled').hide();
    $('#file').val('');
    $('#file').css('display', 'block');
    clearCoords();
});
