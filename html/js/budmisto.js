var pointY;
var lang = 'ru';

$(document).on('mousemove', function (e) {
    document.pointY = e.pageY;
});

function funButton() {
    var w = $(window).width();
    if (w < 940) {
        $('#btn-help').html('+38 (096) 007-71-37');
    } else if (w < 1175) {
        $('#btn-help').html('<span class="glyphicon glyphicon-question-sign"></span>').css("width", "145");
        $("#btn-leftovers").html("Остатки");
    } else {
        $('#btn-help').html('+38 (096) 007-71-37');
    }
}

$(document).ready(function () {
    if (typeof (crntPage) !== 'undefined') {
        if (crntPage == 'main' || crntPage == 'order' || crntPage == 'signup' || crntPage == 'profile' || crntPage == 'admin' || crntPage == 'leftover' || crntPage == 'recovery' || crntPage == 'user' || crntPage == 'shops') {
            try {
                $('#phone').mask("(000) 000-00-00", {placeholder: "Номер телефона"});
            } catch (err) {
            }
        }

        if (crntPage == 'profile') {
            $('.extraphone').mask('(000) 000-00-00');
        }

        if (crntPage == 'profile' || crntPage == 'user') {
            if (has_gal) {
                $('.owl-carousel').owlCarousel({
                    loop: false,
                    nav: true,
                    dots: false,
                    navText: ['&laquo;', '&raquo;'],
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 3,
                            nav: true
                        },
                        600: {
                            items: 4,
                            nav: true
                        },
                        768: {
                            items: 5,
                            nav: true
                        },
                        1080: {
                            items: 6,
                            nav: true
                        }
                    }
                });
            }
        }

        if (crntPage == 'main' || crntPage == 'profile' || crntPage == 'catalog' || crntPage == 'myorder' || crntPage == 'catusers' || crntPage == 'admin' || crntPage == 'shops') {
            try {
                $('#sid').remoteChained({
                    parents: '#tid',
                    url: '/catalog/subdata?ref=' + crntPage,
                    loading: '...'
                });
                $('#sid_1').remoteChained({
                    parents: '#tid_1',
                    url: '/catalog/subdata',
                    loading: '...'
                });
            } catch (err) {
            }
        }

        if (crntPage == 'activity' || crntPage == 'user') {
            $('#btn-sel').each(function () {
                $(this).on('click', function (e) {
                    e.preventDefault();
                    var form = $(this).closest('form');
                    var btn = $(this);
                    var userId = $(this).data('uid');
                    btn.addClass('off');
                    form.addClass('off-form');
                    $.ajax({
                        type: 'post',
                        url: '/token',
                        dataType: 'json',
                        success: function (result) {
                            form.find('#csrf-token').attr('name', result.csrf_name).val(result.csrf_value);
                            $.ajax({
                                type: 'post',
                                url: '/catalog/choice',
                                data: form.serialize(),
                                success: function (res) {

                                    if (res.indexOf('removed') != -1) {
                                        btn.removeClass('btn btn-warning').addClass('btn btn-primary');
                                        btn.html(langSelect);
                                        $('#lbl' + userId).html('');
                                    } else if (res.indexOf('blocked') != -1) {
                                        $('#blocked_choice').modal('show');
                                    } else if (res.indexOf('selected') != -1) {
                                        if (crntPage == 'user') {
                                            window.location.reload();
                                        }
                                        $("#back-call").show();
                                        btn.removeClass('btn btn-primary').addClass('btn btn-warning');
                                        btn.html(langUnSelect);
                                        $('#lbl' + userId).html('<span class="glyphicon glyphicon-ok"></span> ' + langSelected);
                                        $('#btn-sel').parent("form").hide();
                                        $('#phone').show();
                                        selectedW.push(userId);
                                    }
                                    btn.removeClass('off');
                                    form.removeClass('off-form');
                                },
                                error: function () {
                                    btn.removeClass('off');
                                    form.removeClass('off-form');
                                }
                            });
                        }
                    });
                });
            });
        }

        if (crntPage == 'main' || crntPage == 'catusers' || crntPage == 'myorders' || crntPage == 'leftover') {
            try {
                $('.grid').masonry({
                    itemSelector: '.grid-item',
                    transitionDuration: 0
                });
            } catch (err) {
            }
        }

        if (crntPage == 'main') {
            $('#desc').on('focus', function () {
                $(this).css('height', 100);
                $('#fun-title').css('padding-top', '70px');
            }).on('blur', function () {
                $(this).css('height', 34);
                $('#fun-title').css('padding-top', '0');
            });

            $(window).resize(function () {
                funButton();
            });

            $(".news-carousel").bootstrapNews({
                newsPerPage: 3,
                autoplay: true,
                pauseOnHover: true,
                navigation: false,
                direction: 'down',
                newsTickerInterval: 7500
            });

            $('#owl-carousel').owlCarousel({
                loop: true,
                nav: true,
                dots: false,
                items: 4,
                lazyLoad: true,
                autoplay: true,
                autoplayTimeout: 3000,
                autoplayHoverPause: true,
                autoplaySpeed: 3000,
                navText: ['&laquo;', '&raquo;'],
                margin: 15,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    768: {
                        items: 3
                    },
                    1080: {
                        items: 4
                    }
                }
            });
        }
    }

    try {
        $.scrollUp({
            scrollDistance: 200,
            scrollSpeed: 400,
            scrollImg: true
        });
    } catch (err) {
    }

});

var alert_fields = '';
function doFormSubmit(form_id, listFields, cAction) {

    var form = $('#' + form_id);

    form.addClass('off-form');
    $('.submit button').addClass('blockSubmit');
    $('.search button').addClass('blockSubmit');

    if (listFields) {

        var valid = validate(listFields, form);

        if (!valid) {

            form.removeClass('off-form');
            $('.submit button').removeClass('blockSubmit');
            $('.search button').removeClass('blockSubmit');

            if (form_id == "edit-form") {
                alert_fields = alert_fields.substring(0, alert_fields.length - 1);
                alert_fields = alert_fields.split("|");
                var add = "<hr><div style='text-align:left;'>";

                alert_fields.forEach(function (entry) {
                    if (entry == "name") {
                        add = add + " " + "- Введите Ваше имя <br>";
                    }
                    if (entry == "phone") {
                        add = add + " " + "- Введите номер телефона <br>";
                    }
                    if (entry == "desc") {
                        add = add + " " + "- Введите описание <br>";
                    }
                    if (entry == "tid") {
                        add = add + " " + "- Выберите вид работ <br>";
                    }
                    if (entry == "sid") {
                        add = add + " " + "- Уточните вид работ <br>";
                    }

                });
                add = add + "</div>";
                alertBM.alert(langFiedsEmpty + add);

            } else if (form_id == "profile-form") {
                alert_fields = alert_fields.substring(0, alert_fields.length - 1);
                alert_fields = alert_fields.split("|");

                var add = "<hr><div style='text-align:left;'>";

                alert_fields.forEach(function (entry) {
                    if (entry == "name") {
                        add = add + " " + "- Введите Ваше имя <br>";
                    }
                    if (entry == "phone") {
                        add = add + " " + "- Введите номер телефона <br>";
                    }
                    if (entry == "desc") {
                        add = add + " " + "- Введите описание <br>";
                    }
                    if (entry == "autocomplete") {
                        add = add + " " + "- Введите город <br>";
                    }
                    if (entry == "specializations") {
                        add = add + " " + "- Выберите специализацию<br>";
                    }
                });

                add = add + "</div>";

                alertBM.alert(langFiedsEmpty + add);

            } else if (form_id == "add-form") {

                alert_fields = alert_fields.substring(0, alert_fields.length - 1);
                alert_fields = alert_fields.split("|");

                var add = "<hr><div style='text-align:left;'>";

                alert_fields.forEach(function (entry) {

                    if (entry == "name") {
                        add = add + " " + "- Введите название магазина <br>";
                    }
                    if (entry == "email1") {
                        add = add + " " + "- Введите E-Mail <br>";
                    }
                    if (entry == "phone1") {
                        add = add + " " + "- Введите телефон <br>";
                    }
                    if (entry == "placeid") {
                        add = add + " " + "- Введите город <br>";
                    }
                    if (entry == "tid") {
                        add = add + " " + "- Введите специализацию <br>";
                    }
                    if (entry == "sid") {
                        add = add + " " + "- Уточните вид работ <br>";
                    }

                });

                add = add + "</div>";

                alertBM.alert(langFiedsEmpty + add);
            } else if (form_id == "order-form") {

                alert_fields = alert_fields.substring(0, alert_fields.length - 1);
                alert_fields = alert_fields.split("|");

                var add = "<hr><div style='text-align:left;'>";

                alert_fields.forEach(function (entry) {

                    if (entry == "locality")
                        form.find('#' + entry).siblings('#autocomplete').css('border', '2px solid red');
                    else
                        form.find('#' + entry).css('border', '2px solid red');

                    if (entry == "desc") {
                        add = add + " " + "- Введите описание <br>";
                    }
                    if (entry == "locality") {
                        add = add + " " + "- Введите город <br>";
                    }
                    if (entry == "phone") {
                        add = add + " " + "- Введите телефон <br>";
                    }

                });

                add = add + "</div>";

                alertBM.alert(langFiedsEmpty + add);
            } else {
                alertBM.alert(langFiedsEmpty);
            }
            alert_fields = '';
            return false;
        } else {
            if (form_id == "profile-form") {

                if ($(".wysibb-body").text() == "" && $("#confirm_alert").val() != 1) {

                    //Вывод предупреждения только 1 раз
                    //  $("#confirm_alert").val(1);    
                    alertBM.alert("Если Вы не введете описание,<br> Ваш профиль не будет отображаться в результатах поиска<br>\n\
                            <button class=\"btn btn-success btn-lg\" onclick=\"$('.wysibb-body').focus();$('.wysibb').css('border','2px solid blue');\">Перейти к описанию</button>\n\
                            <button class=\"btn btn-warning\" onclick=\"not_enter_descr();\">перейти к заказам без добавления описания</button>\n\
                        ");

                    form.removeClass('off-form');
                    $('.submit button').removeClass('blockSubmit');
                    $('.search button').removeClass('blockSubmit');

                    return false;
                } else {
                    $("#desc").val($(".wysibb-body").text());
                }
            }
        }
    }

    $.ajax({
        type: 'post',
        url: '/token',
        dataType: 'json',
        success: function (result) {

            form.find('#csrf-token').attr('name', result.csrf_name).val(result.csrf_value);
            if (cAction) {
                var res = checkAction(cAction, form);
                if (!res) {
                    form.removeClass('off-form');
                    $('.submit button').removeClass('blockSubmit');
                    $('.search button').removeClass('blockSubmit');
                    return false;
                }
            }

            if (form_id == "offer-form-admin") {
                $("#img_ajax").fadeIn();
                $.ajax({
                    type: 'post',
                    url: '/workz/orders/view/' + $("#ajax_offer_id").val(),
                    dataType: 'json',
                    data: {
                        add_filter_user: $("#do-submit-btn").attr("add_user_id")
                    },
                    success: function (result) {
                        if ($("#do-submit-btn").attr("stars") == 10) {
                            send_sms(result['user_phone'], 3, 1);     // Автоматическая отправка смс для 10 звездных при добавлении из фильтра                   
                        } else {
                            send_sms(result['user_phone'], 2, 1);     // Автоматическая отправка смс при добавлении из фильтра                   
                        }

                        alertBM.alert("Специалист успешно добавлен! ");
                        $("#img_ajax").fadeOut();

                    },
                    error: function () {
                        $("#img_ajax").fadeOut();
                        alertBM.alert("Ошибка добавления специалиста! Проверьте вводимые данные!");
                    }
                });
            } else if (form_id == "text-form") {

                var text = $("#field-text-info").parent(".wysibb-text").find(".wysibb-body").text();
                var matches = text.match(/(\d{1,5}).(\d{1,5}).(\d{1,5})/gi);

                if (matches) {
                    alertBM.alert("Похоже, что Вы ввели номер телефона в поле описания! В этом поле контактные данные запрещены!");
                    return false;
                } else {
                    form.submit();
                }

            } else if (form_id == "order-form1") {
                $.ajax({
                    type: 'post',
                    url: window.location.href,
                    dataType: 'html',
                    data: {
                        "set_referer_user": 1
                    },
                    success: function (res) {
                        console.log(res);
                        form.submit();
                    },
                    error: function () {
                        console.log("Error ajax form!");
                    }
                });
            } else if (form_id == "specnewform") {
                $.ajax({
                    type: 'post',
                    url: "/my/spec",
                    dataType: 'html',
                    data: {
                        speciality: $("#tid").val(),
                        subspeciality: $("#sid").val()
                    },
                    success: function (res) {
                        $('.submit button').removeClass('blockSubmit');
                        $('#user_specializations').html(res);
                        if (res != '') {
                            $("#specializations").val("1");
                        }
                    },
                    error: function () {
                        console.log("Error ajax form!");
                    }
                });
            } else {
                form.submit();
            }

        },
        error: function (error) {
            form.removeClass('off-form');
            $('.submit button').removeClass('blockSubmit');
            $('.search button').removeClass('blockSubmit');
            console.log(error);
        }
    });
    return false;
}
;

function not_enter_descr() {
    $(".wysibb-body").text("Описание не заполнено");
    $('#subm_btn').trigger('click');
}

function checkAction(cAction, form) {

    var res;
    var actlist = cAction.split('|');
    for (var i = 0; i < actlist.length; i++) {
        switch (actlist[i]) {
            case 'passwdconfirm':
                res = passwdConfirm();
                break;
            case 'checkuser':
                res = isUserExists(form);
                break;
            case 'checkemail':
                res = isGoodEmail(form);
                break;
            case 'checkphone':
                res = checkMobilePhone(form);
                break;
            case 'profilefiles':
                res = testProfileFiles();
                break;
        }
        if (!res)
            return false;
    }
    return true;
}

function passwdConfirm() {
    var p1 = $('#passwd1').val();
    var p2 = $('#passwd2').val();
    if (p1 != p2) {
        alertBM.alert(langPasswdConfirm);
        return false;
    } else {
        return true;
    }
}

function confirmDelete(url) {
    if (confirm('Удалить?')) {
        return true;
    } else {
        return false;
    }
}

function confirmDialog(what) {
    switch (what) {
        case 'orderOpen':
            var tt = "Открыть заявку?\nЗаявка будет продлена на 7 дней";
            break;
        case 'orderDelete':
            var tt = 'Удалить заявку?';
            break;
        case 'orderClose':
            var tt = 'Закрыть заявку?';
            break;
        case 'addrDelete':
            var tt = 'Удалить адрес?';
            break;
    }
    if (confirm(tt)) {
        return true;
    } else {
        return false;
    }
}

function isUserExists(form) {
    var ret = true;
    $.ajax({
        async: false,
        type: 'post',
        url: '/users/available',
        data: form.serialize(),
        success: function (result) {
            //console.log(result);
            if (result == '42') {
                alertBM.alert('SERVER ERROR');
                ret = false;
            } else if (result.indexOf('AllOk') == -1) {
                //alertBM.alert(result);

                doSignin();
                if (result.indexOf('@') > 0) {
                    $('[href="#email-tab"]').trigger("click");
                    $("#login-email").val(result);
                } else {
                    $('[href="#phone-tab"]').trigger("click");
                    $("#login-phone").val(result);
                }

                ret = false;
                saveFormData(form);
            } else {
                ret = true;
            }
        },
        error: function () {
            alertBM.alert('SYSTEM FAILURE');
            ret = false;
        }
    });
    return ret;
}

function saveFormData(form) {
    $.ajax({
        async: false,
        type: 'post',
        url: '/users/savedata',
        data: form.serialize(),
        success: function (result) {
            console.log(result);
        }
    });
}

function isGoodEmail(form) {
    var email = form.find('#email').val();
    if (!email)
        return true;

    if (email.indexOf('@') < 1) {
        alertBM.alert(langErrorNotEmail);
        return false;
    }
    var ret = false;
    $.ajax({
        type: 'post',
        url: '/mail',
        async: false,
        data: {mail: email},
        success: function (result) {
            if (result.indexOf('AllOk') == -1) {
                alertBM.alert(langErrorEmail);
            } else {
                ret = true;
            }
        },
        error: function () {
            alertBM.alert('SYSTEM FAILURE');
        }
    });
    return ret;
}

function checkMobilePhone(form) {
    if (form['selector'] == "#order-form1") {
        var phone = form.find('#phone1').val();
    } else {
        var phone = form.find('#phone').val();
    }

    if (!phone)
        return false;
    var codes = ['(050)', '(095)', '(066)', '(099)', '(067)', '(098)', '(097)', '(096)', '(068)', '(093)', '(063)', '(073)', '(091)', '(092)', '(094)'];
    for (var i = 0; i < codes.length; i++) {
        if (phone.indexOf(codes[i]) != -1) {
            //console.log(codes[i]);
            return true;
        }
    }
    alertBM.alert("Ошибка кода мобильного. Откорректируйте данные");
    return false;
}

function testProfileFiles() {
    var ret = true;
    $('#input-block').each(function () {
        var title = $(this).find('input[name^=titles]').val();
        var file = $(this).find('input[name^=files]').val();
        console.log(title, file);
        if ((!title && file) || (title && !file))
            ret = false;
    });
    if (!ret)
        alertBM.alert(langFiedsEmpty);
    return ret;
}

function validate(listFields, form) {

    var result = true;
    var required = listFields.split('|');

    for (var i = 0; i < required.length; i++) {

        var field = form.find('#' + required[i]);
        var field_val = field.val();

        if (required[i] == "autocomplete") {
            if (form.find('#' + required[i]).length == 0)
                continue;
        }
           
        if (!field_val || field_val == 0 || field_val == 'undefined' || field_val == "") {

            alert_fields = alert_fields + required[i] + "|";
            result = false;
        }else{
            if (required[i] == "locality")
                field.siblings('#autocomplete').css('border', '1px solid gray');
            else
                field.css('border', '1px solid gray');
        }
    }

    return result;
}

var alertBM = (function () {
    "use strict";

    var elem, that = {};

    that.init = function () {
        var elem = document.createElement('div');
        elem.setAttribute('class', 'alert-bm alert');
        elem.setAttribute('style', 'display: none');
        document.getElementsByTagName('body')[0].appendChild(elem);
        elem.onclick = function () {
            elem.style.display = 'none';
        };
    };

    that.alert = function (text, type) {
        var tmp = $('.alert-bm');
        if (!tmp.length)
            that.init();
        $('.alert-bm').attr('class', "alert-bm alert");
        type = typeof type !== 'undefined' ? type : 'alert-danger';

        $('.alert-bm').addClass(type);
        var y = (document.pointY > 300) ? document.pointY : 200;
        $('.alert-bm').css('top', 120);
        $('.alert-bm').html(text);
        $('.alert-bm').delay(100).fadeIn();
    };

    return that;
}());

function fillInAddress() {

    $('#administrative_area_level_1').val('');
    $('#administrative_area_level_2').val('');
    $('#administrative_area_level_3').val('');
    $('#placeaddr').val('');
    $('#location').val('');
    $('#route').val('');
    $('#sublocality_level_1').val('');
    var place = autocomplete.getPlace();
    
    $('#placelat').val(place.geometry.location.lat());
    $('#placelng').val(place.geometry.location.lng());
    $('#placeid').val(place.place_id);
    $('#placeaddr').val(place.formatted_address);
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        administrative_area_level_2: 'long_name',
        administrative_area_level_3: 'long_name',
        country: 'long_name',
        sublocality_level_1: 'long_name'
    };

    var has_route = false, has_town = false, not_ukraine = true;



    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            var place_type = place.address_components[i].types[0];
            if (place_type == 'locality')
                has_town = true;
            if (place_type == 'country') {
                if (val == 'Украина' || val == 'Україна' || val == 'Ukraine')
                    not_ukraine = false;
            }
            $('#' + place_type).val(val);

            /*Google не проставляет киевскую область*/
            if (val == 'город Киев') {
                $('#administrative_area_level_1').val('Киевская область');
            }
            if (val == 'місто Київ') {
                $('#administrative_area_level_1').val('Київська область');
            }
        }
    }

    if (not_ukraine) {
        var txt = 'Введите адрес в Украине';
        clearMyLocation(txt);
        return false;
    }

    if (!has_town) {
        if (lang == 'uk')
            var txt = 'Адреса скасована.<br /><small><sub>Вкажіть місто або селище.</sub></small>';
        else
            var txt = 'Адрес отменён.<br /><small><sub>Укажите город или посёлок.</sub></small>';
        clearMyLocation(txt);
        return false;
    }

    $('#chplace').val(1);
}

function fillInAddressq() {
    $('#administrative_area_level_11').val('');
    $('#administrative_area_level_21').val('');
    $('#administrative_area_level_31').val('');
    $('#placeaddr1').val('');
    $('#location1').val('');
    $('#route1').val('');
    $('#sublocality_level_11').val('');
    var place = autocompletee.getPlace();
    $('#placelat1').val(place.geometry.location.lat());
    $('#placelng1').val(place.geometry.location.lng());
    $('#placeid1').val(place.place_id);
    $('#placeaddr1').val(place.formatted_address);
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        administrative_area_level_2: 'long_name',
        administrative_area_level_3: 'long_name',
        country: 'long_name',
        sublocality_level_1: 'long_name'
    };

    var has_route = false, has_town = false, not_ukraine = true;


    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            var place_type = place.address_components[i].types[0];

            if (place_type == 'locality')
                has_town = true;
            if (place_type == 'country') {
                if (val == 'Украина' || val == 'Україна' || val == 'Ukraine')
                    not_ukraine = false;
            }
            $('#' + place_type + '1').val(val);

            /*Google не проставляет киевскую область*/
            if (val == 'город Киев') {
                $('#administrative_area_level_11').val('Киевская область');
            }
            if (val == 'місто Київ') {
                $('#administrative_area_level_11').val('Київська область');
            }
        }
    }

    if (not_ukraine) {
        var txt = 'Введите адрес в Украине';
        clearMyLocation(txt);
        return false;
    }

    if (!has_town) {
        if (lang == 'uk')
            var txt = 'Адреса скасована.<br /><small><sub>Вкажіть місто або селище.</sub></small>';
        else
            var txt = 'Адрес отменён.<br /><small><sub>Укажите город или посёлок.</sub></small>';
        clearMyLocation(txt, 1);
        return false;
    }

    $('#chplace1').val(1);
}


function clearMyLocation(txt, add = false) {
    alertBM.alert(txt);
    $('#locality' + add).val('');
    $('#autocomplete' + add).val('');
    $('#administrative_area_level_1' + add).val('');
    $('#administrative_area_level_2' + add).val('');
    $('#administrative_area_level_3' + add).val('');
    $('#route' + add).val('');
    $('#sublocality_level_1' + add).val('');
    $('#placeaddr' + add).val('');
}


$(function ($) {
    function tog(v) {
        return v ? 'addClass' : 'removeClass';
    }
    $(document).on('input', '.clearable', function () {
        $(this)[tog(this.value)]('x');
    }).on('mousemove', '.x', function (e) {
        $(this)[tog(this.offsetWidth - 20 < e.clientX - this.getBoundingClientRect().left)]('onX');
    }).on('mouseleave', '.x', function (e) {
        $(this).removeClass('x onX');
        $(this).addClass('x');
    }).on('touchstart click', '.onX', function (ev) {
        ev.preventDefault();
        $(this).removeClass('x onX').val('').change();

        if ($(this).attr('search_input') == 1) {
            $('#placelat1').val("");
            $('#placelng1').val("");
        } else {
            $('#placetown').val('');
            $('#placeid').val('');
            $('#administrative_area_level_1').val('');
            $('#administrative_area_level_2').val('');
            $('#administrative_area_level_3').val('');
            $('#location').val('');
            $('#placeaddr').val('');
        }

        //$('#autocomplete').attr('readonly', false);
    });
});


$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

$("#filter_action").on("click", function () {

    $("#img_ajax").fadeIn();

    $.ajax({
        type: 'post',
        url: window.location.href,
        dataType: 'html',
        data: {
            filter_stars: $("#filter_stars").val(),
            tid: $("#tid_1").val(),
            sid: $("#sid_1").val(),
            lat: $("#placelat1").val(),
            lng: $("#placelng1").val(),
            distance: $("#distance").val()
        },
        success: function (result) {
            console.log(result);
            $("#img_ajax").fadeOut();
            $("#show_filter_results").html(result);
            $("#show_filter_results").css("border", "1px solid brown").css("height", "300px");
        },
        error: function () {
            $("#img_ajax").fadeOut();
            alertBM.alert("Произошла ошибка запроса! Попробуйте уточнить данные");
        }
    });
});



if (window.location.pathname.indexOf("workz/orders/view") >= 0) {
    console.log("Start auto update user");
    setInterval(function () {
        $.getJSON('/workz/users/checkUpdate', function (result) {
            if (result) {
                if ($("#" + result['id']).length > 0) {
                    $("#" + result['id']).attr('data_email', result['user_email']);
                    alertBM.alert("Данные пользователя " + result['user_name'] + " успешно обновлены!");
                }

            }
        });
    }, 2000);
}




$("#show_filter_results").on("click", ".js_filter_user_hide", function () {
    $(this).closest("tr").fadeOut();
});

$("#show_filter_results").on("click", ".js_filter_user_add", function () {
    var email = ($(this).attr('data_email'));
    $("#do-submit-btn").attr("add_user_id", $(this).attr('id'));
    $("#do-submit-btn").attr("stars", $(this).attr('stars'));
    $("#email").val(email);
    $("#do-submit-btn").trigger("click");
    $(this).closest("tr").fadeOut();
});

$("#form_feedback").on("click", function (event) {
    event.preventDefault();
    form_feedback();
});

function form_feedback() {

    bootbox.dialog({
        onEscape: true,
        backdrop: false,
        locale: lang,
        title: "Форма обратной связи",
        message: '<form class="form-horizontal" id="loginForm" method="post">' +
                'Менеджер по работе с клиентами: <b> +38 (096) 007-71-37</b><hr>' +
                '<div class="tab-content">' +
                '<div class="tab-pane active" id="phone-tab">' +
                '<div class="form-group" style="margin-bottom:0">' +
                '<label class="col-md-3 control-label" for="callback_phone">Ваш телефон</label>' +
                '<div class="col-md-8">' +
                '<input id="callback_phone" name="phone" type="text" class="form-control input-md" required="required" placeholder="Телефон" value="'+$('#phone').val()+'">' +
                '</div>' +
                '</div>' +
                '</div></div>\n\
                         <div class="blank25"></div>' +
                '<div class="form-group" style="margin-bottom:0">' +
                '<label class="col-md-3 control-label" for="callback_username">Ваше имя </label>' +
                '<div class="col-md-8">' +
                '<input id="callback_username" name="callback_username" type="text" class="form-control input-md " placeholder="Имя">' +
                '</div></div>' +
                '</form>',
        buttons: {
            phone_me: {
                label: 'Оставить заявку',
                className: 'btn-success btn-login',
                callback: function () {

                    if ($("#callback_phone").val() == "") {
                        $("#callback_phone").val('').css("border", "2px solid red").attr('placeholder', 'Это обязательное поле!');
                    } else {
                        $(".js_close_callback").trigger("click");
                        $.post("/index/callback", {
                            callback_phone: $("#callback_phone").val(), 
                            callback_name: $("#callback_username").val(),
                            callback_desc: $("#desc").val(),
                            callback_city: $("#locality").val(),
                        }, function (result) {
                            console.log(result);
                            alertBM.alert("Спасибо! Мы скоро с Вами свяжемся!", 'alert-info');
                        })
                    }
                    return false;
                }
            },
            cancel: {
                label: "Отмена",
                className: 'btn-default js_close_callback',
            }
        }
    });

    try {
        $('#callback_phone').mask('+38 (999) 999-99-99');
    } catch (err) {
    }
}