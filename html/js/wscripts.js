
$(document).ready(function(){
    $('[data-toggle=collapse]').click(function(){
        $(this).find('i').toggleClass('glyphicon-chevron-right glyphicon-chevron-down');
    });
});

function confirmAdmin(what) {
    var tt;
    switch(what){
        case 'user':  tt = 'Удалить пользователя?'; break;
        case 'order':  tt = 'Удалить заявку?'; break;
        case 'shop':  tt = 'Удалить магазин?'; break;
        case 'delOffer':  tt = 'Удалить предложение?'; break;
        case 'orderClose':  tt = 'Закрыть заявку?'; break;
        case 'comercial': tt = 'Удалить рекламный блок?'; break;
        default:  tt = 'Удалить11'; break;
    }
    if (confirm(tt)) {
        return true;
    } else {
        return false;
    }
}