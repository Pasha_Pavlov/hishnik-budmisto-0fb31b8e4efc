
var myDropzone = new Dropzone('#uploadImages', {
    uploadMultiple: false,
    autoQueue: false,
    parallelUploads: 1,
    maxFiles: 10,
    maxFilesize: 2,
    addRemoveLinks: true,
    acceptedFiles: '.png, .jpg, .jpeg, .gif',
    init: function() {
        this.on('removedfile',
        function(file) {
            deleteFile(file.name);
        });
    },
    dictDefaultMessage: "Нажмите для выбора изображения",
    dictFallbackMessage: "Ваш браузер не поддерживается",
    dictFileTooBig: "Файл слишком большой ({{filesize}}МБ). Максимальный размер: {{maxFilesize}}МБ.",
    dictInvalidFileType: "Вы не можете загружать файлы этого типа",
    dictResponseError: "Ошибка сервера {{statusCode}}.",
    dictCancelUpload: "Отменить загрузку",
    dictCancelUploadConfirmation: "Хотите отменить загрузку?",
    dictRemoveFile: "Удалить файл",
    dictRemoveFileConfirmation: null,
    dictMaxFilesExceeded: "Загружено максимальное количество файлов"
});

myDropzone.on('addedfile', function(file) {
    if (file.width < 300 || file.height < 300) {
        alertBM.alert('Фото очень маленькое (минимум 300х300 точек)');
        return false;
    }
    var form = $('#uploadImages');
    $.ajax({
        type: 'post',
        url: '/token',
        dataType: 'json',
        success: function(result){
            form.find('#csrf-token').attr('name', result.csrf_name).val(result.csrf_value);
            myDropzone.enqueueFile(file);
        },
        error: function(error) {
            console.log(error);
        }
    });
});

myDropzone.on('queuecomplete', function(progress) {
    
});

function deleteFile(file) {
    $.ajax({
        type: 'post',
        url: '/delete',
        data: 'file='+file,
        error: function(){
            alertBM.alert('SYSTEM FAILURE');
        }
    });
}

$('#addnewfile').on('click', function() {
    $('#input-block').clone(true).appendTo('#uploadFiles');
    var tmp = $('#uploadFiles').children().last();
    tmp.find('input[name^=titles]').val('');
    tmp.find('input[name^=files]').val('');
});

$('.fileInput').each(function(){
    $(this).on('change', function() {
        var fsize = $(this).context.files[0].size;
        if (fsize > 2097152) {
            $(this).val('');
            alertBM.alert('Файл слишком большой,<br />не более 2Мб');
            return false;
        }
    });
});
