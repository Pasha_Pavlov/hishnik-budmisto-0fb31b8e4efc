$(document).ready(function(){
    $('#page-body').before("<div class='black_fon'></div>");
});

$('.js_handler_orders').hover(function () {    
    $('.black_fon').show();
    $(this).css('z-index', '3').find('.order').addClass('white_bg');
}, function () {    
    $('.black_fon').hide();
    $(this).css('z-index', '0').find('.order').removeClass('white_bg');
});

$('.js_handler_builders').hover(function () {   
    $('.black_fon').show();
    $(this).css('z-index', '3').find('.builders').addClass('white_bg');
}, function () {    
    $('.black_fon').hide();
    $(this).css('z-index', '0').find('.builders').removeClass('white_bg');
});



$('#bottom_slider').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 2000,

    prevArrow: '<button type="button" class="butt_prev"><img src="/images/arrow-left.png"></button>',
    nextArrow: '<button type="button" class="butt_next"><img src="/images/arrow-right.png"></button>',
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]

});