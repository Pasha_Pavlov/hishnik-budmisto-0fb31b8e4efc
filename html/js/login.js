function doSignin() {

    bootbox.dialog({
        onEscape: true,
        backdrop: false,
        locale: lang,
        title: "",
        message: '<form class="form-horizontal" id="loginForm" method="post">' +
                 '<input type="hidden" id="csrf-login">' +
                 '<ul class="nav nav-pills" style="display:none">' +
                   '<li style="display:none">' +
                     '<a href="#email-tab" data-toggle="tab">С помощью E-mail</a>' +
                   '</li>' +
                   '<li class="active">' +
                     '<a href="#phone-tab"  data-toggle="tab">С помощью телефона</a>' +
                 '</li></ul><div class="blank25"></div>' +
                 '<div class="tab-content">' +
                   '<div class="tab-pane " id="email-tab">' +
                     '<div class="form-group" style="margin-bottom:0">' +
                       '<label class="col-md-2 control-label" for="email">' + langEmail + '</label>' +
                       '<div class="col-md-10">' +
                          '<input id="login-email" name="email" type="email" class="form-control input-md" required="required">' +
                       '</div>' +
                     '</div>' +
                   '</div>' +
                   '<div class="tab-pane active" id="phone-tab">' +
                     '<div class="form-group" style="margin-bottom:0">' +
                       '<label class="col-md-2 control-label" for="phone">' + langPhone + '</label>' +
                       '<div class="col-md-10">' +
                         '<input id="login-phone" name="phone" type="text" class="form-control input-md" required="required">' +
                       '</div>' +
                     '</div>' +
                 '</div></div><div class="blank25"></div>' +
                 '<div class="form-group" style="margin-bottom:0">' +
                   '<label class="col-md-2 control-label" for="passwd">' + langPasswd + '</label>' +
                   '<div class="col-md-10">' +
                     '<input id="login-passwd" name="passwd" type="password" class="form-control input-md required="required"">' +
                     '<div style="text-align:right;margin-top:10px;font-size:18px;" id="recovery_div"><strong><a href="javascript:void(0); goRecovery()">получить пароль на телефон</a></strong></div>' +
                   '</div></div>' +
                 '</form>',
        buttons: {
            login: {
                label: langOk,
                className: 'btn-success btn-login',
                callback: function() {
                    return false;
                }
            },
            
            cancel: {
                label: langCancel,
                className: 'btn-default js_cancel'
            },
            
            register: {
                label: "Регистрация",
                className: 'btn-warning',
                callback: function() {                    
                    window.open("/users/signup");
                }
            },
            
            
        }
    });

    $('.btn-login').on('click', function(e) {
        formSubmit();
    });

    $('#login-passwd').keydown(function(e) {
        if (e.keyCode == 13) {
            formSubmit();
        }
    });
    try {
        $('#login-phone').mask("(000) 000-00-00",{placeholder: "(095) 123 45 67"});
    } catch(err) { }
}

function show_reg_form(){
    $("#js_reg_form,.js-popup").fadeIn();
};

function formSubmit() {
    var email = $('#login-email').val();
    var phone = $('#login-phone').val();
    var passwd = $('#login-passwd').val();

    if (!email && !phone) {
        alertBM.alert(langFiedsEmpty);
        return false;
    }
    if(email.indexOf('@') < 1 && !phone && email) {
        alertBM.alert(langErrorNotEmail);
        return false;
    }
    if (!passwd) {
        alertBM.alert(langErrorPasswd);
        return false;
    }

    $.ajax({
        type: 'post',
        url: '/token',
        dataType: 'json',
        success: function(result){
            $('input#csrf-login').attr('name', result.csrf_name).val(result.csrf_value);
            $.ajax({
                type: 'post',
                url: '/users/signin',
                data: $('#loginForm').serialize(),
                success: function(result){
                    console.log(result);
                    if(result == '42') {
                        alertBM.alert('SERVER ERROR');
                    } else if(result == 1) {
                        window.location = '/my/?referer_login';
                    }else if(result == 2) {
                        window.location = '/my/offers?referer_login';
                    }
                    else {
                        alertBM.alert(langErrorLogin);
                        $('#login-passwd').val('');
                    }
                },
                error: function(){
                    alertBM.alert('SYSTEM FAILURE');
                }
            });
        },
        error: function(){
            alertBM.alert(langErrorToken);
        }
    });
}


$("#js_close_popup").on("click",function(e){
    e.preventDefault();
    $("#js_info,.js-popup").fadeOut();
});



function doOpenInfo() {   
    $("#js_info,.js-popup").fadeIn();
   
}

function goRecovery(){
 
    
    $.ajax({
        type: 'post',
        url: '/users/recovery',
        dataType: 'html',
        data: {
            phone:$("#login-phone").val(),
            email:$("#login-email").val(),
        },
        success: function (res) {
            console.log(res);	
            if(res == 'success_phone'){
                $("#recovery_div").html("<span style='color: red;'>пароль отправлен на телефон</red>");
            }else if(res == 'success_email'){
                alertBM.alert('Новый пароль был отправлен на E-Mail','alert-success');
            }
        },
        error: function () {
            console.log("Error ajax form!");	
        }
    });
    
}