<?php

    /**
     * BudMisto
     * 
     * @package      BudMisto
     * @author       Alex Grey
     * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
     * @license      http://opensource.org/licenses/MIT
     * @link         http://budmisto.com.ua
     * @since        Version 1.0.0
     *
     */
    /**
     * config/servises.php
     *
     * Servises location
     *
     */
    $config = require APP_DIR . 'config/config.php';
    $di->setShared('config', $config);

    $router = require APP_DIR . 'config/routes.php';
    $di->set('router', $router);

    $di->set('url', function()
    {
        $url = new \Phalcon\Mvc\Url();
        $url->setBaseUri('/');
        return $url;
    });

    $di->set(
            'flashSession', function (){
                return new Phalcon\Flash\Session();
            }
    );

    $config = $di->getShared('config');

    $di->setShared('db', function() use ($config, $application)
    {
        try
        {
            $dbh = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
                'host' => $config->database->host,
                'username' => $config->database->username,
                'password' => $config->database->password,
                'dbname' => $config->database->dbname,
                'charset' => 'utf8',
                'options' => [PDO::ATTR_CASE => PDO::CASE_LOWER,
                    PDO::ATTR_PERSISTENT => false,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_ORACLE_NULLS  => PDO::NULL_NATURAL]
            ));
            return $dbh;
        } catch (PDOException $e)
        {
            if($application->request->isAjax())
                echo 42; // temporary for ajax requests, need create helper for error message
            else
                echo '<h1>SERVER&nbsp;&nbsp;ERROR</h1>';
            die;
        }
    });

    $di->setShared('logger', function()
    {
        return new \Phalcon\Logger\Adapter\File(APP_DIR . 'var/log/output.log');
    });

    $di->set('viewCache', function() use ($config)
    {
        $frontCache = new \Phalcon\Cache\Frontend\Output(array(
            'lifetime' => $config->application->default_cache_lifetime
        ));

        $cache = new \Phalcon\Cache\Backend\File($frontCache, array(
            'cacheDir' => APP_DIR . 'var/cache/'
        ));
        return $cache;
    });

    $di->setShared('session', function()
    {
        //session_set_cookie_params(3600 * 24 * 120);
        $session = new \Phalcon\Session\Adapter\Files([
            'lifetime' => 3600 * 24 * 30,
            'uniqueId' => 'budmisto2016'
        ]);
        $session->start();
        return $session;
    });

    $di->setShared('security', function()
    {
        return (new \Phalcon\Security());
    });

    $di->set('crypt', function () use ($config)
    {
        $crypt = new \Phalcon\Crypt();
        $crypt->setKey($config->application->crypt_salt);
        return $crypt;
    });

    $di->set('cookies', function()
    {
        $cookies = new \Phalcon\Http\Response\Cookies();
        $cookies->useEncryption(true);
        return $cookies;
    });

    $generator = require APP_DIR . 'app/helpers/generator.php';
    $di->setShared('generator', $generator);

    $text_engine = require APP_DIR . 'app/helpers/textengine.php';
    $di->setShared('textengine', $text_engine);

    $files_work = require APP_DIR . 'app/helpers/fileswork.php';
    $di->setShared('fileswork', $files_work);
    