<?php

/**
 * BudMisto
 * 
 * @package      BudMisto
 * @author       Alex Grey
 * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
 * @license      http://opensource.org/licenses/MIT
 * @link         http://budmisto.com.ua
 * @since        Version 1.0.0
 *
 */


/**
 * config/routes.php
 *
 * Routes list
 *
 */

$router = new \Phalcon\Mvc\Router(false);
$router->removeExtraSlashes(true);
$router->setDefaultModule('front');


// front routes

$router->add('/{alt:[a-z0-9\.\_]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'altView'
));

$router->add('/', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'index',
    'action' => 'index'
));

$router->add('/confidencial', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'index',
    'action' => 'confidencial'
));

$router->add('/cron/send_all/{position:[0-9]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'connect',
    'action' => 'sendAll'
));

$router->add('/cron/send_one', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'connect',
    'action' => 'sendOne'
));

$router->add('/cron/premium_block', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'premiumBlock'
));

$router->add('/cron/cron_series', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'cronSeries'
));

$router->add('/index/{crypt:.{1,}}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'index',
    'action' => 'fastLogin'
));

$router->add('/index_offer/{crypt:.{1,}}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'index',
    'action' => 'fastLoginOffer'
));

$router->add('/index/callback', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'index',
    'action' => 'callback'
));

$router->add('/test', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'index',
    'action' => 'test'
));

$router->add('/404', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'index',
    'action' => 'route404'
));

$router->add('/403', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'index',
    'action' => 'route403'
));

$router->add('/(helpcenter|contacts|admin)', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'index',
    'action' => 1
));





// users

$router->add('/users/(signin|signup|signout|available|recovery|changerole|needhelp|savedata)', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 1
));

$router->add('/users/getplaces/{id:[0-9]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'getplaces'
));

$router->add('/users/placedel/{id:[0-9]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'delplace'
));

$router->add('/commercial-options', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'setCommercialsOptions'
));

$router->add('/shop-info', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'shopInfo'
));

$router->add('/del-commercial-from-session', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'delCommercialFromSession'
));

$router->add('/my', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'index'
));

$router->add('/my/offers', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'offers'
));

$router->add('/my/{id:[0-9]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'activ'
));

$router->add('/my/order/{id:[0-9]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'order'
));

$router->add('/my/orders', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'orders'
));

$router->add('/my/order/close/{id:[0-9]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'orderclose'
));

$router->add('/my/order/open/{id:[0-9]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'orderopen'
));

$router->add('/my/(profile|security|spec)', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 1
    
));

$router->add('/my/spec/:action/:params', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 1,
    'params' => 2
));

$router->add('/my/profile/:action/:params', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 1,
    'params' => 2
));

$router->add('/user/{id:[0-9]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'view'
));

$router->add('/(confirm|emailconfirm)', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 1
));

$router->add('/users/callrequest', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'users',
    'action' => 'callrequest'
));





// catalog

$router->add('/catalog', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'index'
));

$router->add('/catalog/temp_phone', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'tempPhone'
));

$router->add('/catalog/ajax_order', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'AjaxOrder'
));

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'toprubric'
));

$router->add('/catalog/users', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'users'
));

$router->add('/catalog/orders', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'orders'
));

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/{sub_rubric:[0-9a-z\-]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'subrubric'
));

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/{sub_rubric:[0-9a-z\-]+}/users', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'subusers'
));

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/{sub_rubric:[0-9a-z\-]+}/{place:(([0-9]+)([\-])([a-z\-]+))}/users', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'subusers'
));

/*$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/{place:[loc0-9]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'toprubric'
));*/

/*$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/{sub_rubric:[0-9a-z\-]+}/{place:[loc0-9]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'subrubric'
));*/

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/users', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'topusers'
));

/*$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/{place:[loc0-9]+}/users', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'topusers'
));*/

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/orders', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'toporders'
));

/*$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/{place:[loc0-9]+}/orders', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'toporders'
));*/

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/{sub_rubric:[0-9a-z\-]+}/orders', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'suborders'
));

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/{sub_rubric:[0-9a-z\-]+}/{place:(([0-9]+)([\-])([a-z\-]+))}/orders', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'suborders'
));

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/{sub_rubric:[0-9a-z\-]+}/{place:(([0-9]+)([\-])([a-z\-]+))}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'subrubric'
));

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/{place:(([0-9]+)([\-])([a-z\-]+))}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'toprubric'
));

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/{place:(([0-9]+)([\-])([a-z\-]+))}/users', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'topusers'
));

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/{place:(([0-9]+)([\-])([a-z\-]+))}/orders', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'toporders'
));

$router->add('/catalog/{place:(([0-9]+)([\-])([a-z\-]+))}/orders', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'indexcity'
));

$router->add('/catalog/{place:(([0-9]+)([\-])([a-z\-]+))}/users', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'indexcity'
));

$router->add('/catalog/{place:(([0-9]+)([\-])([a-z\-]+))}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'indexcity'
));

$router->add('/catalog/subdata/:params', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'subdata',
    'params' => 1
));

$router->add('/catalog/select', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'select'
));

$router->add('/catalog/order/{id:[0-9]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'vieworder'
));

$router->add('/catalog/order/create', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'createorder'
));

$router->add('/catalog/order/set_eternal_freeze', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'seteternalfreeze'
));

$router->add('/catalog/choice', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'choice'
));

$router->add('/catalog/offer', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'offer'
));

$router->add('/catalog/deloffer/{id:[0-9]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'deloffer'
));





// common

$router->add('/token', array(
    'module' => 'common',
    'namespace' => 'Budmisto\Modules\Common\Controllers\\',
    'controller' => 'index',
    'action' => 'token'
));

$router->add('/avatar', array(
    'module' => 'common',
    'namespace' => 'Budmisto\Modules\Common\Controllers\\',
    'controller' => 'index',
    'action' => 'avatar'
));

$router->add('/upload', array(
    'module' => 'common',
    'namespace' => 'Budmisto\Modules\Common\Controllers\\',
    'controller' => 'index',
    'action' => 'upload'
));

$router->add('/delete', array(
    'module' => 'common',
    'namespace' => 'Budmisto\Modules\Common\Controllers\\',
    'controller' => 'index',
    'action' => 'delete'
));

$router->add('/mail', array(
    'module' => 'common',
    'namespace' => 'Budmisto\Modules\Common\Controllers\\',
    'controller' => 'index',
    'action' => 'chkmail'
));

$router->add('/user', array(
    'module' => 'common',
    'namespace' => 'Budmisto\Modules\Common\Controllers\\',
    'controller' => 'index',
    'action' => 'getuser'
));



// admin

$router->add('/workz', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'index'
));

$router->add('/workz/get_hash/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'getHash'
));

$router->add('/workz/board', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'board'
));

$router->add('/workz/quit', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'quit'
));

$router->add('/workz/orders/view/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'orderview'
));

$router->add('/workz/orders/deloffer/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'orderdeloffer'
));

$router->add('/workz/orders/create', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'ordercreate'
));

$router->add('/workz/orders/edit', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'orderedit'
));

$router->add('/workz/orders/search', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'ordersearch'
));

$router->add('/workz/orders/blank', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'ordersblank'
));

$router->add('/workz/orders/close/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'orderclose'
));

$router->add('/workz/orders/delete/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'orderdelete'
));

$router->add('/workz/orders/set_cron_status', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'cronStatus'
));

$router->add('/workz/orders/set_old_status', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'oldStatus'
));

$router->add('/workz/orders/set_premium_block_status', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'premiumBlockStatus'
));

$router->add('/workz/orders/delete_premium_block', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'deletePremiumBlock'
));

$router->add('/workz/orders/activate-eternal-freeze', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'activateEternalFreeze'
));

$router->add('/workz/orders', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'orders'
));

$router->add('/workz/users', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'users'
));

$router->add('/workz/users/view/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'userview'
));

$router->add('/workz/users/search', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'usersearch'
));

$router->add('/workz/users/sms-sender', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'smsSender'
));

$router->add('/workz/users/sms-orders', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'smsOrders'
));

$router->add('/workz/users/sms-get-order', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'smsGetOrder'
));

$router->add('/workz/users/sms-set-order', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'smsSetOrder'
));

$router->add('/workz/users/sms-delete-order', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'smsDeleteOrder'
));

$router->add('/workz/users/edit', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'useredit'
));
$router->add('/workz/users/checkUpdate', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'checkUpdate'
));
$router->add('/workz/users/marked', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'usersmarked'
));

$router->add('/workz/users/commented', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'userscommented'
));

$router->add('/workz/users/create', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'usercreate'
));

$router->add('/workz/users/addspec', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'useraddspec'
));

$router->add('/workz/users/delspec', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'userdelspec'
));

$router->add('/workz/users/delplace', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'userdelplace'
));

$router->add('/workz/users/blocked', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'usersblocked'
));

$router->add('/workz/users/block/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'userblock'
));

$router->add('/workz/users/delete/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'userdelete'
));

$router->add('/workz/changerole', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'changerole'
));

$router->add('/workz/city/catalog', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'citycatalog'
));

$router->add('/workz/city/unchained', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'unchainedcity'
));

$router->add('/workz/city/setchain', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'setchaincity'
));

$router->add('/workz/emailexist', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'emailexist'
));

$router->add('/workz/phoneexist', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'phoneexist'
));

$router->add('/workz/settings', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'settings'
));

$router->add('/workz/setregion', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'setregion'
));

$router->add('/workz/spec/list', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'speclist'
));

$router->add('/workz/spec/subedit/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'specsubedit'
));

$router->add('/workz/spec/topedit/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'spectopedit'
));

$router->add('/workz/spec/specrefadd', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'specrefadd'
));

$router->add('/workz/spec/specrefdel/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'specrefdel'
));

$router->add('/workz/spec/add', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'specadd'
));

$router->add('/workz/shops/list', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'shoplist'
));

$router->add('/workz/shops/add', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'shopadd'
));

$router->add('/workz/shops/delete/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'shopdelete'
));

$router->add('/workz/shops/edit/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'shopedit'
));

$router->add('/workz/shops/commercial-interested/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'commercialInterested'
));

$router->add('/workz/shops/delspec', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'shopdelspec'
));

$router->add('/workz/shops/delplace', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'shopdelplace'
));

$router->add('/workz/shops/network', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'shopnet'
));

$router->add('/workz/shops/addnetwork', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'shopnetadd'
));

$router->add('/workz/shops/editnetwork/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'shopnetedit'
));

$router->add('/workz/leftovers', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'leftovers'
));

$router->add('/workz/leftovers/delete/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'leftoversdelete'
));

$router->add('/workz/leftovers/close/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'leftoversclose'
));


/*
    Ротатор
 */



$router->add('/workz/comercial/delete/{shop_id:[0-9]+}/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'index',
    'action' => 'comercial_delete'
));

$router->add('/workz/rotator', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'rotator',
    'action' => 'index'
));
$router->add('/workz/textbanners', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'rotator',
    'action' => 'textbanners'
));

$router->add('/workz/ctr_filter', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'rotator',
    'action' => 'ctr_filter'
));


$router->add('/workz/rotator/text_order', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'rotator',
    'action' => 'text_order'
));

$router->add('/workz/rotator/text_order/{spec_main:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'rotator',
    'action' => 'text_order_get'
));

/*
 *   
 */


$router->add('/workz/rotator/edit/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'rotator',
    'action' => 'edit'
));
$router->add('/workz/textbanners/edit/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'rotator',
    'action' => 'textbannersedit'
));


$router->add('/workz/rotator/delete/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'rotator',
    'action' => 'delete'
));
$router->add('/workz/textbanners/delete/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'rotator',
    'action' => 'textbannersdelete'
));


$router->add('/workz/rotator/create', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'rotator',
    'action' => 'create'
));
$router->add('/workz/textbanners/create', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'rotator',
    'action' => 'textbannerscreate'
));


$router->add('/workz/callback', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'rotator',
    'action' => 'callback'
));
$router->add('/workz/callback/edit/{id:[0-9]+}', array(
    'module' => 'admin',
    'namespace' => 'Budmisto\Modules\Admin\Controllers\\',
    'controller' => 'rotator',
    'action' => 'callback_edit'
));
// Leftovers

$router->add('/leftovers', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'leftovers',
    'action' => 'index'
));

$router->add('/leftovers/view/{id:[0-9]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'leftovers',
    'action' => 'view'
));

$router->add('/leftovers/{top_rubric:[0-9a-z\-]+}/{shipping:(pickup|delivery)}/{place:(([0-9]+)([\-])([a-z\-]+))}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'leftovers',
    'action' => 'toprubric'
));

$router->add('/leftovers/{top_rubric:[0-9a-z\-]+}/{place:(([0-9]+)([\-])([a-z\-]+))}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'leftovers',
    'action' => 'toprubric'
));

$router->add('/leftovers/{top_rubric:[0-9a-z\-]+}/{shipping:(pickup|delivery)}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'leftovers',
    'action' => 'toprubric'
));

$router->add('/leftovers/{top_rubric:[0-9a-z\-]+}/{shipping:(pickup|delivery)}/{lotype:(sell|buy)}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'leftovers',
    'action' => 'toprubric'
));

$router->add('/leftovers/{top_rubric:[0-9a-z\-]+}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'leftovers',
    'action' => 'toprubric'
));

$router->add('/leftovers/{top_rubric:[0-9a-z\-]+}/{lotype:(sell|buy)}', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'leftovers',
    'action' => 'toprubric'
));

$router->add('/leftovers/select', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'leftovers',
    'action' => 'select'
));

$router->add('/leftovers/add', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'leftovers',
    'action' => 'add'
));

$router->add('/leftovers/addsell', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'leftovers',
    'action' => 'addsell'
));

$router->add('/leftovers/addbuy', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'leftovers',
    'action' => 'addbuy'
));





// Error 404

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/loc{place:[0-9]+}', array(
    'module' => 'common',
    'namespace' => 'Budmisto\Modules\Common\Controllers\\',
    'controller' => 'index',
    'action' => 'fixloc'
));

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/{sub_rubric:[0-9a-z\-]+}/loc{place:[0-9]+}', array(
    'module' => 'common',
    'namespace' => 'Budmisto\Modules\Common\Controllers\\',
    'controller' => 'index',
    'action' => 'fixloc'
));

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/loc{place:[0-9]+}/users', array(
    'module' => 'common',
    'namespace' => 'Budmisto\Modules\Common\Controllers\\',
    'controller' => 'index',
    'action' => 'fixloc'
));

$router->add('/catalog/{top_rubric:[0-9a-z\-]+}/loc{place:[0-9]+}/orders', array(
    'module' => 'common',
    'namespace' => 'Budmisto\Modules\Common\Controllers\\',
    'controller' => 'index',
    'action' => 'fixloc'
));

$router->add('/catalog/info', array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'catalog',
    'action' => 'info'
));



$router->notFound(array(
    'module' => 'front',
    'namespace' => 'Budmisto\Modules\Front\Controllers\\',
    'controller' => 'index',
    'action' => 'route404'
));

return $router;
