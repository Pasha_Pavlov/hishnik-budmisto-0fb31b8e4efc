<?php

/**
 * BudMisto
 * 
 * @package      BudMisto
 * @author       Alex Grey
 * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
 * @license      http://opensource.org/licenses/MIT
 * @link         http://budmisto.com.ua
 * @since        Version 1.0.0
 *
 */


/**
 * config/modules.php
 *
 * Modules list
 *
 */

return array(
    'front' => array(
        'className' => 'Budmisto\Modules\Front\Module',
        'path' => APP_DIR . 'app/modules/front/Module.php'
    ),
     'admin' => array(
        'className' => 'Budmisto\Modules\Admin\Module',
        'path' => APP_DIR . 'app/modules/admin/Module.php'
    ),
     'common' => array(
        'className' => 'Budmisto\Modules\Common\Module',
        'path' => APP_DIR . 'app/modules/common/Module.php'
    )
);
