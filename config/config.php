<?php

/**
 * BudMisto
 * 
 * @package      BudMisto
 * @author       Alex Grey
 * @copyright    Copyright © 2016 Alex Grey (http://grey.kiev.ua/)
 * @license      http://opensource.org/licenses/MIT
 * @link         http://budmisto.com.ua
 * @since        Version 1.0.0
 *
 */


/**
 * config/config.php
 *
 * Main config array
 *
 */


$host = isset($_SERVER['HTTP_HOST'])?$_SERVER['HTTP_HOST']:'';
$current_site_url = 'budmisto';
if ($host != $current_site_url)
    header('Location: https://' . $current_site_url);

return new \Phalcon\Config(array(
    'database' => array(
        'host' => 'localhost',
        'username' => 'root',
        'password' => '',
        'dbname' => 'budmisto'
    ),
    'application' => array(
        'default_cache_lifetime' => 1, //86400
        'remind_code_lifetime' => 300, //86400
        'crypt_salt' => '8oDOcsJDMyCV4ez1y&_EV45ttD-kOQqQ',
        'crypt_code' => 'E@zyvSiVdbMlPR@f_upyHdHQ&k5hJURi',
        'default_lang' => 'ru',
        'default_radius' => 1000,
	'site_url' => 'https://' . $current_site_url,
        'per_page' => 30,
        'admin_per_page' => 30,
        'email_from' => '<robot@budmisto.com.ua>',
        'map_key' => 'AIzaSyDqovS4n5PyVFAa3IrMEtoZHEtcSLcQYh8',
	//'map_key' => 'AIzaSyDB5B2QItn07d5TiufCncu0HUWM6YqRBMQ',
		
        'server_key' => 'AIzaSyA2LHsmBk__9kZrIkTVPYcCs2KomcEL-UI',
        'admin_email' => 'budmisto.kiev.ua@gmail.com#tanya.budmisto@gmail.com'
    ),
    'lists' => array(
        'small_brief' => 99,
        'big_brief' => 180,
        'limit' => 10
    ),
    'images' => array(
        'ava_dir' => APP_DIR . 'html/data/avatars/',
        'max_width' => 4500,
        'max_height' => 4500,
        'thumb_size' => 150
    ),
    'files' => array(
        'tempDir' => APP_DIR . 'html/data/uploads/',
        'users' => APP_DIR . 'html/data/users/'
    ),
    'other' => array(
	'connect_text' => 'Звоните, договоримся!',
        'admin_email'=>"ua.budmisto@gmail.com",
		 'admin_phone'=>"(096) 007-71-37",
        'stat_track' => "",
        'banner1' => '<table width="100%" height="80"><tr><td align="center"><h1>Вам нужны строители?</h1> Создайте заявку - сегодня позвонят!<br /><strong class="text-danger">Для строителей регистрация бесплатная!</strong></td></tr></table>',
	'banner2' => '<div class="blank25"></div><div style="margin:0 auto; max-width:728px;"><a href="/leftovers"><img src="/images/leftovers.jpg" class="img-responsive" /></a></div>'
    )
));
